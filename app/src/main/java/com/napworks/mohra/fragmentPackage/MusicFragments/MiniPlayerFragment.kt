package com.napworks.mohra.fragmentPackage.MusicFragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.napworks.mohra.R


class MiniPlayerFragment : Fragment() {



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
var view =inflater.inflate(R.layout.fragment_mini_player, container, false)
    return view
    }
}