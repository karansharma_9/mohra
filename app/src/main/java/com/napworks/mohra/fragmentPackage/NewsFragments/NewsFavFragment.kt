package com.napworks.mohra.fragmentPackage.NewsFragments

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.napworks.mohra.R
import com.napworks.mohra.activitiesPackage.HomeScreenActivity
import com.napworks.mohra.adapterPackage.News.FavNewsRecyclerAdapter
import com.napworks.mohra.dialogPackage.LoadingDialog
import com.napworks.mohra.interfacePackage.News.FavNewsInterface
import com.napworks.mohra.modelPackage.News.FavNewsInnerModel
import com.napworks.mohra.modelPackage.News.FavNewsModel
import com.napworks.mohra.utilPackage.ApiCalls
import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.utilPackage.MyConstants
import com.wang.avi.AVLoadingIndicatorView

class NewsFavFragment : Fragment(), FavNewsInterface, View.OnClickListener {
    lateinit var recyclerViewFavNews: RecyclerView
    var menuBackPress: ImageView? = null
    private lateinit var favNewsRecyclerAdapter: FavNewsRecyclerAdapter
    var innerList: ArrayList<FavNewsInnerModel>? = null
    var loadingDialog: LoadingDialog? = null
    var loadingWidget: LinearLayout? = null
    var noFavNews: LinearLayout? = null

    private var sharedPreferences: SharedPreferences? = null
    var TAG = javaClass.simpleName;


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        var view = inflater.inflate(R.layout.fragment_news_fav, container, false)
        loadingDialog = LoadingDialog(activity!!)

        recyclerViewFavNews  = view.findViewById(R.id.recyclerViewFavNews)
        menuBackPress = view.findViewById(R.id.menuBackPress)
        loadingWidget = view.findViewById(R.id.loadingWidget)
        noFavNews = view.findViewById(R.id.noFavNews)

        innerList = ArrayList<FavNewsInnerModel>()

        sharedPreferences = getActivity()!!.getSharedPreferences(MyConstants.SHARED_PREFERENCE,
            AppCompatActivity.MODE_PRIVATE)
        getFavApi(sharedPreferences!!)
        menuBackPress!!.setOnClickListener(this)
        return view
    }

    fun getFavApi(sharedPreferences: SharedPreferences)
    {
        ApiCalls.getFavouriteNews(
            sharedPreferences,
            this
        )
    }

    override fun onFavNewsInterfaceResponse(status: String?, favNewsModel: FavNewsModel?) {
        when (status) {
            MyConstants.GO_TO_RESPONSE -> {
                if (favNewsModel != null) {
                    if (favNewsModel.status != null) {
                        when {
                            favNewsModel.status.equals("1") -> {
                                loadingWidget!!.visibility = View.GONE
                                recyclerViewFavNews!!.visibility = View.VISIBLE
                                val innerCategoriesData: ArrayList<FavNewsInnerModel> = favNewsModel.favnewsData
                                innerList!!.addAll(innerCategoriesData)
                                if(innerList!!.size == 0)
                                {
                                    loadingWidget!!.visibility = View.GONE
                                    recyclerViewFavNews!!.visibility = View.GONE
                                    noFavNews!!.visibility = View.VISIBLE
                                }
                                CommonMethods.showLog(TAG,"length => " + innerList!!.size)
                                recyclerViewFavNews.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, true)
                                favNewsRecyclerAdapter = FavNewsRecyclerAdapter(activity!!, innerList!!)
                                recyclerViewFavNews.adapter = favNewsRecyclerAdapter
                            }
                            favNewsModel.status.equals("0") -> {
                                loadingWidget!!.visibility = View.GONE
                                recyclerViewFavNews!!.visibility = View.VISIBLE
                                CommonMethods.showLog(TAG,favNewsModel.message)
                            }
                            favNewsModel.status.equals("10") -> {
                                loadingWidget!!.visibility = View.GONE
                                recyclerViewFavNews!!.visibility = View.VISIBLE
                                CommonMethods.showLog(TAG,favNewsModel.message)
                            }
                            else -> {
                                loadingWidget!!.visibility = View.GONE
                                recyclerViewFavNews!!.visibility = View.VISIBLE
                                CommonMethods.showLog(TAG,
                                    R.string.someErrorOccurredText.toString())
                            }
                        }
                    } else {
                        loadingWidget!!.visibility = View.GONE
                        recyclerViewFavNews!!.visibility = View.VISIBLE
                        CommonMethods.showLog(TAG,
                            R.string.someErrorOccurredText.toString())
                    }
                } else {
                    loadingWidget!!.visibility = View.GONE
                    recyclerViewFavNews!!.visibility = View.VISIBLE
                    CommonMethods.showLog(TAG,
                        R.string.someErrorOccurredText.toString())
                }
            }
            MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                loadingWidget!!.visibility = View.GONE
                recyclerViewFavNews!!.visibility = View.VISIBLE
                CommonMethods.showLog(TAG,
                    R.string.someErrorOccurredText.toString())
            }
        }
    }

    override fun onClick(v: View?) {
        if(v?.id == R.id.menuBackPress)
        {
            val intent = Intent(activity, HomeScreenActivity::class.java)
            this.startActivity(intent)
            ActivityCompat.finishAffinity(activity!!);
        }
    }

}