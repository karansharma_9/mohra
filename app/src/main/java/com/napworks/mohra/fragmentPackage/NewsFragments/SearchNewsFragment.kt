package com.napworks.mohra.fragmentPackage.NewsFragments

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.napworks.mohra.R
import com.napworks.mohra.activitiesPackage.HomeScreenActivity
import com.napworks.mohra.adapterPackage.News.SearchNewsRecyclerAdapter
import com.napworks.mohra.dialogPackage.CommonMessageDialog
import com.napworks.mohra.dialogPackage.LoadingDialog
import com.napworks.mohra.interfacePackage.News.GetNewsCategoriesInterface
import com.napworks.mohra.interfacePackage.News.SearchNewsInterface
import com.napworks.mohra.modelPackage.News.*
import com.napworks.mohra.utilPackage.ApiCalls
import org.json.JSONArray
import com.napworks.mohra.adapterPackage.News.PronounSpinnerAdapter
import com.napworks.mohra.designPackage.CustomCategoryContainer
import com.napworks.mohra.interfacePackage.CustomCategoryContainerInterface
import com.napworks.mohra.interfacePackage.music.SearchFilterAdapterOnClick
import com.napworks.mohra.modelPackage.MusicModel.RecentlyPlayedModel
import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.utilPackage.MyConstants
import kotlinx.android.synthetic.main.fragment_search_news.*


class SearchNewsFragment : Fragment(), GetNewsCategoriesInterface, SearchNewsInterface,
    View.OnClickListener, AdapterView.OnItemSelectedListener, SearchFilterAdapterOnClick, CustomCategoryContainerInterface {
    var TAG = javaClass.simpleName;
    var spinner: Spinner? = null
    var editTextSearch: EditText? = null
    var searchIcon: ImageView? = null
    var menuBackPress: ImageView? = null
    lateinit var recyclerViewNewsSearch: RecyclerView
    var sharedPreferences: SharedPreferences? = null
    var commonMessageDialog: CommonMessageDialog? = null
    var loadingDialog: LoadingDialog? = null
    var loadingWidget: LinearLayout? = null
    private lateinit var searchNewsRecyclerAdapter: SearchNewsRecyclerAdapter
    var selectedCategoryList : ArrayList<GetNewsInnerCategoriesModel>? = null
    var getCategory: ArrayList<GetNewsInnerCategoriesModel>? = null
    var newsList: ArrayList<SearchNewsInnerModel>? = null
    var spinnerAdapter: PronounSpinnerAdapter? = null
    var SearchTextEditText = ""
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_search_news, container, false)
        spinner = view.findViewById(R.id.spinner)
        loadingDialog = LoadingDialog(activity!!)
        menuBackPress = view.findViewById(R.id.menuBackPress)
        commonMessageDialog = CommonMessageDialog(activity!!)
        editTextSearch = view.findViewById(R.id.editTextSearch)
        searchIcon = view.findViewById(R.id.searchIcon)
        loadingWidget = view.findViewById(R.id.loadingWidget)
        recyclerViewNewsSearch = view.findViewById(R.id.recyclerViewNewsSearch)
        getCategory = ArrayList<GetNewsInnerCategoriesModel>()
        newsList = ArrayList<SearchNewsInnerModel>()
        selectedCategoryList = ArrayList<GetNewsInnerCategoriesModel>()
        sharedPreferences = activity!!.getSharedPreferences(MyConstants.SHARED_PREFERENCE, AppCompatActivity.MODE_PRIVATE)
        getCategoriesApi(sharedPreferences!!)
        spinner!!.setOnItemSelectedListener(this);
        searchIcon!!.setOnClickListener(this);
        menuBackPress!!.setOnClickListener(this)
        return view
    }


    fun getCategoriesApi(sharedPreferences: SharedPreferences) {
//        loadingDialog!!.showDialog()
        ApiCalls.getNewsCategories(
            sharedPreferences,
            this
        )
    }

    fun  getNewsApi(sharedPreferences: SharedPreferences, SearchText: String)
    {
        var json = prepareJSON()
        SearchTextEditText = SearchText

        CommonMethods.showLog(TAG,"json   $json")
        loadingWidget!!.visibility = View.VISIBLE
//        loadingDialog!!.showDialog()
        ApiCalls.searchNews(
            sharedPreferences,
            SearchTextEditText,
            json,
            this
        )
    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.searchIcon)
        {
             SearchTextEditText = editTextSearch?.text.toString().trim()
             getNewsApi(sharedPreferences!!,SearchTextEditText)
        }
        else if(v?.id == R.id.menuBackPress)
        {
            val intent = Intent(activity, HomeScreenActivity::class.java)
            this.startActivity(intent)
            ActivityCompat.finishAffinity(activity!!);
        }
    }

    override fun onGetNewsCategoriesInterfaceResponse(
        status: String?,
        getNewsCategoriesModel: GetNewsCategoriesModel?,
    ) {
        when (status) {
            MyConstants.GO_TO_RESPONSE -> {
                if (getNewsCategoriesModel != null) {
                    if (getNewsCategoriesModel.status != null) {
                        when {
                            getNewsCategoriesModel.status.equals("1") ->
                            {
                                loadingDialog!!.hideDialog()
                                val innerCategoriesData: ArrayList<GetNewsInnerCategoriesModel> = getNewsCategoriesModel.data
                                getCategory!!.add(0,GetNewsInnerCategoriesModel("","Select Category",false))
                                getCategory!!.addAll(innerCategoriesData)
                                setSpinner()
                            }
                            getNewsCategoriesModel.status.equals("0") ->
                            {
                                loadingDialog!!.hideDialog()
                                CommonMethods.showLog(TAG, getNewsCategoriesModel.message)
                            }
                            getNewsCategoriesModel.status.equals("10") ->
                            {
                                loadingDialog!!.hideDialog()

                                CommonMethods.showLog(TAG, getNewsCategoriesModel.message)
                            }
                            else -> {
                                loadingDialog!!.hideDialog()

                                CommonMethods.showLog(TAG,
                                    R.string.someErrorOccurredText.toString())
                            }
                        }
                    } else {
                        loadingDialog!!.hideDialog()

                        CommonMethods.showLog(TAG,
                            R.string.someErrorOccurredText.toString())
                    }
                } else {
                    loadingDialog!!.hideDialog()

                    CommonMethods.showLog(TAG,
                        R.string.someErrorOccurredText.toString())
                }
            }
            MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                CommonMethods.showLog(TAG,
                    R.string.someErrorOccurredText.toString())
            }
        }
    }

    private fun setSpinner() {
        spinnerAdapter = PronounSpinnerAdapter(activity!!,
            R.layout.layout_spinner,
            R.id.title,
            getCategory!!, this)
        spinner!!.adapter = spinnerAdapter
        spinnerAdapter!!.notifyDataSetChanged()
    }

    private fun prepareJSON(): String {
        val jsonArray = JSONArray()
        if (selectedCategoryList!!.isNotEmpty()) {
            for (i in selectedCategoryList!!) {
                if(i.isSelected != false) {
                    jsonArray.put(i.categoryId)
                }
            }
        }
        return jsonArray.toString()
    }

    override fun onSearchNewsResponse(status: String?, searchNewsModel: SearchNewsModel?) {
        when (status) {

            MyConstants.GO_TO_RESPONSE -> {
                if (searchNewsModel != null) {
                    if (searchNewsModel.status != null) {
                        when {
                            searchNewsModel.status.equals("1") -> {

                                loadingWidget!!.visibility = View.GONE
                                newsList!!.clear()


                                val innerCategoriesData: ArrayList<SearchNewsInnerModel> = searchNewsModel.newsData
                                newsList!!.addAll(innerCategoriesData)
                                if(newsList!!.size == 0)
                                {
                                    noResult.visibility = View.VISIBLE
                                    recyclerViewNewsSearch.visibility =View.GONE
                                }
                                else
                                {
                                    recyclerViewNewsSearch.visibility = View.VISIBLE
                                    noResult.visibility =View.GONE
                                }
                                recyclerViewNewsSearch.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
                                searchNewsRecyclerAdapter = SearchNewsRecyclerAdapter(activity!!, newsList!!)
                                recyclerViewNewsSearch.adapter = searchNewsRecyclerAdapter
                            }
                            searchNewsModel.status.equals("0") -> {
                                loadingWidget!!.visibility = View.GONE
                                if(newsList!!.size != 0)
                                {
                                    newsList!!.clear()
                                    searchNewsRecyclerAdapter.notifyDataSetChanged();
                                }
                            }
                            searchNewsModel.status.equals("10") -> {
                                loadingWidget!!.visibility = View.GONE
                            }
                            else -> {
                                loadingWidget!!.visibility = View.GONE

                                CommonMethods.showLog(TAG,
                                    R.string.someErrorOccurredText.toString())
                            }
                        }
                    } else {
                        loadingWidget!!.visibility = View.GONE

                        CommonMethods.showLog(TAG,
                            R.string.someErrorOccurredText.toString())
                    }
                } else {
                    CommonMethods.showLog(TAG,
                        R.string.someErrorOccurredText.toString())
                }
            }
            MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                CommonMethods.showLog(TAG,
                    R.string.someErrorOccurredText.toString())
            }
        }
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
       val innerModel :GetNewsInnerCategoriesModel = parent!!.selectedItem as GetNewsInnerCategoriesModel
//        val innerModel: GetNewsInnerCategoriesModel =parent.getSelectedItem() as RelationshipInnerModel
        if (parent.selectedItemPosition != 0) {
            getCategory!![parent.selectedItemPosition].isSelected = !getCategory!![parent.selectedItemPosition].isSelected!!
            if (selectedCategoryList!!.size == 0)
            {
                selectedCategoryList!!.add(innerModel)
            }
            else
            {
                var isExist = false

                for (i in 0 until selectedCategoryList!!.size)
                {
                    if (innerModel.categoryId ==  selectedCategoryList!![i].categoryId)
                    {
//                        selectedCategoryList!![i].isSelected = false
//                        isExist = true
//                        selectedCategoryList!!.add(innerModel)
                        isExist = true
                    }

                }
                if (!isExist)
                {
                    selectedCategoryList!!.add(innerModel)
                }
            }
            spinner!!.setSelection(0)
        }
        else
        {
            CommonMethods.showLog(TAG, "ZERO")
        }

        checkAndShowSelectedRelation()
//        getNewsApi(sharedPreferences!!,SearchTextEditText)
    }


    private fun checkAndShowSelectedRelation() {
        inflateLinearLayout.visibility = View.VISIBLE
        inflateLinearLayout.post(Runnable {
          var  width = inflateLinearLayout.getMeasuredWidth()
            if (selectedCategoryList!!.size > 0) {
                CommonMethods.showLog(TAG,"List size sending to customContainer  " + selectedCategoryList!!.size )
                inflateLinearLayout.removeAllViews()
                CustomCategoryContainer(activity!!,
                   "",
                    selectedCategoryList!!,
                    inflateLinearLayout,
                    false,
                    width,
                    this)
            }
        })
    }



    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

    override fun onCustomCategoryContainerResponse(
        dataList: MutableList<GetNewsInnerCategoriesModel>?,
        position: Int,
        type: String?,
    ) {

        CommonMethods.showLog(TAG,"position click by container  $position")
        selectedCategoryList!!.removeAt(position)
        checkAndShowSelectedRelation()
        CommonMethods.showLog(TAG , "size of list after clicking =>  " + selectedCategoryList!!.size)
        getCategory!![type!!.toInt()].isSelected = false
        spinnerAdapter!!.notifyDataSetChanged()
        newsList!!.clear()
        searchNewsRecyclerAdapter.notifyDataSetChanged();
//        getNewsApi(sharedPreferences!!,SearchTextEditText)
        if( selectedCategoryList!!.size > 0)
        {
            inflateLinearLayout.visibility = View.VISIBLE
        }
        else
        {
            inflateLinearLayout.visibility = View.GONE
        }
    }


    override fun onSearchFilterAdapterOnClick(status: String?, position: Int) {
        CommonMethods.showLog(TAG," karan adapter on click")
//        setSpinner()
//        if(getCategory!!.size != 0)
//        {
//            getCategory!!.removeAt(position)
//        }
//        getNewsApi(sharedPreferences!!,"")
    }
}