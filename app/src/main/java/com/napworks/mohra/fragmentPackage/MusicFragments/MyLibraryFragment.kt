package com.napworks.mohra.fragmentPackage.MusicFragments

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.napworks.mohra.R
import com.napworks.mohra.activitiesPackage.ForgotPasswordActivity
import com.napworks.mohra.activitiesPackage.HomeScreenActivity
import com.napworks.mohra.activitiesPackage.MusicActivities.CreatePlaylistActivity
import com.napworks.mohra.activitiesPackage.NewsActivities.SingleNewsActivity
import com.napworks.mohra.adapterPackage.MusicAdapter.LibraryTypesRecyclerAdapter
import com.napworks.mohra.adapterPackage.MusicAdapter.PlaylistTileMyLibraryRecyclerAdapter
import com.napworks.mohra.interfacePackage.music.GetMusicHomeDataInterface
import com.napworks.mohra.interfacePackage.music.LibraryTypeSelectAdapterOnClick
import com.napworks.mohra.modelPackage.MusicModel.*
import com.napworks.mohra.utilPackage.ApiCalls
import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.utilPackage.MyConstants
import com.wang.avi.AVLoadingIndicatorView
import kotlin.math.roundToInt


class MyLibraryFragment : Fragment(), LibraryTypeSelectAdapterOnClick, View.OnClickListener,
    GetMusicHomeDataInterface {
    var TAG = javaClass.simpleName;
    lateinit var recyclerViewLibraryTypes: RecyclerView
    lateinit var recyclerViewYourPlaylist: RecyclerView
    lateinit var createPlayList: LinearLayout
    lateinit var loadingView: AVLoadingIndicatorView
    lateinit var mainLinear: LinearLayout
    lateinit var menuBackPress: ImageView
    private lateinit var libraryTypesRecyclerAdapter: LibraryTypesRecyclerAdapter
    private lateinit var playlistTileMyLibraryRecyclerAdapter: PlaylistTileMyLibraryRecyclerAdapter
    var list : ArrayList<LibraryTypesModel>? = null
    var playList : ArrayList<PlaylistFromMyLibraryModel>? = null
    var libraryDatalist : ArrayList<MusicModel>? = null
    var innerList : ArrayList<MusicInnerModel>? = null

    private var sharedPreferences: SharedPreferences? = null

    var upperIndex:Int = 0





    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_my_library, container, false)
        sharedPreferences = activity!!.getSharedPreferences(
            MyConstants.SHARED_PREFERENCE,AppCompatActivity.MODE_PRIVATE)


            recyclerViewLibraryTypes  = view.findViewById(R.id.recyclerViewLibraryTypes)
        recyclerViewYourPlaylist  = view.findViewById(R.id.recyclerViewYourPlaylist)
        menuBackPress  = view.findViewById(R.id.menuBackPress)
        createPlayList  = view.findViewById(R.id.createPlayList)
        loadingView  = view.findViewById(R.id.loadingView)
        mainLinear  = view.findViewById(R.id.mainLinear)
        list = ArrayList<LibraryTypesModel> ()
        playList = ArrayList<PlaylistFromMyLibraryModel> ()
        innerList = ArrayList()
        libraryDatalist = ArrayList()
        CommonMethods.showLog(TAG,"fragment get hit 1")


        mainLinear.visibility = View.GONE
        loadingView.visibility = View.VISIBLE

        var screenWidth : Int = (CommonMethods.getWidth(activity!!) / 4.0).roundToInt()
        val density = CommonMethods.getScreenDensity(activity!!).toInt() * 10
        screenWidth = screenWidth - density

//
//        list!!.add(LibraryTypesModel("Playlist",false))
//        list!!.add(LibraryTypesModel("Artist",false))
//        list!!.add(LibraryTypesModel("Albums",false))
//        list!!.add(LibraryTypesModel("Favourite",false))
//
//
//        playList!!.add(PlaylistFromMyLibraryModel("Create Playlist","",""))
//        playList!!.add(PlaylistFromMyLibraryModel("Dance Hits","",""))
//        playList!!.add(PlaylistFromMyLibraryModel("New of January","",""))
//        playList!!.add(PlaylistFromMyLibraryModel("Popular Russian","",""))
//        playList!!.add(PlaylistFromMyLibraryModel("Classical Music","",""))
//        playList!!.add(PlaylistFromMyLibraryModel("Best Works","",""))
//        playList!!.add(PlaylistFromMyLibraryModel("Nothing To Say","",""))


        recyclerViewLibraryTypes.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, true)
//        list!![0].isSelected=true
        libraryTypesRecyclerAdapter = LibraryTypesRecyclerAdapter(activity!!, libraryDatalist!!,screenWidth,this)
        recyclerViewLibraryTypes.adapter = libraryTypesRecyclerAdapter



        recyclerViewYourPlaylist.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, true)
        playlistTileMyLibraryRecyclerAdapter = PlaylistTileMyLibraryRecyclerAdapter(
            activity!!,
            innerList!!
        )
        recyclerViewYourPlaylist.adapter = playlistTileMyLibraryRecyclerAdapter

        recyclerViewYourPlaylist.isNestedScrollingEnabled = false



        hitMusicLibraryDataApi()

        createPlayList.setOnClickListener(this)
        menuBackPress.setOnClickListener(this)

        return view
    }

    private fun hitMusicLibraryDataApi(){
        ApiCalls.getMusicLibraryData(sharedPreferences!!,this)
    }

    override fun onLibraryTypeSelectAdapterOnClick(
        status: String?,
        libraryTypesModel: MusicModel,
        position: Int
    ) {

        upperIndex = position

        if (upperIndex == 0){
            createPlayList.visibility = View.VISIBLE
        }
        else{
            createPlayList.visibility = View.GONE
        }

        for (data in libraryDatalist!!) {
            if (data.title == libraryTypesModel.title) {
                data.isSelected = true
            } else {
                data.isSelected = false
            }
        }
        libraryTypesRecyclerAdapter.notifyDataSetChanged()

        playlistTileMyLibraryRecyclerAdapter.notifyItemRangeRemoved(0,innerList!!.size)
        innerList!!.clear()
        for (data in libraryDatalist!!.get(upperIndex).innerList) {
            val model : MusicInnerModel = data
            model.type = libraryDatalist!!.get(upperIndex).title
            innerList!!.add(model)
        }

//        innerList!!.addAll(libraryDatalist!!.get(upperIndex).innerList)
        playlistTileMyLibraryRecyclerAdapter.notifyDataSetChanged()
//        for (listdata in list!!)
//        {
//            listdata.isSelected= false
//        }
//
//        libraryTypesModel.isSelected = true
//
//        libraryTypesRecyclerAdapter.notifyDataSetChanged()

    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.createPlayList) {
            val intent = Intent(activity, CreatePlaylistActivity::class.java)
            this.startActivity(intent)
        }
        else if(v?.id == R.id.menuBackPress)
        {
//            val intent = Intent(activity, HomeScreenActivity::class.java)
//            startActivity(intent)
            activity!!.finish()
        }
    }

    override fun onGetMusicHomeDataResponse(
        status: String?,
        getMusicHomeDataModel: GetMusicHomeDataModel?
    ) {
        loadingView.visibility = View.GONE
        mainLinear.visibility = View.VISIBLE
        when (status) {
            MyConstants.GO_TO_RESPONSE -> {
                if (getMusicHomeDataModel != null) {
                    if (getMusicHomeDataModel.status != null) {
                        CommonMethods.showLog(TAG,"getMusicHomeData Status : "+getMusicHomeDataModel.status)
                        when {
                            getMusicHomeDataModel.status.equals("1") -> {
                                libraryDatalist!!.addAll(getMusicHomeDataModel.libraryDataList)
                                libraryDatalist!!.get(upperIndex).isSelected = true
                                CommonMethods.showLog(TAG,"MUSIC LIST SIZE : "+libraryDatalist!!.size)
                                libraryTypesRecyclerAdapter.notifyDataSetChanged()

                                for (data in getMusicHomeDataModel.libraryDataList.get(upperIndex).innerList) {
                                    val model : MusicInnerModel = data
                                    model.type = getMusicHomeDataModel.libraryDataList.get(upperIndex).title
                                    innerList!!.add(model)
                                }

//                                innerList!!.addAll(getMusicHomeDataModel.libraryDataList.get(upperIndex).innerList)
                                CommonMethods.showLog(TAG,"trendingList LIST SIZE : "+innerList!!.size)
                                playlistTileMyLibraryRecyclerAdapter.notifyDataSetChanged()

                            }
                            getMusicHomeDataModel.status.equals("0") -> {
                                CommonMethods.showLog(TAG, getMusicHomeDataModel.message)
                            }
                            getMusicHomeDataModel.status.equals("10") -> {
                                CommonMethods.showLog(TAG, getMusicHomeDataModel.message)
                            }
                            else -> {
                                CommonMethods.showLog(TAG,
                                    R.string.someErrorOccurredText.toString())
                            }
                        }
                    } else {
                        CommonMethods.showLog(TAG,
                            R.string.someErrorOccurredText.toString())
                    }
                } else {
                    CommonMethods.showLog(TAG,
                        R.string.someErrorOccurredText.toString())
                }
            }
            MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                CommonMethods.showLog(TAG,
                    R.string.someErrorOccurredText.toString())
            }
        }

    }
}