package com.napworks.mohra.fragmentPackage.MusicFragments

import android.os.Bundle
import android.transition.Visibility
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.napworks.mohra.R
import com.napworks.mohra.adapterPackage.MusicAdapter.PlaylistDetailsRecyclerAdapter
import com.napworks.mohra.modelPackage.MusicModel.PlaylistFromMyLibraryModel

class PlayListDetailsFragment : Fragment() {
    lateinit var recyclerViewSongsList: RecyclerView
    private lateinit var playlistDetailsRecyclerAdapter: PlaylistDetailsRecyclerAdapter
    lateinit var noSongsLayout: TextView
    lateinit var hasSongsLayout: LinearLayout

    var playList : ArrayList<PlaylistFromMyLibraryModel>? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_play_list_details, container, false)
        recyclerViewSongsList  = view.findViewById(R.id.recyclerViewSongsList)
        noSongsLayout  = view.findViewById(R.id.noSongsLayout)
        hasSongsLayout  = view.findViewById(R.id.hasSongsLayout)
        playList = ArrayList<PlaylistFromMyLibraryModel> ()

        playList!!.add(PlaylistFromMyLibraryModel("Create Playlist","",""))
        playList!!.add(PlaylistFromMyLibraryModel("Dance Hits","",""))
        playList!!.add(PlaylistFromMyLibraryModel("New of January","",""))
        playList!!.add(PlaylistFromMyLibraryModel("Popular Russian","",""))
        playList!!.add(PlaylistFromMyLibraryModel("Classical Music","",""))
        if(playList!!.size != 0)
        {
            hasSongsLayout.visibility = View.VISIBLE
            noSongsLayout.visibility = View.GONE
        }
        else
        {
            hasSongsLayout.visibility = View.GONE
        }
        recyclerViewSongsList.isNestedScrollingEnabled = false;
        recyclerViewSongsList.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, true)
//        playlistDetailsRecyclerAdapter = PlaylistDetailsRecyclerAdapter(activity!!, playList!!)
//        recyclerViewSongsList.adapter = playlistDetailsRecyclerAdapter

        return view
    }
}