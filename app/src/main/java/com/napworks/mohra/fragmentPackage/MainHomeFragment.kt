package com.napworks.mohra.fragmentPackage

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.napworks.gamaloto.interfacePackage.OnHomeScreenCategoryAdapterClick
import com.napworks.mohra.R
import com.napworks.mohra.activitiesPackage.MusicActivities.MusicMainActivity
import com.napworks.mohra.activitiesPackage.NewsActivities.NewsHomeScreenActivity
import com.napworks.mohra.adapterPackage.CategoryContentRecyclerAdapter
import com.napworks.mohra.adapterPackage.CategoryRecyclerAdapter
import com.napworks.mohra.adapterPackage.HomePageAdapter
import com.napworks.mohra.interfacePackage.News.GetNewsCategoriesInterface
import com.napworks.mohra.modelPackage.CategoryInnerModel
import com.napworks.mohra.modelPackage.CategoryListModel
import com.napworks.mohra.modelPackage.News.GetNewsCategoriesModel
import com.napworks.mohra.modelPackage.News.GetNewsInnerCategoriesModel
import com.napworks.mohra.utilPackage.ApiCalls
import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.utilPackage.MyConstants
import kotlinx.android.synthetic.main.fragment_home.*

class MainHomeFragment(homeScreenActivity: Activity) : Fragment(), View.OnClickListener,
    OnHomeScreenCategoryAdapterClick, GetNewsCategoriesInterface {
    lateinit var recyclerViewHomePageCategory: RecyclerView
    lateinit var recyclerViewHomeCategoryContent: RecyclerView
    lateinit var menuButton: ImageView
    lateinit var healthTileHome: LinearLayout
    lateinit var musicTileHome: LinearLayout
    lateinit var newsTab: LinearLayout
    private var sharedPreferences: SharedPreferences? = null
    var innerList: ArrayList<CategoryInnerModel>? = null
    var TAG = javaClass.simpleName;
    var homePageAdapter: HomePageAdapter? = null
    val activity = homeScreenActivity
    private lateinit var categoryRecyclerAdapter: CategoryRecyclerAdapter

    var list : ArrayList<CategoryListModel>? = null
    var newsTypeListCategories : ArrayList<GetNewsInnerCategoriesModel>? = null


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        val view: View = inflater.inflate(R.layout.fragment_home, container, false)
        sharedPreferences = getActivity()!!.getSharedPreferences(MyConstants.SHARED_PREFERENCE,
            AppCompatActivity.MODE_PRIVATE)
        getNewsApi(sharedPreferences!!)
        recyclerViewHomePageCategory=view.findViewById(R.id.recyclerViewHomePageCategory)
        recyclerViewHomeCategoryContent=view.findViewById(R.id.recyclerViewHomeCategoryContent)
        menuButton=view.findViewById(R.id.menuButton)
        healthTileHome=view.findViewById(R.id.healthTileHome)
        musicTileHome=view.findViewById(R.id.musicTileHome)
        newsTab=view.findViewById(R.id.newsTab)
        list = ArrayList<CategoryListModel> ()
        innerList = ArrayList<CategoryInnerModel>()
        newsTypeListCategories = ArrayList<GetNewsInnerCategoriesModel>()


//        answereList!!.add(AnswerModel("1","Male","",R.drawable.male,false))
//        answereList!!.add(AnswerModel("2","Female","",R.drawable.female,false))
//        productList!!.add(
//                QuestionAnswereModel(" Your Personality","1","what is your gender", MyConstants.PERSONALITY, 10,answereList!!,"description",false)
//        )
        innerList!!.add(CategoryInnerModel("1", "normal", ""))
        innerList!!.add(CategoryInnerModel("1", "add", ""))
        innerList!!.add(CategoryInnerModel("1", "normal", ""))
        innerList!!.add(CategoryInnerModel("1", "normal", ""))
        list!!.add(CategoryListModel("All", false, innerList!!))
        innerList = ArrayList<CategoryInnerModel>()

        innerList!!.add(CategoryInnerModel("1", "add", ""))
        list!!.add(CategoryListModel("News", false, innerList!!))
        innerList = ArrayList<CategoryInnerModel>()

        innerList!!.add(CategoryInnerModel("1", "normal", ""))
        list!!.add(CategoryListModel("Sports", false, innerList!!))
        innerList = ArrayList<CategoryInnerModel>()

        innerList!!.add(CategoryInnerModel("1", "normal", ""))
        list!!.add(CategoryListModel("games", false, innerList!!))
        innerList = ArrayList<CategoryInnerModel>()

        innerList!!.add(CategoryInnerModel("1", "normal", ""))
        list!!.add(CategoryListModel("Horoscope", false, innerList!!))
        innerList = ArrayList<CategoryInnerModel>()
        var index = 0

        recyclerViewHomeCategoryContent.isNestedScrollingEnabled = false;
        recyclerViewHomeCategoryContent.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, true)
        val CategoryContentRecyclerAdapter = CategoryContentRecyclerAdapter(activity!!, list!![index].innerData as ArrayList<CategoryInnerModel>)
        index ++
        recyclerViewHomeCategoryContent.adapter = CategoryContentRecyclerAdapter
        healthTileHome
        menuButton.setOnClickListener(this)
        healthTileHome.setOnClickListener(this)
        musicTileHome.setOnClickListener(this)
        newsTab.setOnClickListener(this)

        return view
    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.menuButton)
        {
            drawerLayout.openDrawer(GravityCompat.START)
        }
        else if(v?.id == R.id.healthTileHome)
        {
            val intent = Intent(getActivity(), com.napworks.mohra.activitiesPackage.HealthActivities.HealthStartActivity::class.java)
            startActivity(intent)
        }
        else if(v?.id == R.id.musicTileHome)
        {
            val intent = Intent(getActivity(), MusicMainActivity::class.java)
            intent.putExtra("currentLocation",0)
            startActivity(intent)
        }
        else if(v?.id == R.id.newsTab)
        {
            val intent = Intent(getActivity(), NewsHomeScreenActivity::class.java)
            startActivity(intent)
        }
    }

    fun getNewsApi(sharedPreferences: SharedPreferences)
    {
        ApiCalls.getNewsCategories(
            sharedPreferences,
            this
        )
    }



    override fun onHomeScreenCategoryAdapterClick(
        status: String?,
        getNewsInnerCategoriesModel: GetNewsInnerCategoriesModel,
        position: Int,
    ) {
        for (listdata in newsTypeListCategories!!)
        {
            listdata.isSelected= false
        }

        getNewsInnerCategoriesModel.isSelected = true

        categoryRecyclerAdapter.notifyDataSetChanged()
    }

    override fun onGetNewsCategoriesInterfaceResponse(
        status: String?,
        getNewsCategoriesModel: GetNewsCategoriesModel?,
    ) {
        when (status) {
            MyConstants.GO_TO_RESPONSE -> {
                CommonMethods.showLog(TAG,"login  status  " + getNewsCategoriesModel!!.data)
                if (getNewsCategoriesModel != null) {
                    if (getNewsCategoriesModel.status != null) {
                        when {
                            getNewsCategoriesModel.status.equals("1") -> {
                                val innerCategoriesData: ArrayList<GetNewsInnerCategoriesModel> = getNewsCategoriesModel.data
                                newsTypeListCategories?.addAll(innerCategoriesData)
                                newsTypeListCategories!![0].isSelected=true
                                recyclerViewHomePageCategory.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, true)
                                categoryRecyclerAdapter = CategoryRecyclerAdapter(activity!!, newsTypeListCategories!!,this)
                                recyclerViewHomePageCategory.adapter = categoryRecyclerAdapter
                            }
                            getNewsCategoriesModel.status.equals("0") -> {
                                CommonMethods.showLog(TAG,getNewsCategoriesModel.message)
                            }
                            getNewsCategoriesModel.status.equals("10") -> {
                                CommonMethods.showLog(TAG,getNewsCategoriesModel.message)
                            }
                            else -> {
                                CommonMethods.showLog(TAG,
                                    R.string.someErrorOccurredText.toString())
                            }
                        }
                    } else {
                        CommonMethods.showLog(TAG,
                            R.string.someErrorOccurredText.toString())
                    }
                } else {
                    CommonMethods.showLog(TAG,
                        R.string.someErrorOccurredText.toString())
                }
            }
            MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                CommonMethods.showLog(TAG,
                    R.string.someErrorOccurredText.toString())
            }
        }
    }
}