package com.napworks.mohra.fragmentPackage.MusicFragments

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.napworks.mohra.R
import com.napworks.mohra.activitiesPackage.HomeScreenActivity
import com.napworks.mohra.adapterPackage.MusicAdapter.*
import com.napworks.mohra.adapterPackage.News.RelatedNewsRecyclerAdapter
import com.napworks.mohra.designPackage.CustomTagContainer
import com.napworks.mohra.interfacePackage.music.MusicTypeSelectAdapterOnClick
import com.napworks.mohra.interfacePackage.music.AdapterOnClick
import com.napworks.mohra.interfacePackage.music.GetMusicHomeDataInterface
import com.napworks.mohra.interfacePackage.music.SelectedSongAdapterOnClick
import com.napworks.mohra.modelPackage.MusicModel.*
import com.napworks.mohra.modelPackage.News.NewsDataModel
import com.napworks.mohra.modelPackage.News.NewsDetailsInnerDataModel
import com.napworks.mohra.utilPackage.ApiCalls
import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.utilPackage.MyConstants
import com.wang.avi.AVLoadingIndicatorView
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.roundToInt


class MusicHomeFragment(musicMainActivity: Activity,interfaceA : AdapterOnClick) : Fragment(),
    MusicTypeSelectAdapterOnClick, SelectedSongAdapterOnClick ,View.OnClickListener,
    GetMusicHomeDataInterface {
    var TAG = javaClass.simpleName;
    private var sharedPreferences: SharedPreferences? = null
    var activityMusic = musicMainActivity
    var activityInterface = interfaceA
    var getAllMusic: ArrayList<MusicFilesModel>? = null
    lateinit var playlistTitle: TextView
    lateinit var recyclerViewRecentlyPlayed: RecyclerView
    lateinit var recyclerViewPopularArtist: RecyclerView
    lateinit var recyclerViewYourPlaylist: RecyclerView
    lateinit var recyclerViewTrending: RecyclerView
    lateinit var recyclerViewMusicType: RecyclerView
    lateinit var menuBackPress: ImageView
    lateinit var loadingView: AVLoadingIndicatorView
    lateinit var mainLinear: LinearLayout
    private lateinit var musicTypeRecyclerAdapter: MusicTypeRecyclerAdapter
    private lateinit var recentlyPlayedRecyclerAdapter: RecentlyPlayedRecyclerAdapter
    private lateinit var trendingMusicRecyclerAdapter: TrendingMusicRecyclerAdapter
    private lateinit var popularArtistRecyclerAdapter: PopularArtistRecyclerAdapter
    private lateinit var yourPlaylistRecyclerAdapter: YourPlaylistRecyclerAdapter

    var homeDatalist : ArrayList<MusicModel>? = null
    var trendingList : ArrayList<MusicInnerModel>? = null
    var popularList : ArrayList<MusicInnerModel>? = null
    var playlistList : ArrayList<MusicModel>? = null

    var upperIndex:Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view: View = inflater.inflate(R.layout.fragment_music_home, container, false)

        CommonMethods.showLog(TAG,"Music Home Fragment Calls")
        sharedPreferences = activity!!.getSharedPreferences(MyConstants.SHARED_PREFERENCE,
            AppCompatActivity.MODE_PRIVATE)
        getAllMusic = ArrayList<MusicFilesModel>()
        recyclerViewRecentlyPlayed = view.findViewById(R.id.recyclerViewRecentlyPlayed)
        recyclerViewPopularArtist  = view.findViewById(R.id.recyclerViewPopularArtist)
        recyclerViewYourPlaylist  = view.findViewById(R.id.recyclerViewYourPlaylist)
        recyclerViewTrending  = view.findViewById(R.id.recyclerViewTrending)
        recyclerViewMusicType  = view.findViewById(R.id.recyclerViewMusicType)
        playlistTitle  = view.findViewById(R.id.playlistTitle)
        loadingView  = view.findViewById(R.id.loadingView)
        mainLinear  = view.findViewById(R.id.mainLinear)
        menuBackPress  = view.findViewById(R.id.menuBackPress)
//        list = ArrayList()
        homeDatalist = ArrayList()
        trendingList = ArrayList()
        popularList = ArrayList()
        playlistList = ArrayList()
        mainLinear.visibility = View.GONE
        loadingView.visibility = View.VISIBLE


        var screenWidth : Int = (CommonMethods.getWidth(activity!!) / 3.0).roundToInt()
        val density = CommonMethods.getScreenDensity(activity!!).toInt() * 20
        screenWidth = screenWidth - density


        recyclerViewRecentlyPlayed.layoutManager = LinearLayoutManager(activityMusic, LinearLayoutManager.HORIZONTAL, true)
        recentlyPlayedRecyclerAdapter = RecentlyPlayedRecyclerAdapter(activityMusic, trendingList!!,activityInterface,this)
        recyclerViewRecentlyPlayed.adapter = recentlyPlayedRecyclerAdapter

        recyclerViewTrending.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, true)
        trendingMusicRecyclerAdapter = TrendingMusicRecyclerAdapter(activityMusic, trendingList!!,activityInterface,this)
        recyclerViewTrending.adapter = trendingMusicRecyclerAdapter

        recyclerViewPopularArtist.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, true)
        popularArtistRecyclerAdapter = PopularArtistRecyclerAdapter(activity!!, popularList!!)
        recyclerViewPopularArtist.adapter = popularArtistRecyclerAdapter

        recyclerViewYourPlaylist.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, true)
        yourPlaylistRecyclerAdapter = YourPlaylistRecyclerAdapter(activity!!, playlistList!!)
        recyclerViewYourPlaylist.adapter = yourPlaylistRecyclerAdapter

        recyclerViewMusicType.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, true)
        musicTypeRecyclerAdapter = MusicTypeRecyclerAdapter(activity!!, homeDatalist!!,screenWidth,this)
        recyclerViewMusicType.adapter = musicTypeRecyclerAdapter


        hitMusicHomeDataApi()


//        getAllMusic!!.add(MusicFilesModel("DBS","https://firebasestorage.googleapis.com/v0/b/fir-4e33c.appspot.com/o/Dragon%20Ball%20Super%20Soundtrack%20Full-%20Mastered%20Ultra%20Instinct%20-%20Akira%20Kushida%20(320%20%20kbps).mp3?alt=media&token=2a6a475c-7f4d-465d-b88d-a595ef5ca42a","",false))
//        getAllMusic!!.add(MusicFilesModel("L's","https://firebasestorage.googleapis.com/v0/b/fir-4e33c.appspot.com/o/L's%20Theme%20-%20Death%20Note%20%5BPiano%20Tutorial%5D%20(Synthesia)%20(320%20%20kbps).mp3?alt=media&token=368f8723-b53f-40ce-9580-d07c57165d83","",false))
//        getAllMusic!!.add(MusicFilesModel("Ultra","https://firebasestorage.googleapis.com/v0/b/fir-4e33c.appspot.com/o/Ultra%20Instinct%20Theme%20(Official%20Version)%20(320%20%20kbps).mp3?alt=media&token=ffa9260c-2697-4fe4-8776-1cc0a45b2f6a","",false))
//        getAllMusic!!.add(MusicFilesModel("Tu","https://firebasestorage.googleapis.com/v0/b/fir-4e33c.appspot.com/o/Mere%20Naam%20Tu%20-%20Zero%20320%20Kbps.mp3?alt=media&token=1325552c-a9b1-4c7a-827c-437d1e5ee791","",false))
//
//        list!!.add(RecentlyPlayedModel("1", "Trending",false,""))
//        list!!.add(RecentlyPlayedModel("2", "New This Week",false,""))
//        list!!.add(RecentlyPlayedModel("3", "New Artist",false,""))
//        list!!.add(RecentlyPlayedModel("4", "My List",false,""))



        menuBackPress.setOnClickListener(this)
        return view
    }

    private fun hitMusicHomeDataApi(){
           ApiCalls.getMusicHomeData(sharedPreferences!!,this)
    }

    fun updateList(SongNumberPlaying: Int)
    {
//        for (listdata in getAllMusic!!)
//        {
//            listdata.isSelected= false
//        }
//        getAllMusic!![SongNumberPlaying].isSelected = true
//        recentlyPlayedRecyclerAdapter.notifyDataSetChanged()
        CommonMethods.showLog(TAG,"update list")
    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.menuBackPress) {
            val editor = sharedPreferences!!.edit()
            editor.putBoolean(MyConstants.MUSIC_PLAYING, false)
            editor.apply()
//            val intent = Intent(activity, HomeScreenActivity::class.java)
//            startActivity(intent)
            activity!!.finish()
        }
    }




    override fun onMusicTypeSelectAdapterOnClick(
        status: String?,
        innerModel: MusicModel,
        position: Int,
    ) {

        upperIndex = position
        for (data in homeDatalist!!) {
            if (data.title == innerModel.title) {
                data.isSelected = true
            } else {
                data.isSelected = false
            }
        }
            musicTypeRecyclerAdapter.notifyDataSetChanged()

        trendingMusicRecyclerAdapter.notifyItemRangeRemoved(0,trendingList!!.size)
        trendingList!!.clear()
        trendingList!!.addAll(homeDatalist!!.get(upperIndex).innerList)
        trendingMusicRecyclerAdapter.notifyDataSetChanged()

//        recyclerViewTrending.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, true)
//        trendingMusicRecyclerAdapter = TrendingMusicRecyclerAdapter(activityMusic, trendingList!!,activityInterface,this)
//        recyclerViewTrending.adapter = trendingMusicRecyclerAdapter
    }

    override fun onSelectedSongAdapterOnClick(
        status: String?,
        musicFilesModel: MusicFilesModel,
        position: Int,
    )
    {
        for (listdata in getAllMusic!!)
        {
            listdata.isSelected= false
        }

        musicFilesModel.isSelected = true

        recentlyPlayedRecyclerAdapter.notifyDataSetChanged()
        CommonMethods.showLog(TAG,"hit click")
    }

    override fun onGetMusicHomeDataResponse(
        status: String?,
        getMusicHomeDataModel: GetMusicHomeDataModel?
    ) {
        loadingView.visibility = View.GONE
        mainLinear.visibility = View.VISIBLE
        when (status) {
            MyConstants.GO_TO_RESPONSE -> {
                if (getMusicHomeDataModel != null) {
                    if (getMusicHomeDataModel.status != null) {
                        CommonMethods.showLog(TAG,"getMusicHomeData Status : "+getMusicHomeDataModel.status)
                        when {
                            getMusicHomeDataModel.status.equals("1") -> {
                                homeDatalist!!.addAll(getMusicHomeDataModel.musicList)
                                homeDatalist!!.get(upperIndex).isSelected = true
                                CommonMethods.showLog(TAG,"MUSIC LIST SIZE : "+homeDatalist!!.size)
                                musicTypeRecyclerAdapter.notifyDataSetChanged()

//                                trendingList = ArrayList()
                                trendingList!!.addAll(getMusicHomeDataModel.musicList.get(upperIndex).innerList)
                                CommonMethods.showLog(TAG,"trendingList LIST SIZE : "+trendingList!!.size)
                                trendingMusicRecyclerAdapter.notifyDataSetChanged()
//                                recyclerViewTrending.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, true)
//                                trendingMusicRecyclerAdapter = TrendingMusicRecyclerAdapter(activityMusic, trendingList!!,activityInterface,this)
//                                recyclerViewTrending.adapter = trendingMusicRecyclerAdapter

                                popularList!!.addAll(getMusicHomeDataModel.popularArtistList)
                                popularArtistRecyclerAdapter.notifyDataSetChanged()

                                playlistList!!.addAll(getMusicHomeDataModel.playlistList)
                                yourPlaylistRecyclerAdapter.notifyDataSetChanged()

                                checkAndShow()

                            }
                            getMusicHomeDataModel.status.equals("0") -> {
                                CommonMethods.showLog(TAG, getMusicHomeDataModel.message)
                            }
                            getMusicHomeDataModel.status.equals("10") -> {
                                CommonMethods.showLog(TAG, getMusicHomeDataModel.message)
                            }
                            else -> {
                                CommonMethods.showLog(TAG,
                                    R.string.someErrorOccurredText.toString())
                            }
                        }
                    } else {
                        CommonMethods.showLog(TAG,
                            R.string.someErrorOccurredText.toString())
                    }
                } else {
                    CommonMethods.showLog(TAG,
                        R.string.someErrorOccurredText.toString())
                }
            }
            MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                CommonMethods.showLog(TAG,
                    R.string.someErrorOccurredText.toString())
            }
        }
    }

    private fun checkAndShow() {

//        if (){
//
//        }
//        else{
//
//        }

        if (playlistList!!.size==0){
            playlistTitle.visibility = View.GONE
        }
        else{
            playlistTitle.visibility = View.VISIBLE
        }

    }

}