package com.napworks.mohra.fragmentPackage

import android.content.*
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.R
import com.napworks.mohra.activitiesPackage.HomeScreenActivity
import com.napworks.mohra.adapterPackage.DrawerRecyclerViewAdapter
import com.napworks.mohra.dialogPackage.LogOutMessageDialog
import com.napworks.mohra.modelPackage.DrawerListModel
import com.napworks.mohra.utilPackage.MyConstants
import kotlinx.android.synthetic.main.fragment_home.*
import kotlin.collections.ArrayList


class NavigationFragment : Fragment(), View.OnClickListener {
    private val TAG = javaClass.simpleName
    private var sharedPreferences: SharedPreferences? = null

    lateinit var profileImg: ImageView
    lateinit var recyclerView: RecyclerView
    lateinit var bottomLayoutDrawer: LinearLayout
    lateinit var name: TextView
    lateinit var emailText: TextView
    lateinit var backDrawer: ImageView

    lateinit var logOutMessageDialog: LogOutMessageDialog

    var mainBroadcastReceiver: MainBroadcastReceiver? = null
    var intentFilter: IntentFilter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_navigation, container, false)
        if (activity != null) {
            mainBroadcastReceiver = MainBroadcastReceiver()
            intentFilter = IntentFilter(MyConstants.NAVIGATION_FRAGMENT)
            sharedPreferences =
                activity!!.getSharedPreferences(MyConstants.SHARED_PREFERENCE, Context.MODE_PRIVATE)
            val list: ArrayList<DrawerListModel> = ArrayList()
            logOutMessageDialog= LogOutMessageDialog(activity!!)

            profileImg=view.findViewById(R.id.profileImg)
            backDrawer=view.findViewById(R.id.backDrawer)
            recyclerView=view.findViewById(R.id.recyclerViewDrawer)
            bottomLayoutDrawer=view.findViewById(R.id.bottomLayoutDrawer)
            name=view.findViewById(R.id.nameTextDrawer)
            emailText=view.findViewById(R.id.emailText)
            bottomLayoutDrawer.setOnClickListener(this)
            backDrawer.setOnClickListener(this)

            list.add(DrawerListModel(R.drawable.profile, getString(R.string.profile)))
            list.add(DrawerListModel(R.drawable.barcode, getString(R.string.barcode)))
            list.add(DrawerListModel(R.drawable.scan, getString(R.string.scan)))
            list.add(DrawerListModel(R.drawable.mywallet, getString(R.string.wallet)))
            list.add(DrawerListModel(R.drawable.settingsfill, getString(R.string.setting)))
            list.add(DrawerListModel(R.drawable.contactus, getString(R.string.help)))

            recyclerView.layoutManager = LinearLayoutManager(activity)
            val drawerRecyclerViewAdapter =
                DrawerRecyclerViewAdapter(activity!!, sharedPreferences!!, list,logOutMessageDialog)
            recyclerView.adapter = drawerRecyclerViewAdapter
            setData()
        }
        return view
    }

    fun setData() {
        if (activity != null) {
            val firstName = sharedPreferences!!.getString(MyConstants.FIRST_NAME, "")
            val familyName = sharedPreferences!!.getString(MyConstants.FAMILY_NAME, "")
            val finalName = firstName.plus(" ").plus(familyName)
            name.text = finalName
            emailText.text = sharedPreferences!!.getString(MyConstants.EMAIL, "")
//            val imagePath = sharedPreferences!!.getString(MyConstants.IMAGE, "")
//            var requestOptions = RequestOptions()
//            requestOptions = requestOptions.placeholder(R.drawable.white_logo)
//            requestOptions = requestOptions.error(R.drawable.white_logo)
//            if (!imagePath.equals("", ignoreCase = true)) {
//                Glide.with(activity!!)
//                    .asBitmap()
//                    .load(imagePath)
//                    .into(profileImg)
//            } else {
            Glide.with(this)
                .load(R.drawable.logosplashscreen)
                    .apply(RequestOptions.circleCropTransform())
//                    .apply(requestOptions)
                .into(profileImg)
//            }
        }
    }

    inner class MainBroadcastReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val type = intent.getStringExtra(MyConstants.BROADCAST_TYPE)
            CommonMethods.showLog(TAG, " type : $type")
            if (type != null && type.equals(MyConstants.UPDATE_PROFILE)) {
                setData()
            }
        }
    }

    override fun onDestroy() {
        if (activity != null) {
            super.onDestroy()
            activity!!.unregisterReceiver(mainBroadcastReceiver)
        }
    }

    override fun onResume() {
        if (activity != null) {
            super.onResume()
            activity!!.registerReceiver(mainBroadcastReceiver, intentFilter)
        }
    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.bottomLayoutDrawer) {
            logOutMessageDialog.showDialog(activity!!.getString(R.string.log_out_message))
        }
        else if(v?.id == R.id.backDrawer)
        {
            CommonMethods.showLog("Nav","clicked back  ")
            (activity as HomeScreenActivity?)?.closeDrawer()
//            activity.closeDrawer()
//            drawerLayout.openDrawer(GravityCompat.END)
        }
    }
}