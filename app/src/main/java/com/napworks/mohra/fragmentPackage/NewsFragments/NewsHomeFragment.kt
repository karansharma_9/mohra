package com.napworks.mohra.fragmentPackage.NewsFragments

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat.finishAffinity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.napworks.mohra.R
import com.napworks.mohra.activitiesPackage.HomeScreenActivity
import com.napworks.mohra.activitiesPackage.NewsActivities.SingleNewsActivity
import com.napworks.mohra.adapterPackage.CategoryRecyclerAdapter
import com.napworks.mohra.adapterPackage.News.NewVerticalRecyclerAdapter
import com.napworks.mohra.adapterPackage.News.NewsTypePageAdapter
import com.napworks.mohra.adapterPackage.News.NewsTypeRecyclerAdapter
import com.napworks.mohra.dialogPackage.LoadingDialog
import com.napworks.mohra.interfacePackage.News.GetNewsCategoriesInterface
import com.napworks.mohra.interfacePackage.News.GetNewsInterface
import com.napworks.mohra.interfacePackage.News.NewsTypeSelectAdapterOnClick
import com.napworks.mohra.modelPackage.News.*
import com.napworks.mohra.utilPackage.ApiCalls
import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.utilPackage.MyConstants
import java.util.*
import kotlin.collections.ArrayList

var sharedPreferences: SharedPreferences? = null

class NewsHomeFragment : Fragment(), GetNewsCategoriesInterface, NewsTypeSelectAdapterOnClick,
    GetNewsInterface, View.OnClickListener {

    var TAG = javaClass.simpleName;
    lateinit var recyclerViewNews: RecyclerView
    lateinit var recyclerViewNewsType: RecyclerView
    private lateinit var newsTypeRecyclerAdapter: NewsTypeRecyclerAdapter
    var newsTypePageAdapter: NewsTypePageAdapter? = null
    var newsChannelName: TextView? = null
    var categoryHighlight: TextView? = null
    var menuBackPress: ImageView? = null
    var haveNewsLayout: LinearLayout? = null
    var noNewsLayout: LinearLayout? = null
    var loadingWidget: LinearLayout? = null
    var newsBox1: LinearLayout? = null
    var titleHighLightNews: TextView? = null
    var highlightImage: ImageView? = null
    var timeOfNewHighlight: TextView? = null
    var Type: String? = null
    var list: ArrayList<NewsTypeModel>? = null
    var newsId : Int = 0
    var newsTypeListCategories: ArrayList<GetNewsInnerCategoriesModel>? = null
    private lateinit var categoryRecyclerAdapter: CategoryRecyclerAdapter
    private lateinit var newVerticalRecyclerAdapter: NewVerticalRecyclerAdapter
    var newsList : ArrayList<NewsDataModel>? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_news_home, container, false)
        recyclerViewNewsType = view.findViewById(R.id.recyclerViewNewsType)

        newsChannelName = view.findViewById(R.id.newsChannelName)
        loadingWidget = view.findViewById(R.id.loadingWidget)
        recyclerViewNews  = view.findViewById(R.id.recyclerViewNews)
        titleHighLightNews = view.findViewById(R.id.titleHighLightNews)
        menuBackPress = view.findViewById(R.id.menuBackPress)
        noNewsLayout = view.findViewById(R.id.noNewsLayout)
        haveNewsLayout = view.findViewById(R.id.haveNewsLayout)
        categoryHighlight = view.findViewById(R.id.categoryHighlight)
        timeOfNewHighlight = view.findViewById(R.id.timeOfNewHighlight)
        highlightImage = view.findViewById(R.id.highlightImage)
        newsBox1 = view.findViewById(R.id.newsBox1)
        haveNewsLayout!!.visibility = View.GONE
        sharedPreferences = activity!!.getSharedPreferences(MyConstants.SHARED_PREFERENCE, AppCompatActivity.MODE_PRIVATE)
        list = ArrayList<NewsTypeModel>()
        newsList = ArrayList<NewsDataModel>()
        newsTypeListCategories = ArrayList<GetNewsInnerCategoriesModel>()
        list!!.add(NewsTypeModel("TOP NEWS", false))
        list!!.add(NewsTypeModel("WORLD NEWS", false))
        list!!.add(NewsTypeModel("POLITCS", false))
        list!!.add(NewsTypeModel("ENTERTAINMENT", false))
        noNewsLayout!!.visibility = View.GONE
        newsBox1!!.setOnClickListener(this)
        menuBackPress!!.setOnClickListener(this)
        getNewsApi(sharedPreferences!!)
        return view
    }

    fun getNewsApi(sharedPreferences: SharedPreferences) {
//        loadingDialog!!.showDialog()
        ApiCalls.getNewsCategories(
            sharedPreferences,
            this
        )
    }

    override fun onGetNewsCategoriesInterfaceResponse(
        status: String?,
        getNewsCategoriesModel: GetNewsCategoriesModel?,
    ) {
        when (status) {
            MyConstants.GO_TO_RESPONSE -> {
                if (getNewsCategoriesModel != null) {
                    if (getNewsCategoriesModel.status != null) {
                        when {
                            getNewsCategoriesModel.status.equals("1") ->
                            {
                                val innerCategoriesData: ArrayList<GetNewsInnerCategoriesModel> =
                                getNewsCategoriesModel.data
                                Type = innerCategoriesData[0].category
                                newsTypeListCategories?.addAll(innerCategoriesData)
                                newsTypeListCategories!![0].isSelected = true
                                recyclerViewNewsType.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, true)
                                newsTypeRecyclerAdapter = NewsTypeRecyclerAdapter(activity!!, newsTypeListCategories!!, this)
                                recyclerViewNewsType.adapter = newsTypeRecyclerAdapter
                                var id =  newsTypeListCategories!![0].categoryId
                                callNewsApi(id)
                            }
                            getNewsCategoriesModel.status.equals("0") ->
                            {
                            }
                            getNewsCategoriesModel.status.equals("10") ->
                            {
                            }
                            else -> {

                                CommonMethods.showLog(TAG,
                                    R.string.someErrorOccurredText.toString())
                            }
                        }
                    } else {
                        CommonMethods.showLog(TAG,
                            R.string.someErrorOccurredText.toString())
                    }
                } else {

                    CommonMethods.showLog(TAG,
                        R.string.someErrorOccurredText.toString())
                }
            }
            MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                CommonMethods.showLog(TAG,
                    R.string.someErrorOccurredText.toString())
            }
        }
    }

    fun callNewsApi(categoryId: String?)
    {
        CommonMethods.showLog(TAG,"categoryId  $categoryId")
//        loadingDialog!!.showDialog()
        haveNewsLayout!!.visibility = View.GONE
        ApiCalls.getNews(
            sharedPreferences!!,
            categoryId.toString(),
            this
        )
    }
    override fun onNewsTypeSelectAdapterOnClick(
        status: String?,
        getNewsInnerCategoriesModel: GetNewsInnerCategoriesModel,
        position: Int, ) {
        for (listdata in newsTypeListCategories!!) {
            listdata.isSelected = false
        }
        Type = getNewsInnerCategoriesModel.category
        getNewsInnerCategoriesModel.isSelected = true
        newsTypeRecyclerAdapter.notifyDataSetChanged()
        var categoryId = getNewsInnerCategoriesModel.categoryId
        callNewsApi(categoryId)
        loadingWidget!!.visibility = View.VISIBLE

    }

    override fun onGetNewsInterfaceResponse(status: String?, getNewsModel: GetNewsModel?) {
        when (status) {
            MyConstants.GO_TO_RESPONSE -> {
                if (getNewsModel != null) {
                    if (getNewsModel.status != null) {
                        when {
                            getNewsModel.status.equals("1") -> {
//                                val innerCategoriesData: ArrayList<GetNewsModel> =
//                                    getNewsModel.newsData
//                                   loadingDialog!!.hideDialog()
                                loadingWidget!!.visibility = View.GONE
                                haveNewsLayout!!.visibility = View.VISIBLE
                                newsList!!.clear()
                                var hightLightNews : NewsDataModel  = getNewsModel.highlightedNews
                                var innerData : ArrayList<NewsDataModel> = getNewsModel.newsData
                                newsList!!.addAll(innerData)
                                if(hightLightNews != null)
                                {
                                    newsId  = hightLightNews.newsId!!
                                    noNewsLayout!!.visibility = View.GONE
                                    haveNewsLayout!!.visibility = View.VISIBLE
                                    newsChannelName!!.text = hightLightNews.newsChannel
                                    titleHighLightNews!!.text = hightLightNews.title
                                    categoryHighlight!!.text = Type

                                    Glide.with(this)
                                        .load(hightLightNews.image)
                                        .into(highlightImage!!);

                                    val date = Date()
                                    val timeMilli: Long = date.getTime()
                                    var time = hightLightNews.time!!.toLong()
                                    var show = (timeMilli / 1000) - time

                                    timeOfNewHighlight!!.text = CommonMethods.timeAgoDisplay(show)
                                }
                                else{
                                    noNewsLayout!!.visibility = View.VISIBLE
                                    haveNewsLayout!!.visibility = View.GONE
                                }
                                recyclerViewNews.setOnTouchListener(View.OnTouchListener { v, event -> true })
                                recyclerViewNews.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, true)
                                newVerticalRecyclerAdapter = NewVerticalRecyclerAdapter(activity!!, newsList!!,Type , sharedPreferences)
                                recyclerViewNews.adapter = newVerticalRecyclerAdapter
                            }
                            getNewsModel.status.equals("0") -> {
                                loadingWidget!!.visibility = View.GONE

                            }
                            getNewsModel.status.equals("10") -> {
                                loadingWidget!!.visibility = View.GONE

                            }
                            else -> {
                                loadingWidget!!.visibility = View.GONE


                                CommonMethods.showLog(TAG,
                                    R.string.someErrorOccurredText.toString())
                            }
                        }
                    } else {
                        loadingWidget!!.visibility = View.GONE

                        CommonMethods.showLog(TAG,
                            R.string.someErrorOccurredText.toString())
                    }
                } else {
                    CommonMethods.showLog(TAG,
                        R.string.someErrorOccurredText.toString())
                }
            }
            MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                CommonMethods.showLog(TAG,
                    R.string.someErrorOccurredText.toString())
            }
        }
    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.newsBox1) {
            val intent = Intent(activity, SingleNewsActivity::class.java)
            intent.putExtra("newsID",newsId)
            this.startActivity(intent)
        }
        else if(v?.id == R.id.menuBackPress)
        {
            val intent = Intent(activity, HomeScreenActivity::class.java)
            this.startActivity(intent)
            finishAffinity(activity!!);
        }
    }
}


