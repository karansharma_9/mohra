package com.napworks.mohra.fragmentPackage.NewsFragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnTouchListener
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.napworks.mohra.R
import com.napworks.mohra.activitiesPackage.NewsActivities.SingleNewsActivity
import com.napworks.mohra.adapterPackage.News.NewVerticalRecyclerAdapter
import com.napworks.mohra.adapterPackage.News.OnSpotNewsRecyclerAdapter
import com.napworks.mohra.modelPackage.MusicModel.PlaylistFromMyLibraryModel
import com.napworks.mohra.modelPackage.MusicModel.RecentlyPlayedModel


class TopNewsFragment : Fragment(), View.OnClickListener {
    lateinit var recyclerViewNews: RecyclerView
    lateinit var recyclerViewNews2: RecyclerView
    lateinit var recyclerViewOnTheSpot: RecyclerView
    lateinit var newsBox1: LinearLayout
    lateinit var newsBox2: LinearLayout
    var list : ArrayList<RecentlyPlayedModel>? = null

    private lateinit var newVerticalRecyclerAdapter: NewVerticalRecyclerAdapter
    var playList : ArrayList<PlaylistFromMyLibraryModel>? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_top_news, container, false)
        recyclerViewNews  = view.findViewById(R.id.recyclerViewNews)
        recyclerViewNews2  = view.findViewById(R.id.recyclerViewNews2)
        newsBox1  = view.findViewById(R.id.newsBox1)
        newsBox2  = view.findViewById(R.id.newsBox2)
        recyclerViewOnTheSpot = view.findViewById(R.id.recyclerViewOnTheSpot)

        playList = ArrayList<PlaylistFromMyLibraryModel> ()


        list = ArrayList<RecentlyPlayedModel> ()

        list!!.add(RecentlyPlayedModel("1", "Trending",false,""))
        list!!.add(RecentlyPlayedModel("2", "New This Week",false,""))
        list!!.add(RecentlyPlayedModel("3", "New Artist",false,""))
        list!!.add(RecentlyPlayedModel("4", "My List",false,""))



        recyclerViewOnTheSpot.layoutManager = LinearLayoutManager(activity,
            LinearLayoutManager.HORIZONTAL,
            true)
        val OnSpotNewsRecyclerAdapter = OnSpotNewsRecyclerAdapter(activity!!, list!!)
        recyclerViewOnTheSpot.adapter = OnSpotNewsRecyclerAdapter

        playList!!.add(PlaylistFromMyLibraryModel("Create Playlist","",""))
        playList!!.add(PlaylistFromMyLibraryModel("Dance Hits","",""))
        playList!!.add(PlaylistFromMyLibraryModel("New of January","",""))
        playList!!.add(PlaylistFromMyLibraryModel("Popular Russian","",""))
        playList!!.add(PlaylistFromMyLibraryModel("Classical Music","",""))
        playList!!.add(PlaylistFromMyLibraryModel("Best Works","",""))
//        playList!!.add(PlaylistFromMyLibraryModel("Nothing To Say","",""))

//        recyclerViewNews.setOnTouchListener(OnTouchListener { v, event -> true })
//        recyclerViewNews.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, true)
//        newVerticalRecyclerAdapter = NewVerticalRecyclerAdapter(activity!!, playList!!)
//        recyclerViewNews.adapter = newVerticalRecyclerAdapter

//        recyclerViewNews2.setOnTouchListener(OnTouchListener { v, event -> true })
//        recyclerViewNews2.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, true)
//        newVerticalRecyclerAdapter = NewVerticalRecyclerAdapter(activity!!, playList!!)
//        recyclerViewNews2.adapter = newVerticalRecyclerAdapter

        newsBox1.setOnClickListener(this)
        newsBox2.setOnClickListener(this)
        return view
    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.ResetButton) {
            val intent = Intent(activity, SingleNewsActivity::class.java)
            this.startActivity(intent)
        }
        else{
            val intent = Intent(activity, SingleNewsActivity::class.java)
            this.startActivity(intent)
        }
    }
}