package com.napworks.mohra.adapterPackage

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.napworks.mohra.R
import com.napworks.mohra.modelPackage.GetIntroDataInnerModel
import com.napworks.mohra.utilPackage.CommonMethods
import java.util.*


class WelcomeViewPagerAdapter(
    var context: Context, // Array of images
    var dataForPager: ArrayList<GetIntroDataInnerModel>,
    val activity: Activity,
)
    : PagerAdapter(), View.OnClickListener {

    var mLayoutInflater: LayoutInflater
    override fun getCount(): Int {
       return dataForPager.size

    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object` as LinearLayout
    }



    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView: View = mLayoutInflater.inflate(R.layout.view_pager_welcomepage,
            container,
            false)

        CommonMethods.showLog("", "size is ${dataForPager.size}")

        val imagesWelcomePageViewer = itemView.findViewById<View>(R.id.imagesWelcomePageViewer) as ImageView
//        val drawerLayout = itemView.findViewById<View>(R.id.drawerLayout) as DrawerLayout
        val title = itemView.findViewById<View>(R.id.titleViewPagerWelcomePage) as TextView
        val description = itemView.findViewById<View>(R.id.descriptionViewPagerWelcomePage) as TextView

        title.text = dataForPager[position].title
        description.text = dataForPager[position].description
        Glide.with(activity).load(dataForPager[position].image).apply(RequestOptions()
            .placeholder(R.drawable.logosplashscreen))
            .into(imagesWelcomePageViewer)
        Objects.requireNonNull(container).addView(itemView)
        return itemView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as LinearLayout)
    }

    init {
        dataForPager = dataForPager
        mLayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.menuButton)
        {
//            drawerLayout.openDrawer(GravityCompat.START)
        }
    }


}