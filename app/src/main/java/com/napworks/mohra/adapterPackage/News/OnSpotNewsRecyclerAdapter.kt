package com.napworks.mohra.adapterPackage.News

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView

import com.napworks.mohra.R
import com.napworks.mohra.modelPackage.MusicModel.RecentlyPlayedModel


class OnSpotNewsRecyclerAdapter(val activity: Activity, val list: ArrayList<RecentlyPlayedModel>,
) : RecyclerView.Adapter<OnSpotNewsRecyclerAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MyViewHolder {
        val view: View = LayoutInflater.from(activity).inflate(R.layout.layout_on_the_spot_news, viewGroup, false)
        return MyViewHolder(view, activity);
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bindData(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class MyViewHolder(itemView: View, sentActivity: Activity, ) : RecyclerView.ViewHolder(itemView), View.OnClickListener
    {
        val TAG: String = javaClass.simpleName
        val activity = sentActivity
        var layoutCategory: LinearLayout = itemView.findViewById(R.id.layoutCategory)
        var innerModel = RecentlyPlayedModel()
        fun bindData(recentlyPlayedModel: RecentlyPlayedModel) {
            innerModel = recentlyPlayedModel

        }

        override fun onClick(v: View) {

        }
    }



}