package com.napworks.mohra.adapterPackage.MusicAdapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.napworks.mohra.R
import com.napworks.mohra.activitiesPackage.MusicActivities.PlaylistDetails
import com.napworks.mohra.modelPackage.MusicModel.MusicInnerModel
import com.napworks.mohra.modelPackage.MusicModel.PlaylistFromMyLibraryModel
import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.utilPackage.MyConstants


class PlaylistTileMyLibraryRecyclerAdapter(val activity: Activity,
                                           val list: ArrayList<MusicInnerModel>) : RecyclerView.Adapter<PlaylistTileMyLibraryRecyclerAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MyViewHolder {
        val view: View = LayoutInflater.from(activity).inflate(R.layout.layout_playlist_mylibrary, viewGroup, false)
        return MyViewHolder(view, activity);
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bindData(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class MyViewHolder(
        itemView: View,
        sentActivity: Activity) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val TAG: String = javaClass.simpleName
        val activity = sentActivity
        var type = ""
        var innerModel = MusicInnerModel()
        var layoutCategory: LinearLayout = itemView.findViewById(R.id.layoutCategory)
        var titlePlaylistMyLibrary : TextView = itemView.findViewById(R.id.titlePlaylistMyLibrary)
        var totalSongs : TextView = itemView.findViewById(R.id.totalSongs)
        var image : ImageView = itemView.findViewById(R.id.image)

        fun bindData(playlistFromMyLibraryModel: MusicInnerModel) {
            innerModel = playlistFromMyLibraryModel
            type = innerModel.type.toString()
            CommonMethods.showLog(TAG,"TYPE : "+type)
            titlePlaylistMyLibrary.text = innerModel.innerTitle
            var requestOptions = RequestOptions()
            requestOptions = requestOptions.transforms(CenterCrop(), RoundedCorners(16))
            Glide.with(activity)
                .load(innerModel.image)
                .apply(requestOptions)
                .into(image)


                if (innerModel.totalSongs != null) {
                    if (innerModel.totalSongs!! > 1) {
                        totalSongs.text = innerModel.totalSongs.toString().plus(" Songs")

                    } else {
                        totalSongs.text = innerModel.totalSongs.toString().plus(" Song")
                    }
                }
                else{
                    totalSongs.text = innerModel.artistName
                }

            layoutCategory.setOnClickListener(this)

        }

        override fun onClick(v: View?) {
            if(v?.id==R.id.layoutCategory){
                if(type=="Playlist"){
                    val intent = Intent(activity, PlaylistDetails::class.java)
                    intent.putExtra(MyConstants.CALLED_FROM, MyConstants.PLAYLIST_DETAIL)
                    intent.putExtra(MyConstants.ID,innerModel.innerId)
                    activity.startActivity(intent)
                }
                else if (type == "Albums"){
                    val intent = Intent(activity, PlaylistDetails::class.java)
                    intent.putExtra(MyConstants.CALLED_FROM, MyConstants.ALBUM_DETAIL)
                    intent.putExtra(MyConstants.ID,innerModel.innerId)
                    activity.startActivity(intent)
                }
            }
        }
    }
}