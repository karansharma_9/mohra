package com.napworks.mohra.adapterPackage

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.napworks.mohra.R
import com.napworks.mohra.modelPackage.AvtarModel
import com.napworks.mohra.modelPackage.DarkImagesModel
import com.napworks.mohra.utilPackage.CommonMethods

class AvtarRecyclerAdapter(
    val activity: Activity,
    val list: ArrayList<DarkImagesModel>,
    val screenWidth: Int
) : RecyclerView.Adapter<AvtarRecyclerAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(activity).inflate(R.layout.avtar_layout, viewGroup, false)
        return MyViewHolder(view, activity);
    }

    override fun onBindViewHolder(viewHolder: MyViewHolder, i: Int) {
        viewHolder.bindData(list.get(i), screenWidth)
    }

    override fun getItemCount(): Int {
        return list.size
        CommonMethods.showLog("list size  ======> ", list.size.toString())
    }

    class MyViewHolder(
        itemView: View,
        sentActivity: Activity
    ) : RecyclerView.ViewHolder(itemView) {
        val TAG: String = javaClass.simpleName
        val activity = sentActivity
        var relativeLayout: LinearLayout = itemView.findViewById(R.id.linearlayoutAvtar)
        var titleName: TextView = itemView.findViewById(R.id.titleName)
        var imageViewSmallAvtar: ImageView = itemView.findViewById(R.id.imageViewSmallAvtar)
        var innerModel = DarkImagesModel()
        fun bindData(avtarModel: DarkImagesModel, screenWidth: Int) {
            innerModel = avtarModel
            CommonMethods.showLog("image data ", innerModel.image)
            val layoutParams = LinearLayout.LayoutParams(relativeLayout.layoutParams)
            layoutParams.width = screenWidth + 1 - 1
            layoutParams.height = screenWidth + 1 - 1 + 120
            relativeLayout.layoutParams = layoutParams
            titleName.text = innerModel.title
            Glide.with(activity).load(innerModel.image).apply(RequestOptions()
                .placeholder(R.drawable.logosplashscreen))
                .into(imageViewSmallAvtar)

        }

    }
}