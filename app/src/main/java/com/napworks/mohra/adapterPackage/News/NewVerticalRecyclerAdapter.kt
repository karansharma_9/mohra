package com.napworks.mohra.adapterPackage.News

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.napworks.mohra.activitiesPackage.NewsActivities.SingleNewsActivity
import com.napworks.mohra.modelPackage.News.NewsDataModel
import com.napworks.mohra.utilPackage.CommonMethods
import android.view.*
import android.widget.*
import com.napworks.mohra.R
import com.napworks.mohra.interfacePackage.CommonStatusInterface
import com.napworks.mohra.modelPackage.CommonStatusModel
import com.napworks.mohra.utilPackage.ApiCalls
import com.napworks.mohra.utilPackage.MyConstants
import java.util.*
import kotlin.collections.ArrayList


class NewVerticalRecyclerAdapter(
    val activity: Activity,
    val list: ArrayList<NewsDataModel>,
    type1: String?,
    var sharedPreferences: SharedPreferences?,
) : RecyclerView.Adapter<NewVerticalRecyclerAdapter.MyViewHolder>() {
    var catType :String = type1.toString()
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MyViewHolder {
        val view: View = LayoutInflater.from(activity).inflate(R.layout.layout_news_recyclerview, viewGroup, false)
        return MyViewHolder(view, activity, catType!!,sharedPreferences);
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bindData(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class MyViewHolder(
        itemView: View,
        sentActivity: Activity,
        Type: String,
        sharedPreferences: SharedPreferences?,
    ) : RecyclerView.ViewHolder(itemView), View.OnClickListener,
        PopupMenu.OnMenuItemClickListener, CommonStatusInterface {
        val TAG: String = javaClass.simpleName
        val activity = sentActivity
        var newId = 0
        var popupMenu : PopupMenu ? =null
        var category = Type
        var isFav = false
        var item: MenuItem? = null
        var shared = sharedPreferences
        var innerModel = NewsDataModel()
        var titleVerticalNews : TextView = itemView.findViewById(R.id.titleVerticalNews)
        var newsChannelNameVertical : TextView = itemView.findViewById(R.id.newsChannelNameVertical)
        var imageViewVerticalNews : ImageView = itemView.findViewById(R.id.imageViewVerticalNews)
        var moreIcon : ImageView = itemView.findViewById(R.id.moreIcon)
        var layoutCategoryVertical : LinearLayout = itemView.findViewById(R.id.layoutCategoryVertical)
        var typeCategory : TextView = itemView.findViewById(R.id.typeCategory)
        var timeForFavNews : TextView = itemView.findViewById(R.id.timeForFavNews)

        fun bindData(newsDataModel: NewsDataModel) {
            innerModel = newsDataModel
            typeCategory.text = category
            titleVerticalNews.text = innerModel.title
            newsChannelNameVertical.text = innerModel.newsChannel

            val date = Date()
            val timeMilli: Long = date.getTime()
            var time = innerModel.time!!.toLong()
            var show = (timeMilli / 1000) - time

            timeForFavNews.text = CommonMethods.timeAgoDisplay(show)
            var requestOptions = RequestOptions()
             popupMenu = PopupMenu(
                activity,
                moreIcon
            )
            popupMenu!!.menuInflater.inflate(
                com.napworks.mohra.R.menu.menu_like_unlike,
                popupMenu!!.menu
            )
            var fav = newsDataModel.isFavourite
            item = popupMenu!!.menu.findItem(R.id.favMenu)

            if(fav == 0 )
            {
                isFav = false
                item!!.setTitle("Mark as Favourite");
            }
            else
            {
                isFav = true
                item!!.setTitle(" Remove from Favourites")
            }
            popupMenu!!.setOnMenuItemClickListener(this)
            requestOptions = requestOptions.transforms(CenterCrop(), RoundedCorners(16))
            Glide.with(activity)
                .load(innerModel.image)
                .apply(requestOptions)
                .into(imageViewVerticalNews)
            layoutCategoryVertical.setOnClickListener(this)
            moreIcon.setOnClickListener(this)
            newId = innerModel.newsId!!
//            titlePlaylistMyLibrary.text = innerModel.name
        }

        override fun onClick(v: View?) {
            if (v?.id == com.napworks.mohra.R.id.layoutCategoryVertical) {
                val intent = Intent(activity, SingleNewsActivity::class.java)
                intent.putExtra("newsID",newId)
                activity.startActivity(intent)
            }
            else if(v?.id == com.napworks.mohra.R.id.moreIcon)
            {
                popupMenu?.show()
            }
        }

        override fun onMenuItemClick(item: MenuItem?): Boolean {
            var TAG = javaClass.simpleName;
            val id = item!!.itemId
            CommonMethods.showLog(TAG,"moreIcon  $id")
            CommonMethods.showLog(TAG,"moreIcon")
            setFav(shared!!, innerModel.newsId!!)
            return false
        }

        fun setFav(sharedPreferences: SharedPreferences, newsID: Int)
        {
            var value = 5
            if (isFav== false)
            {
                isFav = true
                value = 1
                item!!.setTitle(" Remove from Favourites")
            }
            else
            {
                isFav = false
                value = 0
                item!!.setTitle("Mark as Favourite");
            }
            ApiCalls.favouriteNews(
                sharedPreferences,
                value.toString(),
                newsID.toString(),
                this

            )
        }

        override fun onCommonStatusInterfaceResponse(
            status: String?,
            commonStatusModel: CommonStatusModel?,
        )
        {
            when (status) {
                MyConstants.GO_TO_RESPONSE -> {
                    CommonMethods.showLog(TAG, "login  status  " + commonStatusModel!!.status)
                    if (commonStatusModel != null) {
                        if (commonStatusModel.status != null) {
                            when {
                                commonStatusModel.status.equals("1") -> {
                                }
                                commonStatusModel.status.equals("0") -> {
                                    CommonMethods.showLog(TAG, commonStatusModel.message)
                                }
                                commonStatusModel.status.equals("10") -> {
                                    CommonMethods.showLog(TAG, commonStatusModel.message)
                                }
                                else -> {
                                    CommonMethods.showLog(TAG,
                                        R.string.someErrorOccurredText.toString())
                                }
                            }
                        } else {
                            CommonMethods.showLog(TAG,
                                R.string.someErrorOccurredText.toString())
                        }
                    } else {
                        CommonMethods.showLog(TAG,
                            R.string.someErrorOccurredText.toString())
                    }
                }
                MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                    CommonMethods.showLog(TAG,
                        R.string.someErrorOccurredText.toString())
                }
            }
        }

    }
}