package com.napworks.mohra.adapterPackage.News

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.napworks.mohra.R
import com.napworks.mohra.fragmentPackage.NewsFragments.SearchNewsFragment
import com.napworks.mohra.modelPackage.News.GetNewsInnerCategoriesModel


class PronounSpinnerAdapter(
    context: Activity,
    resource: Int,
    textViewResourceId: Int,
    list: List<GetNewsInnerCategoriesModel>,
   var searchNewsFragment: SearchNewsFragment,
) :
    ArrayAdapter<GetNewsInnerCategoriesModel?>(context, resource, textViewResourceId, list),
    View.OnClickListener {
    private val TAG = javaClass.simpleName
    private val layoutInflater: LayoutInflater
    var list: List<GetNewsInnerCategoriesModel>
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.layout_spinner, parent, false)
        }
        val innerData: GetNewsInnerCategoriesModel? = getItem(position)
        val textView = convertView!!.findViewById<TextView>(R.id.title)
        val selectedCategoryTick = convertView.findViewById<ImageView>(R.id.selectedCategoryTick)
        val mainLinear = convertView.findViewById<LinearLayout>(R.id.mainLinear)
        if (innerData != null)
        {

            if (innerData.isSelected)
            {
                selectedCategoryTick.visibility = View.VISIBLE
            }
            else
            {
                selectedCategoryTick.visibility = View.INVISIBLE
            }
            textView.setText(innerData.category)
        }

//        mainLinear.setOnClickListener(this)
        return convertView
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return getView(position, convertView, parent)
    }

    init {
        layoutInflater = context.layoutInflater
        this.list = list
    }

    override fun onClick(v: View?) {
//        if (v?.id == R.id.mainLinear)
//        {
//            searchNewsFragment.onSearchFilterAdapterOnClick("1",0)
//        }
    }
}