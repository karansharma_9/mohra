package com.napworks.mohra.adapterPackage.News

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView

import com.napworks.mohra.R
import com.napworks.mohra.interfacePackage.News.NewsTypeSelectAdapterOnClick
import com.napworks.mohra.modelPackage.News.GetNewsInnerCategoriesModel
import com.napworks.mohra.utilPackage.CommonMethods


class NewsTypeRecyclerAdapter(val activity: Activity, val list: ArrayList<GetNewsInnerCategoriesModel>, val newsTypeSelectAdapterOnClick : NewsTypeSelectAdapterOnClick) : RecyclerView.Adapter<NewsTypeRecyclerAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MyViewHolder {
        val view: View = LayoutInflater.from(activity).inflate(R.layout.layout_news_type_tile, viewGroup, false)
        return MyViewHolder(view, activity,newsTypeSelectAdapterOnClick);
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bindData(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class MyViewHolder(itemView: View, sentActivity: Activity, newsTypeSelectAdapterOnClick: NewsTypeSelectAdapterOnClick, ) : RecyclerView.ViewHolder(itemView), View.OnClickListener
    {
        val TAG: String = javaClass.simpleName
        val activity = sentActivity
        var layoutNewsType: LinearLayout = itemView.findViewById(R.id.layoutNewsType)
        var newsTypeTitle: TextView = itemView.findViewById(R.id.newsTypeTitle)
        var viewInNewsType: View = itemView.findViewById(R.id.viewInNewsType)
        var innerModel = GetNewsInnerCategoriesModel()
        val interfaceNews : NewsTypeSelectAdapterOnClick = newsTypeSelectAdapterOnClick
        fun bindData(newsCategoriesModel: GetNewsInnerCategoriesModel) {
            innerModel = newsCategoriesModel
            newsTypeTitle.text = innerModel.category
            CommonMethods.showLog(TAG,"title  =>  " + innerModel.category)
            if(innerModel.isSelected == true)
            {
                viewInNewsType.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.button_welcome_screen) );
                newsTypeTitle.setTextColor(ContextCompat.getColor(activity, R.color.colorPrimary));
            }
            else
            {
                viewInNewsType.setBackgroundColor(ContextCompat.getColor(activity, R.color.backLightGrey))
                newsTypeTitle.setTextColor(ContextCompat.getColor(activity, R.color.black));
            }
            layoutNewsType.setOnClickListener(this)

        }

        override fun onClick(v: View) {
            if (v?.id == R.id.layoutNewsType) {
                interfaceNews.onNewsTypeSelectAdapterOnClick("1",innerModel,adapterPosition)
            }
        }
    }



}