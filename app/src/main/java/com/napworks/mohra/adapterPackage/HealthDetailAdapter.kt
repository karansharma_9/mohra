package com.napworks.mohra.adapterPackage

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.napworks.mohra.utilPackage.CommonMethods


class HealthDetailAdapter(fm: FragmentManager) :
    FragmentPagerAdapter(fm) {
    var TAG = javaClass.simpleName;
    private val fragments: MutableList<Fragment> = ArrayList()
    private val fragmentTitle: MutableList<String> = ArrayList()
    fun add(fragment: Fragment, title: String) {
        fragments.add(fragment)
        fragmentTitle.add(title)
    }

    override fun getItem(position: Int): Fragment {
        CommonMethods.showLog(TAG," pos " + position)
        return fragments[position]
    }

    override fun getCount(): Int {
        return fragments.size
    }


    override fun getPageTitle(position: Int): CharSequence? {
        return fragmentTitle[position]
    }
}