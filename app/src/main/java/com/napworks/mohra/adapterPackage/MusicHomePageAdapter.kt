package com.napworks.mohra.adapterPackage

import android.app.Activity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.napworks.mohra.fragmentPackage.MusicFragments.MusicHomeFragment
import com.napworks.mohra.fragmentPackage.MusicFragments.MyLibraryFragment
import com.napworks.mohra.fragmentPackage.MusicFragments.PlayListDetailsFragment
import com.napworks.mohra.interfacePackage.music.AdapterOnClick


class MusicHomePageAdapter(
    fm: FragmentManager,
    private val introList: List<String?>?,
    private val clickInterface: AdapterOnClick,
    var activity: Activity
) :
    FragmentStatePagerAdapter(fm) {
    var TAG = javaClass.simpleName;
    private val fragments: MutableList<Fragment> = ArrayList()
    private val fragmentTitle: MutableList<String> = ArrayList()

//    private val introList: List<String>


    override fun getItem(position: Int): Fragment {
        var fragment: Fragment? = null
        if (position == 0) {
            fragment = MusicHomeFragment(activity,clickInterface)
        } else if (position == 1) {
            fragment = MyLibraryFragment()
        } else {
           fragment = PlayListDetailsFragment()
        }
        return fragment
    }

    override fun getCount(): Int {
        return introList!!.size
    }

    init {
//        this.introList = introList as List<String>
    }
}