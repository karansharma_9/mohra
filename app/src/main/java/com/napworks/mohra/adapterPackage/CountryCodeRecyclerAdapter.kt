package com.napworks.mohra.adapterPackage

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.napworks.mohra.R
import com.napworks.mohra.modelPackage.CountryListModel
import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.utilPackage.MyConstants


class CountryCodeRecyclerAdapter(
    val activity: Activity,
    val list: ArrayList<CountryListModel>,
) : RecyclerView.Adapter<CountryCodeRecyclerAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(activity).inflate(R.layout.layout_country, viewGroup, false)
        return MyViewHolder(view, activity);
    }

    override fun onBindViewHolder(viewHolder: MyViewHolder, i: Int) {
        viewHolder.bindData(list[i])
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class MyViewHolder(
        itemView: View,
        sentActivity: Activity,
    ) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val TAG: String = javaClass.simpleName
        val activity = sentActivity
        var countryLongCode: TextView = itemView.findViewById(R.id.countryLongCode)
        var countryName: TextView = itemView.findViewById(R.id.countryName)
        var countryCode: TextView = itemView.findViewById(R.id.countryCode)
        var mainCountryLayout: RelativeLayout = itemView.findViewById(R.id.mainCountryLayout)
        var innerModel = CountryListModel()
        fun bindData(countryModel: CountryListModel) {
            innerModel = countryModel

            countryLongCode.text =  innerModel.shortCode
            countryName.text = innerModel.countryName
            countryCode.text = "+"+innerModel.phoneCode
            mainCountryLayout.setOnClickListener(this)

        }


        override fun onClick(v: View?) {
            if (v?.id == R.id.mainCountryLayout) {

                CommonMethods.showLog("adapter  countryName => ",  innerModel.countryName)
                CommonMethods.showLog("adapter  countryId  => ",  innerModel.countryId)
                CommonMethods.showLog("adapter  longCode  => ",  innerModel.longCode)
                CommonMethods.showLog("adapter  shortCode  => ",  innerModel.shortCode)
                CommonMethods.showLog("adapter  phoneCode  => ",  innerModel.phoneCode)
                val intent = Intent()
                intent.putExtra(MyConstants.COUNTRY_MODEL,innerModel)
                intent.putExtra(MyConstants.COUNTRY_CALLING_CODE,
                    innerModel.phoneCode)
                intent.putExtra(MyConstants.COUNTRY_ID,
                    innerModel.countryId)
                intent.putExtra(MyConstants.COUNTRY_NAME,
                    innerModel.countryName)
                intent.putExtra(MyConstants.COUNTRY_SHORT_CODE,
                    innerModel.shortCode)
                activity.setResult(Activity.RESULT_OK, intent)
                activity.finish()
                activity.overridePendingTransition(0, 0)
            }
        }


    }
}