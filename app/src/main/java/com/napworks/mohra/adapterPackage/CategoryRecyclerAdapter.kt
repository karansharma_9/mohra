package com.napworks.mohra.adapterPackage

import android.app.Activity
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.napworks.gamaloto.interfacePackage.OnHomeScreenCategoryAdapterClick
import com.napworks.mohra.R
import com.napworks.mohra.modelPackage.News.GetNewsInnerCategoriesModel
import com.napworks.mohra.utilPackage.CommonMethods


class CategoryRecyclerAdapter(val activity: Activity, val list: ArrayList<GetNewsInnerCategoriesModel>, val onHomeScreenCategoryAdapterClick : OnHomeScreenCategoryAdapterClick) : RecyclerView.Adapter<CategoryRecyclerAdapter.MyViewHolder>() {
    //    private val confirmationDialog: CommonConfirmationDialog
    var UPDATE_PROFILE_REQUEST_CODE = 13
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MyViewHolder {
        val view: View = LayoutInflater.from(activity).inflate(R.layout.category_list, viewGroup, false)
        return MyViewHolder(view, activity,onHomeScreenCategoryAdapterClick);
    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bindData(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }



    class MyViewHolder(
        itemView: View,
        sentActivity: Activity,
        onHomeScreenCategoryAdapterClick: OnHomeScreenCategoryAdapterClick, ) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        val activity : Activity = sentActivity
        val TAG: String = javaClass.simpleName
        var layoutCategory: LinearLayout = itemView.findViewById(R.id.layoutCategory)
        var titleCate: TextView = itemView.findViewById(R.id.titleCate)
        var innerModel = GetNewsInnerCategoriesModel()
        val interfaceOnClick: OnHomeScreenCategoryAdapterClick = onHomeScreenCategoryAdapterClick

        fun bindData(getNewsInnerCategoriesModel: GetNewsInnerCategoriesModel) {
            innerModel = getNewsInnerCategoriesModel
            titleCate.text = innerModel.category
            CommonMethods.showLog(TAG,"isSelected  =>  " + innerModel.isSelected)
            if(innerModel.isSelected!!)
            {
                layoutCategory.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.button_welcome_screen) );
                titleCate.setTextColor(Color.parseColor("#FFFFFFFF"));
            }
            else
            {
                layoutCategory.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.white_button) );
                titleCate.setTextColor(Color.parseColor("#FF000000"));
            }
            layoutCategory.setOnClickListener(this)

        }

        override fun onClick(v: View?) {
            if (v?.id == R.id.layoutCategory) {
                interfaceOnClick.onHomeScreenCategoryAdapterClick("1",innerModel,adapterPosition)
            }
            }
        }
    }
