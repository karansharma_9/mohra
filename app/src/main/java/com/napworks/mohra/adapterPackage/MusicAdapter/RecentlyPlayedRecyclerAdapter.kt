package com.napworks.mohra.adapterPackage.MusicAdapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.napworks.mohra.R
import com.napworks.mohra.activitiesPackage.MusicActivities.MusicMainActivity
import com.napworks.mohra.interfacePackage.music.AdapterOnClick
import com.napworks.mohra.interfacePackage.music.MusicTypeSelectAdapterOnClick
import com.napworks.mohra.interfacePackage.music.SelectedSongAdapterOnClick
import com.napworks.mohra.modelPackage.MusicModel.MusicFilesModel
import com.napworks.mohra.modelPackage.MusicModel.MusicInnerModel


class RecentlyPlayedRecyclerAdapter(
    val activity: Activity,
    val list: ArrayList<MusicInnerModel>,
    val activityInterface: AdapterOnClick,
    val selectedSongAdapterOnClick: SelectedSongAdapterOnClick
) : RecyclerView.Adapter<RecentlyPlayedRecyclerAdapter.MyViewHolder>()
{
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MyViewHolder {
        val view: View = LayoutInflater.from(activity).inflate(R.layout.recently_played_music_tile, viewGroup, false)
        return MyViewHolder(view, activity,activityInterface,list,selectedSongAdapterOnClick);
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bindData(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class MyViewHolder(
        itemView: View,
        sentActivity: Activity,
        activityInterface: AdapterOnClick,
        list: ArrayList<MusicInnerModel>,
        selectedSongAdapterOnClick: SelectedSongAdapterOnClick,
    ) : RecyclerView.ViewHolder(itemView), View.OnClickListener
    {
        val TAG: String = javaClass.simpleName
        val activity = sentActivity
        var musicMainActivity: MusicMainActivity? = null
        var layoutSong: LinearLayout = itemView.findViewById(R.id.layoutSong)
        var isPlayingImage: ImageView = itemView.findViewById(R.id.isPlayingImage)
        var innerModel = MusicInnerModel()
        var interFace = activityInterface
        val interfaceOnClickMusicType: SelectedSongAdapterOnClick = selectedSongAdapterOnClick
        var listMusicFiles : ArrayList<MusicInnerModel> = list
        fun bindData(musicFilesModel: MusicInnerModel) {
//            musicMainActivity = MusicMainActivity()
            innerModel = musicFilesModel
            if(innerModel.isSelected == true)
            {
                isPlayingImage.setBackgroundResource(R.drawable.playing)
            }
            else
            {
                isPlayingImage.setBackgroundResource(0)
            }
            layoutSong.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            if (v?.id == R.id.layoutSong) {
//                interFace.onAdapterClick(innerModel,adapterPosition,listMusicFiles)
//                interfaceOnClickMusicType.onSelectedSongAdapterOnClick("1",innerModel,adapterPosition)
            }
        }
    }



}