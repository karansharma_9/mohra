package com.napworks.mohra.adapterPackage.News

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions

import com.napworks.mohra.R
import com.napworks.mohra.activitiesPackage.NewsActivities.SingleNewsActivity
import com.napworks.mohra.modelPackage.MusicModel.RecentlyPlayedModel
import com.napworks.mohra.modelPackage.News.NewsDataModel
import com.napworks.mohra.modelPackage.News.TagListModel


class RelatedNewsRecyclerAdapter(val activity: Activity, val list: ArrayList<NewsDataModel>,
) : RecyclerView.Adapter<RelatedNewsRecyclerAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MyViewHolder {
        val view: View = LayoutInflater.from(activity).inflate(R.layout.layout_related_news, viewGroup, false)
        return MyViewHolder(view, activity);
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bindData(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class MyViewHolder(itemView: View, sentActivity: Activity, ) : RecyclerView.ViewHolder(itemView), View.OnClickListener
    {
        val TAG: String = javaClass.simpleName
        val activity = sentActivity
        var newsID = 0
        var layoutRelatedNews: LinearLayout = itemView.findViewById(R.id.layoutRelatedNews)
        var imageRelatedNews: ImageView = itemView.findViewById(R.id.imageRelatedNews)
        var titleRelatedNews: TextView = itemView.findViewById(R.id.titleRelatedNews)
        var relatedNewsChannelName: TextView = itemView.findViewById(R.id.relatedNewsChannelName)
        var innerModel = NewsDataModel()
        fun bindData(newsDataModel: NewsDataModel) {
            innerModel = newsDataModel
            var requestOptions = RequestOptions()
            requestOptions = requestOptions.transforms(CenterCrop(), RoundedCorners(16))
            Glide.with(activity)
                .load(innerModel.image)
                .apply(requestOptions)
                .into(imageRelatedNews)
            titleRelatedNews.text = innerModel.title
            relatedNewsChannelName.text = innerModel.newsChannel
            newsID = innerModel.newsId!!
            layoutRelatedNews.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            if (v?.id == R.id.layoutRelatedNews) {
                val intent = Intent(activity, SingleNewsActivity::class.java)
                intent.putExtra("newsID",newsID)
                activity.startActivity(intent)
            }
        }
    }



}