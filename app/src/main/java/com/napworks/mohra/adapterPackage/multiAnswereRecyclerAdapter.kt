package com.napworks.mohra.adapterPackage

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.napworks.gamaloto.interfacePackage.OnMultiAnswerAdapterClick
import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.R
import com.napworks.mohra.modelPackage.GetPersonalityAnswerInnerModel
import com.napworks.mohra.utilPackage.MyConstants


class multiAnswereRecyclerAdapter(val activity: Activity, val list: ArrayList<GetPersonalityAnswerInnerModel>, val screenWidth: Int, val onMultiAnswerAdapterClick: OnMultiAnswerAdapterClick, val typeofquestion: String) : RecyclerView.Adapter<multiAnswereRecyclerAdapter.MyViewHolder>() {
    private val TAG = javaClass.simpleName
    var type = typeofquestion
    var sentList = list
    var previousSelectedAnswer : String = ""

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MyViewHolder {
        val view: View = LayoutInflater.from(activity).inflate(R.layout.personality_test_item_two_item, viewGroup, false)
        return MyViewHolder(view, activity, onMultiAnswerAdapterClick);
    }

    override fun onBindViewHolder(viewHolder: MyViewHolder, i: Int) {
        viewHolder.bindData(list[i], screenWidth, typeofquestion)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun updatePreviousSelectedAnswer(previousSelectedAnswer1: String) {
       previousSelectedAnswer = previousSelectedAnswer1
    }

    class MyViewHolder(
            itemView: View,
            sentActivity: Activity,
            onMultiAnswerAdapterClick: OnMultiAnswerAdapterClick
    ) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val TAG: String = javaClass.simpleName
        val activity = sentActivity
        val interfaceMultiAnswer: OnMultiAnswerAdapterClick = onMultiAnswerAdapterClick
        var mainLayoutItem: RelativeLayout = itemView.findViewById(R.id.mainLayoutItem)
        var imageMainItem: ImageView = itemView.findViewById(R.id.imageMainItem)
        var selectbutton: ImageView = itemView.findViewById(R.id.selectbutton)
        var answerMainItem: TextView = itemView.findViewById(R.id.answerMainItem)

        var innerModel = GetPersonalityAnswerInnerModel()


        fun bindData(answerModelData: GetPersonalityAnswerInnerModel, screenWidth: Int, typeofquestion: String) {
            innerModel = answerModelData
            var type = typeofquestion
            val layoutParams = LinearLayout.LayoutParams(mainLayoutItem.layoutParams)

            if (type == MyConstants.PREFERENCES) {
                layoutParams.width = screenWidth + 1 - 1
                layoutParams.height = screenWidth + 1 - 1
                mainLayoutItem.layoutParams = layoutParams


            } else {
                val screenHeight=screenWidth*1.5
                layoutParams.width = screenWidth + 1 - 1
                layoutParams.height = screenHeight.toInt()
                mainLayoutItem.layoutParams = layoutParams
            }

            Glide.with(activity).load(innerModel.image).apply(RequestOptions()
                .placeholder(R.drawable.logosplashscreen))
                .into(imageMainItem)

            if (innerModel.isAnswerSelected) {
                CommonMethods.showLog(TAG, "Answer Selected")
                selectbutton!!.setImageResource(R.drawable.selectedanswere);
                mainLayoutItem.background = ContextCompat.getDrawable(activity, R.drawable.buttonborder);
            } else {
                selectbutton!!.setImageResource(R.drawable.unselectedanswere);
                mainLayoutItem.background = ContextCompat.getDrawable(activity, R.drawable.edittextborder)

            }

            answerMainItem.text = innerModel.answer
            mainLayoutItem.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            if (v?.id == R.id.mainLayoutItem) {
                interfaceMultiAnswer.onMultiAnswerAdapterClickResponse("1", innerModel, adapterPosition)
            }
        }
    }
}