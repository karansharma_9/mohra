package com.napworks.mohra.adapterPackage.MusicAdapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.napworks.mohra.R
import com.napworks.mohra.activitiesPackage.MusicActivities.PlaylistDetails
import com.napworks.mohra.interfacePackage.music.AddSongsAdapterClick
import com.napworks.mohra.interfacePackage.music.LibraryTypeSelectAdapterOnClick
import com.napworks.mohra.modelPackage.MusicModel.MusicInnerModel
import com.napworks.mohra.modelPackage.MusicModel.PlaylistFromMyLibraryModel
import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.utilPackage.MyConstants


class AddSongsRecyclerAdapter(val activity: Activity,
                              val list: ArrayList<MusicInnerModel>,
                              val addSongsAdapterClick: AddSongsAdapterClick) : RecyclerView.Adapter<AddSongsRecyclerAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MyViewHolder {
        val view: View = LayoutInflater.from(activity).inflate(R.layout.layout_add_songs, viewGroup, false)
        return MyViewHolder(view, activity,addSongsAdapterClick);
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bindData(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class MyViewHolder(
        itemView: View,
        sentActivity: Activity,
        addSongsAdapterClick: AddSongsAdapterClick) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val TAG: String = javaClass.simpleName
        val activity = sentActivity
        var type = ""
        var innerModel = MusicInnerModel()
        var layoutCategory: LinearLayout = itemView.findViewById(R.id.layoutCategory)
        var titlePlaylistMyLibrary : TextView = itemView.findViewById(R.id.titlePlaylistMyLibrary)
        var selectedImage : ImageView = itemView.findViewById(R.id.selectedImage)
        var totalSongs : TextView = itemView.findViewById(R.id.totalSongs)
        var image : ImageView = itemView.findViewById(R.id.image)
        val addSongsAdapterClickType: AddSongsAdapterClick = addSongsAdapterClick


        fun bindData(playlistFromMyLibraryModel: MusicInnerModel) {
            innerModel = playlistFromMyLibraryModel
            type = innerModel.type.toString()
            CommonMethods.showLog(TAG,"TYPE : "+type)
            selectedImage.setColorFilter(activity.resources.getColor(R.color.colorPrimary))
            titlePlaylistMyLibrary.text = innerModel.innerTitle
            var requestOptions = RequestOptions()
            requestOptions = requestOptions.transforms(CenterCrop(), RoundedCorners(16))
            Glide.with(activity)
                .load(innerModel.image)
                .apply(requestOptions)
                .into(image)

            if(innerModel.isSelected!!){
                selectedImage.visibility = View.VISIBLE
            }
            else{
                selectedImage.visibility = View.GONE
            }


            totalSongs.text = innerModel.artistName

            layoutCategory.setOnClickListener(this)

        }

        override fun onClick(v: View?) {
            if(v?.id==R.id.layoutCategory){
                addSongsAdapterClickType.onAddSongsAdapterOnClick("1",innerModel,adapterPosition)
            }
        }
    }
}