package com.napworks.mohra.adapterPackage.News

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.napworks.mohra.R
import com.napworks.mohra.activitiesPackage.NewsActivities.SingleNewsActivity
import com.napworks.mohra.modelPackage.News.NewsDataModel
import com.napworks.mohra.modelPackage.News.SearchNewsInnerModel
import com.napworks.mohra.utilPackage.CommonMethods
import java.util.*
import kotlin.collections.ArrayList


class SearchNewsRecyclerAdapter(
    val activity: Activity,
    val list: ArrayList<SearchNewsInnerModel>,
) : RecyclerView.Adapter<SearchNewsRecyclerAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MyViewHolder {
        val view: View = LayoutInflater.from(activity).inflate(R.layout.layout_news_recyclerview, viewGroup, false)
        return MyViewHolder(view, activity);
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bindData(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class MyViewHolder(itemView: View, sentActivity: Activity) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val TAG: String = javaClass.simpleName
        val activity = sentActivity
        var newId = 0
        var innerModel = SearchNewsInnerModel()
        var titleVerticalNews : TextView = itemView.findViewById(R.id.titleVerticalNews)
        var newsChannelNameVertical : TextView = itemView.findViewById(R.id.newsChannelNameVertical)
        var imageViewVerticalNews : ImageView = itemView.findViewById(R.id.imageViewVerticalNews)
        var layoutCategoryVertical : LinearLayout = itemView.findViewById(R.id.layoutCategoryVertical)
        var typeCategory : TextView = itemView.findViewById(R.id.typeCategory)
        var timeForFavNews : TextView = itemView.findViewById(R.id.timeForFavNews)

        fun bindData(newsDataModel: SearchNewsInnerModel) {
            innerModel = newsDataModel
            titleVerticalNews.text = innerModel.title
            newsChannelNameVertical.text = innerModel.newsChannel
            var requestOptions = RequestOptions()
            requestOptions = requestOptions.transforms(CenterCrop(), RoundedCorners(16))
            Glide.with(activity)
                .load(innerModel.image)
                .apply(requestOptions)
                .into(imageViewVerticalNews)
            layoutCategoryVertical.setOnClickListener(this)

            val date = Date()
            val timeMilli: Long = date.getTime()
            var time = innerModel.time!!.toLong()
            var show = (timeMilli / 1000) - time
            typeCategory.text = innerModel.categoryName
            timeForFavNews.text = CommonMethods.timeAgoDisplay(show)
            newId = innerModel.newsId!!.toInt()
//            titlePlaylistMyLibrary.text = innerModel.name
        }

        override fun onClick(v: View?) {
            if (v?.id == R.id.layoutCategoryVertical) {
                val intent = Intent(activity, SingleNewsActivity::class.java)
                intent.putExtra("newsID",newId)
                activity.startActivity(intent)
            }
        }
    }
}