package com.napworks.mohra.adapterPackage

import android.app.Activity
import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.R
import com.napworks.mohra.dialogPackage.LogOutMessageDialog
import com.napworks.mohra.modelPackage.DrawerListModel


class DrawerRecyclerViewAdapter(
    val activity: Activity,
    val sharedPreferences: SharedPreferences,
    val list: ArrayList<DrawerListModel>,
    val logOutMessageDialog: LogOutMessageDialog
) : RecyclerView.Adapter<DrawerRecyclerViewAdapter.MyViewHolder>() {
    //    private val confirmationDialog: CommonConfirmationDialog
    var UPDATE_PROFILE_REQUEST_CODE = 13
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(activity).inflate(R.layout.drawer_list, viewGroup, false)
        return MyViewHolder(view, activity, sharedPreferences,logOutMessageDialog);
    }

    override fun onBindViewHolder(viewHolder: MyViewHolder, i: Int) {
        viewHolder.bindData(list.get(i))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class MyViewHolder(
        itemView: View,
        sentActivity: Activity,
        sentLogOutMessageDialog: SharedPreferences,
        sentSharedPreferences: LogOutMessageDialog
    ) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener
//        , ConfirmationInterface
    {
        val TAG: String = javaClass.simpleName

        val activity = sentActivity
        val logOutMessageDialog = sentLogOutMessageDialog
        val sharedPreferences = sentSharedPreferences
        var textView: TextView = itemView.findViewById(R.id.textView)
        var icon: ImageView = itemView.findViewById(R.id.icon)
        var outerLayout: LinearLayout = itemView.findViewById(R.id.outerLayout)

        var innerModel = DrawerListModel()

        fun bindData(drawerListModel: DrawerListModel) {
            innerModel = drawerListModel
//            val layoutParams = LinearLayout.LayoutParams(imageView.getLayoutParams())
////            layoutParams.width = screenWidth + 1 - 1
//            layoutParams.height = screenHeight + 1 - 1
//            imageView.setLayoutParams(layoutParams)
            outerLayout.setOnClickListener(this)

            CommonMethods.showLog(TAG, "Image : " + innerModel.image)
            textView.text = innerModel.text
            Glide.with(activity).load(innerModel.image).into(icon)
        }


        override fun onClick(v: View) {
            when (v.id) {
                R.id.outerLayout -> {
                    CommonMethods.showLog(TAG, "CLICKED ON $adapterPosition")
//                    (activity as HomeActivity).closeDrawer()
                    val text: String = innerModel.text
                    when {
//                        text.equals(activity.getString(R.string.home)) -> {
//                            val intent = Intent(activity, HomeActivity::class.java)
//                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
//                            activity.startActivity(intent)
////                        }
//                        text.equals(activity.getString(R.string.cart)) -> {
//                            val intent = Intent(activity, CartActivity::class.java)
//                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
//                            activity.startActivity(intent)
//                        }
//                        text.equals(activity.getString(R.string.account)) -> {
//                            val intent = Intent(activity, MyAccountActivity::class.java)
//                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
//                            activity.startActivity(intent)
//                        }
//                        text.equals(activity.getString(R.string.how_to_play)) -> {
//                            val intent = Intent(activity, HomeActivity::class.java)
//                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
//                            activity.startActivity(intent)
//                        }
//                        text.equals(activity.getString(R.string.orders)) -> {
//                            val intent = Intent(activity, HomeActivity::class.java)
//                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
//                            activity.startActivity(intent)
//                        }
//                        text.equals(activity.getString(R.string.contactUs)) -> {
//                            val intent = Intent(activity, HomeActivity::class.java)
//                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
//                            activity.startActivity(intent)
//                        } text.equals(activity.getString(R.string.aboutUs)) -> {
//                            val intent = Intent(activity, HomeActivity::class.java)
//                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
//                            activity.startActivity(intent)
//                        }
//                        text.equals(activity.getString(R.string.log_out)) -> {
//
//
//                            //                        confirmationDialog.showDialog(
//                            //                            MyConstants.LOGOUT, activity.getString(R.string.app_name),
//                            //                            activity.getString(R.string.logoutText),
//                            //                            this
//                            //                        )
//                        }
                    }
                }
            }
        }

//        fun confirmationResponse(id: String, confirmationValue: String) {
//            if (id.equals(MyConstants.LOGOUT, ignoreCase = true)) {
//                if (confirmationValue.equals(MyConstants.YES, ignoreCase = true)) {
//                    CommonMethods.logout(activity)
//                }
//            }
//        }
    }
}