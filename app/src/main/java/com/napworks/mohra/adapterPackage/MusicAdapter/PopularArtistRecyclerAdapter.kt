package com.napworks.mohra.adapterPackage.MusicAdapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions

import com.napworks.mohra.R
import com.napworks.mohra.modelPackage.MusicModel.MusicInnerModel
import com.napworks.mohra.modelPackage.MusicModel.RecentlyPlayedModel


class PopularArtistRecyclerAdapter(val activity: Activity, val list: ArrayList<MusicInnerModel>,
) : RecyclerView.Adapter<PopularArtistRecyclerAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MyViewHolder {
        val view: View = LayoutInflater.from(activity).inflate(R.layout.layout_popular_artist_tile, viewGroup, false)
        return MyViewHolder(view, activity);
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bindData(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class MyViewHolder(itemView: View, sentActivity: Activity ) : RecyclerView.ViewHolder(itemView), View.OnClickListener
    {
        val TAG: String = javaClass.simpleName
        val activity = sentActivity
        var layoutCategory: LinearLayout = itemView.findViewById(R.id.layoutCategory)
        var artistName: TextView = itemView.findViewById(R.id.artistName)
        var image: ImageView = itemView.findViewById(R.id.image)
        var innerModel = MusicInnerModel()
        fun bindData(recentlyPlayedModel: MusicInnerModel) {
            innerModel = recentlyPlayedModel
            artistName.setText(innerModel.artistName)
            var requestOptions = RequestOptions()
            requestOptions = requestOptions.transforms(CenterCrop(), RoundedCorners(16))
            Glide.with(activity)
                .load(innerModel.image)
                .apply(requestOptions)
                .into(image)

        }

        override fun onClick(v: View) {

        }
    }



}