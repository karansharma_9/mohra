package com.napworks.mohra.adapterPackage

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.napworks.mohra.R
import com.napworks.mohra.modelPackage.CategoryInnerModel


class CategoryContentRecyclerAdapter(
    val activity: Activity,
    val list: ArrayList<CategoryInnerModel>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int):  RecyclerView.ViewHolder {

        val inflater: LayoutInflater = LayoutInflater.from(viewGroup.context)
        val view : View
        if(viewType == 1)
        {
            view = inflater.inflate(R.layout.category_content_list, viewGroup, false)
            return  MyViewHolder(view, activity)
        }
        else
        {
            view = inflater.inflate(R.layout.challenge_tile, viewGroup, false)
            return  MyViewHolderTwo(view, activity)
        }
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(getItemViewType(position) == 1 ){
            (holder as MyViewHolder).bindData(list[position])
        }
        else
        {
            (holder as MyViewHolderTwo).bindData(list[position])
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun getItemViewType(position: Int): Int {
        return if(list[position].type == "add")
        {
             0
        }
        else
        {
            1
        }
    }




    class MyViewHolder(itemView: View, sentActivity: Activity) : RecyclerView.ViewHolder(itemView)
    {
        val TAG: String = javaClass.simpleName
        val activity = sentActivity
        var innerModel = CategoryInnerModel()

        fun bindData(drawerListModel: CategoryInnerModel) {
            innerModel = drawerListModel

        }
    }

    class MyViewHolderTwo(itemView: View, sentActivity: Activity) : RecyclerView.ViewHolder(itemView)
    {
        val TAG: String = javaClass.simpleName
        val activity = sentActivity
        var innerModel = CategoryInnerModel()

        fun bindData(drawerListModel: CategoryInnerModel) {
            innerModel = drawerListModel
        }
    }



}