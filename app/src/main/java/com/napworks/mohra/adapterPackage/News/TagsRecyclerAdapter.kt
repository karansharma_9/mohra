package com.napworks.mohra.adapterPackage.News

import android.app.Activity
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.napworks.gamaloto.interfacePackage.OnHomeScreenCategoryAdapterClick
import com.napworks.mohra.R
import com.napworks.mohra.modelPackage.CategoryListModel
import com.napworks.mohra.modelPackage.News.TagListModel


class TagsRecyclerAdapter(val activity: Activity, val list: ArrayList<TagListModel>) : RecyclerView.Adapter<TagsRecyclerAdapter.MyViewHolder>() {
    //    private val confirmationDialog: CommonConfirmationDialog
    var UPDATE_PROFILE_REQUEST_CODE = 13
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MyViewHolder {
        val view: View = LayoutInflater.from(activity).inflate(R.layout.layout_tag_list, viewGroup, false)
        return MyViewHolder(view, activity);
    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bindData(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }



    class MyViewHolder(
        itemView: View,
        sentActivity: Activity,
        ) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        val activity : Activity = sentActivity
        val TAG: String = javaClass.simpleName
        var layoutTag: LinearLayout = itemView.findViewById(R.id.layoutTag)
        var titleTag: TextView = itemView.findViewById(R.id.titleTag)
        var innerModel = TagListModel()
        fun bindData(categoryListModel: TagListModel) {
            innerModel = categoryListModel
            titleTag.text = innerModel.text
//            if(innerModel.isSelected)
//            {
//                layoutTag.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.button_welcome_screen) );
//                titleTag.setTextColor(Color.parseColor("#FFFFFFFF"));
//            }
//            else
//            {
//                layoutTag.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.white_button) );
//                titleTag.setTextColor(Color.parseColor("#FF000000"));
//            }
            layoutTag.setOnClickListener(this)

        }

        override fun onClick(v: View?) {
            if (v?.id == R.id.layoutCategory) {
            }
            }
        }
    }
