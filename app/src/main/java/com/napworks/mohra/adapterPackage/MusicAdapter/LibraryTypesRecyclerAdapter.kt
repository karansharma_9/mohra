package com.napworks.mohra.adapterPackage.MusicAdapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.napworks.mohra.R
import com.napworks.mohra.interfacePackage.music.LibraryTypeSelectAdapterOnClick
import com.napworks.mohra.modelPackage.MusicModel.MusicModel


class LibraryTypesRecyclerAdapter(val activity: Activity, val list: ArrayList<MusicModel>,
                                  val screenWidth : Int,
                                  val libraryTypeSelectAdapterOnClick : LibraryTypeSelectAdapterOnClick) : RecyclerView.Adapter<LibraryTypesRecyclerAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MyViewHolder {
        val view: View = LayoutInflater.from(activity).inflate(R.layout.layout_library_type_tile, viewGroup, false)
        return MyViewHolder(view, activity,libraryTypeSelectAdapterOnClick);
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bindData(list[position],screenWidth)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class MyViewHolder(
        itemView: View,
        sentActivity: Activity,
        libraryTypeSelectAdapterOnClick: LibraryTypeSelectAdapterOnClick, ) : RecyclerView.ViewHolder(itemView), View.OnClickListener
    {
        val TAG: String = javaClass.simpleName
        val activity = sentActivity
        var innerModel = MusicModel()
        var layoutLibraryType : LinearLayout = itemView.findViewById(R.id.layoutLibraryType)
        var libraryTypeTitle : TextView = itemView.findViewById(R.id.libraryTypeTitle)
        var viewInLibraryType : View = itemView.findViewById(R.id.viewInLibraryType)
        val interfaceOnClickLibraryType: LibraryTypeSelectAdapterOnClick = libraryTypeSelectAdapterOnClick
        fun bindData(libraryTypesModel: MusicModel, screenWidth: Int) {
            val layoutParams = this.layoutLibraryType.layoutParams
            layoutParams.width = screenWidth + 1 - 1
            layoutParams.height = 80
            layoutLibraryType.layoutParams = layoutParams

            innerModel = libraryTypesModel
            libraryTypeTitle.text = innerModel.title
            if(innerModel.isSelected!!)
            {
                viewInLibraryType.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.button_welcome_screen) );
                libraryTypeTitle.setTextColor(ContextCompat.getColor(activity, R.color.colorPrimary));
            }
            else
            {
                viewInLibraryType.setBackgroundColor(ContextCompat.getColor(activity, R.color.backLightGrey))
                libraryTypeTitle.setTextColor(ContextCompat.getColor(activity, R.color.black));
            }
            layoutLibraryType.setOnClickListener(this)

        }

        override fun onClick(v: View) {
            if (v?.id == R.id.layoutLibraryType) {
                interfaceOnClickLibraryType.onLibraryTypeSelectAdapterOnClick("1",innerModel,adapterPosition)
            }
        }
    }



}