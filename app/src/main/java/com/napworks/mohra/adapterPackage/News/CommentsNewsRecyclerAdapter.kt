package com.napworks.mohra.adapterPackage.News

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions

import com.napworks.mohra.R
import com.napworks.mohra.activitiesPackage.NewsActivities.SingleNewsActivity
import com.napworks.mohra.modelPackage.MusicModel.RecentlyPlayedModel
import com.napworks.mohra.modelPackage.News.NewsDataModel
import com.napworks.mohra.modelPackage.News.TagListModel
import com.napworks.mohra.modelPackage.News.getCommentsNewsInnerModel
import com.napworks.mohra.utilPackage.CommonMethods
import java.util.*
import kotlin.collections.ArrayList


class CommentsNewsRecyclerAdapter(val activity: Activity, val list: ArrayList<getCommentsNewsInnerModel>,
) : RecyclerView.Adapter<CommentsNewsRecyclerAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MyViewHolder {
        val view: View = LayoutInflater.from(activity).inflate(R.layout.layout_comment_news, viewGroup, false)
        return MyViewHolder(view, activity);
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bindData(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class MyViewHolder(itemView: View, sentActivity: Activity, ) : RecyclerView.ViewHolder(itemView)
    {
        val TAG: String = javaClass.simpleName
        val activity = sentActivity
        var innerModel = getCommentsNewsInnerModel()

        var nameOfCommentor: TextView = itemView.findViewById(R.id.nameOfCommentor)
        var comment: TextView = itemView.findViewById(R.id.comment)
        var commentTime: TextView = itemView.findViewById(R.id.commentTime)
        fun bindData(commentData: getCommentsNewsInnerModel) {
            innerModel = commentData
            nameOfCommentor.text = innerModel.commentBy
            comment.text = innerModel.comment
            val date = Date()
            val timeMilli: Long = date.getTime()
            var time = innerModel.commentTime!!.toLong()
            var show = (timeMilli / 1000) - time

            commentTime.text = CommonMethods.timeAgoDisplay(show)
        }
    }
}