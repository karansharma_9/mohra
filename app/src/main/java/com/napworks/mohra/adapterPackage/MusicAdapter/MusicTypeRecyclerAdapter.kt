package com.napworks.mohra.adapterPackage.MusicAdapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.napworks.mohra.interfacePackage.music.MusicTypeSelectAdapterOnClick

import com.napworks.mohra.R
import com.napworks.mohra.modelPackage.MusicModel.MusicModel


class MusicTypeRecyclerAdapter(val activity: Activity, val list: ArrayList<MusicModel>,val screenWidth:Int, val musicTypeSelectAdapterOnClick : MusicTypeSelectAdapterOnClick) : RecyclerView.Adapter<MusicTypeRecyclerAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MyViewHolder {
        val view: View = LayoutInflater.from(activity).inflate(R.layout.layout_music_type_tile, viewGroup, false)
        return MyViewHolder(view, activity,musicTypeSelectAdapterOnClick);
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bindData(list[position],screenWidth)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class MyViewHolder(
        itemView: View,
        sentActivity: Activity,
        musicTypeSelectAdapterOnClick: MusicTypeSelectAdapterOnClick,
    ) : RecyclerView.ViewHolder(itemView), View.OnClickListener
    {
        val TAG: String = javaClass.simpleName
        val activity = sentActivity
        var layoutMusicType: LinearLayout = itemView.findViewById(R.id.layoutMusicType)
        var musicTypeTitle: TextView = itemView.findViewById(R.id.musicTypeTitle)
        var viewInMusicType: View = itemView.findViewById(R.id.viewInMusicType)
        var innerModel = MusicModel()
        val interfaceOnClickMusicType: MusicTypeSelectAdapterOnClick = musicTypeSelectAdapterOnClick



        fun bindData(recentlyPlayedModel: MusicModel, screenWidth: Int) {

            val layoutParams = this.layoutMusicType.layoutParams
            layoutParams.width = screenWidth + 1 - 1
            layoutParams.height = 80
            layoutMusicType.layoutParams = layoutParams


            innerModel = recentlyPlayedModel
            musicTypeTitle.text = innerModel.title
            if(innerModel.isSelected!!)
            {
                viewInMusicType.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.button_welcome_screen) );
                musicTypeTitle.setTextColor(ContextCompat.getColor(activity, R.color.colorPrimary));
            }
            else
            {
                viewInMusicType.setBackgroundColor(ContextCompat.getColor(activity, R.color.backLightGrey))
                musicTypeTitle.setTextColor(ContextCompat.getColor(activity, R.color.black));
            }
            layoutMusicType.setOnClickListener(this)

        }

        override fun onClick(v: View) {
            if (v.id == R.id.layoutMusicType) {
                interfaceOnClickMusicType.onMusicTypeSelectAdapterOnClick("1",innerModel,adapterPosition)
            }
        }
    }



}