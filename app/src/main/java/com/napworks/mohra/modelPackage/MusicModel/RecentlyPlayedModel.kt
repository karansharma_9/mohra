package com.napworks.mohra.modelPackage.MusicModel

public class RecentlyPlayedModel(
    var  songName: String,
    var artistName : String,
    var isSelected : Boolean,
    var imageUrl : String,
    ) {
    constructor() : this( "", "",false,"")
}