package com.napworks.mohra.modelPackage.News

class SearchNewsInnerModel  {
    var newsId: String? = null
    var newsChannel: String? = null
    var time: Long? = null
    var image: String? = null
    var title: String? = null
    var categoryName: String? = null
    var isLiked: Int? = null
    var isFavourite: Int? = null
}