package com.napworks.mohra.modelPackage;

/**
 * Created by PankajKapoor on 08-07-2017.
 */

public class FileChoosedModel {
    private String path;
    private String type;
    private String fileName;

    public FileChoosedModel(String path, String type) {
        this.path = path;
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
