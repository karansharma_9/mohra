package com.napworks.mohra.modelPackage

import java.io.Serializable

class GetIntroDataInnerModel (
    var title: String,
    var description: String,
    var image: String)
    : Serializable {
    constructor() : this("", "","")
}