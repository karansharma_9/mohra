package com.napworks.mohra.modelPackage

class SendOtpModel {
    var status: String? = null
    var message: String? = null
    var otpId: String? = null
    var otp: Int? = null
}