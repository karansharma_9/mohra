package com.napworks.mohra.modelPackage.News

class GetCommentsNewsModel  {
    var status: String? = null
    var message: String? = null
    var commentList : ArrayList<getCommentsNewsInnerModel> = ArrayList()
}