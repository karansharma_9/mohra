package com.napworks.mohra.modelPackage

import java.io.Serializable

class CountryListModel  (var countryId:String,
                         var countryName: String,
                         var phoneCode: String,
                         var shortCode: String,
                         var longCode: String):Serializable {
//    var countryId: String? = null
//    var countryName: String? = null
//    var phoneCode: String? = null
//    var shortCode: String? = null
//    var longCode: String? = null

    constructor():this("","","","","")

}
