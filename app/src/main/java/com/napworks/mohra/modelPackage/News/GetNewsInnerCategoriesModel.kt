package com.napworks.mohra.modelPackage.News

import java.io.Serializable

class GetNewsInnerCategoriesModel  (
    var categoryId:String,
    var category: String,
    var isSelected:Boolean)
    : Serializable {
    constructor() : this("", "",false)
}