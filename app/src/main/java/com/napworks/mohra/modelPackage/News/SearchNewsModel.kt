package com.napworks.mohra.modelPackage.News

class SearchNewsModel  {
    var status: String? = null
    var message: String? = null
    var newsData: ArrayList<SearchNewsInnerModel> = ArrayList()
}