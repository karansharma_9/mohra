package com.napworks.mohra.modelPackage

class PersonalityFinalScreenDataModel {
    var status: String? = null
    var message: String? = null
    var userPersonality: userPersonalityModel = userPersonalityModel()
    var darkImages: ArrayList<DarkImagesModel> = ArrayList()

}