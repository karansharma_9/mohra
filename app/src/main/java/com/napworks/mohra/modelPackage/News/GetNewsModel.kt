package com.napworks.mohra.modelPackage.News

class GetNewsModel  {
    var status: String? = null
    var message: String? = null
    var newsData: ArrayList<NewsDataModel> = ArrayList()
    var highlightedNews: NewsDataModel = NewsDataModel()
}