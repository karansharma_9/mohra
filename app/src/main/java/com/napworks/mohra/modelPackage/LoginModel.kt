package com.napworks.mohra.modelPackage

class LoginModel {
    var status: String? = null
    var message: String? = null
    var errorData: String? = null
    var userData: LoginInnerModel = LoginInnerModel()
}