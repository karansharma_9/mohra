package com.napworks.mohra.modelPackage

class GetPersonalityDataInnerModel {
    var questionId: String? = null
    var question: String? = null
    var questionType: String? = null
    var answerList: ArrayList<GetPersonalityAnswerInnerModel> = ArrayList()
}