package com.napworks.mohra.modelPackage.News

class getCommentsNewsInnerModel  {
    var commentId: String? = null
    var comment: String? = null
    var commentTime : Long? = null
    var commentBy : String? = null
    var isCommentedByMe : Int? = null
}