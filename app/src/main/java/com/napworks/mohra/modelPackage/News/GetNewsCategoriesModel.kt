package com.napworks.mohra.modelPackage.News

class GetNewsCategoriesModel  {
    var status: String? = null
    var message: String? = null
    var data: ArrayList<GetNewsInnerCategoriesModel> = ArrayList()
}