package com.napworks.mohra.modelPackage.News

class SearchCategoryListModel  {
    var categoryId: String? = null
    var categoryName: String? = null
    var isSelected: Boolean? = null
}