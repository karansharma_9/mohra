package com.napworks.mohra.modelPackage.MusicModel

class MusicModel {

    var id : String? = null
    var title : String? = null
    var image : String? = null
    var isSelected : Boolean? = false
    var totalSongs : Int? = null
    var innerList:ArrayList<MusicInnerModel> = ArrayList()

}