package com.napworks.mohra.modelPackage

import java.io.Serializable

class QuestionAnswereModel (
    var title: String,
    var questionId:String,
    var question: String,
    var type : String,
    var progress : Int,
    var answerList:List<AnswerModel>,
    var description: String,
    var isQuestionAnswered:Boolean)
    : Serializable {
        constructor() : this("", "", "","",0, emptyList(),"",false)
}