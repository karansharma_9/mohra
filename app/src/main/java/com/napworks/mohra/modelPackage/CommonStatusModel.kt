package com.napworks.mohra.modelPackage

class CommonStatusModel {
    var status: String? = null
    var errorData: String? = null
    var message: String? = null
    var userId: String? = null
    var otpId: String? = null
    var otp: Int? = null
}