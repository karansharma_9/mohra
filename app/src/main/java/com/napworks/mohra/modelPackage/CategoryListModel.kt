package com.napworks.mohra.modelPackage

public class CategoryListModel(
    var  text: String,
    var isSelected : Boolean,
    var innerData: List<CategoryInnerModel>
    ) {
    constructor() : this( "", false,emptyList())
}