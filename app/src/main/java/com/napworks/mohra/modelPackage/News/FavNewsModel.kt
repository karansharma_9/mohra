package com.napworks.mohra.modelPackage.News

class FavNewsModel  {
    var status: String? = null
    var message: String? = null
    var favnewsData: ArrayList<FavNewsInnerModel> = ArrayList()
}