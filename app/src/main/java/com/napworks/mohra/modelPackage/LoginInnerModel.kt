package com.napworks.mohra.modelPackage

class LoginInnerModel {
    var auth: String? = null
    var session: String? = null
    var userId: String? = null
    var firstName: String? = null
    var email: String? = null
    var googleId: String? = null
    var facebookId: String? = null
    var lastLogin: Long? = null
    var createdTime: Long? = null
    var isPasswordEntered: Int? = null
    var birthDate: Long? = null
    var mobile: String? = null
    var familyName: String? = null
    var userName: String? = null
    var userType: String? = null
    var countryId: String? = null
    var loginType: String? = null
    var isPersonalityVerified: Int? = null
}