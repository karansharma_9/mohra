package com.napworks.mohra.modelPackage

class GetPersonalityDataModel {
    var status: String? = null
    var message: String? = null
    var questionsList: ArrayList<GetPersonalityDataInnerModel> = ArrayList()
}