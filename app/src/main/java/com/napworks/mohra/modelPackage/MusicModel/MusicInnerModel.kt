package com.napworks.mohra.modelPackage.MusicModel

class MusicInnerModel {
    var innerId : String? = null
    var innerTitle : String? = null
    var image : String? = null
    var url : String? = null
    var artistId : String? = null
    var albumId : String? = null
    var artistName : String? = null
    var albumName : String? = null
    var totalSongs : Int? = null
    var isFavourite : Int? = null
    var isSelected : Boolean? = false
    var createdTime : Double? = null
    var description : String? = null
    var type : String? = ""
}