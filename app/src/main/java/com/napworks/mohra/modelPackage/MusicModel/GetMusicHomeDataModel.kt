package com.napworks.mohra.modelPackage.MusicModel

import com.napworks.mohra.modelPackage.News.FavNewsInnerModel

class GetMusicHomeDataModel {
    var status: String? = null
    var message: String? = null
    var errorData: String? = null
    var musicList: ArrayList<MusicModel> = ArrayList()
    var popularArtistList: ArrayList<MusicInnerModel> = ArrayList()
    var playlistList: ArrayList<MusicModel> = ArrayList()
    var libraryDataList: ArrayList<MusicModel> = ArrayList()
}