package com.napworks.mohra.modelPackage

class GetIntroDataModel {
    var status: String? = null
    var message: String? = null
    var introData: ArrayList<GetIntroDataInnerModel> = ArrayList()
}