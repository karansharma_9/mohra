package com.napworks.mohra.modelPackage

import java.io.Serializable
class AnswerModel  (
        var answerId:String,
        var answer: String,
        var imageUrl:String,
        var image:Int,
        var isAnswerSelected:Boolean)
    : Serializable {
    constructor() : this("", "","",0,false)
}