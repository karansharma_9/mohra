package com.napworks.mohra.modelPackage.News

class LikeUnlikeModel  {
    var status: String? = null
    var message: String? = null
    var count: Int? = null
    var isLiked: Int? = null
}