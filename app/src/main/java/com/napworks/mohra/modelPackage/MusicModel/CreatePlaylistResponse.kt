package com.napworks.mohra.modelPackage.MusicModel

import com.napworks.mohra.modelPackage.News.FavNewsInnerModel

class CreatePlaylistResponse {
    var status: String? = null
    var message: String? = null
    var errorData: String? = null
    var data: MusicModel = MusicModel()

}