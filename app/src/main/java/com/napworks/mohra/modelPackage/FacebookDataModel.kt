package com.napworks.mohra.modelPackage

class FacebookDataModel {
    var userName: String? = null
    var firstName: String? = null
    var lastName: String? = null
    var email: String? = null
    var profileImage: String? = null
    var accountId: String? = null
}