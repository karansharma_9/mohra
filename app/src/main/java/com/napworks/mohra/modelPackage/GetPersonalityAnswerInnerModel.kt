package com.napworks.mohra.modelPackage

import java.io.Serializable

class GetPersonalityAnswerInnerModel(
    var answerId: String,
    var answer: String,
    var image: String,
    var innerType: String,
    var isAnswerSelected: Boolean,
) : Serializable {
    constructor() : this("", "", "", "", false)
}