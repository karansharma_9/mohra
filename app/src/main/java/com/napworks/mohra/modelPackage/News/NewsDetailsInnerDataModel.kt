package com.napworks.mohra.modelPackage.News

class NewsDetailsInnerDataModel  {
    var newsId: String? = null
    var newsChannel: String? = null
    var time: Long? = null
    var image: String? = null
    var title: String? = null
    var news: String? = null
    var commentCount: Int? = null
    var likesCount: Int? = null
    var tags: List<String>? = null
    var isLiked : Int ? = null
    var isFavourite : Int ? = null
}