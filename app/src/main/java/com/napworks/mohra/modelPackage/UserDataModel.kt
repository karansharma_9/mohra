package com.napworks.mohra.modelPackage

import java.io.Serializable

class UserDataModel (
        var first_name:String,
        var last_name: String,
        var password: String,
        var birth_date: Long,
        var email: String,
        var phonenumber: String,
        var user_name: String,
        var country_id: String,
        var userType: String, )
    : Serializable {
    constructor() : this("", "","",0,"","","","","")

}