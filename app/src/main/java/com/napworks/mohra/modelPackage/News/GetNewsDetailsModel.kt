package com.napworks.mohra.modelPackage.News

class GetNewsDetailsModel  {
    var status: String? = null
    var message: String? = null
    var data: NewsDetailsInnerDataModel = NewsDetailsInnerDataModel()
    var relatedNews: ArrayList<NewsDataModel> = ArrayList()
}