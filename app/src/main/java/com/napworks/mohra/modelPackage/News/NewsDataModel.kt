package com.napworks.mohra.modelPackage.News

class NewsDataModel {
    var newsId: Int? = null
    var newsChannel: String? = null
    var type: String? = null
    var time: Long? = null
    var image: String? = null
    var title: String? = null
    var isFavourite: Int? = null

}