package com.napworks.mohra.modelPackage.MusicModel

import com.napworks.mohra.modelPackage.News.FavNewsInnerModel

class GetMusicModel {
    var status: String? = null
    var message: String? = null
    var errorData: String? = null
    var musicList: ArrayList<MusicInnerModel> = ArrayList()
}