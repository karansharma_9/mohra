package com.napworks.mohra.modelPackage

import java.io.Serializable

class CategoryInnerModel (
    var title: String,
    var type: String,
    var imageUrl: String,
    )
    : Serializable {
    constructor() : this("", "","")
}