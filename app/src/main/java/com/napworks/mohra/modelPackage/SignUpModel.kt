package com.napworks.mohra.modelPackage

class SignUpModel {
    var status: String? = null
    var message: String? = null
    var userData: SignUpInnerModel = SignUpInnerModel()
}