package com.napworks.mohra.modelPackage.MusicModel

public class MusicFilesModel(
    var name : String,
    var path : String,
    var duration : String,
    var isSelected : Boolean,
    ) {
    constructor() : this( "", "","",false)
}