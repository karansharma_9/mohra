package com.napworks.mohra.modelPackage

class userPersonalityModel {
    var personalityTitle: String? = null
    var shortDescription: String? = null
    var longDescription: String? = null
    var image: String? = null
    var personalityUrl: String? = null
    var share: String? = null
    var link: String? = null
}