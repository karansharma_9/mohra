package com.napworks.mohra.dialogPackage;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.napworks.mohra.R;
import com.napworks.mohra.utilPackage.CommonMethods;
import com.napworks.mohra.utilPackage.MyConstants;

import java.io.File;



public class ImageChooserDialog implements View.OnClickListener {
    private final Dialog dialog;
    private Activity activity;
    private String path = "";
    private String TAG = getClass().getSimpleName();
    TextView gallery, camera, cancel;
    int galleryCode = 0, cameraCode = 0;
    private String type = MyConstants.PHOTOS;

    public ImageChooserDialog(Activity activity) {
        this.activity = activity;
        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_image_chooser);
        Window window = dialog.getWindow();
        if (window != null) {
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
            layoutParams.copyFrom(window.getAttributes());
            layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(layoutParams);
            gallery = dialog.findViewById(R.id.gallery);
            camera = dialog.findViewById(R.id.camera);
            cancel = dialog.findViewById(R.id.cancel);
            gallery.setOnClickListener(this);
            camera.setOnClickListener(this);
            cancel.setOnClickListener(this);
        }
    }

    public void showDialog(String type) {
        if (!dialog.isShowing()) {
            this.type = type;
            galleryCode = MyConstants.VIDEO_GALLERY_RESPONSE_CODE;
            cameraCode = MyConstants.CAMERA_VIDEO_REQUEST_CODE;
            camera.setVisibility(View.VISIBLE);
            dialog.show();
        }
    }

    public void showDialog() {
        if (!dialog.isShowing()) {
            type = MyConstants.PHOTOS;
            galleryCode = MyConstants.GALLERY_RESPONSE_CODE;
            cameraCode = MyConstants.CAMERA_IMAGE_REQUEST_CODE;
            camera.setVisibility(View.VISIBLE);
            dialog.show();
        }
    }

//    public void showDialog(int galleryCode, int cameraCode) {
//        if (!dialog.isShowing()) {
//            this.galleryCode = galleryCode;
//            this.cameraCode = cameraCode;
//            dialog.show();
//        }
//    }

    public void hideDialog() {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.camera:
                captureImage(cameraCode);
                break;
            case R.id.gallery:
                chooseImage();
                break;
            case R.id.cancel:
                hideDialog();
                break;
        }
    }

    public void chooseImage() {

        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) +
                ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            CommonMethods.Companion.showLog(TAG, "Not Granted ");
            ActivityCompat.requestPermissions(activity, new String[]{
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE}, MyConstants.STORAGE_PERMISSION_REQUEST);

        } else {

            if (type.equalsIgnoreCase(MyConstants.PHOTOS)) {
                CommonMethods.Companion.showLog(TAG, "Photos");
                hideDialog();
                Intent intent = new Intent();
                intent.setType("image/*");
//                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                activity.startActivityForResult(Intent.createChooser(intent, activity.getString(R.string.choose_image_from)),
                        galleryCode);
            } else {
                CommonMethods.Companion.showLog(TAG, "Videos");
                hideDialog();
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                activity.startActivityForResult(Intent.createChooser(intent, activity.getString(R.string.choose_video_from)),
                        galleryCode);
            }
        }
    }


    public void captureImage(int requestCode) {
        cameraCode = requestCode;
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) +
                ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) +
                ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            CommonMethods.Companion.showLog(TAG, "Not Granted ");
            ActivityCompat.requestPermissions(activity, new String[]{
                    Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, MyConstants.CAMERA_PERMISSION_REQUEST);

        } else {
            Intent intent;
            if (cameraCode == MyConstants.CAMERA_VIDEO_REQUEST_CODE) {
                intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                File direct = new File(Environment.getExternalStorageDirectory() + File.separator +
                        activity.getString(R.string.app_name));
                if (!direct.exists()) {
                    File wallpaperDirectory = new File(Environment.getExternalStorageDirectory() + File.separator +
                            activity.getString(R.string.app_name) + File.separator);
                    wallpaperDirectory.mkdirs();
                }
                String imagename = "myVideo-" + System.currentTimeMillis() + ".mp4";
                File file = new File(new File(Environment.getExternalStorageDirectory() + File.separator +
                        activity.getString(R.string.app_name) + File.separator), imagename);
                if (file.exists()) {
                    file.delete();
                }
                path = Environment.getExternalStorageDirectory() + File.separator +
                        activity.getString(R.string.app_name) + File.separator + imagename;
//        Uri imageUri = Uri.fromFile(file);
                Uri imageUri = FileProvider.getUriForFile(activity, activity.getString(R.string.file_provider_authority), file);
                CommonMethods.Companion.showLog(TAG, "Uri " + imageUri + " Path : " + path);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                activity.startActivityForResult(intent, requestCode);
            } else {
                intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File direct = new File(Environment.getExternalStorageDirectory() + File.separator +
                        activity.getString(R.string.app_name));
                if (!direct.exists()) {
                    File wallpaperDirectory = new File(Environment.getExternalStorageDirectory() + File.separator +
                            activity.getString(R.string.app_name) + File.separator);
                    wallpaperDirectory.mkdirs();
                }
                String imagename = "myImage-" + System.currentTimeMillis() + ".png";
                File file = new File(new File(Environment.getExternalStorageDirectory() + File.separator +
                        activity.getString(R.string.app_name) + File.separator), imagename);
                if (file.exists()) {
                    file.delete();
                }
                path = Environment.getExternalStorageDirectory() + File.separator +
                        activity.getString(R.string.app_name) + File.separator + imagename;
//        Uri imageUri = Uri.fromFile(file);
                Uri imageUri = FileProvider.getUriForFile(activity, activity.getString(R.string.file_provider_authority), file);
                CommonMethods.Companion.showLog(TAG, "Uri " + imageUri + " Path : " + path);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                activity.startActivityForResult(intent, requestCode);
            }


            hideDialog();
        }
    }

//    public void captureImage(int requestCode) {
//        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//            CommonMethods.Companion.showLog(TAG, "Not Granted ");
//            ActivityCompat.requestPermissions(activity, new String[]{
//                    Manifest.permission.CAMERA}, MyConstants.CAMERA_PERMISSION_REQUEST);
//        } else {
//            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//            File direct = new File(Environment.getExternalStorageDirectory() + File.separator +
//                    activity.getString(R.string.app_name));
//            if (!direct.exists()) {
//                File wallpaperDirectory = new File(Environment.getExternalStorageDirectory() + File.separator +
//                        activity.getString(R.string.app_name) + File.separator);
//                wallpaperDirectory.mkdirs();
//            }
//            String imagename = "myImage-" + System.currentTimeMillis() + ".png";
//            File file = new File(new File(Environment.getExternalStorageDirectory() + File.separator +
//                    activity.getString(R.string.app_name) + File.separator), imagename);
//            if (file.exists()) {
//                file.delete();
//            }
//            path = Environment.getExternalStorageDirectory() + File.separator +
//                    activity.getString(R.string.app_name) + File.separator + imagename;
//            CommonMethods.Companion.showLog(TAG, "Path " + path + "");
////        Uri imageUri = Uri.fromFile(file);
//            Uri imageUri = FileProvider.getUriForFile(activity, activity.getString(R.string.file_provider_authority), file);
//            CommonMethods.Companion.showLog(TAG, "Uri " + imageUri + "");
//            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
//            activity.startActivityForResult(intent, requestCode);
//            hideDialog();
//        }
//    }


    public String getPath() {
        CommonMethods.Companion.showLog(TAG, "Path : " + path);
        return path;
    }
}
