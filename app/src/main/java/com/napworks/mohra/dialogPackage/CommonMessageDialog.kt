package com.napworks.mohra.dialogPackage

import android.app.Activity
import android.app.Dialog
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.TextView
import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.R



class CommonMessageDialog(activity: Activity) : View.OnClickListener {
    private val dialog: Dialog

    val TAG: String = "CommonMessageDialog"

    var okText: TextView
    var message: TextView

    public fun showDialog(messageText: String?) {
        val isShow: Boolean = dialog.isShowing;
        if (!dialog.isShowing) {
            message.text = messageText
            dialog.show()
        }
    }

    public fun hideDialog() {
        if (dialog.isShowing) {
            dialog.dismiss()
        }
    }

    override fun onClick(v: View) {
        if (v.id == R.id.okText) {
            CommonMethods.showLog(TAG, "Ok Click")
            hideDialog()
        }
    }


    init {
        CommonMethods.showLog(TAG, "INIT WORKS")
        dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_common_message)
        message = dialog.findViewById(R.id.message)
        okText = dialog.findViewById(R.id.okText)
        val window = dialog.window
        if (window != null) {
            dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
            val layoutParams = WindowManager.LayoutParams()
            layoutParams.copyFrom(window.attributes)
            layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT
            layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT
            window.attributes = layoutParams
            okText.setOnClickListener(this)
            dialog.window!!.setGravity(Gravity.CENTER)
        }
    }
}