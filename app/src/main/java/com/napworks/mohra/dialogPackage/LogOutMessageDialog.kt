package com.napworks.mohra.dialogPackage

import android.app.Activity
import android.app.Dialog
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.TextView
import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.R


class LogOutMessageDialog(activity: Activity) : View.OnClickListener {
    private val dialog: Dialog

    var activityText=activity

    val TAG: String = "CommonMessageDialog"

    var cancelButton: TextView
    lateinit var logOutButton: TextView
    var message: TextView

    public fun showDialog(messageText: String?) {
        val isShow: Boolean = dialog.isShowing;
        if (!dialog.isShowing) {
            message.text = messageText
            dialog.show()
        }
    }

    public fun hideDialog() {
        if (dialog.isShowing) {
            dialog.dismiss()
        }
    }

    override fun onClick(v: View) {
        if (v.id == R.id.cancelButton) {
            CommonMethods.showLog(TAG, "Ok Click")
            hideDialog()
        }
        else if(v.id == R.id.logOutButton){
            hideDialog()
            CommonMethods.callLogout(activityText)
        }
    }


    init {
        CommonMethods.showLog(TAG, "INIT WORKS")
        dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_logout_message)
        message = dialog.findViewById(R.id.message)
        cancelButton = dialog.findViewById(R.id.cancelButton)
        logOutButton = dialog.findViewById(R.id.logOutButton)
        val window = dialog.window
        if (window != null) {
            dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
            val layoutParams = WindowManager.LayoutParams()
            layoutParams.copyFrom(window.attributes)
            layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT
            layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT
            window.attributes = layoutParams
            cancelButton.setOnClickListener(this)
            logOutButton.setOnClickListener(this)
            dialog.window!!.setGravity(Gravity.CENTER)
        }
    }
}