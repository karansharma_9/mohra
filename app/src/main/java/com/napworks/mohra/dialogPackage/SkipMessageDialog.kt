package com.napworks.mohra.dialogPackage

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.SharedPreferences
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.TextView
import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.R
import com.napworks.mohra.activitiesPackage.HomeScreenActivity
import com.napworks.mohra.modelPackage.CommonStatusModel
import com.napworks.mohra.utilPackage.ApiCalls
import com.napworks.mohra.utilPackage.MyConstants


class SkipMessageDialog(activity: Activity) : View.OnClickListener,
    com.napworks.mohra.interfacePackage.UpdateStatusPersonalityInterface {
    private val dialog: Dialog
    var activityText=activity
    var commonMessageDialog: CommonMessageDialog? = null
    var loadingDialog: LoadingDialog? = null


    val TAG: String = "SkipMessageDialog"

    var yesButton: TextView
    var noButton: TextView
    var message: TextView

    var sharedPreferences : SharedPreferences? = null

    public fun showDialog(
        messageText: String?,
        sharedPreferences: SharedPreferences?,
        loadingDialog: LoadingDialog?,
        commonMessageDialog: CommonMessageDialog?
    ) {
        val isShow: Boolean = dialog.isShowing;
        this.sharedPreferences =sharedPreferences
        this.loadingDialog =loadingDialog
        this.commonMessageDialog =commonMessageDialog
        if (!dialog.isShowing) {
            message.text = messageText
            dialog.show()
        }
    }

    public fun hideDialog() {
        if (dialog.isShowing) {
            dialog.dismiss()
        }
    }

    override fun onClick(v: View) {
        if (v.id == R.id.noButton) {
            CommonMethods.showLog(TAG, "Ok Click")
            hideDialog()
        }
        else if(v.id == R.id.yesButton)
        {
            loadingDialog!!.showDialog()
            hideDialog()
            updatePersonality()
        }
    }

    fun updatePersonality()
    {
//        loadingDialog!!.showDialog()
        ApiCalls.updateStatusPersonality(
            sharedPreferences!!,
            "2",
            this
        )
    }


    init {
        CommonMethods.showLog(TAG, "INIT WORKS")
        dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_skip_message)
        message = dialog.findViewById(R.id.message)
        yesButton = dialog.findViewById(R.id.yesButton)
        noButton = dialog.findViewById(R.id.noButton)
        val window = dialog.window
        if (window != null) {
            dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
            val layoutParams = WindowManager.LayoutParams()
            layoutParams.copyFrom(window.attributes)
            layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT
            layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT
            window.attributes = layoutParams
            yesButton.setOnClickListener(this)
            noButton.setOnClickListener(this)
            dialog.window!!.setGravity(Gravity.CENTER)
        }
    }

    override fun onUpdateStatusPersonalityInterfaceResponse(status: String?, commonStatusModel: CommonStatusModel?, )
    {
        when (status) {
            MyConstants.GO_TO_RESPONSE -> {
                CommonMethods.showLog(TAG,"send Otp  status  " + commonStatusModel!!.message)
                if (commonStatusModel != null) {
                    if (commonStatusModel.status != null) {
                        when {
                            commonStatusModel.status.equals("1") -> {
                                loadingDialog!!.hideDialog()
                                val editor = sharedPreferences?.edit()
                                editor?.putInt(MyConstants.VERIFY, 2)
                                editor?.apply()

                                val intent = Intent(activityText, HomeScreenActivity::class.java)
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                                activityText.startActivity(intent)
                                activityText.finishAffinity()//


                            }
                            commonStatusModel.status.equals("0") -> {
                                loadingDialog!!.hideDialog()
                                commonMessageDialog!!.showDialog(commonStatusModel.message)
                            }
                            else -> {
                                loadingDialog!!.hideDialog()
                            }
                        }
                    } else {
                        loadingDialog!!.hideDialog()
                    }
                } else {
                    loadingDialog!!.hideDialog()
                }
            }
            MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                loadingDialog!!.hideDialog()
            }
        }

    }
}