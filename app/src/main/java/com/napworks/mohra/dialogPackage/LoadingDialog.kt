package com.napworks.mohra.dialogPackage

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Gravity
import android.view.Window
import com.napworks.mohra.R

class LoadingDialog(var context: Context) {
    var dialog: Dialog

    /*
     *  Method to show dialog
     */
    fun showDialog() {
        if (!dialog.isShowing) {
            dialog.show()
        }
    }

    /*
     *  Method to hide dialog
     */
    fun hideDialog() {
        if (dialog.isShowing) {
            dialog.dismiss()
        }
    }

    init {
        dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_loading)
        val window = dialog.window
        window!!.setGravity(Gravity.CENTER)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
    }
}