package com.napworks.mohra.designPackage

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.napworks.gamaloto.interfacePackage.OnMultiAnswerAdapterClick
import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.R
import com.napworks.mohra.activitiesPackage.PersonalityActivities.PersonalityTestFinalActivity
import com.napworks.mohra.adapterPackage.multiAnswereRecyclerAdapter
import com.napworks.mohra.dialogPackage.CommonMessageDialog
import com.napworks.mohra.dialogPackage.LoadingDialog
import com.napworks.mohra.modelPackage.*
import com.napworks.mohra.utilPackage.ApiCalls
import com.napworks.mohra.utilPackage.MyConstants
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import kotlin.math.roundToInt


class CustomPersonalityTestContainer(activity: Activity, list: List<GetPersonalityDataInnerModel>, mainLayout: LinearLayout ,
loadingDialog: LoadingDialog, commonMessageDialog: CommonMessageDialog,sharedPreferences: SharedPreferences) : OnMultiAnswerAdapterClick,
    View.OnClickListener, com.napworks.mohra.interfacePackage.UploadPersonalityInterface {
    private val TAG = javaClass.simpleName

    private var sentActivity: Activity = activity
    private var sentSharedPreferences :SharedPreferences=sharedPreferences
    private var sentLoadingDialog :LoadingDialog = loadingDialog
    private var sentCommonMessageDialog :CommonMessageDialog =commonMessageDialog
    private var questionsList: List<GetPersonalityDataInnerModel> = list
    private var mainContainer: LinearLayout = mainLayout

    lateinit var recyclerView: RecyclerView

    private lateinit var multiAnswereAdapter: multiAnswereRecyclerAdapter


    var index = 0
    var previousSelectedAnswer : String = ""

    var titleOnTestScreen: TextView? = null
    var menuBackPress: ImageView? = null
    var questionOnTestScreen: TextView? = null
    var personalityNextButton: TextView? = null

    var progressBarOnTestScreen: ProgressBar? = null


    init {
        setContainerDetail()
    }

    private fun setContainerDetail() {
        if (questionsList.isNotEmpty()) {
            addDataInContainer(questionsList[index])
        } else {
            CommonMethods.showLog(TAG, "Empty List")
        }
    }


    private fun addDataInContainer(innerModel: GetPersonalityDataInnerModel) {
        if (innerModel != null) {

            mainContainer.removeAllViews();

            val inflater =
                sentActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val mainView: View =
                inflater.inflate(R.layout.personality_test_main_layout, mainContainer, false)
            mainContainer.addView(mainView)

            recyclerView = mainView.findViewById<View>(R.id.recyclerView) as RecyclerView
            titleOnTestScreen = mainView.findViewById<View>(R.id.titleOnTestScreen) as TextView
            questionOnTestScreen =
                mainView.findViewById<View>(R.id.questionOnTestScreen) as TextView
            personalityNextButton =
                mainView.findViewById<View>(R.id.personalityNextButton) as TextView
            progressBarOnTestScreen =
                mainView.findViewById<View>(R.id.progressBarOnTestScreen) as ProgressBar
            menuBackPress = mainView.findViewById<View>(R.id.menuBackPress) as ImageView
            var typeofQuestion = innerModel.questionType

            if(typeofQuestion == MyConstants.PERSONALITY)
            {
                titleOnTestScreen!!.text = "Your Personality"
            }
            else
            {
                titleOnTestScreen!!.text = "Your Preferences"
            }

            progressBarOnTestScreen!!.progress = index + 1
            progressBarOnTestScreen!!.max = questionsList.size
            personalityNextButton!!.visibility = View.GONE;

            menuBackPress!!.setOnClickListener(this)
            personalityNextButton!!.setOnClickListener(this)


            if (index == questionsList.size - 1) {
                personalityNextButton!!.visibility = View.VISIBLE;
            }

            if (innerModel.question != "") {

                questionOnTestScreen!!.visibility = View.VISIBLE;
                questionOnTestScreen!!.text = innerModel.question
            } else {
                CommonMethods.showLog(TAG,"no title available")
                questionOnTestScreen!!.visibility = View.GONE;
            }

            var answereModel = innerModel.answerList
            if (index != 0)
            {
                if(previousSelectedAnswer!= " " && previousSelectedAnswer != "common")
                {
                    var isPreviousTypeExist = false
                    answereModel =  ArrayList<GetPersonalityAnswerInnerModel>()
                    for (innerAnsModel in innerModel.answerList) {
                        if (previousSelectedAnswer == innerAnsModel.innerType) {
                            isPreviousTypeExist = true
                            answereModel.add(innerAnsModel)
                        }
                    }
                    if (!isPreviousTypeExist) {
                        answereModel=innerModel.answerList
                    }
                }
            }




            var screenWidth: Int = CommonMethods.getWidth(sentActivity) / 2
            screenWidth =
                (screenWidth - CommonMethods.getScreenDensity(sentActivity) * 30).roundToInt()
            multiAnswereAdapter = multiAnswereRecyclerAdapter(sentActivity,
                answereModel!! as ArrayList<GetPersonalityAnswerInnerModel>,
                screenWidth,
                this,
                typeofQuestion.toString())
            val gridLayoutManager = GridLayoutManager(sentActivity, 2)
            recyclerView.layoutManager = gridLayoutManager
            recyclerView.adapter = multiAnswereAdapter
        }
    }


    fun Delay() {
        Handler().postDelayed({
            if (index < questionsList.size-1) {
                index++
                setContainerDetail()
            }
        }, 300)
    }


    override fun onMultiAnswerAdapterClickResponse(
        status: String?,
        getPersonalityAnswerInnerModel: GetPersonalityAnswerInnerModel,
        position: Int
    ) {
        if (status.equals("1")) {
            CommonMethods.showLog(TAG,"INDEX : "+index)
            CommonMethods.showLog(TAG,"QUES LIST SIZE  : "+questionsList.size)
            previousSelectedAnswer = getPersonalityAnswerInnerModel.innerType
            for (answerModel in questionsList[index].answerList) {
                if (getPersonalityAnswerInnerModel.answerId==answerModel.answerId)
                {
                    answerModel.isAnswerSelected = true
                }
                else
                {
                    answerModel.isAnswerSelected=false
                }
            }
            multiAnswereAdapter.notifyDataSetChanged()

            if (index < questionsList.size - 1) {
                Delay()
            }

        }

    }

    private fun prepareJSON(): String {
        val jsonArray = JSONArray()
        if (questionsList.isNotEmpty()) {
            for (i in questionsList.indices) {
                val questionModel : GetPersonalityDataInnerModel = questionsList.get(i)
                for(j in questionModel.answerList.indices)
                {
                    val answerInnerModel: GetPersonalityAnswerInnerModel =questionModel.answerList.get(j)
                    if(answerInnerModel.isAnswerSelected) {
                        val jsonObject1 = JSONObject()
                        try {
                            jsonObject1.put("questionId", questionModel.questionId)
                            jsonObject1.put("answerId", answerInnerModel.answerId)
                            jsonArray.put(jsonObject1)
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                }
            }
        }
        return jsonArray.toString()
    }


    override fun onClick(v: View?) {
        if (v?.id == R.id.menuBackPress) {

            if(index != 0)
            {
                index--
                for (innerAnsModel in questionsList[index].answerList) {
                    if ( innerAnsModel.isAnswerSelected) {
                        previousSelectedAnswer = innerAnsModel.innerType
                    }
                }
                CommonMethods.showLog(TAG," numbr " + previousSelectedAnswer)
                setContainerDetail()
            }
            else
            {
                sentActivity.finish()
            }
        }
        else if(v?.id == R.id.personalityNextButton)
        {
            CommonMethods.showLog(TAG,"json  "+ prepareJSON())
            CommonMethods.showLog(TAG," auth  " +sentSharedPreferences.getString(MyConstants.AUTH, ""))
            CommonMethods.showLog(TAG," SESSION_TOKEN  " +sentSharedPreferences.getString(
                MyConstants.SESSION_TOKEN, ""))
            CommonMethods.showLog(TAG," USER_ID  " +sentSharedPreferences.getString(MyConstants.USER_ID, ""))
            sentLoadingDialog.showDialog()
            ApiCalls.uploadPersonalityData(
                sentSharedPreferences,
                prepareJSON(),
                this
            )
        }
    }

    override fun onUploadPersonalityInterfaceResponse(status: String?, commonStatusModel: CommonStatusModel?, ) {
        sentLoadingDialog!!.hideDialog()
        CommonMethods.showLog(TAG,"Upload Data  status  " + commonStatusModel!!.message)
        CommonMethods.showLog(TAG,"status  " + commonStatusModel!!.status)
        when (status) {
            MyConstants.GO_TO_RESPONSE -> {
                if (commonStatusModel != null) {
                    if (commonStatusModel.status != null) {
                        when {
                            commonStatusModel.status.equals("1") -> {
                                val editor : SharedPreferences.Editor =sentSharedPreferences.edit()
                                editor.putInt(MyConstants.VERIFY,1)
                                editor.apply()
                                val intent = Intent(sentActivity, PersonalityTestFinalActivity::class.java)
                                sentActivity.finishAffinity()
                sentActivity.startActivity(intent)


                            }
                            commonStatusModel.status.equals("0") -> {
                                sentCommonMessageDialog!!.showDialog(commonStatusModel.message)
                            }
                            commonStatusModel.status.equals("10") -> {
                                CommonMethods.callLogout(sentActivity)
                            }
                            else -> {
                                sentCommonMessageDialog!!.showDialog(sentActivity.getString(R.string.someErrorOccurredText))
                            }
                        }
                    } else {
                        sentCommonMessageDialog!!.showDialog(sentActivity.getString(R.string.someErrorOccurredText))
                    }
                } else {
                    sentCommonMessageDialog!!.showDialog(sentActivity.getString(R.string.someErrorOccurredText))
                }
            }
            MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                sentCommonMessageDialog!!.showDialog(sentActivity.getString(R.string.someErrorOccurredText))
            }
        }

    }
}

