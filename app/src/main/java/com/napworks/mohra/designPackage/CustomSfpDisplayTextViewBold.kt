package com.napworks.mohra.designPackage

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView

class CustomSfpDisplayTextViewBold(context: Context, attrs: AttributeSet?) :
    AppCompatTextView(context, attrs) {
    private fun init(context: Context) {
        val face = Typeface.createFromAsset(context.assets, "fonts/SFProDisplayBold.ttf")
        typeface = face
    }

    init {
        init(context)
    }
}