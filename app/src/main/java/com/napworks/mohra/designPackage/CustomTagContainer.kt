package com.napworks.mohra.designPackage
import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.napworks.mohra.R
import com.napworks.mohra.interfacePackage.CustomCategoryContainerInterface
import com.napworks.mohra.modelPackage.News.GetNewsInnerCategoriesModel

import com.napworks.mohra.utilPackage.CommonMethods




class CustomTagContainer(
    private val activity: Activity,  dataList: List<String>,
    private val mainContainer: LinearLayout, dataEntered: Boolean, private val width: Int,
) {
    private val b = false
    private val TAG = javaClass.simpleName
    private var checkWidth = 0
    private var linearLayout: LinearLayout? = null
    private val dataList:List<String>
    private var checked = false
    private val dataEntered: Boolean
    private val currentWidth = 0
    private var type = ""
    private fun showData() {
        addDataInContainer()
        for (i in dataList.indices) {
            addDataInSubContainer(i)
        }
    }

    private fun addDataInContainer() {
        val layoutInflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        if (layoutInflater != null) {
            val view: View = layoutInflater.inflate(R.layout.layout_empty_tile, mainContainer, false)
            linearLayout = view.findViewById(R.id.innerLinearLayout)
            linearLayout!!.setLayoutParams(LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT))
            //            linearLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
            mainContainer.addView(linearLayout)
        }
    }

    private fun addDataInSubContainer(position: Int) {
        val layoutInflaterINNER =
            activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        if (layoutInflaterINNER != null) {
//            if (dataList[position].isSelected == true) {
                val viewINNER: View =
                    layoutInflaterINNER.inflate(R.layout.layout_tag_list, linearLayout, false)
                val title = viewINNER.findViewById<TextView>(R.id.titleTag)
                val textLayout = viewINNER.findViewById<LinearLayout>(R.id.layoutTag)
                title.text = dataList[position]
                val textLayoutParams = textLayout.layoutParams as LinearLayout.LayoutParams
                val tagButtonLayoutParams = title.layoutParams as LinearLayout.LayoutParams
                title.measure(0, 0)
                checkWidth = checkWidth + title.measuredWidth + textLayoutParams.leftMargin +
                        textLayoutParams.rightMargin + tagButtonLayoutParams.leftMargin +
                        tagButtonLayoutParams.rightMargin
                checkUncheckValue(position, title, textLayout)
                if (checked) {
                    checked = false
                    linearLayout!!.addView(viewINNER)
                } else {
                    if (checkWidth < width) {
                        linearLayout!!.addView(viewINNER)
                    } else {
                        checked = true
                        checkWidth = 0
                        linearLayout!!.removeView(viewINNER)
                        addDataInContainer()
                        addDataInSubContainer(position)
                    }
                }
//                textLayout.setOnClickListener {
//
////                dataList[position].setSelected(!dataList[position].isSelected())
////                checkUncheckValue(position, title, textLayout)
//                    customDataContainerInterface.onCustomCategoryContainerResponse(dataList,
//                        position,
//                        dataList[position].categoryId)
//                }
//            }
        }
    }

    private fun checkUncheckValue(position: Int, title: TextView, textLayout: LinearLayout) {
//        if (dataList[position].isSelected!!) {
//            title.setTextColor(ContextCompat.getColor(activity, R.color.black))
//            textLayout.background =
//                ContextCompat.getDrawable(activity, R.drawable.round_grey_button)
//        } else {
//            title.setTextColor(ContextCompat.getColor(activity, R.color.white))
//            textLayout.background =
//                ContextCompat.getDrawable(activity, R.drawable.button_welcome_screen)
//        }

        textLayout.background =
            ContextCompat.getDrawable(activity, R.drawable.round_grey_button)
    }

    init {
        this.dataList = dataList
        this.dataEntered = dataEntered
        this.type = type
        showData()
    }
}