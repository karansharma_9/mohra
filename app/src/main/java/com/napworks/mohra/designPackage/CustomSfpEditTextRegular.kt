package com.napworks.mohra.designPackage

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatEditText

class CustomSfpEditTextRegular(context: Context, attrs: AttributeSet?) :
    AppCompatEditText(context, attrs) {
    private fun init(context: Context) {
        val face = Typeface.createFromAsset(context.assets, "fonts/SFProTextRegular.ttf")
        typeface = face
    }

    init {
        init(context)
    }
}