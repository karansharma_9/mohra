package com.napworks.mohra.interfacePackage.News;

import com.napworks.mohra.modelPackage.News.FavNewsModel;
import com.napworks.mohra.modelPackage.News.LikeUnlikeModel;


public interface LikeAndUnlikeInterface {
    public void onLikeAndUnlikeResponse(String status, LikeUnlikeModel likeUnlikeModel);

}
