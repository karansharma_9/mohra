package com.napworks.mohra.interfacePackage;

import com.napworks.mohra.modelPackage.CountryCodeModel;
import com.napworks.mohra.modelPackage.GetIntroDataModel;


public interface CountryCodeDataInterface {
    public void onCountryCodeDataInterfaceResponse(String status, CountryCodeModel countryCodeModel);

}
