package com.napworks.mohra.interfacePackage;

import com.napworks.mohra.modelPackage.GetIntroDataModel;


public interface GetIntroDataInterface {
    public void onGetIntroDataInterfaceResponse(String status, GetIntroDataModel getIntroDataModel);

}
