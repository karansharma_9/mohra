package com.napworks.mohra.interfacePackage.News;

import com.napworks.mohra.modelPackage.News.GetNewsCategoriesModel;



public interface GetNewsCategoriesInterface {
    public void onGetNewsCategoriesInterfaceResponse(String status, GetNewsCategoriesModel getNewsCategoriesModel);

}
