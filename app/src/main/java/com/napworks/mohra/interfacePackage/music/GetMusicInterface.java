package com.napworks.mohra.interfacePackage.music;

import com.napworks.mohra.modelPackage.MusicModel.GetMusicModel;


public interface GetMusicInterface {
    public void onGetMusicResponse(String status, GetMusicModel getMusicModel);

}
