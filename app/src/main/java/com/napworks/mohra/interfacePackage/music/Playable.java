package com.napworks.mohra.interfacePackage.music;

public interface Playable {
    void onTrackPrevious();
    void onTrackPlay();
    void onTrackPause();
    void onTrackNext();
}
