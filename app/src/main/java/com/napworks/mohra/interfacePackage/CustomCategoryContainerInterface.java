package com.napworks.mohra.interfacePackage;

import com.napworks.mohra.modelPackage.CommonStatusModel;
import com.napworks.mohra.modelPackage.News.GetNewsInnerCategoriesModel;

import java.util.List;


public interface CustomCategoryContainerInterface {
    public void onCustomCategoryContainerResponse(List<GetNewsInnerCategoriesModel> dataList, int position, String type);

}
