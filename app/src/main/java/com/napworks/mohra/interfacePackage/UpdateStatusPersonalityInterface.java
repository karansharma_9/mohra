package com.napworks.mohra.interfacePackage;

import com.napworks.mohra.modelPackage.CommonStatusModel;


public interface UpdateStatusPersonalityInterface {
    public void onUpdateStatusPersonalityInterfaceResponse(String status, CommonStatusModel commonStatusModel);

}
