package com.napworks.mohra.interfacePackage.music


import com.napworks.mohra.modelPackage.MusicModel.MusicModel

interface MusicTypeSelectAdapterOnClick {
    fun onMusicTypeSelectAdapterOnClick(
        status: String?,
        innerModel: MusicModel,
        position: Int
    )
}