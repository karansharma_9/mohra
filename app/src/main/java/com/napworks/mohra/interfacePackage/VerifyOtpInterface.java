package com.napworks.mohra.interfacePackage;

import com.napworks.mohra.modelPackage.CommonStatusModel;


public interface VerifyOtpInterface {
    public void onVerifyOtpInterfaceResponse(String status, CommonStatusModel commonStatusModel);

}
