package com.napworks.mohra.interfacePackage.music

import com.napworks.mohra.modelPackage.MusicModel.MusicFilesModel


interface AdapterOnClick {
    fun onAdapterClick(
        musicFilesModel: Any?,
        adapterPosition: Int,
        listMusicFiles: ArrayList<MusicFilesModel>
    )
}