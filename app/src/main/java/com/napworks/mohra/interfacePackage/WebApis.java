package com.napworks.mohra.interfacePackage;


import com.napworks.mohra.modelPackage.CommonStatusModel;
import com.napworks.mohra.modelPackage.CountryCodeModel;
import com.napworks.mohra.modelPackage.GetIntroDataModel;
import com.napworks.mohra.modelPackage.GetPersonalityDataModel;
import com.napworks.mohra.modelPackage.LoginModel;
import com.napworks.mohra.modelPackage.MusicModel.CreatePlaylistResponse;
import com.napworks.mohra.modelPackage.MusicModel.GetMusicHomeDataModel;
import com.napworks.mohra.modelPackage.MusicModel.GetMusicModel;
import com.napworks.mohra.modelPackage.News.AddCommentModel;
import com.napworks.mohra.modelPackage.News.FavNewsModel;
import com.napworks.mohra.modelPackage.News.GetNewsCategoriesModel;
import com.napworks.mohra.modelPackage.News.GetNewsDetailsModel;
import com.napworks.mohra.modelPackage.News.GetNewsModel;
import com.napworks.mohra.modelPackage.News.LikeUnlikeModel;
import com.napworks.mohra.modelPackage.News.GetCommentsNewsModel;
import com.napworks.mohra.modelPackage.News.SearchNewsModel;
import com.napworks.mohra.modelPackage.PersonalityFinalScreenDataModel;
import com.napworks.mohra.modelPackage.SendOtpModel;
import com.napworks.mohra.modelPackage.SignUpModel;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface WebApis {

    @FormUrlEncoded
    @POST("sendOtp")
    Call<SendOtpModel> sendOtp(@Field("mobileNumber") String mobileNumber,
                               @Field("isLive") String isLive);

    @FormUrlEncoded
    @POST("verifyOtp")
    Call<CommonStatusModel> verifyOtp(@Field("otpId") String otpId,
                                      @Field("otp") String otp);

    @FormUrlEncoded
    @POST("signUp")
    Call<SignUpModel> signUp(@Field("email") String email,
                             @Field("password") String password,
                             @Field("request_from") String request_from,
                             @Field("first_name") String first_name,
                             @Field("family_name") String family_name,
                             @Field("mobile") String mobile,
                             @Field("birthDate") Long birthDate,
                             @Field("userType") String userType,
                             @Field("userName") String userName,
                             @Field("countryId") String countryId);

    @FormUrlEncoded
    @POST("login")
    Call<LoginModel> login(@Field("email") String email,
                           @Field("password") String password,
                           @Field("request_from") String request_from,
                           @Field("google_id") String google_id,
                           @Field("facebook_id") String facebook_id,
                           @Field("type") String type,
                           @Field("first_name") String first_name,
                           @Field("family_name") String family_name,
                           @Field("mobile") String mobile,
                           @Field("birthDate") Long birthDate,
                           @Field("userType") String userType,
                           @Field("userName") String userName,
                           @Field("countryId") String countryId);

    @GET("getIntroData")
    Call<GetIntroDataModel> getIntroData();

    @GET("getPersonalityData")
    Call<GetPersonalityDataModel> getPersonalityData();

    @FormUrlEncoded
    @POST("uploadPersonalityData")
    Call<CommonStatusModel> uploadPersonalityData(@Header("Auth") String Auth,
                                                  @Field("session") String session,
                                                  @Field("user_id") String user_id,
                                                  @Field("personalityData") String personalityData);

    @FormUrlEncoded
    @POST("updateStatusPersonality")
    Call<CommonStatusModel> updateStatusPersonality(@Header("Auth") String Auth,
                                                    @Field("session") String session,
                                                    @Field("user_id") String user_id,
                                                    @Field("personalityStatus") String personalityData);

    @FormUrlEncoded
    @POST("getUserPersonality")
    Call<PersonalityFinalScreenDataModel> getUserPersonality(@Header("Auth") String Auth,
                                                             @Field("session") String session,
                                                             @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("checkMobileExist")
    Call<CommonStatusModel> checkMobileExist(@Field("mobile") String mobile,
                                             @Field("countryId") String countryId);

    @FormUrlEncoded
    @POST("forgetUpdatePassword")
    Call<CommonStatusModel> forgetUpdatePassword(@Field("user_id") String user_id,
                                                 @Field("password") String password);

    @FormUrlEncoded
    @POST("checkEmailMobileExists")
    Call<CommonStatusModel> checkEmailMobileExists(@Field("mobile") String mobile,
                                                   @Field("countryId") String countryId,
                                                   @Field("email") String email);

    @GET("getCountries")
    Call<CountryCodeModel> getCountries();


    @FormUrlEncoded
    @POST("getNewsCategories")
    Call<GetNewsCategoriesModel> getNewsCategories(@Header("Auth") String Auth,
                                                   @Field("session") String session,
                                                   @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("getNews")
    Call<GetNewsModel> getNews(@Header("Auth") String Auth,
                               @Field("session") String session,
                               @Field("user_id") String user_id,
                               @Field("categoryId") String categoryId);

    @FormUrlEncoded
    @POST("getNewsDetail")
    Call<GetNewsDetailsModel> getNewsDetail(@Header("Auth") String Auth,
                                            @Field("session") String session,
                                            @Field("user_id") String user_id,
                                            @Field("newsId") String newsId);

    @FormUrlEncoded
    @POST("favouriteNews")
    Call<CommonStatusModel> favouriteNews(@Header("Auth") String Auth,
                                          @Field("session") String session,
                                          @Field("user_id") String user_id,
                                          @Field("value") String value,
                                          @Field("newsId") String newsId);

    @FormUrlEncoded
    @POST("getFavouriteNews")
    Call<FavNewsModel> getFavouriteNews(@Header("Auth") String Auth,
                                        @Field("session") String session,
                                        @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("likeUnlikePost")
    Call<LikeUnlikeModel> likeUnlikePost(@Header("Auth") String Auth,
                                         @Field("session") String session,
                                         @Field("user_id") String user_id,
                                         @Field("postId") String postId,
                                         @Field("type") String type
    );

    @FormUrlEncoded
    @POST("getComments")
    Call<GetCommentsNewsModel> getComments(@Header("Auth") String Auth,
                                           @Field("session") String session,
                                           @Field("user_id") String user_id,
                                           @Field("postId") String postId,
                                           @Field("type") String type);

    @FormUrlEncoded
    @POST("addComment")
    Call<AddCommentModel> addComment(@Header("Auth") String Auth,
                                     @Field("session") String session,
                                     @Field("user_id") String user_id,
                                     @Field("postId") String postId,
                                     @Field("type") String type,
                                     @Field("comment") String comment);

    @FormUrlEncoded
    @POST("searchNews")
    Call<SearchNewsModel> searchNews(@Header("Auth") String Auth,
                                     @Field("session") String session,
                                     @Field("user_id") String user_id,
                                     @Field("search") String search,
                                     @Field("categoryId") String categoryId);

    @FormUrlEncoded
    @POST("updateUser")
    Call<CommonStatusModel> update(@Header("Auth") String Auth,
                                   @Field("session") String session,
                                   @Field("user_id") String user_id,
                                   @Field("first_name") String first_name,
                                   @Field("family_name") String family_name,
                                   @Field("mobile") String mobile,
                                   @Field("birthDate") Long birthDate,
                                   @Field("userType") String userType,
                                   @Field("userName") String userName,
                                   @Field("countryId") String countryId);

    @FormUrlEncoded
    @POST("getMusicHomeData")
    Call<GetMusicHomeDataModel> getMusicHomeData(@Header("Auth") String Auth,
                                                 @Field("session") String session,
                                                 @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("getLibraryData")
    Call<GetMusicHomeDataModel> getLibraryData(@Header("Auth") String Auth,
                                               @Field("session") String session,
                                               @Field("user_id") String user_id);

    @Multipart
    @POST("uploadAppImage")
    Call<ResponseBody> uploadAppImage(@Header("Auth") String Auth,
                                      @Part("session") String session,
                                      @Part("user_id") String user_id,
                                      @Part MultipartBody.Part image);

    @FormUrlEncoded
    @POST("createPlaylist")
    Call<CreatePlaylistResponse> createPlaylist(@Header("Auth") String Auth,
                                                @Field("session") String session,
                                                @Field("user_id") String user_id,
                                                @Field("image") String image,
                                                @Field("title") String title);

    @FormUrlEncoded
    @POST("getPlaylistDetail")
    Call<CreatePlaylistResponse> getPlaylistDetail(@Header("Auth") String Auth,
                                                   @Field("session") String session,
                                                   @Field("user_id") String user_id,
                                                   @Field("playlistId") String playlistId);

    @FormUrlEncoded
    @POST("getAlbumDetail")
    Call<CreatePlaylistResponse> getAlbumDetail(@Header("Auth") String Auth,
                                                @Field("session") String session,
                                                @Field("user_id") String user_id,
                                                @Field("albumId") String albumId);

    @FormUrlEncoded
    @POST("getMusic")
    Call<GetMusicModel> getMusic(@Header("Auth") String Auth,
                                 @Field("session") String session,
                                 @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("uploadSongsPLaylist")
    Call<CommonStatusModel> uploadSongsPLaylist(@Header("Auth") String Auth,
                                                @Field("session") String session,
                                                @Field("user_id") String user_id,
                                                @Field("song_id") String song_id,
                                                @Field("playlist_id") String playlist_id);

}
