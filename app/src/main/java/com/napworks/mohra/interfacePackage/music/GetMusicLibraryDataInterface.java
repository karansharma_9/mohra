package com.napworks.mohra.interfacePackage.music;

import com.napworks.mohra.modelPackage.MusicModel.GetMusicHomeDataModel;


public interface GetMusicLibraryDataInterface {
    public void onGetMusicLibraryDataResponse(String status, GetMusicHomeDataModel getMusicHomeDataModel);

}
