package com.napworks.mohra.interfacePackage.News;

import com.napworks.mohra.modelPackage.News.FavNewsModel;
import com.napworks.mohra.modelPackage.News.GetNewsCategoriesModel;


public interface FavNewsInterface {
    public void onFavNewsInterfaceResponse(String status, FavNewsModel favNewsModel);

}
