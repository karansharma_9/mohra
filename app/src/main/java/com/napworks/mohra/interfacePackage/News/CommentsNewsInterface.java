package com.napworks.mohra.interfacePackage.News;

import com.napworks.mohra.modelPackage.News.GetCommentsNewsModel;


public interface CommentsNewsInterface {
    public void onCommentsNewsResponse(String status, GetCommentsNewsModel getCommentsNewsModel);

}
