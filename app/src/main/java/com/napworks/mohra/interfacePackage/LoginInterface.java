package com.napworks.mohra.interfacePackage;

import com.napworks.mohra.modelPackage.LoginModel;


public interface LoginInterface {
    public void onLoginInterfaceResponse(String status, LoginModel loginModel);

}
