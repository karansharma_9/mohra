package com.napworks.mohra.interfacePackage.News;

import com.napworks.mohra.modelPackage.News.AddCommentModel;
import com.napworks.mohra.modelPackage.News.GetCommentsNewsModel;


public interface AddCommentsInterface {
    public void onAddCommentsResponse(String status, AddCommentModel addCommentModel);

}
