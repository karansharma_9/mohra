package com.napworks.mohra.interfacePackage;

import com.napworks.mohra.modelPackage.CommonStatusModel;


public interface UploadPersonalityInterface {
    public void onUploadPersonalityInterfaceResponse(String status, CommonStatusModel commonStatusModel);

}
