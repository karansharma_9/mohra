package com.napworks.mohra.interfacePackage.music


import com.napworks.mohra.modelPackage.MusicModel.LibraryTypesModel
import com.napworks.mohra.modelPackage.MusicModel.MusicInnerModel
import com.napworks.mohra.modelPackage.MusicModel.MusicModel

interface AddSongsAdapterClick {
    fun onAddSongsAdapterOnClick(
        status: String?,
        innerModel: MusicInnerModel,
        position: Int
    )
}