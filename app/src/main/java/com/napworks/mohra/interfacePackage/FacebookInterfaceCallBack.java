package com.napworks.mohra.interfacePackage;


import com.napworks.mohra.modelPackage.FacebookDataModel;

public interface FacebookInterfaceCallBack {

    public void getFacebookCall(String status, FacebookDataModel facebookDataModel);
}
