package com.napworks.mohra.interfacePackage.music;

import com.napworks.mohra.modelPackage.MusicModel.CreatePlaylistResponse;
import com.napworks.mohra.modelPackage.MusicModel.GetMusicHomeDataModel;


public interface CreatePlaylistInterface {
    public void onCreatePlaylistResponse(String status, CreatePlaylistResponse  createPlaylistResponse);

}
