package com.napworks.mohra.interfacePackage.music


import com.napworks.mohra.modelPackage.MusicModel.LibraryTypesModel
import com.napworks.mohra.modelPackage.MusicModel.MusicModel

interface LibraryTypeSelectAdapterOnClick {
    fun onLibraryTypeSelectAdapterOnClick(
        status: String?,
        libraryTypesModel: MusicModel,
        position: Int
    )
}