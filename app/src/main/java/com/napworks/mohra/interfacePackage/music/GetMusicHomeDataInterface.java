package com.napworks.mohra.interfacePackage.music;

import com.napworks.mohra.modelPackage.MusicModel.GetMusicHomeDataModel;


public interface GetMusicHomeDataInterface {
    public void onGetMusicHomeDataResponse(String status, GetMusicHomeDataModel getMusicHomeDataModel);

}
