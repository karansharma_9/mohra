package com.napworks.mohra.interfacePackage;

import com.napworks.mohra.modelPackage.GetIntroDataModel;
import com.napworks.mohra.modelPackage.PersonalityFinalScreenDataModel;


public interface GetUserPersonalityInterface {
    public void onGetUserPersonalityInterfaceResponse(String status, PersonalityFinalScreenDataModel personalityFinalScreenDataModel);

}
