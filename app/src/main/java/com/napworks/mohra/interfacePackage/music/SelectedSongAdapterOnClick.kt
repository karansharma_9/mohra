package com.napworks.mohra.interfacePackage.music


import com.napworks.mohra.modelPackage.MusicModel.MusicFilesModel

interface SelectedSongAdapterOnClick {
    fun onSelectedSongAdapterOnClick(
        status: String?,
        musicFilesModel: MusicFilesModel,
        position: Int
    )
}