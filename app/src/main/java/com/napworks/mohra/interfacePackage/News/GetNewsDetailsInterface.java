package com.napworks.mohra.interfacePackage.News;

import com.napworks.mohra.modelPackage.News.GetNewsDetailsModel;
import com.napworks.mohra.modelPackage.News.GetNewsModel;


public interface GetNewsDetailsInterface {
    public void onGetNewsDetailsInterfaceResponse(String status, GetNewsDetailsModel getNewsDetailsModel);

}
