package com.napworks.mohra.interfacePackage.music


import com.napworks.mohra.modelPackage.MusicModel.MusicFilesModel

interface ShowIconAdapterOnClick {
    fun onShowIconAdapterOnClick(
        status: String?,
        position: Int
    )
}