package com.napworks.mohra.interfacePackage;

import com.napworks.mohra.modelPackage.CommonStatusModel;
import com.napworks.mohra.modelPackage.GetIntroDataModel;


public interface CommonStatusInterface {
    public void onCommonStatusInterfaceResponse(String status, CommonStatusModel commonStatusModel);

}
