package com.napworks.mohra.interfacePackage;

import com.napworks.mohra.modelPackage.SignUpModel;


public interface SignUpInterface {
    public void onSignUpInterfaceResponse(String status, SignUpModel signUpModel);

}
