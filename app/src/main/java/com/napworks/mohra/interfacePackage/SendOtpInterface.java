package com.napworks.mohra.interfacePackage;

import com.napworks.mohra.modelPackage.SendOtpModel;


public interface SendOtpInterface {
    public void onSendOtpInterfaceResponse(String status, SendOtpModel sendOtpModel);

}
