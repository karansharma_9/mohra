package com.napworks.mohra.interfacePackage.News;

import com.napworks.mohra.modelPackage.News.AddCommentModel;
import com.napworks.mohra.modelPackage.News.SearchNewsModel;


public interface SearchNewsInterface {
    public void onSearchNewsResponse(String status, SearchNewsModel searchNewsModel);

}
