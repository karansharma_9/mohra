package com.napworks.mohra.interfacePackage.music


import com.napworks.mohra.modelPackage.MusicModel.RecentlyPlayedModel

interface SearchFilterAdapterOnClick {
    fun onSearchFilterAdapterOnClick(
        status: String?,
        position: Int
    )
}