package com.napworks.mohra.interfacePackage.News


import com.napworks.mohra.modelPackage.News.GetNewsInnerCategoriesModel

interface NewsTypeSelectAdapterOnClick {
    fun onNewsTypeSelectAdapterOnClick(
        status: String?,
        getNewsInnerCategoriesModel: GetNewsInnerCategoriesModel,
        position: Int
    )
}