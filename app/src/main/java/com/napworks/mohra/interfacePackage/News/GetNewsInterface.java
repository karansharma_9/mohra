package com.napworks.mohra.interfacePackage.News;

import com.napworks.mohra.modelPackage.News.GetNewsCategoriesModel;
import com.napworks.mohra.modelPackage.News.GetNewsModel;


public interface GetNewsInterface {
    public void onGetNewsInterfaceResponse(String status, GetNewsModel getNewsModel);

}
