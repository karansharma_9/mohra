package com.napworks.mohra.interfacePackage;

public interface UploadImageInterface {
    void onUploadImageResponse(String status, String response, String type);
}
