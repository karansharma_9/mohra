package com.napworks.gamaloto.interfacePackage

import com.napworks.mohra.modelPackage.News.GetNewsInnerCategoriesModel


interface OnHomeScreenCategoryAdapterClick {
    fun onHomeScreenCategoryAdapterClick(
        status: String?,
        getNewsInnerCategoriesModel: GetNewsInnerCategoriesModel,
        position: Int
    )
}