package com.napworks.mohra.interfacePackage;

import com.napworks.mohra.modelPackage.GetPersonalityDataModel;


public interface GetPersonalityDataInterface {
    public void onGetPersonalityDataInterfaceResponse(String status, GetPersonalityDataModel getPersonalityDataModel);

}
