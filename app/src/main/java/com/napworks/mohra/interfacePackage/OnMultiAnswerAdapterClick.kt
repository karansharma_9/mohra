package com.napworks.gamaloto.interfacePackage


import com.napworks.mohra.modelPackage.GetPersonalityAnswerInnerModel

interface OnMultiAnswerAdapterClick {
    fun onMultiAnswerAdapterClickResponse(status: String?, getPersonalityAnswerInnerModel: GetPersonalityAnswerInnerModel, position: Int)
}