package com.napworks.mohra.activitiesPackage

import android.app.DatePickerDialog
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.napworks.mohra.R
import com.napworks.mohra.activitiesPackage.PersonalityActivities.WelcomePersonalityTestActivity
import com.napworks.mohra.dialogPackage.CommonMessageDialog
import com.napworks.mohra.dialogPackage.LoadingDialog
import com.napworks.mohra.interfacePackage.CommonStatusInterface
import com.napworks.mohra.interfacePackage.UploadPersonalityInterface
import com.napworks.mohra.modelPackage.CommonStatusModel
import com.napworks.mohra.modelPackage.LoginInnerModel
import com.napworks.mohra.modelPackage.UserDataModel
import com.napworks.mohra.utilPackage.ApiCalls
import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.utilPackage.MyConstants
import kotlinx.android.synthetic.main.toolbar_layout.*
import java.text.SimpleDateFormat
import java.util.*


class RegisterIntroActivity : AppCompatActivity(), View.OnClickListener,
     CommonStatusInterface {

    var TAG = javaClass.simpleName;

    lateinit var upperButtonRegisterIntro: TextView
    lateinit var basbooshButton: TextView
    lateinit var firstNameEditText: EditText
    lateinit var lastNameEditText: EditText
    lateinit var editTextBirthDAy: EditText
    private var sharedPreferences: SharedPreferences? = null
    var timestamp : Long = 0
    var userId: String? = ""
    var userType: String? = null
    private var myCalendar: Calendar? = null
    private lateinit var userData: UserDataModel
    lateinit var dateOfBithPicker: LinearLayout
    var loadingDialog: LoadingDialog? = null
    var commonMessageDialog: CommonMessageDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_intro)
        userData=UserDataModel()
        loadingDialog = LoadingDialog(this)

        sharedPreferences = getSharedPreferences(MyConstants.SHARED_PREFERENCE, MODE_PRIVATE)
        commonMessageDialog = CommonMessageDialog(this)
        upperButtonRegisterIntro = findViewById(R.id.upperButtonRegisterIntro)
        firstNameEditText = findViewById(R.id.firstNameEditText)
        lastNameEditText = findViewById(R.id.lastNameEditText)
        dateOfBithPicker =  findViewById(R.id.dateOfBithPicker)
        editTextBirthDAy =  findViewById(R.id.editTextBirthDAy)
        basbooshButton =  findViewById(R.id.basbooshButton)
        upperButtonRegisterIntro.setOnClickListener(this)
        dateOfBithPicker.setOnClickListener(this)
        basbooshButton.setOnClickListener(this)
        menuBackPress.setOnClickListener(this)
        userId = intent.getStringExtra(MyConstants.USER_ID)
        CommonMethods.showLog(TAG,"UserId   $userId")
        editTextBirthDAy.isFocusable = false;
        editTextBirthDAy.isClickable = false;

        myCalendar = Calendar.getInstance()

    }

     fun datePicker()
     {
         val c = Calendar.getInstance()
         val year = c.get(Calendar.YEAR)
         val month = c.get(Calendar.MONTH)
         val day = c.get(Calendar.DAY_OF_MONTH)
         val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

             var realMonth = monthOfYear + 1
             var selectedday  = dayOfMonth.toString()
             var selectedmonth  = realMonth.toString()
             if(dayOfMonth<10)
             {
                 selectedday = "0$selectedday"
             }
             else
             {
                 selectedday= dayOfMonth.toString()

             }

             if(realMonth<10)
             {
                 CommonMethods.showLog(TAG,"real  " + realMonth)
                 selectedmonth = "0$selectedmonth"
             }
             else
             {
                 selectedmonth= realMonth.toString()
             }
             editTextBirthDAy.setText("$selectedday / $selectedmonth / $year")
             val date = SimpleDateFormat("dd-MM-yyyy").parse("$dayOfMonth-${realMonth}-$year")
             timestamp = date.time
         }, year, month, day)
         dpd.datePicker.maxDate = System.currentTimeMillis();
         dpd.show()
     }

     fun passData(type: String)
     {
         val firstNameEditTextText: String = firstNameEditText.text.toString().trim()
         val lastNameEditTextText: String = lastNameEditText.text.toString().trim()
         val editTextBirthDAyText: String = editTextBirthDAy.text.toString().trim()

         if (TextUtils.isEmpty(firstNameEditTextText)) {
             commonMessageDialog?.showDialog(getString(R.string.enterName))
             firstNameEditText.requestFocus()
         } else if (TextUtils.isEmpty(lastNameEditTextText)) {
             commonMessageDialog?.showDialog(getString(R.string.enterLastName))
             lastNameEditText.requestFocus()
         } else if (TextUtils.isEmpty(editTextBirthDAyText)) {
             commonMessageDialog?.showDialog(getString(R.string.selectDateOfBirth))
             lastNameEditText.requestFocus()
         }else {
             userData.first_name = firstNameEditTextText
             userData.last_name = lastNameEditTextText
             userData.userType = type
             userData.birth_date = timestamp
             val intent = Intent(baseContext, RegisterGoogleActivity::class.java)
             intent.putExtra("CLIENT_DATA_FROM_INTRO_SCREEN", userData);
             startActivity(intent)
         }
     }

     fun updateApi(type : String)
     {
         userType = type
         val firstNameEditTextText: String = firstNameEditText.text.toString().trim()
         val lastNameEditTextText: String = lastNameEditText.text.toString().trim()
         val editTextBirthDAyText: String = editTextBirthDAy.text.toString().trim()

         if (TextUtils.isEmpty(firstNameEditTextText)) {
             commonMessageDialog?.showDialog(getString(R.string.enterName))
             firstNameEditText.requestFocus()
         } else if (TextUtils.isEmpty(lastNameEditTextText)) {
             commonMessageDialog?.showDialog(getString(R.string.enterLastName))
             lastNameEditText.requestFocus()
         } else if (TextUtils.isEmpty(editTextBirthDAyText)) {
             commonMessageDialog?.showDialog(getString(R.string.selectDateOfBirth))
             lastNameEditText.requestFocus()
         }else {
             userData.first_name = firstNameEditTextText
             userData.last_name = lastNameEditTextText
             userData.userType = type
             userData.birth_date = timestamp

//             val editor = sharedPreferences!!.edit()
//             editor.putString(MyConstants.NAME, firstNameEditTextText)
//             editor.putString(MyConstants.LAST_NAME,lastNameEditTextText)
//             editor.putString(MyConstants.USERTYPE,type)
//             editor.putLong(MyConstants.DATEOFBIRTH,timestamp)
////             editor.putBoolean(MyConstants.IS_LOGGED_IN, true)
//             editor.apply()
             CommonMethods.showLog(TAG,"type while hitting api  $type")
             loadingDialog!!.showDialog()
             CommonMethods.showLog(TAG,"upate hit ")
             ApiCalls.update(
                 sharedPreferences!!,
                 firstNameEditTextText,
                 lastNameEditTextText,
                 "",
                 timestamp,
                 type,
                 "",
                 "",
                 this

             )
         }
     }


    override fun onClick(v: View?) {
        if (v?.id == R.id.upperButtonRegisterIntro)
        {
            if(userId != null)
            {
                updateApi(MyConstants.CAKE)
            }
            else
            {
                var type = MyConstants.CAKE
                passData(type)
            }
        }
        else if(v?.id == R.id.basbooshButton)
        {
            if(userId != null)
            {
                updateApi(MyConstants.BASBOOSH)
            }
            else
            {
                var type = MyConstants.BASBOOSH
                passData(type)
            }
        }
        else if(v?.id == R.id.dateOfBithPicker)
        {
            datePicker()
        }
        else if(v?.id == R.id.menuBackPress)
        {
            onBackPressed()
        }
    }

    override fun onCommonStatusInterfaceResponse(
        status: String?,
        commonStatusModel: CommonStatusModel?,
    ) {
        when (status) {
            MyConstants.GO_TO_RESPONSE -> {
                CommonMethods.showLog(TAG,"login  status  " + commonStatusModel!!.message)
                if (commonStatusModel != null) {
                    if (commonStatusModel.status != null) {
                        when {
                            commonStatusModel.status.equals("1") -> {
                                loadingDialog!!.hideDialog()
                                val editor = sharedPreferences!!.edit()
                                editor.putString(MyConstants.NAME, firstNameEditText.text.toString().trim())
                                editor.putString(MyConstants.LAST_NAME,lastNameEditText.text.toString().trim())
                                editor.putString(MyConstants.USERTYPE,userType)
                                editor.putLong(MyConstants.DATEOFBIRTH,timestamp)
//             editor.putBoolean(MyConstants.IS_LOGGED_IN, true)
                                editor.apply()
                                CommonMethods.showLog(TAG,"message  " + commonStatusModel!!.message + "  status     " + commonStatusModel!!.status)
                                val intent = Intent(this, SetUserNameActivity::class.java)
                                intent.putExtra(MyConstants.UDATEUSERNAME,"true") // getText() SHOULD NOT be static!!!
                                startActivity(intent)
                                finishAffinity()
                            }
                            commonStatusModel.status.equals("0") -> {
                                loadingDialog!!.hideDialog()
                                commonMessageDialog!!.showDialog(commonStatusModel.message)
                            }
                            commonStatusModel.status.equals("10") -> {
                                loadingDialog!!.hideDialog()
                                CommonMethods.callLogout(this)
                            }
                            else -> {
                                loadingDialog!!.hideDialog()
                                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                            }
                        }
                    } else {
                        loadingDialog!!.hideDialog()
                        commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                    }
                } else {
                    loadingDialog!!.hideDialog()
                    commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                }
            }
            MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                loadingDialog!!.hideDialog()
                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
            }
        }
    }

}