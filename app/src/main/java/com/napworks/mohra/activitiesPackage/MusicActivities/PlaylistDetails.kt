package com.napworks.mohra.activitiesPackage.MusicActivities

import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.*
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.napworks.mohra.R
import com.napworks.mohra.adapterPackage.MusicAdapter.MusicTypeRecyclerAdapter
import com.napworks.mohra.adapterPackage.MusicAdapter.PlaylistDetailsRecyclerAdapter
import com.napworks.mohra.dialogPackage.CommonMessageDialog
import com.napworks.mohra.dialogPackage.LoadingDialog
import com.napworks.mohra.interfacePackage.music.CreatePlaylistInterface
import com.napworks.mohra.modelPackage.FileChoosedModel
import com.napworks.mohra.modelPackage.MusicModel.CreatePlaylistResponse
import com.napworks.mohra.modelPackage.MusicModel.MusicInnerModel
import com.napworks.mohra.modelPackage.MusicModel.MusicModel
import com.napworks.mohra.modelPackage.MusicModel.PlaylistFromMyLibraryModel
import com.napworks.mohra.utilPackage.ApiCalls
import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.utilPackage.MyConstants
import com.theartofdev.edmodo.cropper.CropImage
import java.io.File
import java.lang.Exception

class PlaylistDetails : AppCompatActivity(), CreatePlaylistInterface, View.OnClickListener {
    var TAG = javaClass.simpleName;

    private var sharedPreferences: SharedPreferences? = null
    var loadingDialog: LoadingDialog? = null
    var commonMessageDialog: CommonMessageDialog? = null


    lateinit var recyclerViewSongsList: RecyclerView
    private lateinit var playlistDetailsRecyclerAdapter: PlaylistDetailsRecyclerAdapter
    lateinit var noSongsLayout: TextView
    lateinit var mainLinear: LinearLayout
    lateinit var playlistTitle: TextView
    lateinit var playlistDesc: TextView
    lateinit var pageTitle: TextView
    lateinit var image: ImageView
    lateinit var backButton: ImageView
    lateinit var menuButton: ImageView
    lateinit var hasSongsLayout: LinearLayout

    var playList : ArrayList<MusicInnerModel>? = null
    var id :String = ""
    var calledFrom = ""



    var musicModel:MusicModel = MusicModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_playlist_details)
        commonMessageDialog = CommonMessageDialog(this)
        loadingDialog = LoadingDialog(this)
        sharedPreferences = getSharedPreferences(MyConstants.SHARED_PREFERENCE, MODE_PRIVATE)

        recyclerViewSongsList  = findViewById(R.id.recyclerViewSongsList)
        noSongsLayout  = findViewById(R.id.noSongsLayout)
        hasSongsLayout  = findViewById(R.id.hasSongsLayout)
        mainLinear  = findViewById(R.id.mainLinear)
        playlistTitle  = findViewById(R.id.playlistTitle)
        playlistDesc  = findViewById(R.id.playlistDesc)
        image  = findViewById(R.id.image)
        pageTitle  = findViewById(R.id.pageTitle)
        backButton  = findViewById(R.id.backButton)
        menuButton  = findViewById(R.id.menuButton)

        calledFrom = intent.getStringExtra(MyConstants.CALLED_FROM).toString()
        id = intent.getStringExtra(MyConstants.ID).toString()


        playList = ArrayList<MusicInnerModel> ()
        recyclerViewSongsList.layoutManager = LinearLayoutManager(this)
        playlistDetailsRecyclerAdapter = PlaylistDetailsRecyclerAdapter(this, playList!!)
        recyclerViewSongsList.adapter = playlistDetailsRecyclerAdapter

//        recyclerViewSongsList.isNestedScrollingEnabled = false;
//        recyclerViewSongsList.layoutManager = LinearLayoutManager(this)
//        playlistDetailsRecyclerAdapter = PlaylistDetailsRecyclerAdapter(this, playList!!)
//        recyclerViewSongsList.adapter = playlistDetailsRecyclerAdapter


        mainLinear.visibility = View.GONE
        checkAndHitApi()


//        playList!!.add(PlaylistFromMyLibraryModel("Create Playlist","",""))
//        playList!!.add(PlaylistFromMyLibraryModel("Dance Hits","",""))
//        playList!!.add(PlaylistFromMyLibraryModel("New of January","",""))
//        playList!!.add(PlaylistFromMyLibraryModel("Popular Russian","",""))
//        playList!!.add(PlaylistFromMyLibraryModel("Classical Music","",""))
//        if(playList!!.size != 0)
//        {
//            hasSongsLayout.visibility = View.VISIBLE
//            noSongsLayout.visibility = View.GONE
//        }
//        else
//        {
//            hasSongsLayout.visibility = View.GONE
//        }

        noSongsLayout.setOnClickListener(this)
        backButton.setOnClickListener(this)
        menuButton.setOnClickListener(this)

    }

    private fun checkAndHitApi() {
        CommonMethods.showLog(TAG, "ID : $id")
        if (calledFrom == MyConstants.CREATE_PLAYLIST||calledFrom==MyConstants.PLAYLIST_DETAIL){
            pageTitle.text = getString(R.string.playlistDetails)
            loadingDialog!!.showDialog()
            ApiCalls.getPlaylistDetails(sharedPreferences!!,id,this)
        }
        else
        {
            pageTitle.text = getString(R.string.albumDetails)
            loadingDialog!!.showDialog()
            ApiCalls.getAlbumDetail(sharedPreferences!!,id,this)
        }

    }

    override fun onCreatePlaylistResponse(
        status: String?,
        createPlaylistResponse: CreatePlaylistResponse?
    ) {
        loadingDialog!!.hideDialog()
        mainLinear.visibility = View.VISIBLE
        when (status) {
            MyConstants.GO_TO_RESPONSE -> {
                if (createPlaylistResponse != null) {
                    if (createPlaylistResponse.status != null) {
                        CommonMethods.showLog(TAG,"playlistDetailApi Status : "+createPlaylistResponse.status)
                        CommonMethods.showLog(TAG,"playlistDetailApi Error : "+createPlaylistResponse.errorData)
                        when {
                            createPlaylistResponse.status.equals("1") -> {
                                musicModel = createPlaylistResponse.data
                                setData()
                                playList!!.clear()
                                playList!!.addAll(createPlaylistResponse.data.innerList)
                                playlistDetailsRecyclerAdapter.notifyDataSetChanged()
                                checkAndShowMessage()
                            }
                            createPlaylistResponse.status.equals("0") -> {
                                CommonMethods.showLog(TAG, createPlaylistResponse.message)
                                commonMessageDialog!!.showDialog(createPlaylistResponse.message)

                            }
                            createPlaylistResponse.status.equals("10") -> {
                                CommonMethods.showLog(TAG, createPlaylistResponse.message)
//                                commonMessageDialog!!.showDialog(createPlaylistResponse.message)
                                CommonMethods.callLogout(this)

                            }
                            else -> {
                                CommonMethods.showLog(TAG,
                                    R.string.someErrorOccurredText.toString())
                                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))

                            }
                        }
                    } else {
                        CommonMethods.showLog(TAG,
                            R.string.someErrorOccurredText.toString())
                        commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))

                    }
                } else {
                    CommonMethods.showLog(TAG,
                        R.string.someErrorOccurredText.toString())
                    commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))

                }
            }
            MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                CommonMethods.showLog(TAG,
                    R.string.someErrorOccurredText.toString())
                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))

            }
        }
    }

    private fun setData() {
        playlistTitle.text = musicModel.title
        if (calledFrom == MyConstants.CREATE_PLAYLIST){
//            shufflePlayButton.setTitle("Add Song")
            playlistDesc.text = "Add song to your playlist"
        }
        else{
//            shufflePlayButton.setTitle("Shuffle Play", for: .normal)
            playlistDesc.text = "Coldplay, Kodaline and many More"
        }

        var requestOptions = RequestOptions()
        requestOptions = requestOptions.transforms(CenterCrop(), RoundedCorners(16))
        Glide.with(this)
            .load(musicModel.image)
            .apply(requestOptions)
            .into(image)


    }

    private fun checkAndShowMessage() {
        if(playList!!.size != 0)
        {
            hasSongsLayout.visibility = View.VISIBLE
            noSongsLayout.visibility = View.GONE
        }
        else
        {
            hasSongsLayout.visibility = View.GONE
        }
    }

    override fun onClick(v: View?) {
        if(v?.id==R.id.noSongsLayout){
            val intent = Intent(this, AddSongsActivity::class.java)
            intent.putExtra(MyConstants.ID,id)
            startActivityForResult(intent,MyConstants.ADD_SONGS_REQUEST_CODE)
        }
        else if (v?.id==R.id.backButton){
            onBackPressed()
        }
        else if (v?.id==R.id.menuButton){
            val popup = PopupMenu(this, menuButton)
            popup.menuInflater.inflate(R.menu.pop_up_menu, popup.menu)
            popup.setOnMenuItemClickListener { item ->
                if (item.itemId == R.id.addRemove) {
                    val intent = Intent(this, AddSongsActivity::class.java)
                    intent.putExtra(MyConstants.ID,id)
                    startActivityForResult(intent,MyConstants.ADD_SONGS_REQUEST_CODE)
                }
                true
            }
            popup.show()

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            MyConstants.ADD_SONGS_REQUEST_CODE -> if (resultCode == RESULT_OK) {
                CommonMethods.showLog(TAG, "OnACTIVITY RESULT")
                loadingDialog!!.showDialog()
                checkAndHitApi()
            }
        }
    }
}