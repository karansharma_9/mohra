package com.napworks.mohra.activitiesPackage

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthOptions
import com.google.firebase.auth.PhoneAuthProvider
import com.napworks.mohra.R
import com.napworks.mohra.dialogPackage.CommonMessageDialog
import com.napworks.mohra.dialogPackage.LoadingDialog
import com.napworks.mohra.interfacePackage.UploadPersonalityInterface
import com.napworks.mohra.modelPackage.CommonStatusModel
import com.napworks.mohra.utilPackage.ApiCalls
import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.utilPackage.MyConstants
import kotlinx.android.synthetic.main.toolbar_layout.*
import java.util.concurrent.TimeUnit

class ForgotPasswordActivity : AppCompatActivity(), View.OnClickListener,
    UploadPersonalityInterface {
    var TAG = javaClass.simpleName;
    lateinit var editTextPhoneNumberForgot: EditText
    lateinit var ResetButton: TextView
    lateinit var textForPhoneCode: TextView
    lateinit var countryCodeLayoutForgotPassword: LinearLayout
    var commonMessageDialog: CommonMessageDialog? = null
    var loadingDialog: LoadingDialog? = null
    private var mAuth: FirebaseAuth? = null
    var storedVerificationId:String =""
    lateinit var phoneNumber:String
    lateinit var codePlusNumber:String
    var phoneCode:String = ""
    var countryId:String = ""
    var userId:String = ""
    lateinit var resendToken: PhoneAuthProvider.ForceResendingToken
    private lateinit var callbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        mAuth = FirebaseAuth.getInstance();

        loadingDialog = LoadingDialog(this)
        commonMessageDialog = CommonMessageDialog(this)
        editTextPhoneNumberForgot = findViewById(R.id.editTextPhoneNumberForgot)
        ResetButton = findViewById(R.id.ResetButton)
        textForPhoneCode = findViewById(R.id.textForPhoneCode)
        countryCodeLayoutForgotPassword = findViewById(R.id.countryCodeLayoutForgotPassword)
        ResetButton.setOnClickListener(this)
        menuBackPress.setOnClickListener(this)
        countryCodeLayoutForgotPassword.setOnClickListener(this)

        callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            override fun onVerificationCompleted(p0: PhoneAuthCredential) {
//                Toast.makeText(this, "Its toast!", Toast.LENGTH_SHORT).show()
                loadingDialog!!.hideDialog()

                CommonMethods.showLog(TAG , " Verification done ")
            }

            override fun onVerificationFailed(p0: FirebaseException) {
//                Toast.makeText(this, "Its toast!", Toast.LENGTH_SHORT).show()
                loadingDialog!!.hideDialog()

                CommonMethods.showLog(TAG , " Verification exception ")

            }

            override fun onCodeSent(
                verificationId: String,
                token: PhoneAuthProvider.ForceResendingToken,
            ) {
                storedVerificationId = verificationId
                resendToken = token
                loadingDialog!!.hideDialog()

                CommonMethods.showLog(TAG,"storedVerificationId => $storedVerificationId  resendToken  => $resendToken")
                val intent = Intent(baseContext, OtpVerifyForgotPasswordActivity::class.java)
                intent.putExtra("storedVerificationId",storedVerificationId)
                intent.putExtra("userId",userId)
                intent.putExtra(MyConstants.CALLED_FROM,MyConstants.FORGOT_PASSWORD)
                intent.putExtra(MyConstants.MOBILE,codePlusNumber)
                intent.putExtra(MyConstants.COUNTRY_ID,countryId)
                intent.putExtra("Mobile_Without_Code",editTextPhoneNumberForgot.text.toString().trim())
                startActivity(intent)
            }
        }


    }

    fun ApiCallVerify()
    {
        loadingDialog!!.showDialog()
        var fullNumber = phoneNumber
        CommonMethods.showLog(TAG,"full Number  => $fullNumber")
        CommonMethods.showLog(TAG,"countryId  => $countryId")
        ApiCalls.checkMobileExist(
            fullNumber,
            countryId,
            this
        )
    }

    private fun sendVerificationCode(number: String) {
        val options = PhoneAuthOptions.newBuilder(mAuth!!)
            .setPhoneNumber(number) // Phone number to verify
            .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
            .setActivity(this) // Activity (for callback binding)
            .setCallbacks(callbacks) // OnVerificationStateChangedCallbacks
            .build()
        PhoneAuthProvider.verifyPhoneNumber(options)
        CommonMethods.showLog("GFG" , "Auth started")
    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.ResetButton) {
            val PhoneNumberText: String = editTextPhoneNumberForgot.text.toString().trim()
            if (TextUtils.isEmpty(PhoneNumberText)) {
                commonMessageDialog?.showDialog(getString(R.string.enterPhone))
                editTextPhoneNumberForgot.requestFocus()
            }
            else if (PhoneNumberText.length != 10) {
                commonMessageDialog?.showDialog(getString(R.string.enterValidPhone))
                editTextPhoneNumberForgot.requestFocus()
            }
            else if (textForPhoneCode.text == "+0") {
                commonMessageDialog?.showDialog(getString(R.string.countryCode))
                editTextPhoneNumberForgot.requestFocus()
            }
            else
            {
                phoneNumber = PhoneNumberText
                codePlusNumber = "+"+phoneCode+phoneNumber
                ApiCallVerify()
            }

        }
        else if(v?.id == R.id.countryCodeLayoutForgotPassword)
        {
            val intent = Intent(baseContext, CountryCodeActivity::class.java)
            startActivityForResult(intent, MyConstants.COUNTRY_REQUEST_CODE)
        }
        else if(v?.id == R.id.menuBackPress)
        {
            onBackPressed()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode== MyConstants.COUNTRY_REQUEST_CODE)
        {
            if(resultCode== RESULT_OK)
            {
                if (data!=null)
                {
                    textForPhoneCode.text = "+"+data.getStringExtra(MyConstants.COUNTRY_CALLING_CODE)
                    textForPhoneCode.setTextColor(resources.getColor(R.color.black));
                    phoneCode = data.getStringExtra(MyConstants.COUNTRY_CALLING_CODE).toString()
                    countryId = data.getStringExtra(MyConstants.COUNTRY_ID).toString()
                }
            }
        }

    }

    override fun onUploadPersonalityInterfaceResponse(
        status: String?,
        commonStatusModel: CommonStatusModel?
    ) {
        when (status) {
            MyConstants.GO_TO_RESPONSE -> {
                CommonMethods.showLog(TAG,"send Otp  status  " + commonStatusModel!!.message)
                if (commonStatusModel != null) {
                    if (commonStatusModel.status != null) {
                        when {

                            commonStatusModel.status.equals("1") -> {
                                sendVerificationCode(codePlusNumber)
                                userId = commonStatusModel.userId.toString()
                                CommonMethods.showLog(TAG,"userID for pass reset  =>  $userId")
//                                Toast.makeText(this, commonStatusModel.message, Toast.LENGTH_SHORT).show()
//                                val intent = Intent(baseContext, OtpVerifyActivity::class.java)
//                                startActivity(intent)
                            }
                            commonStatusModel.status.equals("0") -> {
                                loadingDialog!!.hideDialog()
                                commonMessageDialog!!.showDialog(commonStatusModel.message)
                            }
                            commonStatusModel.status.equals("10") -> {
                                loadingDialog!!.hideDialog()
                                CommonMethods.callLogout(this)
                            }
                            else -> {
                                loadingDialog!!.hideDialog()
                                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                            }
                        }
                    } else {
                        loadingDialog!!.hideDialog()
                        commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                    }
                } else {
                    loadingDialog!!.hideDialog()
                    commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                }
            }
            MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                loadingDialog!!.hideDialog()
                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
            }
        }
    }
}