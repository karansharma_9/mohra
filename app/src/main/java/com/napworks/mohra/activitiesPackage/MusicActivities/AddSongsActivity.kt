package com.napworks.mohra.activitiesPackage.MusicActivities

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.napworks.mohra.R
import com.napworks.mohra.adapterPackage.MusicAdapter.AddSongsRecyclerAdapter
import com.napworks.mohra.adapterPackage.MusicAdapter.PlaylistDetailsRecyclerAdapter
import com.napworks.mohra.dialogPackage.CommonMessageDialog
import com.napworks.mohra.dialogPackage.LoadingDialog
import com.napworks.mohra.interfacePackage.CommonStatusInterface
import com.napworks.mohra.interfacePackage.music.AddSongsAdapterClick
import com.napworks.mohra.interfacePackage.music.GetMusicInterface
import com.napworks.mohra.modelPackage.CommonStatusModel
import com.napworks.mohra.modelPackage.MusicModel.GetMusicModel
import com.napworks.mohra.modelPackage.MusicModel.MusicInnerModel
import com.napworks.mohra.modelPackage.News.LikeUnlikeModel
import com.napworks.mohra.utilPackage.ApiCalls
import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.utilPackage.MyConstants
import org.json.JSONArray

class AddSongsActivity : AppCompatActivity(), GetMusicInterface, AddSongsAdapterClick,
    View.OnClickListener, CommonStatusInterface {
    var TAG = javaClass.simpleName;

    private var sharedPreferences: SharedPreferences? = null
    var loadingDialog: LoadingDialog? = null
    var commonMessageDialog: CommonMessageDialog? = null

    lateinit var recyclerView: RecyclerView
    lateinit var backButton: ImageView


    var musicList : ArrayList<MusicInnerModel>? = null
    var alreadyExistList : ArrayList<MusicInnerModel>? = null
    var id :String = ""


    lateinit var recyclerAdapter : AddSongsRecyclerAdapter
    lateinit var doneButton : TextView




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_songs)

        commonMessageDialog = CommonMessageDialog(this)
        loadingDialog = LoadingDialog(this)
        sharedPreferences = getSharedPreferences(MyConstants.SHARED_PREFERENCE, MODE_PRIVATE)
        id = intent.getStringExtra(MyConstants.ID).toString()


        recyclerView  = findViewById(R.id.recyclerView)
        backButton  = findViewById(R.id.backButton)
        doneButton  = findViewById(R.id.doneButton)
        musicList = ArrayList<MusicInnerModel> ()
        alreadyExistList = ArrayList<MusicInnerModel> ()
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerAdapter = AddSongsRecyclerAdapter(this, musicList!!,this)
        recyclerView.adapter = recyclerAdapter


        hitMusicApi()

        doneButton.setOnClickListener(this)
        backButton.setOnClickListener(this)
    }

    private fun hitMusicApi() {
        loadingDialog!!.showDialog()
        ApiCalls.getMusicData(sharedPreferences!!,this)
    }

    override fun onGetMusicResponse(status: String?, getMusicModel: GetMusicModel?) {
        when (status) {
            MyConstants.GO_TO_RESPONSE -> {
                loadingDialog!!.hideDialog()
                if (getMusicModel != null) {
                    if (getMusicModel.status != null) {
                        CommonMethods.showLog(TAG,"getMusicHomeData Status : "+getMusicModel.status)
                        when {
                            getMusicModel.status.equals("1") -> {

                                musicList!!.addAll(getMusicModel.musicList)
                                CommonMethods.showLog(TAG,"MUSIC LIST SIZE : "+musicList!!.size)

                                checkAndsetUI()
                            }
                            getMusicModel.status.equals("0") -> {
                                CommonMethods.showLog(TAG, getMusicModel.message)
                                commonMessageDialog!!.showDialog(getMusicModel.message)

                            }
                            getMusicModel.status.equals("10") -> {
                                CommonMethods.showLog(TAG, getMusicModel.message)
//                                commonMessageDialog!!.showDialog(createPlaylistResponse.message)
                                CommonMethods.callLogout(this)

                            }
                            else -> {
                                CommonMethods.showLog(TAG,
                                    R.string.someErrorOccurredText.toString())
                                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))

                            }
                        }
                    } else {
                        CommonMethods.showLog(TAG,
                            R.string.someErrorOccurredText.toString())
                    }
                } else {
                    CommonMethods.showLog(TAG,
                        R.string.someErrorOccurredText.toString())
                }
            }
            MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                CommonMethods.showLog(TAG,
                    R.string.someErrorOccurredText.toString())
            }
        }
    }

    private fun checkAndsetUI() {
        if (alreadyExistList!!.size > 0) {
            for (data in musicList!!) {

                CommonMethods.showLog(TAG,"Data : "+data.innerTitle +"Status :"+data.isSelected)
                var isExist = false

                for (alreadyExistModel in alreadyExistList!!) {
                    CommonMethods.showLog(TAG,"alreadyExistModel :"+alreadyExistModel.innerTitle+"Status : "+alreadyExistModel.isSelected)

                    if (alreadyExistModel.innerId == data.innerId) {
                        isExist = true
                    }

                }

                if (isExist) {
                    data.isSelected = true
                }
            }
        }
        recyclerAdapter.notifyDataSetChanged()
    }

    override fun onAddSongsAdapterOnClick(
        status: String?,
        innerModel: MusicInnerModel,
        position: Int
    ) {
        musicList!!.get(position).isSelected = !musicList!!.get(position).isSelected!!
        recyclerAdapter.notifyItemChanged(position)
        var json = prepareJSON()
        CommonMethods.showLog(TAG,"json   $json")
    }

    private fun prepareJSON(): String {
        val jsonArray = JSONArray()
        if (musicList!!.isNotEmpty()) {
            for (i in musicList!!) {
                if(i.isSelected != false) {
                    jsonArray.put(i.innerId)
                }
            }
        }
        return jsonArray.toString()
    }

    override fun onClick(v: View?) {
        if(v?.id==R.id.doneButton){
            val selectedJson = prepareJSON()
            CommonMethods.showLog(TAG,"Selected Json : "+selectedJson)

            if (selectedJson == "[]"){
                commonMessageDialog!!.showDialog(getString(R.string.selectAtLeastOneSong))
            }
            else{
            loadingDialog!!.showDialog()
                ApiCalls.addSongsToPlaylist(sharedPreferences!!,selectedJson,id,this)
            }
        }else if (v?.id==R.id.backButton){
            onBackPressed()
        }
    }

    override fun onCommonStatusInterfaceResponse(
        status: String?,
        commonStatusModel: CommonStatusModel?,
    )
    {
        when (status) {
            MyConstants.GO_TO_RESPONSE -> {
                CommonMethods.showLog(TAG, "addSongs  status  " + commonStatusModel!!.status)
                CommonMethods.showLog(TAG, "addSongs  errorData  " + commonStatusModel!!.errorData)
                if (commonStatusModel != null) {
                    if (commonStatusModel.status != null) {
                        when {
                            commonStatusModel.status.equals("1") -> {
                                Toast.makeText(this, commonStatusModel.message, Toast.LENGTH_SHORT).show()
                                setResult(RESULT_OK)
                                finish()

                            }
                            commonStatusModel.status.equals("0") -> {
                                loadingDialog!!.hideDialog()
                                CommonMethods.showLog(TAG, commonStatusModel.message)
                            }
                            commonStatusModel.status.equals("10") -> {
                                loadingDialog!!.hideDialog()
                                CommonMethods.showLog(TAG, commonStatusModel.message)
                            }
                            else -> {
                                loadingDialog!!.hideDialog()
                                CommonMethods.showLog(TAG,
                                    R.string.someErrorOccurredText.toString())
                            }
                        }
                    } else {
                        loadingDialog!!.hideDialog()
                        CommonMethods.showLog(TAG,
                            R.string.someErrorOccurredText.toString())
                    }
                } else {
                    loadingDialog!!.hideDialog()
                    CommonMethods.showLog(TAG,
                        R.string.someErrorOccurredText.toString())
                }
            }
            MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                CommonMethods.showLog(TAG,
                    R.string.someErrorOccurredText.toString())
            }
        }
    }
}