package com.napworks.mohra.activitiesPackage

import android.Manifest
import android.Manifest.permission
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.napworks.mohra.R
import com.napworks.mohra.adapterPackage.HomePageAdapter
import com.napworks.mohra.fragmentPackage.MainHomeFragment
import com.napworks.mohra.fragmentPackage.MessagesFragment
import com.napworks.mohra.utilPackage.CommonMethods
import kotlinx.android.synthetic.main.activity_home_screen.*
import android.Manifest.permission.GET_ACCOUNTS

import android.Manifest.permission.SEND_SMS

import android.Manifest.permission.RECORD_AUDIO

import androidx.core.app.ActivityCompat
import android.widget.Toast

import android.content.pm.PackageManager
import androidx.core.view.GravityCompat
import kotlinx.android.synthetic.main.fragment_home.*

class HomeScreenActivity : AppCompatActivity(), View.OnClickListener {
    var TAG = javaClass.simpleName;
    var homePageAdapter: HomePageAdapter? = null
    val RequestPermissionCode = 7
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_home_screen)
        homePageAdapter = HomePageAdapter(supportFragmentManager)

        CommonMethods.showLog(TAG,"Home1")

        homePageAdapter!!.add(MainHomeFragment(this), "Page 1")
        homePageAdapter!!.add(MessagesFragment(), "Page 2")
        homePageAdapter!!.add(MessagesFragment(), "Page 3")
        homePageAdapter!!.add(MessagesFragment(), "Page 4")
        homePageAdapter!!.add(MessagesFragment(), "Page 5")

        viewPagerHomePage.adapter = homePageAdapter
        homeBottomBar.setOnClickListener(this)
        messagesBottomBar.setOnClickListener(this)
        momentsBottomBar.setOnClickListener(this)
        shopBottomBar.setOnClickListener(this)
        myLifeBottomBar.setOnClickListener(this)

        RequestMultiplePermission()
    }


    private fun RequestMultiplePermission() {
        ActivityCompat.requestPermissions(this, arrayOf(
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.RECORD_AUDIO,
        ), RequestPermissionCode)
    }



    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {

            RequestPermissionCode -> if (grantResults.size > 0) {
                val CameraPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED
                val FineLocationPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED
                val CoarseLocationPermission = grantResults[2] == PackageManager.PERMISSION_GRANTED
                val RecordAudioPermission = grantResults[3] == PackageManager.PERMISSION_GRANTED

                CommonMethods.showLog(TAG,"CameraPermission    " + CameraPermission + " FineLocationPermission      "+ FineLocationPermission
                +"   CoarseLocationPermission     " +CoarseLocationPermission+" RecordAudioPermission  " +RecordAudioPermission
                )
                if (CameraPermission && FineLocationPermission && CoarseLocationPermission && RecordAudioPermission)
                {
                    CommonMethods.showLog(TAG,"kjbkjbj")
//                    Toast.makeText(this, "Permission Granted", Toast.LENGTH_LONG)
//                        .show()
                } else {
//                    RequestMultiplePermission()
//                    Toast.makeText(this, "Permission Denied", Toast.LENGTH_LONG).show()
                }
            }
        }
    }


    override fun onClick(v: View?) {
        if (v?.id == R.id.homeBottomBar)
        {
            CommonMethods.showLog(TAG, "clicked homeBottomBar")
            viewPagerHomePage.currentItem = 0;
            homePageAdapter!!.notifyDataSetChanged();
//            drawerLayout.openDrawer(GravityCompat.START)
        }
        else if(v?.id == R.id.messagesBottomBar)
        {
            CommonMethods.showLog(TAG, "clicked messagesBottomBar")
            viewPagerHomePage.currentItem = 1;
            homePageAdapter!!.notifyDataSetChanged();
//            drawerLayout.openDrawer(GravityCompat.START)
        }
        else if(v?.id == R.id.momentsBottomBar)
        {
            CommonMethods.showLog(TAG, "clicked momentsBottomBar")
            viewPagerHomePage.currentItem = 2;
            homePageAdapter!!.notifyDataSetChanged();
//            drawerLayout.openDrawer(GravityCompat.START)
        }
        else if(v?.id == R.id.shopBottomBar)
        {
            CommonMethods.showLog(TAG, "clicked shopBottomBar")
            viewPagerHomePage.currentItem = 3;
            homePageAdapter!!.notifyDataSetChanged();
//            drawerLayout.openDrawer(GravityCompat.START)
        }
        else if(v?.id == R.id.myLifeBottomBar)
        {
            CommonMethods.showLog(TAG, "clicked myLifeBottomBar")
            viewPagerHomePage.currentItem = 4;
            homePageAdapter!!.notifyDataSetChanged();
//            drawerLayout.openDrawer(GravityCompat.START)
        }
        }

    fun closeDrawer() {
        CommonMethods.showLog(TAG, "CLOSE")

        drawerLayout.closeDrawer(GravityCompat.START)
    }
}