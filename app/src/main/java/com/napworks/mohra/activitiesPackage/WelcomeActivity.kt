package com.napworks.mohra.activitiesPackage

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.R
import com.napworks.mohra.adapterPackage.WelcomeViewPagerAdapter
import com.napworks.mohra.dialogPackage.CommonMessageDialog
import com.napworks.mohra.dialogPackage.LoadingDialog
import com.napworks.mohra.modelPackage.GetIntroDataInnerModel
import com.napworks.mohra.modelPackage.GetIntroDataModel
import com.napworks.mohra.utilPackage.ApiCalls
import com.google.android.material.tabs.TabLayout
import com.napworks.mohra.utilPackage.MyConstants


class WelcomeActivity : AppCompatActivity(), View.OnClickListener,
    com.napworks.mohra.interfacePackage.GetIntroDataInterface {
    var ViewPagerWelcomePage: ViewPager? = null
    var tabDots: TabLayout? = null
    lateinit var registerButtonWelcomeScreen: TextView
    lateinit var loginButtonWelcomePage: TextView
    lateinit var newExperienceWelcomePage: TextView
    var commonMessageDialog: CommonMessageDialog? = null
    var loadingDialog: LoadingDialog? = null
    var mViewPagerAdapter: WelcomeViewPagerAdapter? = null
    var productList: ArrayList<GetIntroDataInnerModel>? = null
    var TAG = javaClass.simpleName;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
        tabDots = findViewById(R.id.tabDots)
        commonMessageDialog = CommonMessageDialog(this)
        registerButtonWelcomeScreen = findViewById(R.id.registerButtonWelcomeScreen)
        loginButtonWelcomePage = findViewById(R.id.loginButtonWelcomePage)
        newExperienceWelcomePage = findViewById(R.id.newExperienceWelcomePage)
        productList = ArrayList<GetIntroDataInnerModel>()
        registerButtonWelcomeScreen.setOnClickListener(this)
        loginButtonWelcomePage.setOnClickListener(this)

        ViewPagerWelcomePage = findViewById<View>(R.id.viewPagerWelcomePage) as ViewPager

        var text = getString(R.string.new_experience)
        val spannableString = SpannableString(text)
        val green = ForegroundColorSpan(Color.rgb(255,89,33))
        spannableString.setSpan(
            green,
            0, 35, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        newExperienceWelcomePage.text = spannableString

        getIntroData()
    }

    fun getIntroData()
    {
        ApiCalls.getIntroData(
            this
        )
    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.registerButtonWelcomeScreen)
        {
            val intent = Intent(baseContext, RegisterIntroActivity::class.java)
            startActivity(intent)
        }
        else if(v?.id == R.id.loginButtonWelcomePage)
        {
            val intent = Intent(baseContext, LoginActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onGetIntroDataInterfaceResponse(status: String?, commonStatusModel: GetIntroDataModel?, )
    {
        when (status) {
            MyConstants.GO_TO_RESPONSE -> {
                CommonMethods.showLog(TAG,"send Otp  status  " + commonStatusModel!!.message)
                if (commonStatusModel != null) {
                    if (commonStatusModel.status != null) {
                        when {
                            commonStatusModel.status.equals("1") -> {
                                val innerData: ArrayList<GetIntroDataInnerModel> = commonStatusModel.introData
                                productList = innerData
                                mViewPagerAdapter = WelcomeViewPagerAdapter(
                                    this, productList!!, this
                                )
                                ViewPagerWelcomePage!!.adapter = mViewPagerAdapter
                                tabDots?.setupWithViewPager(ViewPagerWelcomePage, true);
                            }
                            commonStatusModel.status.equals("0") -> {
                                loadingDialog!!.hideDialog()
                                commonMessageDialog!!.showDialog(commonStatusModel.message)
                            }
                            commonStatusModel.status.equals("10") -> {
                                loadingDialog!!.hideDialog()
                                CommonMethods.callLogout(this)
                            }
                            else -> {
                                loadingDialog!!.hideDialog()
                                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                            }
                        }
                    } else {
                        loadingDialog!!.hideDialog()
                        commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                    }
                } else {
                    loadingDialog!!.hideDialog()
                    commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                }
            }
            MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                loadingDialog!!.hideDialog()
                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
            }
        }

    }
}