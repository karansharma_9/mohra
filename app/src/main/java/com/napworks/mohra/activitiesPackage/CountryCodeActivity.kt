package com.napworks.mohra.activitiesPackage

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.napworks.mohra.R
import com.napworks.mohra.adapterPackage.CountryCodeRecyclerAdapter
import com.napworks.mohra.dialogPackage.CommonMessageDialog
import com.napworks.mohra.dialogPackage.LoadingDialog
import com.napworks.mohra.interfacePackage.CountryCodeDataInterface
import com.napworks.mohra.modelPackage.*
import com.napworks.mohra.utilPackage.ApiCalls
import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.utilPackage.MyConstants
import kotlinx.android.synthetic.main.activity_country_code.*
import kotlinx.android.synthetic.main.toolbar_layout.*


class CountryCodeActivity : AppCompatActivity(), CountryCodeDataInterface, TextWatcher,
    View.OnClickListener {
    var TAG = javaClass.simpleName;
    var commonMessageDialog: CommonMessageDialog? = null
    var loadingDialog: LoadingDialog? = null
    var innerList: ArrayList<CountryListModel>? = null
    var apiList: ArrayList<CountryListModel>? = null
    lateinit var edtSearch: EditText
    lateinit var cancelSearchButton: ImageView


    private lateinit var countryCodeRecyclerAdapter: CountryCodeRecyclerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_country_code)
        loadingDialog = LoadingDialog(this)
        commonMessageDialog = CommonMessageDialog(this)
        edtSearch = findViewById(R.id.edtSearch)
        cancelSearchButton = findViewById(R.id.cancelSearchButton)
        edtSearch.addTextChangedListener(this)
        cancelSearchButton.setOnClickListener(this)
        menuBackPress.setOnClickListener(this)
        cancelSearchButton.setVisibility(View.GONE);

        innerList = ArrayList<CountryListModel>()
        apiList = ArrayList<CountryListModel>()
        ApiForCountry()

    }

    fun ApiForCountry()
    {loadingDialog!!.showDialog()
        ApiCalls.getCountries(
            this
        )
    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.cancelSearchButton) {
            edtSearch.setText("");
            cancelSearchButton.setVisibility(View.GONE);
        }
        else if(v?.id == R.id.menuBackPress)
        {
            onBackPressed()
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }

    override fun afterTextChanged(s: Editable?) {
        if (edtSearch.text.toString().isNotEmpty()) {
            filter(s.toString());
        }
        else {
            countryCodeRecyclerAdapter.notifyItemRangeRemoved(0 ,innerList!!.size)
            innerList!!.clear()
            innerList!!.addAll(apiList!!)
            countryCodeRecyclerAdapter.notifyDataSetChanged()
        }

    }

    fun updateList(list: ArrayList<CountryListModel>) {

            CommonMethods.showLog(TAG, "size  => " + innerList!!.size)
            innerList!!.addAll(list);
            countryCodeRecyclerAdapter.notifyItemRangeChanged(0, innerList!!.size)

    }



    fun filter(text: String?) {
        CommonMethods.showLog(TAG, "text   $text")
        val temp:  ArrayList<CountryListModel> = ArrayList()
        countryCodeRecyclerAdapter.notifyItemRangeRemoved(0 ,innerList!!.size)
        innerList!!.clear()
        for (d in apiList!!) {
            CommonMethods.showLog(TAG, "country match    $d")
            if (d.countryName!!.toLowerCase().contains(text.toString().toLowerCase())) {
                innerList!!.add(d)
                countryCodeRecyclerAdapter.notifyDataSetChanged()
                CommonMethods.showLog(TAG, "temp   + " + temp.size)

//                updateList(temp)

            }
        }
        //update recyclerview
    }

    override fun onCountryCodeDataInterfaceResponse(
        status: String?,
        countryCodeModel: CountryCodeModel?,
    ) {
        when (status) {
            MyConstants.GO_TO_RESPONSE -> {
                CommonMethods.showLog(TAG,"login  status  " + countryCodeModel!!.message)
                if (countryCodeModel != null) {
                    if (countryCodeModel.status != null) {
                        when {
                            countryCodeModel.status.equals("1") -> {
                                loadingDialog!!.hideDialog()

                                val innerData: ArrayList<CountryListModel> = countryCodeModel.countryList
                                innerList!!.addAll(innerData)
                                apiList!!.addAll(innerData)
                                CommonMethods.showLog(TAG,"list size  country " + innerList!!.size)
                                val linearLayoutManager = LinearLayoutManager(this)
                                linearLayoutManager.reverseLayout = false
                                linearLayoutManager.stackFromEnd = false
                                recyclerViewCountryCode.layoutManager = linearLayoutManager
//                                recyclerViewCountryCode.layoutManager = linearLayoutManager(this,
//                                    LinearLayoutManager.VERTICAL,
//                                    true)

                                countryCodeRecyclerAdapter = CountryCodeRecyclerAdapter(this!!, innerList!!)
                                recyclerViewCountryCode.adapter = countryCodeRecyclerAdapter

                            }
                            countryCodeModel.status.equals("0") -> {
                                loadingDialog!!.hideDialog()
                                commonMessageDialog!!.showDialog(countryCodeModel.message)
                            }
                            countryCodeModel.status.equals("10") -> {
                                loadingDialog!!.hideDialog()
                                CommonMethods.callLogout(this)
                            }
                            else -> {
                                loadingDialog!!.hideDialog()
                                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                            }
                        }
                    } else {
                        loadingDialog!!.hideDialog()
                        commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                    }
                } else {
                    loadingDialog!!.hideDialog()
                    commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                }
            }
            MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                loadingDialog!!.hideDialog()
                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
            }
        }
    }




}