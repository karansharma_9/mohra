package com.napworks.mohra.activitiesPackage.HealthActivities

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.napworks.mohra.R
import kotlinx.android.synthetic.main.activity_health_start.*
import kotlinx.android.synthetic.main.toolbar_layout.*


class HealthStartActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_health_start)
        menuBackPress.setOnClickListener(this)
        startHealthButton.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
      if(v?.id == R.id.menuBackPress)
        {
            onBackPressed()
        }
        else if(v?.id == R.id.startHealthButton)
      {
          val i = Intent(applicationContext, com.napworks.mohra.activitiesPackage.HealthActivities.HealthDeatailActivity::class.java)
          startActivity(i)
      }
    }
}