package com.napworks.mohra.activitiesPackage

import android.annotation.SuppressLint
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.facebook.CallbackManager
import com.facebook.login.LoginManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthOptions
import com.google.firebase.auth.PhoneAuthProvider
import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.R
import com.napworks.mohra.activitiesPackage.PersonalityActivities.WelcomePersonalityTestActivity
import com.napworks.mohra.dialogPackage.CommonMessageDialog
import com.napworks.mohra.dialogPackage.LoadingDialog
import com.napworks.mohra.interfacePackage.FacebookInterfaceCallBack
import com.napworks.mohra.interfacePackage.UploadPersonalityInterface
import com.napworks.mohra.modelPackage.*
import com.napworks.mohra.utilPackage.ApiCalls
import com.napworks.mohra.utilPackage.MyConstants
import kotlinx.android.synthetic.main.toolbar_layout.*
import java.util.*
import java.util.concurrent.TimeUnit

class LoginActivity : AppCompatActivity(), View.OnClickListener,
    com.napworks.mohra.interfacePackage.LoginInterface, FacebookInterfaceCallBack,
    UploadPersonalityInterface {
    private var userId: String = ""
    var TAG = javaClass.simpleName;
    lateinit var loginButton: TextView
    lateinit var recoverPassword: TextView
    lateinit var loginGoogle: TextView
    lateinit var loginFacebook: TextView
    lateinit var emailTextTab: TextView
    lateinit var mobileTextTab: TextView
    lateinit var userNameTextTab: TextView
    lateinit var phoneCodeText: TextView
    var commonMessageDialog: CommonMessageDialog? = null
    var loadingDialog: LoadingDialog? = null
    lateinit var editTextEmailLogin: EditText
    lateinit var editTextPassword: EditText
    lateinit var editTextUserNameLogin: EditText
    lateinit var editTextPhoneNumber: EditText
    lateinit var emailLayout: LinearLayout
    lateinit var mobileLayout: LinearLayout
    lateinit var UsernameLayout: LinearLayout

    lateinit var userNameEditTextLayout: LinearLayout
    lateinit var emailEditTextLayout: LinearLayout
    lateinit var passwordEditTextLayout: LinearLayout
    lateinit var mobileEditTextLayout: LinearLayout

    lateinit var countryCodeLayout: LinearLayout

    private val RC_SIGN_IN = 9001
    private var callbackManager: CallbackManager? = null

    var mGoogleSignInClient: GoogleSignInClient? = null
    private var sharedPreferences: SharedPreferences? = null

    var loginType = MyConstants.EMAIL
    var countryModel = CountryListModel()
    private var mAuth: FirebaseAuth? = null
    var storedVerificationId:String =""
    var finalNumber:String =""
    private lateinit var callbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        loginButton = findViewById(R.id.loginButton)
        loadingDialog = LoadingDialog(this)
        sharedPreferences = getSharedPreferences(MyConstants.SHARED_PREFERENCE, MODE_PRIVATE)
        editTextEmailLogin = findViewById(R.id.editTextEmailLogin)
        recoverPassword = findViewById(R.id.recoverPassword)
        loginFacebook = findViewById(R.id.loginFacebook)
        loginGoogle = findViewById(R.id.loginGoogle)
        editTextPassword = findViewById(R.id.editTextPassword)
        editTextUserNameLogin = findViewById(R.id.editTextUserNameLogin)
        editTextPhoneNumber = findViewById(R.id.editTextPhoneNumber)
        UsernameLayout = findViewById(R.id.UsernameLayout)
        emailLayout = findViewById(R.id.emailLayout)
        mobileLayout = findViewById(R.id.mobileLayout)

        emailTextTab = findViewById(R.id.emailTextTab)
        mobileTextTab = findViewById(R.id.mobileTextTab)
        userNameTextTab = findViewById(R.id.userNameTextTab)

        userNameEditTextLayout = findViewById(R.id.userNameEditTextLayout)
        emailEditTextLayout = findViewById(R.id.emailEditTextLayout)
        passwordEditTextLayout = findViewById(R.id.passwordEditTextLayout)
        mobileEditTextLayout = findViewById(R.id.mobileEditTextLayout)

        countryCodeLayout = findViewById(R.id.countryCodeLayout)
        phoneCodeText = findViewById(R.id.phoneCodeText)
        checkAndSetUI()

        mAuth = FirebaseAuth.getInstance();

        var gso: GoogleSignInOptions? = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build()//
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mGoogleSignInClient!!.signOut()
        callbackManager = CallbackManager.Factory.create()

        commonMessageDialog = CommonMessageDialog(this)
        loginButton.setOnClickListener(this)
        menuBackPress.setOnClickListener(this)
        loginGoogle.setOnClickListener(this)
        loginFacebook.setOnClickListener(this)
        recoverPassword.setOnClickListener(this)

        UsernameLayout.setOnClickListener(this)
        mobileLayout.setOnClickListener(this)
        emailLayout.setOnClickListener(this)

        countryCodeLayout.setOnClickListener(this)


        callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            override fun onVerificationCompleted(p0: PhoneAuthCredential) {
                loadingDialog!!.hideDialog()
                CommonMethods.showLog(TAG , " Verification done ")
            }

            override fun onVerificationFailed(p0: FirebaseException) {
                loadingDialog!!.hideDialog()
                CommonMethods.showLog(TAG , " Verification exception "+p0.message)
            }

            override fun onCodeSent(
                verificationId: String,
                token: PhoneAuthProvider.ForceResendingToken,
            ) {
                storedVerificationId = verificationId
                loadingDialog!!.hideDialog()

                CommonMethods.showLog(TAG,"storedVerificationId => $storedVerificationId")
                val intent = Intent(baseContext, OtpVerifyForgotPasswordActivity::class.java)
                intent.putExtra("storedVerificationId",storedVerificationId)
                intent.putExtra("userId",userId)
                intent.putExtra(MyConstants.CALLED_FROM,MyConstants.LOGIN)
                intent.putExtra(MyConstants.MOBILE,finalNumber)
                intent.putExtra(MyConstants.COUNTRY_ID,countryModel.countryId)
                intent.putExtra("Mobile_Without_Code",editTextPhoneNumber.text.toString().trim())
                startActivity(intent)
            }
        }
    }

    fun loginApi(Email: String,
                 Password: String,
                 googleId: String?,
                 facebookId: String,
                 type: String,
                 firstname: String,
                 familyname: String,
                 mobile:String,
                 userName:String,
                 countryId:String)
    {
        CommonMethods.showLog(TAG,"")
        loadingDialog!!.showDialog()
        ApiCalls.login(
            Email,
            Password,
            type,
            googleId,
            facebookId,
            firstname,
            familyname,
            mobile,
            0,
            "",
            userName,
            countryId,
            this

        )
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onClick(v: View?) {
        if (v?.id == R.id.loginButton) {
            if (loginType.equals(MyConstants.MOBILE)){
                val countryCodeValue: String = phoneCodeText.text.toString().trim()
                val mobileValue: String = editTextPhoneNumber.text.toString().trim()
                if (TextUtils.isEmpty(countryCodeValue)) {
                    commonMessageDialog?.showDialog(getString(R.string.chooseCountryCallingCode))
                } else if (TextUtils.isEmpty(mobileValue)) {
                    commonMessageDialog?.showDialog(getString(R.string.enterMobileNumber))
                    editTextPhoneNumber.requestFocus()
                }
                else{
                    var fullNumber = countryCodeValue.plus(mobileValue)
                    CommonMethods.showLog(TAG,"full Number  => $fullNumber")
                    CommonMethods.showLog(TAG,"countryId  => ${countryModel.countryId}")
                    loadingDialog!!.showDialog()
                    ApiCalls.checkMobileExist(
                        mobileValue,
                        countryModel.countryId,
                        this
                    )
                }
            }
            else if (loginType == MyConstants.USERNAME){
                val editTextUserNameLoginText: String = editTextUserNameLogin.text.toString().trim()
                val editTextPasswordText: String = editTextPassword.text.toString().trim()
                CommonMethods.showLog(TAG,"email  = >  $editTextUserNameLoginText  password  =>    $editTextPasswordText")

                if (TextUtils.isEmpty(editTextUserNameLoginText)) {
                    commonMessageDialog?.showDialog(getString(R.string.enterUserName))
                    editTextEmailLogin.requestFocus()
                } else if (TextUtils.isEmpty(editTextPasswordText)) {
                    commonMessageDialog?.showDialog(getString(R.string.enterPassword))
                    editTextPassword.requestFocus()
                } else {
                    loginApi(
                        "",
                        editTextPasswordText,
                        "",
                        "",
                        MyConstants.USERNAME,
                        "",
                        "",
                        "",
                        editTextUserNameLoginText,
                        ""
                    )
                }

            }
            else{
                val editTextEmailLoginText: String = editTextEmailLogin.text.toString().trim()
                val editTextPasswordText: String = editTextPassword.text.toString().trim()
                CommonMethods.showLog(TAG,"email  = >  $editTextEmailLoginText  password  =>    $editTextPasswordText")
                var email_format = Patterns.EMAIL_ADDRESS.matcher(editTextEmailLoginText).matches()
                if (TextUtils.isEmpty(editTextEmailLoginText)) {
                    commonMessageDialog?.showDialog(getString(R.string.enterEmail))
                    editTextEmailLogin.requestFocus()
                } else if (!email_format) {
                    commonMessageDialog?.showDialog(getString(R.string.email_format))
                    editTextEmailLogin.requestFocus()
                } else if (TextUtils.isEmpty(editTextPasswordText)) {
                    commonMessageDialog?.showDialog(getString(R.string.enterPassword))
                    editTextPassword.requestFocus()
                } else {
                    loginApi(
                        editTextEmailLoginText,
                        editTextPasswordText,
                        "",
                        "",
                        MyConstants.EMAIL,
                        "",
                        "",
                        "",
                        "",
                        ""
                    )
//                val intent = Intent(baseContext, OtpVerifyActivity::class.java)
//                startActivity(intent)
                }
            }

        }
        else if(v?.id == R.id.menuBackPress)
        {
            onBackPressed()
        }
        else if(v?.id == R.id.recoverPassword)
        {
                val intent = Intent(baseContext, ForgotPasswordActivity::class.java)
                startActivity(intent)
        }
        else if(v?.id == R.id.loginGoogle)
        {
            val signInIntent = mGoogleSignInClient!!.signInIntent
            startActivityForResult(signInIntent, RC_SIGN_IN)
        }
        else if(v?.id == R.id.loginFacebook)
        {
            LoginManager.getInstance().logOut()
            LoginManager.getInstance()
                .logInWithReadPermissions(this, Arrays.asList("public_profile", "email"))
            CommonMethods.loginWithFacebook(callbackManager, this)
        }
        else if(v?.id == R.id.countryCodeLayout)
        {
            val intent = Intent(baseContext, CountryCodeActivity::class.java)
            startActivityForResult(intent, MyConstants.COUNTRY_REQUEST_CODE)
        }
        else if(v?.id == R.id.UsernameLayout)
        {
            loginType = MyConstants.USERNAME
            checkAndSetUI()
        }
        else if(v?.id == R.id.mobileLayout)
        {
            loginType = MyConstants.MOBILE
            checkAndSetUI()
        }
        else if(v?.id == R.id.emailLayout)
        {
            loginType = MyConstants.EMAIL
            checkAndSetUI()
        }
    }

    override fun onUploadPersonalityInterfaceResponse(
        status: String?,
        commonStatusModel: CommonStatusModel?
    ) {
        when (status) {
            MyConstants.GO_TO_RESPONSE -> {
                CommonMethods.showLog(TAG,"send Otp  status  " + commonStatusModel!!.message)
                if (commonStatusModel != null) {
                    if (commonStatusModel.status != null) {
                        when {

                            commonStatusModel.status.equals("1") -> {
                                finalNumber = "+".plus(countryModel.phoneCode).plus(editTextPhoneNumber.text.toString().trim())
                                sendVerificationCode(finalNumber)
                                userId = commonStatusModel.userId.toString()
                                CommonMethods.showLog(TAG,"userID for pass reset  =>  $userId")
                            }
                            commonStatusModel.status.equals("0") -> {
                                loadingDialog!!.hideDialog()
                                commonMessageDialog!!.showDialog(commonStatusModel.message)
                            }
                            commonStatusModel.status.equals("10") -> {
                                loadingDialog!!.hideDialog()
                                CommonMethods.callLogout(this)
                            }
                            else -> {
                                loadingDialog!!.hideDialog()
                                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                            }
                        }
                    } else {
                        loadingDialog!!.hideDialog()
                        commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                    }
                } else {
                    loadingDialog!!.hideDialog()
                    commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                }
            }
            MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                loadingDialog!!.hideDialog()
                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
            }
        }
    }

    private fun sendVerificationCode(number: String) {
        val options = PhoneAuthOptions.newBuilder(mAuth!!)
            .setPhoneNumber(number) // Phone number to verify
            .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
            .setActivity(this) // Activity (for callback binding)
            .setCallbacks(callbacks) // OnVerificationStateChangedCallbacks
            .build()
        PhoneAuthProvider.verifyPhoneNumber(options)
        CommonMethods.showLog("GFG" , "Auth started")
    }


    fun checkAndSetUI(){
        if (loginType.equals(MyConstants.MOBILE)){
            mobileLayout.setBackground(ActivityCompat.getDrawable(this, R.drawable.layout_filled_center));
            UsernameLayout.setBackground(ActivityCompat.getDrawable(this, R.drawable.layout_right_tab));
            emailLayout.setBackground(ActivityCompat.getDrawable(this, R.drawable.layout_left_tab));

            emailTextTab.setTextColor(ContextCompat.getColor(this,R.color.black))
            mobileTextTab.setTextColor(ContextCompat.getColor(this,R.color.white))
            userNameTextTab.setTextColor(ContextCompat.getColor(this,R.color.black))

            userNameEditTextLayout.visibility = View.GONE
            emailEditTextLayout.visibility = View.GONE
            passwordEditTextLayout.visibility = View.GONE
            mobileEditTextLayout.visibility = View.VISIBLE
        }
        else if (loginType.equals(MyConstants.USERNAME)){
            editTextPassword.setText("")
            UsernameLayout.setBackground(ActivityCompat.getDrawable(this, R.drawable.layout_filled_right_tab));
            mobileLayout.setBackground(ActivityCompat.getDrawable(this, R.drawable.layouttabscenter));
            emailLayout.setBackground(ActivityCompat.getDrawable(this, R.drawable.layout_left_tab));

            emailTextTab.setTextColor(ContextCompat.getColor(this,R.color.black))
            mobileTextTab.setTextColor(ContextCompat.getColor(this,R.color.black))
            userNameTextTab.setTextColor(ContextCompat.getColor(this,R.color.white))

            userNameEditTextLayout.visibility = View.VISIBLE
            emailEditTextLayout.visibility = View.GONE
            passwordEditTextLayout.visibility = View.VISIBLE
            mobileEditTextLayout.visibility = View.GONE
        }
        else{
            editTextPassword.setText("")
            emailLayout.setBackground(ActivityCompat.getDrawable(this, R.drawable.layout_filled_left_tab));
            mobileLayout.setBackground(ActivityCompat.getDrawable(this, R.drawable.layouttabscenter));
            UsernameLayout.setBackground(ActivityCompat.getDrawable(this, R.drawable.layout_right_tab));

            emailTextTab.setTextColor(ContextCompat.getColor(this,R.color.white))
            mobileTextTab.setTextColor(ContextCompat.getColor(this,R.color.black))
            userNameTextTab.setTextColor(ContextCompat.getColor(this,R.color.black))

            userNameEditTextLayout.visibility = View.GONE
            emailEditTextLayout.visibility = View.VISIBLE
            passwordEditTextLayout.visibility = View.VISIBLE
            mobileEditTextLayout.visibility = View.GONE
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager?.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            CommonMethods.showLog(TAG, "RESULT CODE GOOGLE: $resultCode  ");
            if (resultCode == RESULT_OK) {
                val task = GoogleSignIn.getSignedInAccountFromIntent(data)
                handleSignInResult(task)
            } else if (resultCode == RESULT_CANCELED) {
                loadingDialog!!.hideDialog()
                commonMessageDialog?.showDialog(getString(R.string.googleFailedMessage))
            }
        }
        else if (requestCode== MyConstants.COUNTRY_REQUEST_CODE)
        {
            if(resultCode== RESULT_OK)
            {
                if (data!=null)
                {
                    countryModel = data.getSerializableExtra(MyConstants.COUNTRY_MODEL) as CountryListModel
                    phoneCodeText.text = "+".plus(countryModel.phoneCode)
                    phoneCodeText.setTextColor(resources.getColor(R.color.black));
//                    phoneCodeText.text = "+"+data.getStringExtra(MyConstants.COUNTRY_CALLING_CODE)
//                    phoneCodeText.setTextColor(resources.getColor(R.color.black));
//                    phoneCode = data.getStringExtra(MyConstants.COUNTRY_CALLING_CODE).toString()
//                    countryId = data.getStringExtra(MyConstants.COUNTRY_ID).toString()
                }
            }
        }

    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            if (account != null) {
                CommonMethods.showLog(TAG, "Email" + account.account)
                CommonMethods.showLog(TAG, "DisplayName" + account.displayName)
                CommonMethods.showLog(TAG, "getEmail" + account.email)
                CommonMethods.showLog(TAG, "getFamilyName" + account.familyName)
                CommonMethods.showLog(TAG, "getGivenName" + account.givenName)
                CommonMethods.showLog(TAG, "Id" + account.id)
                CommonMethods.showLog(TAG, "zab" + account.zab())
//                CommonMethods.showLog(TAG, "zac" + account.zac())
                CommonMethods.showLog(TAG, "getPhotoUrl" + account.photoUrl)
                loginApi(account.email!!,"",account.id,"",MyConstants.GOOGLE,account.displayName,account.familyName,"","","")

            } else {
                loadingDialog!!.hideDialog()
                mGoogleSignInClient!!.signOut()
                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
            }
        } catch (e: ApiException) {
            loadingDialog!!.hideDialog()
            commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
            CommonMethods.showLog(TAG, "Google sign in failed" + e.statusCode)
        }
    }

    override fun onLoginInterfaceResponse(status: String?, commonStatusModel: LoginModel?) {
        when (status) {
            MyConstants.GO_TO_RESPONSE -> {
                CommonMethods.showLog(TAG,"login  status  " + commonStatusModel!!.message)
                if (commonStatusModel != null) {
                    if (commonStatusModel.status != null) {
                        when {
                            commonStatusModel.status.equals("1") -> {
                                loadingDialog!!.hideDialog()
                                val innerData: LoginInnerModel = commonStatusModel.userData
                                CommonMethods.showLog(TAG,"usertype    " + innerData.userType)
                                CommonMethods.showLog(TAG,"userid    " + innerData.userId)
                                if(innerData.userType == null)
                                {
                                    CommonMethods.showLog(TAG,"Udated it " + innerData.userId)
                                    val editor = sharedPreferences!!.edit()

                                    editor.putString(MyConstants.AUTH, innerData.auth)
                                    editor.putString(MyConstants.SESSION_TOKEN, innerData.session)
                                    editor.putString(MyConstants.USER_ID, innerData.userId)
                                    editor.putString(MyConstants.NAME, innerData.firstName)
                                    editor.putString(MyConstants.EMAIL, innerData.email)
                                    editor.putString(MyConstants.GOOGLE_ID, innerData.googleId)
                                    editor.putString(MyConstants.FACEBOOK_ID, innerData.facebookId)
                                    editor.putString(MyConstants.FIRST_NAME, innerData.firstName)
                                    editor.putInt(MyConstants.VERIFY, innerData.isPersonalityVerified!!)
                                    editor.putBoolean(MyConstants.IS_LOGGED_IN, true)
                                    editor.apply()
                                    val intent = Intent(this, RegisterIntroActivity::class.java)
                                    intent.putExtra(MyConstants.USER_ID,innerData.userId) // getText() SHOULD NOT be static!!!
                                    startActivity(intent)
                                    finishAffinity()
                                }
                                else
                                {
                                val editor = sharedPreferences!!.edit()
                                editor.putString(MyConstants.AUTH, innerData.auth)
                                editor.putString(MyConstants.SESSION_TOKEN, innerData.session)
                                editor.putString(MyConstants.USER_ID, innerData.userId)
                                editor.putString(MyConstants.NAME, innerData.firstName)
                                editor.putString(MyConstants.EMAIL, innerData.email)
                                editor.putString(MyConstants.GOOGLE_ID, innerData.googleId)
                                editor.putString(MyConstants.FACEBOOK_ID, innerData.facebookId)
                                editor.putString(MyConstants.FIRST_NAME, innerData.firstName)
                                editor.putString(MyConstants.USERTYPE, innerData.userType)
                                editor.putString(MyConstants.USERNAME, innerData.userName)
                                editor.putInt(MyConstants.VERIFY, innerData.isPersonalityVerified!!)
                                editor.putString(MyConstants.FAMILY_NAME,innerData.familyName)
                                editor.putString(MyConstants.MOBILE,innerData.mobile)
                                editor.putString(MyConstants.COUNTRY_ID,innerData.countryId)
                                editor.putString(MyConstants.TYPE,innerData.loginType)
                                editor.putInt(MyConstants.IS_PASSWORD_ENTERED, innerData.isPasswordEntered!!)
                                editor.putLong(MyConstants.DATEOFBIRTH, innerData.birthDate!!)
                                editor.putInt(MyConstants.IS_PASSWORD_ENTERED, innerData.isPasswordEntered!!)

                                editor.putBoolean(MyConstants.IS_LOGGED_IN, true)
                                editor.apply()
                                CommonMethods.showLog(TAG,"USERID ======>  "+innerData.userId )
                                    checkAndHandleView(innerData)
//                                if(innerData.isPersonalityVerified == 0)
//                                {
//                                    CommonMethods.showLog(TAG,"verified not")
//                                    Toast.makeText(this, commonStatusModel.message, Toast.LENGTH_SHORT).show()
//                                    val intent = Intent(baseContext, WelcomePersonalityTestActivity::class.java)
//                                    startActivity(intent)
//                                    finishAffinity()
//                                }
//                                else
//                                {
//                                    CommonMethods.showLog(TAG,"verified ")
//                                    Toast.makeText(this, commonStatusModel.message, Toast.LENGTH_SHORT).show()
//                                    val intent = Intent(baseContext, HomeScreenActivity::class.java)
//                                    startActivity(intent)
//                                    finishAffinity()
//                                }
                                }
                            }
                            commonStatusModel.status.equals("0") -> {
                                loadingDialog!!.hideDialog()
                                commonMessageDialog!!.showDialog(commonStatusModel.message)
                            }
                            commonStatusModel.status.equals("10") -> {
                                loadingDialog!!.hideDialog()
                                CommonMethods.callLogout(this)
                            }
                            else -> {
                                loadingDialog!!.hideDialog()
                                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                            }
                        }
                    } else {
                        loadingDialog!!.hideDialog()
                        commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                    }
                } else {
                    loadingDialog!!.hideDialog()
                    commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                }
            }
            MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                loadingDialog!!.hideDialog()
                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
            }
        }
    }

    private fun checkAndHandleView(innerData: LoginInnerModel) {
        if(innerData.userType==null||innerData.userType == "")
        {
            CommonMethods.showLog(TAG,"1 hit")

            val intent = Intent(baseContext, RegisterIntroActivity::class.java)
            intent.putExtra(MyConstants.USER_ID,userId) // getText() SHOULD NOT be static!!!
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)
        }
        else if(innerData.userName==null||innerData.userName == "")
        {
            CommonMethods.showLog(TAG,"2 hit")

            val intent = Intent(baseContext, SetUserNameActivity::class.java)
            intent.putExtra(MyConstants.UDATEUSERNAME,"true") // getText() SHOULD NOT be static!!!
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)
        }
        else if(innerData.isPersonalityVerified==0)
        {
            CommonMethods.showLog(TAG,"3 hit")
            val intent = Intent(baseContext, WelcomePersonalityTestActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)
        }
        else
        {
            val intent = Intent(baseContext, HomeScreenActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)
        }

    }

    override fun getFacebookCall(status: String?, facebookDataModel: FacebookDataModel?) {
        if (status.equals("0", ignoreCase = true)) {
            loadingDialog!!.hideDialog()
            commonMessageDialog!!.showDialog(getString(R.string.facebookFailedMessage))

        } else if (status.equals("1")) {
            if (facebookDataModel != null) {
                CommonMethods.showLog(TAG, "name " + facebookDataModel.accountId)
                CommonMethods.showLog(TAG, "email " + facebookDataModel.email)
                CommonMethods.showLog(TAG, "profilePhoto  " + facebookDataModel.profileImage)
                CommonMethods.showLog(TAG, "firstName " + facebookDataModel.firstName)
                CommonMethods.showLog(TAG, "LastName   " + facebookDataModel.lastName)
                CommonMethods.showLog(TAG, "username   " + facebookDataModel.userName)
                val firstName: String = facebookDataModel.firstName!!
                val lastName: String = facebookDataModel.lastName!!
                val name: String = "$firstName $lastName"
                var image: String? = ""
                if (facebookDataModel.profileImage != null) {
                    image = facebookDataModel.profileImage
                }
                if (facebookDataModel.email != null && !facebookDataModel.email!!.isEmpty()) {
                    loginApi(facebookDataModel.email!!,"","",facebookDataModel.accountId.toString(),MyConstants.FACEBOOK,facebookDataModel.firstName.toString(),facebookDataModel.lastName.toString(),"","","")
                } else {
                    loadingDialog!!.hideDialog()
                    commonMessageDialog!!.showDialog(getString(R.string.facebookFailedMessage))
                }
            } else {
                loadingDialog!!.hideDialog()
                commonMessageDialog!!.showDialog(getString(R.string.facebookFailedMessage))
            }
        }
    }
}