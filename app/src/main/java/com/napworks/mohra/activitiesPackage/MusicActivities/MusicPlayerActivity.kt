package com.napworks.mohra.activitiesPackage.MusicActivities

import android.content.SharedPreferences
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.napworks.mohra.R
import com.napworks.mohra.modelPackage.MusicModel.MusicFilesModel
import com.wang.avi.AVLoadingIndicatorView
import me.tankery.lib.circularseekbar.CircularSeekBar


class MusicPlayerActivity : AppCompatActivity() {
    var TAG = javaClass.simpleName;

    private var sharedPreferences: SharedPreferences? = null
    var playAndPauseButtonImage: ImageView? = null
    var backPressMusicPlayer: ImageView? = null
    var currentTime: TextView? = null
    var totalTime: TextView? = null
    var loading: AVLoadingIndicatorView? = null
    var seekbarMusicPlayerHorizontal: SeekBar? = null
    var circularProgressBar: CircularSeekBar? = null
    var mediaPlayer: MediaPlayer? = null
    var musicMainActivity: MusicMainActivity? = null
    var handler: Handler = Handler()
    var playPosition : Int = 0
    var getAllMusic: ArrayList<MusicFilesModel>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_music_player)
//        sharedPreferences = getSharedPreferences(MyConstants.SHARED_PREFERENCE, MODE_PRIVATE)
//        playAndPauseButtonImage = findViewById(R.id.playAndPauseButtonImage)
//        backPressMusicPlayer = findViewById(R.id.backPressMusicPlayer)
//        currentTime = findViewById(R.id.currentTime)
//        totalTime = findViewById(R.id.totalTime)
//        seekbarMusicPlayerHorizontal = findViewById(R.id.seekbarMusicPlayerHorizontal)
//        circularProgressBar = findViewById(R.id.circularProgressBar)
//        loading = findViewById(R.id.loading)
//       mediaPlayer  = MediaPlayer()
//        musicMainActivity = MusicMainActivity()
//        getAllMusic = ArrayList<MusicFilesModel>()
//        seekbarMusicPlayerHorizontal!!.max = 100
//        circularProgressBar!!.max = 100F
//        playAndPauseButtonImage!!.setOnClickListener(this)
//        Glide.with(this).load(R.drawable.ed)
//            .apply(RequestOptions().circleCrop())
//            .into(imageMusicPlayer)
//        prepareMediaPlayer()
//
//        seekbarMusicPlayerHorizontal!!.setOnTouchListener(object : View.OnTouchListener {
//                override fun onTouch(v: View?, event: MotionEvent?): Boolean {
//                    when (event?.action) {
//                        MotionEvent.ACTION_DOWN -> seekBar(v)
//                    }
//                    return v?.onTouchEvent(event) ?: false
//                }
//            })
//        backPressMusicPlayer!!.setOnClickListener(this)
    }

//    fun seekBar(v: View?)
//    {
//        var seekbar : SeekBar = v as SeekBar
//        playPosition = (mediaPlayer!!.duration / 100 * seekbar!!.progress).toInt()
//        CommonMethods.showLog(TAG,"  playPosition  $playPosition ")
//        mediaPlayer!!.seekTo(playPosition)
//        currentTime!!.text = millisecondsToTime(mediaPlayer!!.currentPosition.toLong())
//    }
//
//    val updater: Runnable = object : Runnable{
//        override fun run() {
//            updateSeekbar()
//            var currentDuration = mediaPlayer!!.currentPosition
//            currentTime!!.text = millisecondsToTime(currentDuration.toLong())
//        }
//    }
//    fun updateSeekbar(){
//        if (mediaPlayer!!.isPlaying)
//        {
//            var progress : Int = (mediaPlayer!!.currentPosition * 100) / mediaPlayer!!.duration
//            seekbarMusicPlayerHorizontal!!.progress = progress
//            circularProgressBar!!.progress = progress.toFloat()
//            handler.post(updater)
//        }
//    }

//    fun  millisecondsToTime(  milliSeconds : Long) : String
//    {
//        var timerString = ""
//        var secondsString : String?
//        var hours : Int = (milliSeconds /(1000 * 60 * 60)).toInt()
//        var minutes : Int= (milliSeconds % (1000 * 60 * 60) / (1000 * 60)).toInt()
//        var seconds : Int = (milliSeconds % (1000 * 60 * 60) % (1000 * 60) / 1000).toInt()
//
//        if (hours > 0 )
//        {
//            timerString = "$hours:"
//        }
//        if(seconds < 10)
//        {
//            secondsString = "0$seconds"
//        }
//        else
//        {
//            secondsString = "$seconds"
//        }
//
//        timerString = timerString + minutes + ":" + secondsString
//        return timerString
//    }

//    fun prepareMediaPlayer()
//    {
//        try
//        {
//            Handler().postDelayed({
//            CommonMethods.showLog(TAG,"hit player")
//            val uri: Uri = Uri.parse("https://firebasestorage.googleapis.com/v0/b/fir-4e33c.appspot.com/o/Mere%20Naam%20Tu%20-%20Zero%20320%20Kbps.mp3?alt=media&token=1325552c-a9b1-4c7a-827c-437d1e5ee791")
//            mediaPlayer!!.setAudioStreamType(AudioManager.STREAM_MUSIC)
//            mediaPlayer!!.setDataSource(this, uri)
//            mediaPlayer!!.prepare()
//            mediaPlayer!!.start()
//                val editor = sharedPreferences!!.edit()
//                editor.putBoolean(MyConstants.MUSIC_PLAYING, true)
//                editor.apply()
//            CommonMethods.showLog(TAG,"hit player 2")
//            loading!!.visibility = View.GONE
//            playAndPauseButtonImage!!.setBackgroundResource(R.drawable.pause)
//            totalTime!!.text = millisecondsToTime(mediaPlayer!!.duration.toLong())
//            var currentDuration = mediaPlayer!!.currentPosition
//            handler.post(updater)
//            currentTime!!.text = millisecondsToTime(currentDuration.toLong())
//            }, 2000)
//        }
//        catch(e: Exception)
//        {
//            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show()
//        }
//    }

//    override fun onBackPressed() {
//        super.onBackPressed()
//        CommonMethods.showLog(TAG,"clicked backpress")
//        musicMainActivity!!.miniPlayerCall()
//    }

//    override fun onClick(v: View?) {
//        if (v?.id == R.id.playAndPauseButtonImage) {
//            if(mediaPlayer!!.isPlaying)
//            {
//                CommonMethods.showLog(TAG,"Music is paused ")
//                handler.removeCallbacks(updater)
//                mediaPlayer!!.pause()
//                val editor = sharedPreferences!!.edit()
//                editor.putBoolean(MyConstants.MUSIC_PLAYING, false)
//                editor.apply()
//                playAndPauseButtonImage!!.setBackgroundResource(R.drawable.play)
//            }
//            else
//            {
//                handler.post(updater)
//                CommonMethods.showLog(TAG,"Music is Started ")
//                mediaPlayer!!.start()
//                val editor = sharedPreferences!!.edit()
//                editor.putBoolean(MyConstants.MUSIC_PLAYING, true)
//                editor.apply()
//                playAndPauseButtonImage!!.setBackgroundResource(R.drawable.pause)
//            }
//        }
//        else if(v?.id == R.id.backPressMusicPlayer)
//        {
//            onBackPressed()
//        }
//    }
}