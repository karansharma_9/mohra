package com.napworks.mohra.activitiesPackage

import android.R.attr.*
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.facebook.CallbackManager
import com.facebook.login.LoginManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthOptions
import com.google.firebase.auth.PhoneAuthProvider
import com.napworks.mohra.R
import com.napworks.mohra.activitiesPackage.PersonalityActivities.WelcomePersonalityTestActivity
import com.napworks.mohra.dialogPackage.CommonMessageDialog
import com.napworks.mohra.dialogPackage.LoadingDialog
import com.napworks.mohra.interfacePackage.CommonStatusInterface
import com.napworks.mohra.interfacePackage.FacebookInterfaceCallBack
import com.napworks.mohra.interfacePackage.LoginInterface
import com.napworks.mohra.interfacePackage.SignUpInterface
import com.napworks.mohra.modelPackage.*
import com.napworks.mohra.utilPackage.ApiCalls
import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.utilPackage.MyConstants
import java.util.*
import java.util.concurrent.TimeUnit


class RegisterGoogleActivity : AppCompatActivity(), View.OnClickListener,
    com.napworks.mohra.interfacePackage.SendOtpInterface, CommonStatusInterface,
    FacebookInterfaceCallBack, LoginInterface {

    var TAG = javaClass.simpleName;

    lateinit var registerGoogleNextButton: TextView
    lateinit var registerGoogle: TextView
    lateinit var registerFacebook: TextView
    lateinit var phoneCodeText: TextView
    lateinit var skipButtonToolBar: TextView
    lateinit var editTextPasswordRegister: EditText
    lateinit var editTextReEnterPasswordRegister: EditText
    lateinit var editTextEmailRegister: EditText
    lateinit var editTextPhoneNumber: EditText
    lateinit var menuBackPress: ImageView
    lateinit var countryCodeLayout: LinearLayout
    private var mAuth: FirebaseAuth? = null
    lateinit var storedVerificationId:String
    var mGoogleSignInClient: GoogleSignInClient? = null
    lateinit var resendToken: PhoneAuthProvider.ForceResendingToken
    private var callbackManager: CallbackManager? = null
    private var sharedPreferences: SharedPreferences? = null

    var loadingDialog: LoadingDialog? = null
    var phoneCode: String? = null
    var countryId: String? = null
    var phoneNumber: String? = null


    private lateinit var userData: UserDataModel
    var commonMessageDialog: CommonMessageDialog? = null
    private val RC_SIGN_IN = 9001

    private lateinit var callbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_google)
        FirebaseApp.initializeApp(this)

        mAuth = FirebaseAuth.getInstance();
        callbackManager = CallbackManager.Factory.create()
        sharedPreferences = getSharedPreferences(MyConstants.SHARED_PREFERENCE, MODE_PRIVATE)
        var gso: GoogleSignInOptions? = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build()//
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mGoogleSignInClient!!.signOut()
        registerGoogleNextButton = findViewById(R.id.registerGoogleNextButton)
        commonMessageDialog = CommonMessageDialog(this)
        userData = UserDataModel()
        loadingDialog = LoadingDialog(this)
        editTextEmailRegister = findViewById(R.id.editTextEmailRegister)
        registerGoogle = findViewById(R.id.registerGoogle)
        registerFacebook = findViewById(R.id.registerFacebook)
        editTextPhoneNumber = findViewById(R.id.editTextPhoneNumber)
        editTextPasswordRegister = findViewById(R.id.editTextPasswordRegister)
        editTextReEnterPasswordRegister = findViewById(R.id.editTextReEnterPasswordRegister)
        skipButtonToolBar = findViewById(R.id.skipButtonToolBar)
        menuBackPress = findViewById(R.id.menuBackPress)
        phoneCodeText = findViewById(R.id.phoneCodeText)
        countryCodeLayout = findViewById(R.id.countryCodeLayout)
        userData = intent.getSerializableExtra("CLIENT_DATA_FROM_INTRO_SCREEN") as UserDataModel
        registerGoogleNextButton.setOnClickListener(this)
        registerGoogle.setOnClickListener(this)
        menuBackPress.setOnClickListener(this)
        countryCodeLayout.setOnClickListener(this)
        registerFacebook.setOnClickListener(this)
        skipButtonToolBar.isVisible = false

        CommonMethods.getHashKey(this)

        callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            override fun onVerificationCompleted(p0: PhoneAuthCredential) {
//                Toast.makeText(this, "Its toast!", Toast.LENGTH_SHORT).show()
                CommonMethods.showLog(TAG , " Verification done ")
            }

            override fun onVerificationFailed(p0: FirebaseException) {
//                Toast.makeText(this, "Its toast!", Toast.LENGTH_SHORT).show()
                loadingDialog!!.hideDialog()
                CommonMethods.showLog(TAG , " Verification exception ")
            }
            override fun onCodeSent(
                verificationId: String, token: PhoneAuthProvider.ForceResendingToken,
            ) {
                storedVerificationId = verificationId
                resendToken = token
                val editTextEmailRegisterText: String =
                    editTextEmailRegister.text.toString().trim()
                val editTextPhoneNumberText: String =
                    editTextPhoneNumber.text.toString().trim()
                userData.email = editTextEmailRegisterText
                userData.phonenumber = editTextPhoneNumberText
                loadingDialog!!.hideDialog()
                val intent = Intent(baseContext, OtpVerifyActivity::class.java)
                intent.putExtra("storedVerificationId",storedVerificationId)
                intent.putExtra("CLIENT_DATA_FROM_GOOGLE_SCREEN", userData);
//        var otp = commonStatusModel.otp
//        var otpId = commonStatusModel.otpId
//        intent.putExtra(MyConstants.OTP_API, otp.toString());
//        intent.putExtra(MyConstants.OTP_API_ID, otpId.toString());
                val LAUNCH_SECOND_ACTIVITY = 1
                startActivity(intent)
            }
        }
    }


    fun verifyOtpApi(editTextPhoneNumberText: String)
    {
        loadingDialog!!.showDialog()
        if (editTextPhoneNumberText.isNotEmpty()){
            var numberWithCode = "+$phoneCode$editTextPhoneNumberText"
            CommonMethods.showLog(TAG,"Number full with code => $numberWithCode")
            sendVerificationCode(numberWithCode)
        }else{
            Toast.makeText(this,"Enter mobile number", Toast.LENGTH_SHORT).show()
        }
    }
        private fun sendVerificationCode(number: String) {
            CommonMethods.showLog(TAG,"Number     =>         $number")
            val options = PhoneAuthOptions.newBuilder(mAuth!!)
                .setPhoneNumber(number) // Phone number to verify
                .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
                .setActivity(this) // Activity (for callback binding)
                .setCallbacks(callbacks) // OnVerificationStateChangedCallbacks
                .build()
            PhoneAuthProvider.verifyPhoneNumber(options)
           CommonMethods.showLog("GFG" , "Auth started")
        }

    fun checkEmailAndNumberApi(editTextPhoneNumberText: String, editTextEmailRegisterText: String)
    {
        loadingDialog!!.showDialog()
        ApiCalls.checkEmailMobileExists(
            editTextPhoneNumberText,
            countryId,
            editTextEmailRegisterText,
            this
        )
    }



    override fun onClick(v: View?) {
        if (v?.id == R.id.registerGoogleNextButton)
        {

            CommonMethods.showLog(TAG,"phoneCodeText  " + phoneCodeText.text)

            val editTextEmailRegisterText: String = editTextEmailRegister.text.toString().trim()
            val editTextPhoneNumberText: String = editTextPhoneNumber.text.toString().trim()
            val editTextPasswordRegisterText: String = editTextPasswordRegister.text.toString().trim()
            val editTextReEnterPasswordRegisterText: String = editTextReEnterPasswordRegister.text.toString().trim()
            var email_format = Patterns.EMAIL_ADDRESS.matcher(editTextEmailRegisterText).matches()
            if (TextUtils.isEmpty(editTextEmailRegisterText)) {
                commonMessageDialog?.showDialog(getString(R.string.enterEmail))
                editTextEmailRegister.requestFocus()
            }
            else if(!email_format)
            {
                commonMessageDialog?.showDialog(getString(R.string.email_format))
                editTextEmailRegister.requestFocus()
            }
            else if (phoneCodeText.text == "") {
                commonMessageDialog?.showDialog(getString(R.string.countryCode))
                editTextPhoneNumber.requestFocus()
            }
            else if (TextUtils.isEmpty(editTextPhoneNumberText)) {
                commonMessageDialog?.showDialog(getString(R.string.enterPhone))
                editTextPhoneNumber.requestFocus()
            }
            else if (editTextPhoneNumberText.length != 10) {
                commonMessageDialog?.showDialog(getString(R.string.enterValidPhone))
                editTextPhoneNumber.requestFocus()
            }
            else if (TextUtils.isEmpty(editTextPasswordRegisterText)) {
                commonMessageDialog?.showDialog(getString(R.string.enterPassword))
                editTextPasswordRegister.requestFocus()
            }
            else if (editTextPasswordRegisterText.length < 7) {
                commonMessageDialog?.showDialog(getString(R.string.passwordLength))
                editTextPasswordRegister.requestFocus()
            }
            else if (TextUtils.isEmpty(editTextReEnterPasswordRegisterText)) {
                commonMessageDialog?.showDialog(getString(R.string.ReenterPassword))
                editTextPasswordRegister.requestFocus()
            }
            else if (editTextPasswordRegisterText != editTextReEnterPasswordRegisterText) {
                commonMessageDialog?.showDialog(getString(R.string.passwordNotMatch))
                editTextPasswordRegister.requestFocus()
            }
            else {
                userData.password = editTextPasswordRegisterText
                phoneNumber = editTextPhoneNumberText
                checkEmailAndNumberApi(editTextPhoneNumberText,editTextEmailRegisterText)
            }
        }
        else if(v?.id == R.id.menuBackPress)
        {
            onBackPressed()
        }
        else if(v?.id == R.id.countryCodeLayout)
        {
            val intent = Intent(baseContext, CountryCodeActivity::class.java)
            startActivityForResult(intent, MyConstants.COUNTRY_REQUEST_CODE)
        }
        else if(v?.id == R.id.registerGoogle)
        {
            val signInIntent = mGoogleSignInClient!!.signInIntent
            startActivityForResult(signInIntent, RC_SIGN_IN)
        }
        else if(v?.id == R.id.registerFacebook)
        {
            CommonMethods.showLog(TAG, "clicked facebook")

            LoginManager.getInstance().logOut()
            LoginManager.getInstance()
                .logInWithReadPermissions(this, Arrays.asList("public_profile", "email"))
            CommonMethods.loginWithFacebook(callbackManager, this)
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager?.onActivityResult(requestCode, resultCode, data);

        if (requestCode== MyConstants.COUNTRY_REQUEST_CODE)
        {
            if(resultCode== RESULT_OK)
            {
                if (data!=null)
                {
                    phoneCodeText.text = "+"+data.getStringExtra(MyConstants.COUNTRY_CALLING_CODE)
                    phoneCodeText.setTextColor(getResources().getColor(R.color.black));
                    phoneCode = data.getStringExtra(MyConstants.COUNTRY_CALLING_CODE)
                    userData.country_id = data.getStringExtra(MyConstants.COUNTRY_ID).toString()
                    countryId = data.getStringExtra(MyConstants.COUNTRY_ID).toString()
                }
            }
        }
        else if (requestCode == RC_SIGN_IN) {
            CommonMethods.showLog(TAG, "RESULT CODE GOOGLE: $resultCode  ");
            if (resultCode == RESULT_OK) {
                val task = GoogleSignIn.getSignedInAccountFromIntent(data)
                handleSignInResult(task)
            } else if (resultCode == RESULT_CANCELED) {
                loadingDialog!!.hideDialog()
                commonMessageDialog?.showDialog(getString(R.string.googleFailedMessage))
            }
        }

    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
            try {
                val account = completedTask.getResult(ApiException::class.java)
                if (account != null) {
                    CommonMethods.showLog(TAG, "Email" + account.account)
                    CommonMethods.showLog(TAG, "DisplayName" + account.displayName)
                    CommonMethods.showLog(TAG, "getEmail" + account.email)
                    CommonMethods.showLog(TAG, "getFamilyName" + account.familyName)
                    CommonMethods.showLog(TAG, "getGivenName" + account.givenName)
                    CommonMethods.showLog(TAG, "Id" + account.id)
                    CommonMethods.showLog(TAG, "zab" + account.zab())
//                CommonMethods.showLog(TAG, "zac" + account.zac())
                    CommonMethods.showLog(TAG, "getPhotoUrl" + account.photoUrl)
                    LoginApi(account.email!!,account.id,"",MyConstants.GOOGLE,account.displayName,account.familyName)

                } else {
                    loadingDialog!!.hideDialog()
                    mGoogleSignInClient!!.signOut()
                    commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                }
            } catch (e: ApiException) {
                loadingDialog!!.hideDialog()
                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                CommonMethods.showLog(TAG, "Google sign in failed" + e.statusCode)
            }
        }


    fun LoginApi(email: String, googleid: String?,facebookid: String, type: String,firstname: String,familyname: String)
    {
        CommonMethods.showLog(TAG,"login hit")
        CommonMethods.showLog(TAG,"email   $email   password   empty  type  $type  g_id   $id   " +
                " faceb_id  empty   f.N   " + userData.first_name +  "   mob  empty " +
                " b.d   "+ userData.birth_date + " usertype   " + userData.userType + "   username   empty ")
        loadingDialog!!.showDialog()
        ApiCalls.login(
            email,
            "",
            type,
            googleid,
            facebookid,
            firstname,
            familyname,
            "",
            userData.birth_date,
            userData.userType,
            "",
            "",
            this

        )
    }


    override fun onSendOtpInterfaceResponse(status: String?, commonStatusModel: SendOtpModel?) {
        loadingDialog!!.hideDialog()
        when (status) {
            MyConstants.GO_TO_RESPONSE -> {
                if (commonStatusModel != null) {
                    if (commonStatusModel.status != null) {
                        when {

                            commonStatusModel.status.equals("1") -> {
                                val editTextEmailRegisterText: String =
                                    editTextEmailRegister.text.toString().trim()
                                val editTextPhoneNumberText: String =
                                    editTextPhoneNumber.text.toString().trim()
                                userData.email = editTextEmailRegisterText
                                userData.phonenumber = editTextPhoneNumberText
                                loadingDialog!!.hideDialog()
                                Toast.makeText(this, commonStatusModel.message, Toast.LENGTH_SHORT).show()
                                val intent = Intent(baseContext, OtpVerifyActivity::class.java)
                                intent.putExtra("CLIENT_DATA_FROM_GOOGLE_SCREEN", userData);
                                var otp = commonStatusModel.otp
                                var otpId = commonStatusModel.otpId
                                intent.putExtra(MyConstants.OTP_API, otp.toString());
                                intent.putExtra(MyConstants.OTP_API_ID, otpId.toString());
                                startActivity(intent)
                            }
                            commonStatusModel.status.equals("0") -> {
                                loadingDialog!!.hideDialog()
                                commonMessageDialog!!.showDialog(commonStatusModel.message)
                            }
                            commonStatusModel.status.equals("10") -> {
                                loadingDialog!!.hideDialog()
                                CommonMethods.callLogout(this)
                            }
                            else -> {
                                loadingDialog!!.hideDialog()
                                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                            }
                        }
                    } else {
                        loadingDialog!!.hideDialog()
                        commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                    }
                } else {
                    loadingDialog!!.hideDialog()
                    commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                }
            }
            MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                loadingDialog!!.hideDialog()
                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
            }
        }
    }

    override fun onCommonStatusInterfaceResponse(
        status: String?,
        commonStatusModel: CommonStatusModel?,
    ) {
        when (status) {
            MyConstants.GO_TO_RESPONSE -> {
                if (commonStatusModel != null) {
                    if (commonStatusModel.status != null) {
                        when {
                            commonStatusModel.status.equals("1") -> {
                                verifyOtpApi(phoneNumber.toString());

                            }
                            commonStatusModel.status.equals("0") -> {
                                loadingDialog!!.hideDialog()
                                commonMessageDialog!!.showDialog(commonStatusModel.message)
                            }
                            commonStatusModel.status.equals("10") -> {
                                loadingDialog!!.hideDialog()
                                CommonMethods.callLogout(this)
                            }
                            else -> {
                                loadingDialog!!.hideDialog()
                                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                            }
                        }
                    } else {
                        loadingDialog!!.hideDialog()
                        commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                    }
                } else {
                    loadingDialog!!.hideDialog()
                    commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                }
            }
            MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                loadingDialog!!.hideDialog()
                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
            }
        }
    }

    override fun getFacebookCall(status: String?, facebookDataModel: FacebookDataModel?)
    {
        if (status.equals("0", ignoreCase = true)) {
            loadingDialog!!.hideDialog()
            commonMessageDialog!!.showDialog(getString(R.string.facebookFailedMessage))

        } else if (status.equals("1")) {
            if (facebookDataModel != null) {
                CommonMethods.showLog(TAG, "name " + facebookDataModel.accountId)
                CommonMethods.showLog(TAG, "email " + facebookDataModel.email)
                CommonMethods.showLog(TAG, "profilePhoto  " + facebookDataModel.profileImage)
                CommonMethods.showLog(TAG, "firstName " + facebookDataModel.firstName)
                CommonMethods.showLog(TAG, "LastName   " + facebookDataModel.lastName)
                CommonMethods.showLog(TAG, "username   " + facebookDataModel.userName)
                val firstName: String = facebookDataModel.firstName!!
                val lastName: String = facebookDataModel.lastName!!
                val name: String = "$firstName $lastName"
                var image: String? = ""
                if (facebookDataModel.profileImage != null) {
                    image = facebookDataModel.profileImage
                }
                if (facebookDataModel.email != null && !facebookDataModel.email!!.isEmpty()) {
                    LoginApi(facebookDataModel.email!!,"",
                        facebookDataModel.accountId.toString(),MyConstants.FACEBOOK,facebookDataModel.firstName.toString(),facebookDataModel.lastName.toString())
//                    loginApi(
//                        MyConstants.FACEBOOK, facebookDataModel.email!!,
//                        "", name, "", facebookDataModel.accountId!!,
//                        sharedPreferences!!.getString(MyConstants.NOTIFICATION_TOKEN, "").toString()
//                    )
                } else {
                    loadingDialog!!.hideDialog()
                    commonMessageDialog!!.showDialog(getString(R.string.facebookFailedMessage))
                }
            } else {
                loadingDialog!!.hideDialog()
                commonMessageDialog!!.showDialog(getString(R.string.facebookFailedMessage))
            }
        }
    }

    override fun onLoginInterfaceResponse(status: String?, commonStatusModel: LoginModel?) {
        when (status) {
            MyConstants.GO_TO_RESPONSE -> {
                CommonMethods.showLog(TAG,"login  status  " + commonStatusModel!!.status)
                CommonMethods.showLog(TAG,"login  errorData  " + commonStatusModel!!.errorData)
                if (commonStatusModel != null) {
                    if (commonStatusModel.status != null) {
                        when {
                            commonStatusModel.status.equals("1") -> {
                                loadingDialog!!.hideDialog()
                                val innerData: LoginInnerModel = commonStatusModel.userData
                                val editor = sharedPreferences!!.edit()
                                editor.putString(MyConstants.AUTH, innerData.auth)
                                editor.putString(MyConstants.SESSION_TOKEN, innerData.session)
                                editor.putString(MyConstants.USER_ID, innerData.userId)
                                editor.putString(MyConstants.NAME, innerData.firstName)
                                editor.putString(MyConstants.EMAIL, innerData.email)
                                editor.putString(MyConstants.GOOGLE_ID, innerData.googleId)
                                editor.putString(MyConstants.FACEBOOK_ID, innerData.facebookId)
                                editor.putString(MyConstants.FIRST_NAME, innerData.firstName)
                                editor.putString(MyConstants.USERTYPE, innerData.userType)
                                editor.putString(MyConstants.USERNAME, innerData.userName)
                                editor.putInt(MyConstants.VERIFY, innerData.isPersonalityVerified!!)
                                editor.putString(MyConstants.FAMILY_NAME,innerData.familyName)
                                editor.putString(MyConstants.MOBILE,innerData.mobile)
                                editor.putString(MyConstants.COUNTRY_ID,innerData.countryId)
                                editor.putString(MyConstants.TYPE,innerData.loginType)
                                editor.putInt(MyConstants.IS_PASSWORD_ENTERED, innerData.isPasswordEntered!!)
                                editor.putLong(MyConstants.DATEOFBIRTH, innerData.birthDate!!)
                                editor.putBoolean(MyConstants.IS_LOGGED_IN, true)
                                editor.apply()
                                CommonMethods.showLog(TAG,"USERID ======>  "+innerData.userId )
                                checkAndHandleView(innerData)
//                                if(innerData.isPersonalityVerified == 0)
//                                {
//                                    CommonMethods.showLog(TAG,"verified not")
//                                    Toast.makeText(this, commonStatusModel.message, Toast.LENGTH_SHORT).show()
//                                    val intent = Intent(baseContext, WelcomePersonalityTestActivity::class.java)
//                                    startActivity(intent)
//                                    finishAffinity()
//                                }
//                                else
//                                {
//                                    CommonMethods.showLog(TAG,"verified ")
//                                    Toast.makeText(this, commonStatusModel.message, Toast.LENGTH_SHORT).show()
//                                    val intent = Intent(baseContext, HomeScreenActivity::class.java)
//                                    startActivity(intent)
//                                    finishAffinity()
//                                }



                            }
                            commonStatusModel.status.equals("0") -> {
                                loadingDialog!!.hideDialog()
                                commonMessageDialog!!.showDialog(commonStatusModel.message)
                            }
                            commonStatusModel.status.equals("10") -> {
                                loadingDialog!!.hideDialog()
                                CommonMethods.callLogout(this)
                            }
                            else -> {
                                loadingDialog!!.hideDialog()
                                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                            }
                        }
                    } else {
                        loadingDialog!!.hideDialog()
                        commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                    }
                } else {
                    loadingDialog!!.hideDialog()
                    commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                }
            }
            MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                loadingDialog!!.hideDialog()
                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
            }
        }
    }

    private fun checkAndHandleView(innerData: LoginInnerModel) {
        if(innerData.userType==null||innerData.userType == "")
        {
            CommonMethods.showLog(TAG,"1 hit")

            val intent = Intent(baseContext, RegisterIntroActivity::class.java)
            intent.putExtra(MyConstants.USER_ID,innerData.userId) // getText() SHOULD NOT be static!!!
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)
        }
        else if(innerData.userName==null||innerData.userName == "")
        {
            CommonMethods.showLog(TAG,"2 hit")

            val intent = Intent(baseContext, SetUserNameActivity::class.java)
            intent.putExtra(MyConstants.UDATEUSERNAME,"true") // getText() SHOULD NOT be static!!!
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)
        }
        else if(innerData.isPersonalityVerified==0)
        {
            CommonMethods.showLog(TAG,"3 hit")
            val intent = Intent(baseContext, WelcomePersonalityTestActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)
        }
        else
        {
            val intent = Intent(baseContext, HomeScreenActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)
        }

    }

}

