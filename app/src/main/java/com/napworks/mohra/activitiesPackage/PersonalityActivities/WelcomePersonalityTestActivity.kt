package com.napworks.mohra.activitiesPackage.PersonalityActivities

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.R
import com.napworks.mohra.dialogPackage.CommonMessageDialog
import com.napworks.mohra.dialogPackage.LoadingDialog
import com.napworks.mohra.dialogPackage.SkipMessageDialog
import com.napworks.mohra.utilPackage.MyConstants

class WelcomePersonalityTestActivity : AppCompatActivity(), View.OnClickListener {
    var TAG = javaClass.simpleName;
    lateinit var menuBackPress: ImageView
    lateinit var startPersonality: TextView
    lateinit var mainName: TextView
    lateinit var skipButtonToolBar: TextView
    var loadingDialog: LoadingDialog? = null
    var commonMessageDialog: CommonMessageDialog? = null
    var sharedPreferences: SharedPreferences? = null
    var skipMessageDialog: SkipMessageDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome_personality_test)
        menuBackPress =  findViewById(R.id.menuBackPress)
        skipMessageDialog = SkipMessageDialog(this)
        startPersonality =  findViewById(R.id.startPersonality)
        loadingDialog = LoadingDialog(this)
        commonMessageDialog = CommonMessageDialog(this)
        skipButtonToolBar =  findViewById(R.id.skipButtonToolBar)
        mainName =  findViewById(R.id.mainName)
        menuBackPress.setOnClickListener(this)
        startPersonality.setOnClickListener(this)
        sharedPreferences = getSharedPreferences(MyConstants.SHARED_PREFERENCE, MODE_PRIVATE)
        var firstName = sharedPreferences?.getString(MyConstants.FIRST_NAME,"")
        menuBackPress!!.visibility = View.INVISIBLE;
        mainName.text = "Hi "+ firstName
        skipButtonToolBar!!.visibility = View.VISIBLE;
        skipButtonToolBar.setOnClickListener(this)
        CommonMethods.showLog(TAG,"VERIFY "   + sharedPreferences!!.getInt(MyConstants.VERIFY,0).toString())

    }

    override fun onClick(v: View?) {
       if(v?.id == R.id.menuBackPress)
        {
//            onBackPressed()
        }
        else if(v?.id == R.id.startPersonality)
       {
           val intent = Intent(baseContext, PersonalityTestActivity::class.java)
           startActivity(intent)
       }
        else if(v?.id == R.id.skipButtonToolBar)
       {
           skipMessageDialog!!.showDialog(getString(R.string.skipMessage),sharedPreferences,loadingDialog,commonMessageDialog)
       }
    }
}