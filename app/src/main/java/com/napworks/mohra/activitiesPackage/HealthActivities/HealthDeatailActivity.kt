package com.napworks.mohra.activitiesPackage.HealthActivities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.napworks.mohra.R
import com.napworks.mohra.adapterPackage.HealthDetailAdapter
import com.napworks.mohra.fragmentPackage.HealthFragments.HealthDetailBasicDetailFragment
import com.napworks.mohra.fragmentPackage.MessagesFragment
import kotlinx.android.synthetic.main.activity_health_deatail.*

class HealthDeatailActivity : AppCompatActivity() {

    var healthDetailAdapter: HealthDetailAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_health_deatail)

        healthDetailAdapter = HealthDetailAdapter(supportFragmentManager)

        healthDetailAdapter!!.add(HealthDetailBasicDetailFragment(), "Page 1")
        healthDetailAdapter!!.add(MessagesFragment(), "Page 2")
        healthDetailAdapter!!.add(MessagesFragment(), "Page 3")
        healthDetailAdapter!!.add(MessagesFragment(), "Page 4")
        healthDetailAdapter!!.add(MessagesFragment(), "Page 5")

        viewPagerHealthDetails.adapter = healthDetailAdapter
    }
}