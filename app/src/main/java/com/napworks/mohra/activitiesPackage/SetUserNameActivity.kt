package com.napworks.mohra.activitiesPackage

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.isVisible
import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.R
import com.napworks.mohra.activitiesPackage.PersonalityActivities.WelcomePersonalityTestActivity
import com.napworks.mohra.dialogPackage.CommonMessageDialog
import com.napworks.mohra.dialogPackage.LoadingDialog
import com.napworks.mohra.interfacePackage.CommonStatusInterface
import com.napworks.mohra.interfacePackage.UploadPersonalityInterface
import com.napworks.mohra.modelPackage.CommonStatusModel
import com.napworks.mohra.modelPackage.SignUpInnerModel
import com.napworks.mohra.modelPackage.SignUpModel
import com.napworks.mohra.modelPackage.UserDataModel
import com.napworks.mohra.utilPackage.ApiCalls
import com.napworks.mohra.utilPackage.MyConstants

class SetUserNameActivity : AppCompatActivity(), View.OnClickListener,
    com.napworks.mohra.interfacePackage.SignUpInterface,
    CommonStatusInterface {
    var TAG = javaClass.simpleName;

    lateinit var menuBackPress: ImageView
    lateinit var editTextUserName: EditText
    var loadingDialog: LoadingDialog? = null
    lateinit var setUserNameButtonNext: TextView

    lateinit var skipButtonToolBar: TextView
    private var sharedPreferences: SharedPreferences? = null
    var update: String? = "false"
    private  var userData: UserDataModel? = null
    var commonMessageDialog: CommonMessageDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_set_user_name)
        commonMessageDialog = CommonMessageDialog(this)
        sharedPreferences = getSharedPreferences(MyConstants.SHARED_PREFERENCE, MODE_PRIVATE)
        loadingDialog = LoadingDialog(this)
        setUserNameButtonNext = findViewById(R.id.setUserNameButtonNext)
        menuBackPress =  findViewById(R.id.menuBackPress)
        editTextUserName =  findViewById(R.id.editTextUserName)
        setUserNameButtonNext.setOnClickListener(this)
        skipButtonToolBar =  findViewById(R.id.skipButtonToolBar)
        menuBackPress.setOnClickListener(this)
        update = intent.getStringExtra(MyConstants.UDATEUSERNAME)
        CommonMethods.showLog(TAG,"update in setuser  $update")
        if(update == null )
        {
            CommonMethods.showLog(TAG,"data transformed ")
            userData=UserDataModel()
            userData = intent.getSerializableExtra("CLIENT_DATA_FROM_OTP_SCREEN") as UserDataModel
        }
//        CommonMethods.showLog(TAG,"email  "+userData!!.email )
//        CommonMethods.showLog(TAG,"password  "+userData!!.password )
//        CommonMethods.showLog(TAG,"userType  "+userData!!.userType )
//        CommonMethods.showLog(TAG,"first_name  "+userData!!.first_name )
//        CommonMethods.showLog(TAG,"last_name  "+userData!!.last_name )
//        CommonMethods.showLog(TAG,"phonenumber  "+userData!!.phonenumber )
//        CommonMethods.showLog(TAG,"birth_date  "+userData!!.birth_date )
//        CommonMethods.showLog(TAG,"country_id  "+userData!!.country_id )
        skipButtonToolBar.isVisible = false

    }

    fun signUp(editTextUserNameText: String)
    {
        loadingDialog!!.showDialog()
        ApiCalls.signUp(
            userData!!.email,
            userData!!.password,
            userData!!.first_name,
            userData!!.last_name,
            userData!!.phonenumber,
            userData!!.birth_date,
            userData!!.userType,
            editTextUserNameText,
            userData!!.country_id,
            this
        )
    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.setUserNameButtonNext)
        {
            val editTextUserNameText: String = editTextUserName.text.toString().trim()
            if (TextUtils.isEmpty(editTextUserNameText)) {
                commonMessageDialog?.showDialog(getString(R.string.userNameValidation))
                editTextUserName.requestFocus()
            }
            else {
                if(update == "true")
                {
                    var NAME =sharedPreferences!!.getString(MyConstants.NAME, "")
                    var LAST_NAME =sharedPreferences!!.getString(MyConstants.LAST_NAME, "")
                    var DATEOFBIRTH =sharedPreferences!!.getLong(MyConstants.DATEOFBIRTH, 0)
                    var type =sharedPreferences!!.getString(MyConstants.USERTYPE, "")

                    loadingDialog!!.showDialog()
                    ApiCalls.update(
                        sharedPreferences!!,
                        NAME,
                        LAST_NAME,
                        "",
                        DATEOFBIRTH,
                        type,
                        editTextUserNameText,
                        "",
                        this

                    )
                }
                else
                {
                    userData!!.user_name = editTextUserNameText
                    signUp(editTextUserNameText)
                }
            }
        }
        else if(v?.id == R.id.menuBackPress)
        {
            onBackPressed()
        }
    }

    override fun onSignUpInterfaceResponse(status: String?, commonStatusModel: SignUpModel?) {
        when (status) {
            MyConstants.GO_TO_RESPONSE -> {
                if (commonStatusModel != null) {
                    CommonMethods.showLog(TAG, "Sign up get hit : " + commonStatusModel.message)
                    if (commonStatusModel.status != null) {
                        when {

                            commonStatusModel.status.equals("1") -> {
                                loadingDialog!!.hideDialog()
                                val innerData: SignUpInnerModel = commonStatusModel.userData
                                CommonMethods.showLog(TAG,"  userType    " + innerData.userType )
                                CommonMethods.showLog(TAG,"  userName    " + innerData.userName )
                                Toast.makeText(this, commonStatusModel.message, Toast.LENGTH_SHORT).show()
                                val editor = sharedPreferences!!.edit()
                                editor.putString(MyConstants.AUTH, innerData.auth)
                                editor.putString(MyConstants.SESSION_TOKEN, innerData.session)
                                editor.putString(MyConstants.USER_ID, innerData.userId)
                                editor.putString(MyConstants.NAME, innerData.firstName)
                                editor.putString(MyConstants.EMAIL, innerData.email)
                                editor.putString(MyConstants.USERTYPE, innerData.userType)
                                editor.putString(MyConstants.USERNAME, innerData.userName)
                                editor.putString(MyConstants.GOOGLE_ID, innerData.googleId)
                                editor.putString(MyConstants.FACEBOOK_ID, innerData.facebookId)
                                editor.putString(MyConstants.FIRST_NAME, userData!!.first_name)
                                editor.putInt(MyConstants.VERIFY, innerData.isPersonalityVerified!!)
                                editor.putBoolean(MyConstants.IS_LOGGED_IN, true)
//                                if(innerData.isPersonalityVerified == 0)
//                                {
//                                    editor.putBoolean(MyConstants.VERIFY,true)
//                                    editor.putBoolean(MyConstants.IS_LOGGED_IN, false)
//                                    CommonMethods.showLog(TAG,"verified not")
//                                }
//                                else
//                                {
//                                    editor.putBoolean(MyConstants.VERIFY,false)
//                                    editor.putBoolean(MyConstants.IS_LOGGED_IN, true)
//                                    CommonMethods.showLog(TAG,"verified ")
//                                }
                                editor.apply()
                                val intent = Intent(baseContext, WelcomePersonalityTestActivity::class.java)
                                startActivity(intent)
                                finishAffinity()
                            }
                            commonStatusModel.status.equals("0") -> {
                                loadingDialog!!.hideDialog()
                                commonMessageDialog!!.showDialog(commonStatusModel.message)
                            }
                            commonStatusModel.status.equals("2") -> {
                                loadingDialog!!.hideDialog()
                                commonMessageDialog!!.showDialog(commonStatusModel.message)
                            }
                            commonStatusModel.status.equals("10") -> {
                                loadingDialog!!.hideDialog()
                                CommonMethods.callLogout(this)
                            }
                            else -> {
                                loadingDialog!!.hideDialog()
                                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                            }
                        }
                    } else {
                        loadingDialog!!.hideDialog()
                        commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                    }
                } else {
                    loadingDialog!!.hideDialog()
                    commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                }
            }
            MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                loadingDialog!!.hideDialog()
                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
            }
        }

    }


    override fun onCommonStatusInterfaceResponse(
        status: String?,
        commonStatusModel: CommonStatusModel?,
    )
    {
        when (status) {
            MyConstants.GO_TO_RESPONSE -> {
                CommonMethods.showLog(TAG,"upadate  status  " + commonStatusModel!!.status)
                CommonMethods.showLog(TAG,"upadate  message  " + commonStatusModel!!.message)
                if (commonStatusModel != null) {
                    if (commonStatusModel.status != null) {
                        when {
                            commonStatusModel.status.equals("1") -> {
                                loadingDialog!!.hideDialog()

                                val editor = sharedPreferences!!.edit()
                                editor.putString(MyConstants.USERNAME,editTextUserName.text.toString().trim())
                                editor.apply()

                                var verify =sharedPreferences!!.getInt(MyConstants.VERIFY, 0)
                                    CommonMethods.showLog(TAG,"verified not")
                                    Toast.makeText(this, commonStatusModel.message, Toast.LENGTH_SHORT).show()
                                    val intent = Intent(baseContext, WelcomePersonalityTestActivity::class.java)
                                    startActivity(intent)
                                    finishAffinity()



                            }
                            commonStatusModel.status.equals("0") -> {
                                loadingDialog!!.hideDialog()
                                commonMessageDialog!!.showDialog(commonStatusModel.message)
                            }
                            commonStatusModel.status.equals("10") -> {
                                loadingDialog!!.hideDialog()
                                CommonMethods.callLogout(this)
                            }
                            else -> {
                                loadingDialog!!.hideDialog()
                                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                            }
                        }
                    } else {
                        loadingDialog!!.hideDialog()
                        commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                    }
                } else {
                    loadingDialog!!.hideDialog()
                    commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                }
            }
            MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                loadingDialog!!.hideDialog()
                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
            }
        }
    }
}