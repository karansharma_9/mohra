package com.napworks.mohra.activitiesPackage.PersonalityActivities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.napworks.mohra.R
import kotlinx.android.synthetic.main.toolbar_layout.*

class PersonalityDetailsActivity : AppCompatActivity(), View.OnClickListener {


    lateinit var personalityTitleTextView: TextView
    lateinit var shortDescriptionTextView: TextView
    lateinit var urlBit: TextView
    lateinit var imageViewAvtarDetail: ImageView
    lateinit var copyLayout: LinearLayout

    var sharedMessage = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_personality_details)
        val url = intent.extras!!.getString("url")
        val Imageurl = intent.extras!!.getString("Imageurl")
        val shortDescription = intent.extras!!.getString("shortDescription")
        val titlePersonality = intent.extras!!.getString("titlePersonality")
        sharedMessage = intent.extras!!.getString("sharedMessage")!!

        imageViewAvtarDetail = findViewById(R.id.imageViewAvtarDetail)
        urlBit = findViewById(R.id.urlBit)
        shortDescriptionTextView = findViewById(R.id.shortDescriptionTextView)
        personalityTitleTextView = findViewById(R.id.personalityTitleTextView)
        copyLayout = findViewById(R.id.copyLayout)

        urlBit.text = url
        personalityTitleTextView.text = titlePersonality
        shortDescriptionTextView.text = shortDescription

        Glide.with(this).load(Imageurl).apply(
            RequestOptions()
                .placeholder(R.drawable.logosplashscreen))
            .into(imageViewAvtarDetail)


        menuBackPress.setOnClickListener(this)
        copyLayout.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.menuBackPress)
        {
            onBackPressed()
        }else if(v?.id == R.id.copyLayout) {
            shareData()
        }
    }

    fun shareData(){
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.putExtra(Intent.EXTRA_TEXT, sharedMessage)
//        shareIntent.putExtra(Intent.EXTRA_STREAM, listImages!![0])
        shareIntent.type = "text/plain"
        shareIntent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
        shareIntent.flags = Intent.FLAG_GRANT_WRITE_URI_PERMISSION
        shareIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(Intent.createChooser(shareIntent,title))
    }
}