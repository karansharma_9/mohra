package com.napworks.mohra.activitiesPackage

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.napworks.mohra.R
import com.napworks.mohra.dialogPackage.CommonMessageDialog
import com.napworks.mohra.dialogPackage.LoadingDialog
import com.napworks.mohra.interfacePackage.CommonStatusInterface
import com.napworks.mohra.modelPackage.CommonStatusModel
import com.napworks.mohra.utilPackage.ApiCalls
import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.utilPackage.MyConstants
import kotlinx.android.synthetic.main.toolbar_layout.*

class ResetPasswordActivity : AppCompatActivity(), View.OnClickListener, CommonStatusInterface {
    var userId :String = ""
    var TAG = javaClass.simpleName;
    lateinit var newPasswordEditText: EditText
    lateinit var confirmPasswordEditText: EditText
    lateinit var resetPasswordButton: TextView
    var commonMessageDialog: CommonMessageDialog? = null
    var loadingDialog: LoadingDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password)
        userId= intent.getStringExtra("userId").toString()
        loadingDialog = LoadingDialog(this)
        commonMessageDialog = CommonMessageDialog(this)

        newPasswordEditText = findViewById(R.id.newPassword)
        confirmPasswordEditText = findViewById(R.id.confirmPassword)
        resetPasswordButton = findViewById(R.id.resetPasswordButton)
        resetPasswordButton.setOnClickListener(this)
        menuBackPress.setOnClickListener(this)


    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.resetPasswordButton) {
            val newpasswordText: String = newPasswordEditText.text.toString().trim()
            val confirmPassword: String = confirmPasswordEditText.text.toString().trim()
            CommonMethods.showLog(TAG,"newpasswordText  = >  $newpasswordText  confirmPassword  =>    $confirmPassword")
            if (TextUtils.isEmpty(newpasswordText)) {
                commonMessageDialog?.showDialog(getString(R.string.enterPassword))
                newPasswordEditText.requestFocus()
            } else if (newpasswordText.length < 7) {
                commonMessageDialog?.showDialog(getString(R.string.passwordLength))
                newPasswordEditText.requestFocus()
            } else if (newpasswordText != confirmPassword) {
                commonMessageDialog?.showDialog(getString(R.string.passwordNotMatched))
                newPasswordEditText.requestFocus()
            } else {
                ApiForReset(newpasswordText)
//                val intent = Intent(baseContext, OtpVerifyActivity::class.java)
//                startActivity(intent)
            }

        }
        else if(v?.id == R.id.menuBackPress)
        {
            onBackPressed()
        }
    }

    private fun ApiForReset(newpasswordText: String) {
        loadingDialog!!.showDialog()
        ApiCalls.forgetUpdatePassword(
            userId,
            newpasswordText,
            this
        )

    }

    override fun onCommonStatusInterfaceResponse(
        status: String?,
        commonStatusModel: CommonStatusModel?,
    ) {
        when (status) {
            MyConstants.GO_TO_RESPONSE -> {
                if (commonStatusModel != null) {
                    CommonMethods.showLog(
                        TAG,
                        "Sign up API status : " + commonStatusModel.status
                    )
                    if (commonStatusModel.status != null) {
                        when {

                            commonStatusModel.status.equals("1") -> {
                                loadingDialog!!.hideDialog()
                                Toast.makeText(this, commonStatusModel.message, Toast.LENGTH_SHORT).show()
                                val intent = Intent(baseContext, LoginActivity::class.java)
                                startActivity(intent)
                                finishAffinity()
                            }
                            commonStatusModel.status.equals("0") -> {
                                loadingDialog!!.hideDialog()
                                commonMessageDialog!!.showDialog(commonStatusModel.message)
                            }
                            commonStatusModel.status.equals("2") -> {
                                loadingDialog!!.hideDialog()
                                commonMessageDialog!!.showDialog(commonStatusModel.message)
                            }
                            commonStatusModel.status.equals("10") -> {
                                loadingDialog!!.hideDialog()
                                CommonMethods.callLogout(this)
                            }
                            else -> {
                                loadingDialog!!.hideDialog()
                                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                            }
                        }
                    } else {
                        loadingDialog!!.hideDialog()
                        commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                    }
                } else {
                    loadingDialog!!.hideDialog()
                    commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                }
            }
            MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                loadingDialog!!.hideDialog()
                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
            }
        }
    }
}