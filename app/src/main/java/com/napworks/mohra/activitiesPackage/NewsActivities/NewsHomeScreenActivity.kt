package com.napworks.mohra.activitiesPackage.NewsActivities

import android.content.res.ColorStateList
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import com.napworks.mohra.R
import com.napworks.mohra.adapterPackage.News.NewsHomePageAdapter
import com.napworks.mohra.fragmentPackage.NewsFragments.NewsFavFragment
import com.napworks.mohra.fragmentPackage.NewsFragments.NewsHomeFragment
import com.napworks.mohra.fragmentPackage.NewsFragments.SearchNewsFragment
import kotlinx.android.synthetic.main.activity_news_home_screen.*

class NewsHomeScreenActivity : AppCompatActivity(), View.OnClickListener {

    var TAG = javaClass.simpleName;
    var newsHomePageAdapter: NewsHomePageAdapter? = null
    var searchIcon: ImageView? = null
    var homeIconNew: ImageView? = null
    var favNewsIcon: ImageView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_home_screen)
        newsHomePageAdapter = NewsHomePageAdapter(supportFragmentManager)

        newsHomePageAdapter!!.add(NewsHomeFragment(), "Page 1")
        newsHomePageAdapter!!.add(SearchNewsFragment(), "Page 2")
        newsHomePageAdapter!!.add(NewsFavFragment(), "Page 3")
        searchIcon = findViewById(R.id.searchIcon)
        homeIconNew = findViewById(R.id.homeIconNew)
        favNewsIcon = findViewById(R.id.favNewsIcon)

        viewPagerNewsHomePage.adapter = newsHomePageAdapter
        viewPagerNewsHomePage.currentItem = 0;
        homeBottomBarNews.setOnClickListener(this)
        searchBottomBarNews.setOnClickListener(this)
        myFavBottomBarNews.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.homeBottomBarNews)
        {
            viewPagerNewsHomePage.currentItem = 0;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                searchIcon!!.setBackgroundTintList(ColorStateList.valueOf(resources.getColor(R.color.grey)))
                homeIconNew!!.setBackgroundTintList(ColorStateList.valueOf(resources.getColor(R.color.colorPrimary)))
                favNewsIcon!!.setBackgroundTintList(ColorStateList.valueOf(resources.getColor(R.color.grey)))
            };
            newsHomePageAdapter!!.notifyDataSetChanged();
        }
        else if(v?.id == R.id.searchBottomBarNews)
        {
            viewPagerNewsHomePage.currentItem = 1;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                searchIcon!!.setBackgroundTintList(ColorStateList.valueOf(resources.getColor(R.color.colorPrimary)))
                homeIconNew!!.setBackgroundTintList(ColorStateList.valueOf(resources.getColor(R.color.grey)))
                favNewsIcon!!.setBackgroundTintList(ColorStateList.valueOf(resources.getColor(R.color.grey)))
            };
            newsHomePageAdapter!!.notifyDataSetChanged();
        }
        else if(v?.id == R.id.myFavBottomBarNews)
        {
            viewPagerNewsHomePage.currentItem = 2;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                searchIcon!!.setBackgroundTintList(ColorStateList.valueOf(resources.getColor(R.color.grey)))
                homeIconNew!!.setBackgroundTintList(ColorStateList.valueOf(resources.getColor(R.color.grey)))
                favNewsIcon!!.setBackgroundTintList(ColorStateList.valueOf(resources.getColor(R.color.colorPrimary)))
            };
            newsHomePageAdapter!!.notifyDataSetChanged();
        }    }
}