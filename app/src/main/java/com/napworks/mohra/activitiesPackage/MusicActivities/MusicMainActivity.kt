package com.napworks.mohra.activitiesPackage.MusicActivities

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.*
import android.content.res.ColorStateList
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.napworks.mohra.R
import com.napworks.mohra.ServicesPackage.OnClearFromRecentService
import com.napworks.mohra.adapterPackage.MusicHomePageAdapter
import com.napworks.mohra.interfacePackage.music.AdapterOnClick
import com.napworks.mohra.interfacePackage.music.Playable
import com.napworks.mohra.modelPackage.MusicModel.MusicFilesModel
import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.utilPackage.MusicNotification
import com.napworks.mohra.utilPackage.MyConstants
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelSlideListener
import com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelState
import com.wang.avi.AVLoadingIndicatorView
import kotlinx.android.synthetic.main.activity_music_main.*
import me.tankery.lib.circularseekbar.CircularSeekBar


class MusicMainActivity() : AppCompatActivity(), View.OnClickListener ,AdapterOnClick , Playable{

    var TAG = javaClass.simpleName;
    var musicHomePageAdapter: MusicHomePageAdapter? = null
    var playandpausebuttonLayout: FrameLayout? = null
    var bottomLayoutMusicMain :LinearLayout? = null
    var slidingUpPanelLayout: SlidingUpPanelLayout? = null
    var playAndPauseButtonImage: ImageView? = null
    var playAndPauseMiniPlayer: ImageView? = null
    var musicSearchIcon: ImageView? = null
    var musicHomeIcon: ImageView? = null
    var musicMyLibraryIcon: ImageView? = null
    var loading: AVLoadingIndicatorView? = null
    var backPressMusicPlayer: ImageView? = null
    var miniPlayerNextSong: ImageView? = null
    var musicPlayerNextSong: ImageView? = null
    var musicPlayerPrevSong: ImageView? = null
    var imageMusicPlayer: ImageView? = null
    var player: LinearLayout? = null
    var isPlaying : Boolean = false
    var mediaPlayer: MediaPlayer? = null
    var currentTime: TextView? = null
    var titleOfSong: TextView? = null
    var titleOfMiniPlayer: TextView? = null
    var homeText: TextView? = null
    var myLibraryText: TextView? = null
    var searchText: TextView? = null
    var viewPagerMusicHomePage: ViewPager? = null
    var seekbarMusicPlayerHorizontal: SeekBar? = null
    var circularProgressBar: CircularSeekBar? = null
    var handler: Handler = Handler()
    private var sharedPreferences: SharedPreferences? = null
    private var miniPlayerLayout: LinearLayout? = null
    private var openMiniPlayer: LinearLayout? = null
    private var layout: LinearLayout? = null
    private var mainlayout: LinearLayout? = null
    var totalTime: TextView? = null
    var SongNumberPlaying: Int = 0
    var sendPlayButton: Int = 0
    var notificationManager : NotificationManager? = null
    var allMusicList: ArrayList<MusicFilesModel>? = null

    var viewPagerList : ArrayList<String>? = null

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_music_main)
        notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager

        val passedArg : Int = intent.extras!!.getInt("currentLocation")

//        musicHomePageAdapter = MusicHomePageAdapter(supportFragmentManager)
//        musicHomePageAdapter = MusicHomePageAdapter(this.)
        sharedPreferences = getSharedPreferences(MyConstants.SHARED_PREFERENCE, MODE_PRIVATE)
        miniPlayerLayout = findViewById(R.id.miniPlayerLayout)
        openMiniPlayer = findViewById(R.id.openMiniPlayer)
        viewPagerMusicHomePage = findViewById(R.id.viewPagerMusicHomePage)
        backPressMusicPlayer = findViewById(R.id.backPressMusicPlayer)
        slidingUpPanelLayout = findViewById(R.id.slideUp)
        imageMusicPlayer = findViewById(R.id.imageMusicPlayer)
        titleOfSong = findViewById(R.id.titleOfSong)
        titleOfMiniPlayer = findViewById(R.id.titleOfMiniPlayer)
        playAndPauseButtonImage = findViewById(R.id.playAndPauseButtonImage)
        playandpausebuttonLayout = findViewById(R.id.playandpausebuttonLayout)
        seekbarMusicPlayerHorizontal = findViewById(R.id.seekbarMusicPlayerHorizontal)
        musicSearchIcon = findViewById(R.id.musicSearchIcon)
        musicHomeIcon = findViewById(R.id.musicHomeIcon)
        musicMyLibraryIcon = findViewById(R.id.musicMyLibraryIcon)
        layout = findViewById<View>(R.id.innerLayout) as LinearLayout
        mainlayout = findViewById<View>(R.id.mainlayout) as LinearLayout
        playAndPauseMiniPlayer = findViewById(R.id.playAndPauseMiniPlayer)
        bottomLayoutMusicMain = findViewById(R.id.bottomLayoutMusicMain)
        player = findViewById(R.id.player)
        musicPlayerNextSong = findViewById(R.id.musicPlayerNextSong)
        circularProgressBar = findViewById(R.id.circularProgressBar)
        miniPlayerNextSong = findViewById(R.id.miniPlayerNextSong)
        musicPlayerPrevSong = findViewById(R.id.musicPlayerPrevSong)
        homeText = findViewById(R.id.homeText)
        myLibraryText = findViewById(R.id.myLibraryText)
        searchText = findViewById(R.id.searchText)
        seekbarMusicPlayerHorizontal!!.max = 100
        circularProgressBar!!.max = 100F

        allMusicList = ArrayList<MusicFilesModel>()
        viewPagerList = ArrayList<String>()

        viewPagerList!!.add("Home")
        viewPagerList!!.add("My Library")
        viewPagerList!!.add("Search")

//        musicHomePageAdapter!!.add(MusicHomeFragment(this,this), "Page 1")
//        musicHomePageAdapter!!.add(MyLibraryFragment(), "Page 2")
//        musicHomePageAdapter!!.add(PlayListDetailsFragment(), "Page 3")

//
//        viewPagerMusicHomePage.adapter = musicHomePageAdapter
//        val passedArg : Int = intent.extras!!.getInt("currentLocation")
        CommonMethods.showLog(TAG,"PASSED ARG : "+passedArg)
//        viewPagerMusicHomePage.currentItem = passedArg
////        viewPagerMusicHomePage.currentItem = 0;
//        musicHomePageAdapter!!.notifyDataSetChanged();
        mediaPlayer  = MediaPlayer()
        currentTime = findViewById(R.id.currentTime)
        totalTime = findViewById(R.id.totalTime)
        loading = findViewById(R.id.loading)
        homeBottomBarMusic.setOnClickListener(this)
        myLibraryBottomBarMusic.setOnClickListener(this)
        searchBottomBarMusic.setOnClickListener(this)
        backPressMusicPlayer!!.setOnClickListener(this)
        playandpausebuttonLayout!!.setOnClickListener(this)
        playAndPauseButtonImage!!.setOnClickListener(this)
        miniPlayerLayout!!.setOnClickListener(this)
        openMiniPlayer!!.setOnClickListener(this)
        playAndPauseMiniPlayer!!.setOnClickListener(this)
        miniPlayerNextSong!!.setOnClickListener(this)
        musicPlayerNextSong!!.setOnClickListener(this)
        musicPlayerPrevSong!!.setOnClickListener(this)
        bottomLayoutMusicMain!!.visibility = View.VISIBLE

        setUpViewPager()


        val handler = Handler()
        handler.postDelayed(Runnable {
            viewPagerMusicHomePage!!.currentItem = passedArg
            musicHomePageAdapter!!.notifyDataSetChanged()
            checkAndSetUI(passedArg)
        }, 100)


        val observer = bottomLayoutMusicMain!!.viewTreeObserver
        observer.addOnGlobalLayoutListener {
//            CommonMethods.showLog(TAG,"Height o: " + bottomLayoutMusicMain!!.height)
        }
        Glide.with(this).load(R.drawable.ed)
            .apply(RequestOptions().circleCrop())
            .into(imageMusicPlayer!!)
        if(mediaPlayer!!.isPlaying)
        {
            player!!.visibility = View.VISIBLE
        }
        else
        {
            player!!.visibility = View.GONE
        }

        seekbarMusicPlayerHorizontal!!.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener{

            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {

                if(fromUser)
                {
                    CommonMethods.showLog(TAG, "seekbar move $progress----- $fromUser")
                    handler.removeCallbacks(updater)
                    mediaPlayer!!.seekTo(progress)
                }

            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

            }
        })

//        seekbarMusicPlayerHorizontal!!.setOnTouchListener(OnTouchListener { v, event ->
//            if (event.action == MotionEvent.ACTION_MOVE) {
//                CommonMethods.showLog(TAG, "Moved , process data, Moved to :" + seekbarMusicPlayerHorizontal!!.getProgress())
//                circularProgressBar!!.progress = seekbarMusicPlayerHorizontal!!.progress.toFloat()
////                mediaPlayer!!.set = circularProgressBar!!.progress.toInt()
////                seekBar.setProgress(seekBar.getProgress())
//                return@OnTouchListener false
//            }
//            CommonMethods.showLog(TAG, "Touched , Progress :" + seekbarMusicPlayerHorizontal!!.getProgress())
//            true
//        })

        slidingUpPanelLayout!!.addPanelSlideListener(object : PanelSlideListener {
            override fun onPanelSlide(panel: View, slideOffset: Float)
            {
//                if(slideOffset == 1.0F)
//                {
                    slidingUpPanelLayout!!.setTouchEnabled(false)
//                }
//                else
//                {
//                    slidingUpPanelLayout!!.setTouchEnabled(true)
//                }
            }
            override fun onPanelStateChanged(
                panel: View,
                previousState: PanelState,
                newState: PanelState,
            ) {
                //  Toast.makeText(getApplicationContext(),newState.name().toString(),Toast.LENGTH_SHORT).show();
                if (newState.name.equals("Collapsed", ignoreCase = true))
                {
                    miniPlayerLayout!!.visibility = View.VISIBLE
                    val paramsbottom: ViewGroup.LayoutParams? = bottomLayoutMusicMain!!.layoutParams
                    paramsbottom!!.height = 120
                    bottomLayoutMusicMain!!.layoutParams = paramsbottom
                    bottomLayoutMusicMain!!.visibility = View.VISIBLE
                    //action when collapsed
                }
                else if (newState.name.equals("Expanded", ignoreCase = true))
                {
                    val observer = mainlayout!!.viewTreeObserver
                    observer.addOnGlobalLayoutListener {

                        var currentheight = mainlayout!!.height
                        val params: ViewGroup.LayoutParams? = layout!!.layoutParams
                        params!!.height = currentheight
                        layout!!.layoutParams = params
                    }

                    miniPlayerLayout!!.visibility = View.GONE
                    val paramsbottom: ViewGroup.LayoutParams? = bottomLayoutMusicMain!!.layoutParams
                    paramsbottom!!.height = 0
                    bottomLayoutMusicMain!!.layoutParams = paramsbottom
                    bottomLayoutMusicMain!!.visibility = View.GONE
                }
            }
        })
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            createChannel()
            registerReceiver(broadcastReceiver, IntentFilter("TRACKS_TRACKS"))
            startService(Intent(baseContext, OnClearFromRecentService::class.java))
        }
    }

    private fun setUpViewPager() {
        CommonMethods.showLog(TAG, "CALL VIEWPAGER")
        musicHomePageAdapter =
            MusicHomePageAdapter(supportFragmentManager, viewPagerList,this,this)
        viewPagerMusicHomePage!!.adapter = musicHomePageAdapter
        viewPagerMusicHomePage!!.id = (Math.random() * 10000).toInt()
       }

    fun createChannel()
    {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            var channel = NotificationChannel(MusicNotification.CHANNEL_ID,"karan",NotificationManager.IMPORTANCE_LOW)
            if(notificationManager != null)
            {
                notificationManager!!.createNotificationChannel(channel)
            }
        }
    }


//    fun showPlayer()
//    {
//        showMiniPlayer = sharedPreferences!!.getBoolean(MyConstants.MUSIC_PLAYING,false)
//        CommonMethods.showLog(TAG,"showPlayer  2 $showMiniPlayer")
//        if(showMiniPlayer)
//        {
//            CommonMethods.showLog(TAG,"show player")
////            miniMusicPlayer!!.visibility = View.VISIBLE
//        }
//        else
//        {
//            CommonMethods.showLog(TAG,"hide player")
////            miniMusicPlayer!!.visibility = View.GONE
//        }
//    }

    public fun miniPlayerCall(adapterPosition: Int)
    {
        slidingUpPanelLayout!!.panelState = PanelState.EXPANDED
        SongNumberPlaying = adapterPosition
        prepareMediaPlayer()
    }

    fun nextSong()
    {
        var size = allMusicList!!.size
        var playing = SongNumberPlaying
        playing = playing + 1
        if(playing  == size )
        {
//            musicPlayerNextSong!!.visibility =  View.INVISIBLE
            Toast.makeText(this, "No more available", Toast.LENGTH_LONG).show()

        }
        else
        {
            musicPlayerNextSong!!.visibility =  View.VISIBLE
            SongNumberPlaying = SongNumberPlaying + 1
            prepareMediaPlayer()
        }

    }

        fun prepareMediaPlayer()
    {
        var playing : Int = 9
        sendPlayButton = R.drawable.pause
        if(mediaPlayer!!.isPlaying)
        {
             playing = 0
        }
        else
        {
             playing = 1
        }
        MusicNotification.createNotification(this,SongNumberPlaying,allMusicList!!.size , allMusicList , playing)
        var url = allMusicList!![SongNumberPlaying!!].path
//        loading!!.visibility = View.VISIBLE
        titleOfSong!!.text =  allMusicList!![SongNumberPlaying!!].name
        titleOfMiniPlayer!!.text =  allMusicList!![SongNumberPlaying!!].name
        try
        {
            playAndPauseButtonImage!!.setBackgroundResource(R.drawable.play)
            playAndPauseMiniPlayer!!.setBackgroundResource(R.drawable.play)
            Handler().postDelayed({
            val uri: Uri = Uri.parse(url)
            mediaPlayer!!.reset()
            mediaPlayer!!.setAudioStreamType(AudioManager.STREAM_MUSIC)
            mediaPlayer!!.setDataSource(this, uri)
            mediaPlayer!!.prepare()
            mediaPlayer!!.start()
                val editor = sharedPreferences!!.edit()
                editor.putBoolean(MyConstants.MUSIC_PLAYING, true)
                editor.apply()

            playAndPauseButtonImage!!.setBackgroundResource(R.drawable.pause)
            playAndPauseMiniPlayer!!.setBackgroundResource(R.drawable.pause)
            totalTime!!.text = millisecondsToTime(mediaPlayer!!.duration.toLong())
            var currentDuration = mediaPlayer!!.currentPosition
            handler.post(updater)
            currentTime!!.text = millisecondsToTime(currentDuration.toLong())
            }, 1000)
//            loading!!.visibility = View.GONE
        }
        catch(e: Exception)
        {
            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show()
        }
    }

        val updater: Runnable = object : Runnable{
        override fun run() {
            updateSeekbar()
            var currentDuration = mediaPlayer!!.currentPosition
            currentTime!!.text = millisecondsToTime(currentDuration.toLong())
        }
    }

        fun updateSeekbar(){
        if (mediaPlayer!!.isPlaying)
        {
            var progress : Int = (mediaPlayer!!.currentPosition * 100) / mediaPlayer!!.duration
            seekbarMusicPlayerHorizontal!!.progress = progress
            circularProgressBar!!.progress = progress.toFloat()
            handler.post(updater)
        }
    }

    fun  millisecondsToTime(  milliSeconds : Long) : String
    {
        var timerString = ""
        var secondsString : String?
        var hours : Int = (milliSeconds /(1000 * 60 * 60)).toInt()
        var minutes : Int= (milliSeconds % (1000 * 60 * 60) / (1000 * 60)).toInt()
        var seconds : Int = (milliSeconds % (1000 * 60 * 60) % (1000 * 60) / 1000).toInt()

        if (hours > 0 )
        {
            timerString = "$hours:"
        }
        if(seconds < 10)
        {
            secondsString = "0$seconds"
        }
        else
        {
            secondsString = "$seconds"
        }

        timerString = timerString + minutes + ":" + secondsString
        return timerString
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onClick(v: View?) {
        if (v?.id == R.id.homeBottomBarMusic)
        {
//            viewPagerMusicHomePage!!.currentItem = 0;
//            musicHomePageAdapter!!.notifyDataSetChanged();
            checkAndSetUI(0)
//            musicSearchIcon!!.setBackgroundTintList(ColorStateList.valueOf(resources.getColor(R.color.grey)))
//            musicMyLibraryIcon!!.setBackgroundTintList(ColorStateList.valueOf(resources.getColor(R.color.grey)))
//            musicHomeIcon!!.setBackgroundTintList(ColorStateList.valueOf(resources.getColor(R.color.colorPrimary)))
        }
        else if(v?.id == R.id.myLibraryBottomBarMusic)
        {
//            viewPagerMusicHomePage!!.currentItem = 1;
//            musicHomePageAdapter!!.notifyDataSetChanged();
            checkAndSetUI(1)
//            musicSearchIcon!!.setBackgroundTintList(ColorStateList.valueOf(resources.getColor(R.color.grey)))
//            musicMyLibraryIcon!!.setBackgroundTintList(ColorStateList.valueOf(resources.getColor(R.color.colorPrimary)))
//            musicHomeIcon!!.setBackgroundTintList(ColorStateList.valueOf(resources.getColor(R.color.grey)))
        }
        else if(v?.id == R.id.searchBottomBarMusic)
        {
//            viewPagerMusicHomePage!!.currentItem = 2;
//            musicHomePageAdapter!!.notifyDataSetChanged();
            checkAndSetUI(2)
//            musicSearchIcon!!.setBackgroundTintList(ColorStateList.valueOf(resources.getColor(R.color.colorPrimary)))
//            musicMyLibraryIcon!!.setBackgroundTintList(ColorStateList.valueOf(resources.getColor(R.color.grey)))
//            musicHomeIcon!!.backgroundTintList = ColorStateList.valueOf(resources.getColor(R.color.grey))
        }
        else if(v?.id == R.id.backPressMusicPlayer)
        {
            slidingUpPanelLayout!!.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
        }
        else if(v?.id == R.id.playAndPauseButtonImage)
        {
            playAndPause()
        }
        else if(v?.id == R.id.playAndPauseMiniPlayer)
        {
            playAndPause()
        }
        else if(v?.id == R.id.openMiniPlayer)
        {
            slidingUpPanelLayout!!.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
        }
        else if(v?.id == R.id.miniPlayerNextSong)
        {
            nextSong()
        }
        else if(v?.id == R.id.musicPlayerNextSong)
        {
            nextSong()
        }
        else if(v?.id == R.id.musicPlayerPrevSong)
        {
            prevSong()
        }
    }

    private fun checkAndSetUI(position: Int) {
        CommonMethods.showLog(TAG,"CHECK AND SET UI : "+position)
       if (position == 0 ){
           viewPagerMusicHomePage!!.currentItem = position
           musicHomePageAdapter!!.notifyDataSetChanged();
           musicSearchIcon!!.setColorFilter(resources.getColor(R.color.grey));
           musicMyLibraryIcon!!.setColorFilter(resources.getColor(R.color.grey));
           musicHomeIcon!!.setColorFilter(resources.getColor(R.color.colorPrimary));

           homeText!!.setTextColor(ContextCompat.getColor(this,R.color.colorPrimary))
           myLibraryText!!.setTextColor(ContextCompat.getColor(this,R.color.grey))
           searchText!!.setTextColor(ContextCompat.getColor(this,R.color.grey))
       }
       else if (position == 1){
           viewPagerMusicHomePage!!.currentItem = position
           musicHomePageAdapter!!.notifyDataSetChanged();
           musicSearchIcon!!.setColorFilter(resources.getColor(R.color.grey));
           musicMyLibraryIcon!!.setColorFilter(resources.getColor(R.color.colorPrimary));
           musicHomeIcon!!.setColorFilter(resources.getColor(R.color.grey));

           homeText!!.setTextColor(ContextCompat.getColor(this,R.color.grey))
           myLibraryText!!.setTextColor(ContextCompat.getColor(this,R.color.colorPrimary))
           searchText!!.setTextColor(ContextCompat.getColor(this,R.color.grey))
       }
        else{
           viewPagerMusicHomePage!!.currentItem = position
           musicHomePageAdapter!!.notifyDataSetChanged();
           musicSearchIcon!!.setColorFilter(resources.getColor(R.color.colorPrimary));
           musicMyLibraryIcon!!.setColorFilter(resources.getColor(R.color.grey));
           musicHomeIcon!!.setColorFilter(resources.getColor(R.color.grey));

           homeText!!.setTextColor(ContextCompat.getColor(this,R.color.grey))
           myLibraryText!!.setTextColor(ContextCompat.getColor(this,R.color.grey))
           searchText!!.setTextColor(ContextCompat.getColor(this,R.color.colorPrimary))
       }

    }

    fun playAndPause()
    {
        if(mediaPlayer!!.isPlaying)
        {
            handler.removeCallbacks(updater)
            mediaPlayer!!.pause()
            val editor = sharedPreferences!!.edit()
            editor.putBoolean(MyConstants.MUSIC_PLAYING, false)
            editor.apply()
            playAndPauseButtonImage!!.setBackgroundResource(R.drawable.play)
            playAndPauseMiniPlayer!!.setBackgroundResource(R.drawable.play)
        }
        else
        {
            handler.post(updater)
            mediaPlayer!!.start()
            val editor = sharedPreferences!!.edit()
            editor.putBoolean(MyConstants.MUSIC_PLAYING, true)
            editor.apply()
            playAndPauseMiniPlayer!!.setBackgroundResource(R.drawable.pause)
            playAndPauseButtonImage!!.setBackgroundResource(R.drawable.pause)
        }
//
//        if(mediaPlayer!!.isPlaying)
//        {
//            CommonMethods.showLog(TAG,"Music is paused ")
//            handler.removeCallbacks(updater)
//            mediaPlayer!!.pause()
//            val editor = sharedPreferences!!.edit()
//            editor.putBoolean(MyConstants.MUSIC_PLAYING, false)
//            editor.apply()
//            playAndPauseMiniPlayer!!.setBackgroundResource(R.drawable.play)
//            playAndPauseButtonImage!!.setBackgroundResource(R.drawable.play)
//        }
//        else
//        {
//            handler.post(updater)
//            CommonMethods.showLog(TAG,"Music is Started ")
//            mediaPlayer!!.start()
//            val editor = sharedPreferences!!.edit()
//            editor.putBoolean(MyConstants.MUSIC_PLAYING, true)
//            editor.apply()
//            playAndPauseMiniPlayer!!.setBackgroundResource(R.drawable.pause)
//            playAndPauseButtonImage!!.setBackgroundResource(R.drawable.pause)
//        }
    }

    fun prevSong()
    {
        var size = allMusicList!!.size
        var playing = SongNumberPlaying
        playing = playing - 1

        if(playing == -1 )
        {
            Toast.makeText(this, "On more songs", Toast.LENGTH_SHORT).show()        }
        else
        {
            musicPlayerNextSong!!.visibility = View.VISIBLE
            SongNumberPlaying = SongNumberPlaying - 1
            prepareMediaPlayer()
        }
    }

    override fun onAdapterClick(
        musicFilesModel: Any?,
        adapterPosition: Int,
        listMusicFiles: ArrayList<MusicFilesModel>,
    ) {
        CommonMethods.showLog(TAG,"get hit")
        var inner : MusicFilesModel = musicFilesModel as MusicFilesModel
        allMusicList!!.addAll(listMusicFiles)
        miniPlayerCall(adapterPosition)
    }

    private val broadcastReceiver : BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            var action : String = intent.getStringExtra("actionname")!!;
            if(action == MusicNotification.ACTION_PREVIOUS)
            {
                onTrackPrevious()
            }
            else if(action == MusicNotification.ACTION_PLAY)
            {
                if(isPlaying)
                {
                    onTrackPause()
                }
                else
                {
                    onTrackPlay()
                }
            }
            else if(action == MusicNotification.ACTION_NEXT)
            {
                onTrackNext()
            }

//            when (intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE,
//                WifiManager.WIFI_STATE_UNKNOWN)) {
//                WifiManager.WIFI_STATE_ENABLED -> {
//                    wifiSwitch.isChecked = true
//                    wifiSwitch.text = "WiFi is ON"
//                    Toast.makeText(this@MainActivity, "Wifi is On", Toast.LENGTH_SHORT).show()
//                }
//                WifiManager.WIFI_STATE_DISABLED -> {
//                    wifiSwitch.isChecked = false
//                    wifiSwitch.text = "WiFi is OFF"
//                    Toast.makeText(this@MainActivity, "Wifi is Off", Toast.LENGTH_SHORT).show()
//                }
//            }
        }
    }

    override fun onTrackPrevious() {
//        SongNumberPlaying --
//        MusicNotification.createNotification(this,SongNumberPlaying,allMusicList!!.size,sendPlayButton)
         prevSong()
    }

    override fun onTrackPlay() {
        playAndPause()
    }

    override fun onTrackPause() {
    }

    override fun onTrackNext() {
        nextSong()
    }
}