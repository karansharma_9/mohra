package com.napworks.mohra.activitiesPackage.PersonalityActivities

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.napworks.mohra.R
import com.napworks.mohra.activitiesPackage.HomeScreenActivity
import com.napworks.mohra.adapterPackage.AvtarRecyclerAdapter
import com.napworks.mohra.dialogPackage.CommonMessageDialog
import com.napworks.mohra.dialogPackage.LoadingDialog
import com.napworks.mohra.interfacePackage.GetUserPersonalityInterface
import com.napworks.mohra.modelPackage.DarkImagesModel
import com.napworks.mohra.modelPackage.PersonalityFinalScreenDataModel
import com.napworks.mohra.utilPackage.ApiCalls
import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.utilPackage.MyConstants
import kotlinx.android.synthetic.main.activity_personality_test_final.*
import kotlin.math.roundToInt

class PersonalityTestFinalActivity : AppCompatActivity(), View.OnClickListener,
    GetUserPersonalityInterface {

    private lateinit var avtarAdapter: AvtarRecyclerAdapter
    private lateinit var boxPersonality: LinearLayout
    var commonMessageDialog: CommonMessageDialog? = null
    var loadingDialog: LoadingDialog? = null
    var TAG = javaClass.simpleName;
    lateinit var personalityTitleTextView: TextView
    lateinit var name: TextView
    lateinit var shortDescriptionTextView: TextView
    lateinit var urlBit: TextView
    lateinit var copyLayout: LinearLayout
    lateinit var imageViewAvtar: ImageView
    private var sharedPreferences: SharedPreferences? = null


    var url = ""
    var Imageurl = ""
    var shortDescription = ""
    var titlePersonality = ""
    var sharedMessage = ""

    var AvtarList: ArrayList<DarkImagesModel>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_personality_test_final)
        AvtarList = ArrayList()
        boxPersonality = findViewById(R.id.boxPersonality)
        name = findViewById(R.id.name)
        commonMessageDialog = CommonMessageDialog(this)
        loadingDialog = LoadingDialog(this)

        sharedPreferences = getSharedPreferences(MyConstants.SHARED_PREFERENCE, MODE_PRIVATE)

        personalityTitleTextView = findViewById(R.id.personalityTitleTextView)
        shortDescriptionTextView = findViewById(R.id.shortDescriptionTextView)
        urlBit = findViewById(R.id.personalityUrl)
        imageViewAvtar = findViewById(R.id.imageViewAvtar)
        copyLayout = findViewById(R.id.copyLayout)
        boxPersonality.setOnClickListener(this)
        finishButtonPersonality.setOnClickListener(this)
        copyLayout.setOnClickListener(this)

        ApiGetData()
    }


    fun ApiGetData()
    {
        ApiCalls.getUserPersonality(
            sharedPreferences!!,
            this
        )
    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.boxPersonality)
        {
            val intent = Intent(baseContext, PersonalityDetailsActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
            intent.putExtra("url",url)
            intent.putExtra("Imageurl",Imageurl)
            intent.putExtra("shortDescription",shortDescription)
            intent.putExtra("titlePersonality",titlePersonality)
            intent.putExtra("sharedMessage",sharedMessage)
            startActivity(intent)
        }
        else if(v?.id == R.id.finishButtonPersonality)
        {
            val intent = Intent(baseContext, HomeScreenActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
            startActivity(intent)
            finish()
        }
        else if(v?.id == R.id.copyLayout) {
           shareData()
        }
    }

    override fun onGetUserPersonalityInterfaceResponse(
        status: String?,
        personalityFinalScreenDataModel: PersonalityFinalScreenDataModel?,
    ) {
        loadingDialog!!.hideDialog()
        when (status) {
            MyConstants.GO_TO_RESPONSE -> {
                CommonMethods.showLog(TAG,"send Otp  status  " + personalityFinalScreenDataModel!!.message)
                if (personalityFinalScreenDataModel != null) {
                    if (personalityFinalScreenDataModel.status != null) {
                        when {

                            personalityFinalScreenDataModel.status.equals("1") -> {

                                val firstName = sharedPreferences!!.getString(MyConstants.FIRST_NAME,"James")
                                name.setText(firstName.plus(" Personality"))

                                val innerData: ArrayList<DarkImagesModel> = personalityFinalScreenDataModel.darkImages
                                AvtarList = innerData
                                CommonMethods.showLog(TAG,"list size => " + AvtarList!!.size)
                                var screenWidth: Int = CommonMethods.getWidth(this) / 9
                                screenWidth = (screenWidth - CommonMethods.getScreenDensity(this) * 8).roundToInt()
                                avtarAdapter = AvtarRecyclerAdapter(this, AvtarList!!, screenWidth)
                                val gridLayoutManager = GridLayoutManager(this, 9)
                                recyclerViewAvtarPic.setLayoutManager(gridLayoutManager)
                                recyclerViewAvtarPic.setAdapter(avtarAdapter)
                                url = personalityFinalScreenDataModel.userPersonality.personalityUrl.toString()
                                sharedMessage = personalityFinalScreenDataModel.userPersonality.share.toString()
                                urlBit.text = personalityFinalScreenDataModel.userPersonality.personalityUrl.toString()
                                Glide.with(this).load(personalityFinalScreenDataModel.userPersonality.image).apply(
                                    RequestOptions()
                                    .placeholder(R.drawable.logosplashscreen))
                                    .into(imageViewAvtar)
                                Imageurl = personalityFinalScreenDataModel.userPersonality.image.toString()

                                shortDescription = personalityFinalScreenDataModel.userPersonality.shortDescription.toString()
                                shortDescriptionTextView.text = personalityFinalScreenDataModel.userPersonality.shortDescription.toString()

                                titlePersonality = personalityFinalScreenDataModel.userPersonality.personalityTitle.toString()

                                personalityTitleTextView.text = personalityFinalScreenDataModel.userPersonality.personalityTitle.toString()
                            }
                            personalityFinalScreenDataModel.status.equals("0") -> {
                                loadingDialog!!.hideDialog()
                                commonMessageDialog!!.showDialog(personalityFinalScreenDataModel.message)
                            }
                            personalityFinalScreenDataModel.status.equals("10") -> {
                                loadingDialog!!.hideDialog()
                                CommonMethods.callLogout(this)
                            }
                            else -> {
                                loadingDialog!!.hideDialog()
                                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                            }
                        }
                    } else {
                        loadingDialog!!.hideDialog()
                        commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                    }
                } else {
                    loadingDialog!!.hideDialog()
                    commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                }
            }
            MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                loadingDialog!!.hideDialog()
                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
            }
        }

    }

    fun shareData(){
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.putExtra(Intent.EXTRA_TEXT, sharedMessage)
//        shareIntent.putExtra(Intent.EXTRA_STREAM, listImages!![0])
        shareIntent.type = "text/plain"
        shareIntent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
        shareIntent.flags = Intent.FLAG_GRANT_WRITE_URI_PERMISSION
        shareIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(Intent.createChooser(shareIntent,title))
    }
}