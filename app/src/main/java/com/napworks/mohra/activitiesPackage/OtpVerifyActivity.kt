package com.napworks.mohra.activitiesPackage

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import com.google.firebase.auth.PhoneAuthCredential
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.PhoneAuthProvider
import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.R
import com.napworks.mohra.dialogPackage.CommonMessageDialog
import com.napworks.mohra.dialogPackage.LoadingDialog
import com.napworks.mohra.modelPackage.CommonStatusModel
import com.napworks.mohra.modelPackage.UserDataModel
import com.napworks.mohra.utilPackage.ApiCalls
import com.napworks.mohra.utilPackage.MyConstants
import kotlinx.android.synthetic.main.activity_otp_verify.*

class OtpVerifyActivity : AppCompatActivity(), View.OnClickListener, TextWatcher,
    com.napworks.mohra.interfacePackage.VerifyOtpInterface {
    lateinit var otpVerifyButton: TextView
    var TAG = javaClass.simpleName;
    lateinit var menuBackPress: ImageView
    lateinit var skipButtonToolBar: TextView
    private var mAuth: FirebaseAuth? = null
    lateinit var otpEditText1: EditText
    lateinit var otpEditText2: EditText
    lateinit var otpEditText3: EditText
    lateinit var otpEditText4: EditText
    lateinit var otpEditText5: EditText
    lateinit var otpEditText6: EditText
    var storedVerificationId :String = ""
    var loadingDialog: LoadingDialog? = null
    private lateinit var userData: UserDataModel
    var commonMessageDialog: CommonMessageDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp_verify)

        mAuth = FirebaseAuth.getInstance();

        commonMessageDialog = CommonMessageDialog(this)
        otpVerifyButton = findViewById(R.id.otpVerifyButton)
        menuBackPress =  findViewById(R.id.menuBackPress)
        userData=UserDataModel()
        loadingDialog = LoadingDialog(this)
        skipButtonToolBar =  findViewById(R.id.skipButtonToolBar)
        otpEditText1 = findViewById(R.id.otpEditText1)
        otpEditText2 = findViewById(R.id.otpEditText2)
        otpEditText3 = findViewById(R.id.otpEditText3)
        otpEditText4 = findViewById(R.id.otpEditText4)
        otpEditText5 = findViewById(R.id.otpEditText5)
        otpEditText6 = findViewById(R.id.otpEditText6)
        otpVerifyButton.setOnClickListener(this)
        menuBackPress.setOnClickListener(this)

        otpEditText1.addTextChangedListener(this)
        otpEditText2.addTextChangedListener(this)
        otpEditText3.addTextChangedListener(this)
        otpEditText4.addTextChangedListener(this)
        otpEditText5.addTextChangedListener(this)
        otpEditText6.addTextChangedListener(this)
        skipButtonToolBar.isVisible = false
         storedVerificationId= intent.getStringExtra("storedVerificationId")!!
        userData = intent.getSerializableExtra("CLIENT_DATA_FROM_GOOGLE_SCREEN") as UserDataModel
        otpDescription.text = "Enter verifications code below, the code sent to ${userData.phonenumber}"
    }

    fun verifyOtp(enteredCode: String)
    {
        val apiOtp = intent.extras!!.getString(MyConstants.OTP_API)
        val apiOtpId = intent.extras!!.getString(MyConstants.OTP_API_ID)

            loadingDialog!!.showDialog()
            ApiCalls.verifyOtp(
                apiOtpId,
                apiOtp,
                this
            )
    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.otpVerifyButton)
        {
            val enteredCode: String = otpEditText1.text.toString()
                    .trim { it <= ' ' } + otpEditText2.getText().toString()
                    .trim { it <= ' ' } + otpEditText3.getText().toString()
                    .trim { it <= ' ' } + otpEditText4.getText().toString()
                    .trim { it <= ' ' } + otpEditText5.getText().toString()
                    .trim { it <= ' ' } + otpEditText6.getText().toString()
                    .trim { it <= ' ' }
            if( enteredCode.length != 6)
            {
                commonMessageDialog?.showDialog(getString(R.string.enterValidOtp))
                otpEditText1.requestFocus()
            }
            else
            {
                val credential : PhoneAuthCredential = PhoneAuthProvider.getCredential(
                storedVerificationId.toString(), enteredCode)
                loadingDialog!!.showDialog()
                signInWithPhoneAuthCredential(credential)
//                verifyOtp(enteredCode)
            }


        }
        else if(v?.id == R.id.menuBackPress)
        {
            onBackPressed()
        }
    }


    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        mAuth!!.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    Toast.makeText(this,"Success OTP", Toast.LENGTH_SHORT).show()
                    loadingDialog!!.hideDialog()
                    val intent = Intent(baseContext, SetUserNameActivity::class.java)
                    intent.putExtra("CLIENT_DATA_FROM_OTP_SCREEN", userData);
                    startActivity(intent)

                } else {
                    loadingDialog!!.hideDialog()
                    // Sign in failed, display a message and update the UI
                    if (task.exception is FirebaseAuthInvalidCredentialsException) {
                        // The verification code entered was invalid
                        Toast.makeText(this,"Invalid OTP", Toast.LENGTH_SHORT).show()
                    }
                }
            }
    }

    override fun afterTextChanged(s: Editable?) {
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int)
    {
        if (s.hashCode() == otpEditText1.text.hashCode()) {
            if (s?.length == 1) {
                otpEditText2.requestFocus()
            } else {
            }
        } else if (s.hashCode() == otpEditText2.text.hashCode()) {
            if (s?.length == 1) {
                otpEditText3.requestFocus()
            } else {
                otpEditText1.requestFocus()
            }
        } else if (s.hashCode() == otpEditText3.text.hashCode()) {
            if (s?.length == 1) {
                otpEditText4.requestFocus()
            } else {
                otpEditText2.requestFocus()
            }
        } else if (s.hashCode() == otpEditText4.text.hashCode()) {
            if (s?.length == 1) {
                otpEditText5.requestFocus()
            } else {
                otpEditText3.requestFocus()
            }
        } else if (s.hashCode() == otpEditText5.text.hashCode()) {
            if (s?.length == 1) {
                otpEditText6.requestFocus()
            } else {
                otpEditText4.requestFocus()
            }
        } else if (s.hashCode() == otpEditText6.text.hashCode()) {
            if (s?.length == 0) {
                otpEditText5.requestFocus()
            } else {
            }
        }
    }

    override fun onVerifyOtpInterfaceResponse(
        status: String?,
        commonStatusModel: CommonStatusModel?
    )
    {
        when (status) {
            MyConstants.GO_TO_RESPONSE -> {
                if (commonStatusModel != null) {
                    CommonMethods.showLog(
                        TAG,
                        "Verify Otp API  : " + commonStatusModel.message
                    )
                    if (commonStatusModel.status != null) {
                        when {

                            commonStatusModel.status.equals("1") -> {
                                loadingDialog!!.hideDialog()
                                Toast.makeText(this, commonStatusModel.message, Toast.LENGTH_SHORT).show()
                                val intent = Intent(baseContext, SetUserNameActivity::class.java)
                                intent.putExtra("CLIENT_DATA_FROM_OTP_SCREEN", userData);
                                startActivity(intent)
                            }
                            commonStatusModel.status.equals("0") -> {
                                loadingDialog!!.hideDialog()
                                Toast.makeText(this, commonStatusModel.message, Toast.LENGTH_SHORT).show()
                                commonMessageDialog!!.showDialog(commonStatusModel.message)
                            }
                            commonStatusModel.status.equals("10") -> {
                                loadingDialog!!.hideDialog()
                                Toast.makeText(this, commonStatusModel.message, Toast.LENGTH_SHORT).show()
                                CommonMethods.callLogout(this)
                            }
                            else -> {
                                loadingDialog!!.hideDialog()
                                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                            }
                        }
                    } else {
                        loadingDialog!!.hideDialog()
                        commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                    }
                } else {
                    loadingDialog!!.hideDialog()
                    commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                }
            }
            MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                loadingDialog!!.hideDialog()
                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
            }
        }

    }
}