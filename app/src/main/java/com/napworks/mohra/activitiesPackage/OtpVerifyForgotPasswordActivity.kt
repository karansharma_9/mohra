package com.napworks.mohra.activitiesPackage

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import com.napworks.mohra.R
import com.napworks.mohra.activitiesPackage.PersonalityActivities.WelcomePersonalityTestActivity
import com.napworks.mohra.dialogPackage.CommonMessageDialog
import com.napworks.mohra.dialogPackage.LoadingDialog
import com.napworks.mohra.interfacePackage.LoginInterface
import com.napworks.mohra.modelPackage.LoginInnerModel
import com.napworks.mohra.modelPackage.LoginModel
import com.napworks.mohra.utilPackage.ApiCalls
import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.utilPackage.MyConstants

class OtpVerifyForgotPasswordActivity : AppCompatActivity(), TextWatcher, View.OnClickListener,
    LoginInterface {
    var TAG = javaClass.simpleName;
    var loadingDialog: LoadingDialog? = null
    var commonMessageDialog: CommonMessageDialog? = null
    lateinit var menuBackPress: ImageView

    lateinit var otpVerify: TextView
    var storedVerificationId :String = ""
    var userId :String = ""
    var calledFrom :String = ""
    var finalNumber :String = ""
    var countryId :String = ""
    var mobileWithoutCode :String = ""
    private var mAuth: FirebaseAuth? = null
    private var sharedPreferences: SharedPreferences? = null


    lateinit var otpEditText1: EditText
    lateinit var otpEditText2: EditText
    lateinit var otpEditText3: EditText
    lateinit var otpEditText4: EditText
    lateinit var otpEditText5: EditText
    lateinit var otpEditText6: EditText
    lateinit var otpDescription: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp_verify_forgot_password)
        commonMessageDialog = CommonMessageDialog(this)
        loadingDialog = LoadingDialog(this)
        mAuth = FirebaseAuth.getInstance();
        sharedPreferences = getSharedPreferences(MyConstants.SHARED_PREFERENCE, MODE_PRIVATE)

        menuBackPress =  findViewById(R.id.menuBackPress)
        otpDescription = findViewById(R.id.otpDescription)
        otpEditText1 = findViewById(R.id.otpForgotEditText1)
        otpEditText2 = findViewById(R.id.otpForgotEditText2)
        otpEditText3 = findViewById(R.id.otpForgotEditText3)
        otpEditText4 = findViewById(R.id.otpForgotEditText4)
        otpEditText5 = findViewById(R.id.otpForgotEditText5)
        otpEditText6 = findViewById(R.id.otpForgotEditText6)
        otpVerify = findViewById(R.id.otpVerify)
        menuBackPress.setOnClickListener(this)
        otpVerify.setOnClickListener(this)
        storedVerificationId= intent.getStringExtra("storedVerificationId").toString()
        userId= intent.getStringExtra("userId").toString()
        calledFrom= intent.getStringExtra(MyConstants.CALLED_FROM).toString()
        finalNumber= intent.getStringExtra(MyConstants.MOBILE).toString()
        countryId= intent.getStringExtra(MyConstants.COUNTRY_ID).toString()
        mobileWithoutCode= intent.getStringExtra("Mobile_Without_Code").toString()
        CommonMethods.showLog(TAG,"storedVerificationId  otp   =>  $storedVerificationId")

        otpDescription.text="Enter verification code below, the code sent to ".plus(finalNumber)


        otpEditText1.addTextChangedListener(this)
        otpEditText2.addTextChangedListener(this)
        otpEditText3.addTextChangedListener(this)
        otpEditText4.addTextChangedListener(this)
        otpEditText5.addTextChangedListener(this)
        otpEditText6.addTextChangedListener(this)
    }


    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int)
        {
            if (s.hashCode() == otpEditText1.text.hashCode()) {
                if (s?.length == 1) {
                    otpEditText2.requestFocus()
                } else {
                }
            } else if (s.hashCode() == otpEditText2.text.hashCode()) {
                if (s?.length == 1) {
                    otpEditText3.requestFocus()
                } else {
                    otpEditText1.requestFocus()
                }
            } else if (s.hashCode() == otpEditText3.text.hashCode()) {
                if (s?.length == 1) {
                    otpEditText4.requestFocus()
                } else {
                    otpEditText2.requestFocus()
                }
            } else if (s.hashCode() == otpEditText4.text.hashCode()) {
                if (s?.length == 1) {
                    otpEditText5.requestFocus()
                } else {
                    otpEditText3.requestFocus()
                }
            } else if (s.hashCode() == otpEditText5.text.hashCode()) {
                if (s?.length == 1) {
                    otpEditText6.requestFocus()
                } else {
                    otpEditText4.requestFocus()
                }
            } else if (s.hashCode() == otpEditText6.text.hashCode()) {
                if (s?.length == 0) {
                    otpEditText5.requestFocus()
                } else {
                }
            }
        }


    override fun afterTextChanged(s: Editable?) {
    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.menuBackPress)
        {
            onBackPressed()
        }
        else if(v?.id == R.id.otpVerify)
        {
            val enteredCode: String = otpEditText1.text.toString()
                .trim { it <= ' ' } + otpEditText2.getText().toString()
                .trim { it <= ' ' } + otpEditText3.getText().toString()
                .trim { it <= ' ' } + otpEditText4.getText().toString()
                .trim { it <= ' ' } + otpEditText5.getText().toString()
                .trim { it <= ' ' } + otpEditText6.getText().toString()
                .trim { it <= ' ' }
            if( enteredCode.length != 6)
            {
                commonMessageDialog?.showDialog(getString(R.string.enterValidOtp))
                otpEditText1.requestFocus()
            }
            else
            {
                val credential : PhoneAuthCredential = PhoneAuthProvider.getCredential(
                    storedVerificationId.toString(), enteredCode)
                loadingDialog!!.showDialog()
                signInWithPhoneAuthCredential(credential)
            }

        }
    }


    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        mAuth!!.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {

                    if (calledFrom == MyConstants.LOGIN){
                        loadingDialog!!.showDialog()
                        ApiCalls.login(
                            "",
                            "",
                            MyConstants.MOBILE,
                            "",
                            "",
                            "",
                            "",
                            mobileWithoutCode,
                            0,
                            "",
                            "",
                            countryId,
                            this

                        )
                    }
                    else{
                        loadingDialog!!.hideDialog()
                        val intent = Intent(baseContext, ResetPasswordActivity::class.java)
                        intent.putExtra("userId",userId)
                        startActivity(intent)
                    }


                } else {
                    // Sign in failed, display a message and update the UI
                    if (task.exception is FirebaseAuthInvalidCredentialsException) {
                        // The verification code entered was invalid
                        Toast.makeText(this,"Invalid OTP", Toast.LENGTH_SHORT).show()
                    }
                }
            }
    }

    override fun onLoginInterfaceResponse(status: String?, commonStatusModel: LoginModel?) {
        when (status) {
            MyConstants.GO_TO_RESPONSE -> {
                CommonMethods.showLog(TAG,"login  status  " + commonStatusModel!!.message)
                if (commonStatusModel != null) {
                    if (commonStatusModel.status != null) {
                        when {
                            commonStatusModel.status.equals("1") -> {
                                loadingDialog!!.hideDialog()
                                val innerData: LoginInnerModel = commonStatusModel.userData
                                CommonMethods.showLog(TAG,"usertype    " + innerData.userType)
                                CommonMethods.showLog(TAG,"userid    " + innerData.userId)
                                if(innerData.userType == null)
                                {
                                    CommonMethods.showLog(TAG,"Udated it " + innerData.userId)
                                    val editor = sharedPreferences!!.edit()
                                    editor.putString(MyConstants.AUTH, innerData.auth)
                                    editor.putString(MyConstants.SESSION_TOKEN, innerData.session)
                                    editor.putString(MyConstants.USER_ID, innerData.userId)
                                    editor.putString(MyConstants.NAME, innerData.firstName)
                                    editor.putString(MyConstants.EMAIL, innerData.email)
                                    editor.putString(MyConstants.GOOGLE_ID, innerData.googleId)
                                    editor.putString(MyConstants.FACEBOOK_ID, innerData.facebookId)
                                    editor.putString(MyConstants.FIRST_NAME, innerData.firstName)
                                    editor.putInt(MyConstants.VERIFY, innerData.isPersonalityVerified!!)
                                    editor.putBoolean(MyConstants.IS_LOGGED_IN, true)
                                    editor.apply()
                                    val intent = Intent(this, RegisterIntroActivity::class.java)
                                    intent.putExtra(MyConstants.USER_ID,innerData.userId) // getText() SHOULD NOT be static!!!
                                    startActivity(intent)
                                    finishAffinity()
                                }
                                else
                                {
                                    val editor = sharedPreferences!!.edit()
                                    editor.putString(MyConstants.AUTH, innerData.auth)
                                    editor.putString(MyConstants.SESSION_TOKEN, innerData.session)
                                    editor.putString(MyConstants.USER_ID, innerData.userId)
                                    editor.putString(MyConstants.NAME, innerData.firstName)
                                    editor.putString(MyConstants.FAMILY_NAME, innerData.familyName)
                                    editor.putString(MyConstants.EMAIL, innerData.email)
                                    editor.putString(MyConstants.GOOGLE_ID, innerData.googleId)
                                    editor.putString(MyConstants.FACEBOOK_ID, innerData.facebookId)
                                    editor.putString(MyConstants.FIRST_NAME, innerData.firstName)
                                    editor.putString(MyConstants.USERTYPE, innerData.userType)
                                    editor.putString(MyConstants.USERNAME, innerData.userName)
                                    editor.putInt(MyConstants.VERIFY, innerData.isPersonalityVerified!!)
                                    editor.putBoolean(MyConstants.IS_LOGGED_IN, true)
                                    editor.apply()
                                    CommonMethods.showLog(TAG,"USERID ======>  "+innerData.userId )
                                    if(innerData.isPersonalityVerified == 0)
                                    {
                                        CommonMethods.showLog(TAG,"verified not")
                                        Toast.makeText(this, commonStatusModel.message, Toast.LENGTH_SHORT).show()
                                        val intent = Intent(baseContext, WelcomePersonalityTestActivity::class.java)
                                        startActivity(intent)
                                        finishAffinity()
                                    }
                                    else
                                    {
                                        CommonMethods.showLog(TAG,"verified ")
                                        Toast.makeText(this, commonStatusModel.message, Toast.LENGTH_SHORT).show()
                                        val intent = Intent(baseContext, HomeScreenActivity::class.java)
                                        startActivity(intent)
                                        finishAffinity()
                                    }
                                }
                            }
                            commonStatusModel.status.equals("0") -> {
                                loadingDialog!!.hideDialog()
                                commonMessageDialog!!.showDialog(commonStatusModel.message)
                            }
                            commonStatusModel.status.equals("10") -> {
                                loadingDialog!!.hideDialog()
                                CommonMethods.callLogout(this)
                            }
                            else -> {
                                loadingDialog!!.hideDialog()
                                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                            }
                        }
                    } else {
                        loadingDialog!!.hideDialog()
                        commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                    }
                } else {
                    loadingDialog!!.hideDialog()
                    commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                }
            }
            MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                loadingDialog!!.hideDialog()
                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
            }
        }
    }
}