package com.napworks.mohra.activitiesPackage

import android.Manifest
import android.R.attr.data
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.firebase.FirebaseApp
import com.napworks.mohra.R
import com.napworks.mohra.activitiesPackage.PersonalityActivities.PersonalityTestFinalActivity
import com.napworks.mohra.activitiesPackage.PersonalityActivities.WelcomePersonalityTestActivity
import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.utilPackage.MyConstants
import kotlinx.android.synthetic.main.dialog_loading.*


class  SplashScreenActivity : AppCompatActivity() {

    var TAG = javaClass.simpleName;
    var animation: Animation? = null
    lateinit var logo: ImageView
    var sharedPreferences: SharedPreferences? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_screen)

        CommonMethods.getHashKey(this)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        animation = AnimationUtils.loadAnimation(this, R.anim.fade_in)
        logo = findViewById(R.id.logo)
        sharedPreferences = getSharedPreferences(MyConstants.SHARED_PREFERENCE, MODE_PRIVATE)

        CommonMethods.showLog(TAG,"check    " + sharedPreferences?.getBoolean(MyConstants.IS_LOGGED_IN, false))

        if (sharedPreferences?.getBoolean(MyConstants.IS_LOGGED_IN, false)!! == false) {
            CommonMethods.showLog(TAG,"login false")

                val intent = Intent(baseContext, WelcomeActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                startActivity(intent)
                finish()
                overridePendingTransition(0, 0)

//            }

        }
        else
        {
            CommonMethods.showLog(TAG,"login true")
            val USERTYPE =sharedPreferences!!.getString(MyConstants.USERTYPE, "")
            val USERNAME =sharedPreferences!!.getString(MyConstants.USERNAME, "")
            val userId =sharedPreferences!!.getString(MyConstants.USER_ID, "")
            CommonMethods.showLog(TAG,"USERTYPE  $USERTYPE  USERNAME  $USERNAME   userId   $userId")
            animation = AnimationUtils.loadAnimation(this, R.anim.fade_in)
            logo.animation = animation
            animation?.start()
            animation?.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(animation: Animation) {}
                override fun onAnimationEnd(animation: Animation) {


                    if(USERTYPE == "")
                    {
                        CommonMethods.showLog(TAG,"1 hit")

                        val intent = Intent(baseContext, RegisterIntroActivity::class.java)
                        intent.putExtra(MyConstants.USER_ID,userId) // getText() SHOULD NOT be static!!!
                        startActivity(intent)
                        finish()
                        overridePendingTransition(0, 0)
                    }
                    else if(USERNAME == "")
                    {
                        CommonMethods.showLog(TAG,"2 hit")

                        val intent = Intent(baseContext, SetUserNameActivity::class.java)
                        intent.putExtra(MyConstants.UDATEUSERNAME,"true") // getText() SHOULD NOT be static!!!
                        startActivity(intent)
                        finish()
                        overridePendingTransition(0, 0)
                    }
                    else if(sharedPreferences!!.getInt(MyConstants.VERIFY,0)==0)
                    {
                        CommonMethods.showLog(TAG,"3 hit")
                        val intent = Intent(baseContext, WelcomePersonalityTestActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                        startActivity(intent)
                        finish()
                        overridePendingTransition(0, 0)
                    }
                    else
                    {
                        val intent = Intent(baseContext, HomeScreenActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                        startActivity(intent)
                        finish()
                        overridePendingTransition(0, 0)
                    }
                }

                override fun onAnimationRepeat(animation: Animation) {}
            })
        }

//        val intent = Intent(this, PlaylistDetails::class.java)
//        intent.putExtra(MyConstants.CALLED_FROM,MyConstants.CREATE_PLAYLIST)
//        intent.putExtra(MyConstants.ID,"1")
//        startActivity(intent)
    }
}