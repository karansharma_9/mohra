package com.napworks.mohra.activitiesPackage.MusicActivities

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.provider.ContactsContract
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.napworks.mohra.R
import com.napworks.mohra.activitiesPackage.RegisterIntroActivity
import com.napworks.mohra.dialogPackage.CommonMessageDialog
import com.napworks.mohra.dialogPackage.ImageChooserDialog
import com.napworks.mohra.dialogPackage.LoadingDialog
import com.napworks.mohra.interfacePackage.UploadImageInterface
import com.napworks.mohra.interfacePackage.music.CreatePlaylistInterface
import com.napworks.mohra.modelPackage.FileChoosedModel
import com.napworks.mohra.modelPackage.MusicModel.CreatePlaylistResponse
import com.napworks.mohra.modelPackage.MusicModel.MusicInnerModel
import com.napworks.mohra.utilPackage.ApiCalls
import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.utilPackage.MyConstants
import com.theartofdev.edmodo.cropper.CropImage
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.lang.Exception


class CreatePlaylistActivity : AppCompatActivity(), View.OnClickListener, UploadImageInterface,
    CreatePlaylistInterface {
    private var profileUrl: String=""
    private var path: String? = ""
    var TAG = javaClass.simpleName;
    lateinit var imageOfPlaylist: ImageView
    lateinit var nameOfPlaylist: EditText
    lateinit var createPlaylistButton: LinearLayout
    lateinit var menuBackPress: ImageView
    lateinit var cameraIcon: ImageView
    private var sharedPreferences: SharedPreferences? = null

    var loadingDialog: LoadingDialog? = null
    var commonMessageDialog: CommonMessageDialog? = null


    lateinit var imageChooserDialog: ImageChooserDialog

    var imageFilled = 0;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_playlist)
        commonMessageDialog = CommonMessageDialog(this)
        loadingDialog = LoadingDialog(this)
        imageChooserDialog = ImageChooserDialog(this)
        sharedPreferences = getSharedPreferences(MyConstants.SHARED_PREFERENCE, MODE_PRIVATE)


        imageOfPlaylist = findViewById(R.id.imagePicker)
        nameOfPlaylist = findViewById(R.id.nameOfPlaylist)
        createPlaylistButton = findViewById(R.id.createPlaylistButton)
        menuBackPress = findViewById(R.id.menuBackPress)
        cameraIcon = findViewById(R.id.cameraIcon)
        imageOfPlaylist.setOnClickListener(this)
        menuBackPress.setOnClickListener(this)
        nameOfPlaylist.requestFocus()
        nameOfPlaylist.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(str: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(str: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(str: Editable) {
                CommonMethods.showLog(TAG, "imageFilled  => $imageFilled")
                if (str.toString().trim { it <= ' ' }.isNotEmpty() && path != "") {
                    CommonMethods.showLog(TAG, "Not empty")
                    createPlaylistButton.setBackgroundResource(R.color.colorPrimary)
                } else {
                    CommonMethods.showLog(TAG, "Empty")
                    createPlaylistButton.setBackgroundResource(R.color.grey)
                }
            }
        })

        createPlaylistButton.setOnClickListener(this)
    }

    fun pickImageFromGallery() {
        CommonMethods.showLog(TAG, "hit picker")
        var pickPhoto: Intent =
            Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(pickPhoto, 1)
    }

//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        CommonMethods.showLog(TAG,"requestCode  => $requestCode  resultCode  => $resultCode")
//        if(resultCode == -1)
//        {
//            var selectedImage : Uri? = data!!.data
//            CommonMethods.showLog(TAG,"uri  => $selectedImage ")
////            imageOfPlaylist.setImageURI(selectedImage)
//            var requestOptions = RequestOptions()
//            requestOptions = requestOptions.transforms(CenterCrop(), RoundedCorners(16))
//            Glide.with(this)
//                .load(selectedImage)
//                .apply(requestOptions)
//                .into(imageOfPlaylist)
//                 imageFilled = 1
//            CommonMethods.showLog(TAG,"text  => " + nameOfPlaylist.text.length)
//            if(nameOfPlaylist.text.length != 0 )
//            {
//                createPlaylistButton.setBackgroundResource(R.color.colorPrimary)
//            }
//        }
//        else
//        {
//            CommonMethods.showLog(TAG,"Nothing select")
//        }
//    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.imagePicker) {
            imageChooserDialog.showDialog()
//            pickImageFromGallery()
        } else if (v?.id == R.id.menuBackPress) {
            val i = Intent(applicationContext, MusicMainActivity::class.java)
            i.putExtra("currentLocation", 1)
            startActivity(i)
            finishAffinity()
        }
        else if (v?.id == R.id.createPlaylistButton){
            val name: String = nameOfPlaylist.text.toString().trim()
            if (name.isNotEmpty() && path != ""){
                loadingDialog!!.showDialog()
                ApiCalls.uploadImage(sharedPreferences!!, path, MyConstants.PLAYLIST_IMAGE, this)
            }

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            MyConstants.GALLERY_RESPONSE_CODE -> if (resultCode == RESULT_OK) {
                CommonMethods.showLog(TAG, "GalleryWorks")
                if (data != null && data.data != null) {
                    try {
                        CropImage.activity(data.data)
                            .start(this)
                    } catch (e: Exception) {
                        CommonMethods.showLog(TAG, "error " + e.message)
                    }
                }
            }
            CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> {
                val imgResult = CropImage.getActivityResult(data)
                if (resultCode == RESULT_OK) {
                    if (imgResult != null) {
                        val resultUri: Uri = imgResult.getUri()
                        val fileChoosedModel: FileChoosedModel =
                            CommonMethods.checkFileValidOrNot(this, resultUri)!!
                        var path: String = fileChoosedModel.getPath()
                        this.path = path
                        CommonMethods.showLog(TAG, "Image Path :$path")
                        var requestOptions = RequestOptions()
                        requestOptions = requestOptions.placeholder(R.drawable.logosplashscreen)
                        requestOptions = requestOptions.error(R.drawable.logosplashscreen)
                        requestOptions = requestOptions.transforms(CenterCrop(), RoundedCorners(10))
                        Glide.with(this@CreatePlaylistActivity)
                            .load(path)
                            .apply(requestOptions)
                            .into(imageOfPlaylist)
                        cameraIcon.visibility = View.GONE
                        if (nameOfPlaylist.text.length != 0) {
                            createPlaylistButton.setBackgroundResource(R.color.colorPrimary)
                        }

                    }
                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    if (imgResult != null) {
                        val error: Exception = imgResult.getError()
                        CommonMethods.showLog(TAG, "error : " + error.message)
                        Toast.makeText(this, error.localizedMessage, Toast.LENGTH_SHORT).show()
                    }
                } else {
                    CommonMethods.showLog(TAG, "Cancelled")
                }
            }
            MyConstants.CAMERA_IMAGE_REQUEST_CODE -> if (resultCode == RESULT_OK) {
                CommonMethods.showLog(TAG, "CameraWorks")
                Handler().postDelayed({
                    val dummyPath = imageChooserDialog.path
                    CommonMethods.showLog(TAG, "Dummy Path :$dummyPath")
                    val file = File(dummyPath)
                    CommonMethods.showLog(TAG, "Uri :" + Uri.fromFile(file).toString())
                    if (file.exists()) {
                        path = dummyPath
                        val uri = Uri.parse(Uri.fromFile(file).toString())
                        try {
                            CropImage.activity(uri)
                                .start(this@CreatePlaylistActivity)
                        } catch (e: Exception) {
                            CommonMethods.showLog(TAG, "error " + e.message)
                        }
                    } else {
                        val result = file.delete()
                        CommonMethods.showLog(TAG, "Delete result $result")
                    }
                }, 1000)
            } else {
                val file = File(imageChooserDialog.path)
                if (file.exists()) {
                    val result = file.delete()
                    CommonMethods.showLog(TAG, "Delete result Else $result")
                }
            }
        }
    }

    override fun onUploadImageResponse(status: String?, jsonString: String?, type: String?) {
        when (status) {
            "1" -> try {
                CommonMethods.showLog(TAG, "Response $jsonString")
                val jsonObject = JSONObject(jsonString)
                CommonMethods.showLog(TAG, "Size $jsonObject")
                if (jsonObject.getString("status").equals("1", ignoreCase = true)) {
                    val url = jsonObject.getString("pic")
                    CommonMethods.showLog(TAG, "URL : $url")
                    if (type.equals(MyConstants.PLAYLIST_IMAGE, ignoreCase = true)) {
                        profileUrl = url
                        hitCreatePlaylistApi()
                    }
                } else if (jsonObject.getString("status").equals("0", ignoreCase = true)) {
                    loadingDialog!!.hideDialog()
                    val message = jsonObject.getString("message")
                    commonMessageDialog!!.showDialog(message)
                } else if (jsonObject.getString("status").equals("10", ignoreCase = true)) {
                    loadingDialog!!.hideDialog()
                    CommonMethods.callLogout(this)
                } else {
                    loadingDialog!!.hideDialog()
                    commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                }
            } catch (e: JSONException) {
                loadingDialog!!.hideDialog()
                e.printStackTrace()
                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
            }
            "0" -> {
                loadingDialog!!.hideDialog()
                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
            }
            else -> {
                loadingDialog!!.hideDialog()
                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
            }
        }
    }

    private fun hitCreatePlaylistApi() {
        ApiCalls.createPlaylist(sharedPreferences!!,nameOfPlaylist.text.toString().trim(),profileUrl,this)

    }

    override fun onCreatePlaylistResponse(
        status: String?,
        createPlaylistResponse: CreatePlaylistResponse?
    ) {
        loadingDialog!!.hideDialog()
        when (status) {
            MyConstants.GO_TO_RESPONSE -> {
                if (createPlaylistResponse != null) {
                    if (createPlaylistResponse.status != null) {
                        CommonMethods.showLog(TAG,"getMusicHomeData Status : "+createPlaylistResponse.status)
                        when {
                            createPlaylistResponse.status.equals("1") -> {
                                val intent = Intent(this, PlaylistDetails::class.java)
                                intent.putExtra(MyConstants.CALLED_FROM,MyConstants.CREATE_PLAYLIST)
                                intent.putExtra(MyConstants.ID,createPlaylistResponse.data.id)
                                startActivity(intent)
                            }
                            createPlaylistResponse.status.equals("0") -> {
                                CommonMethods.showLog(TAG, createPlaylistResponse.message)
                                commonMessageDialog!!.showDialog(createPlaylistResponse.message)

                            }
                            createPlaylistResponse.status.equals("10") -> {
                                CommonMethods.showLog(TAG, createPlaylistResponse.message)
//                                commonMessageDialog!!.showDialog(createPlaylistResponse.message)
                                CommonMethods.callLogout(this)

                            }
                            else -> {
                                CommonMethods.showLog(TAG,
                                    R.string.someErrorOccurredText.toString())
                                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))

                            }
                        }
                    } else {
                        CommonMethods.showLog(TAG,
                            R.string.someErrorOccurredText.toString())
                        commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))

                    }
                } else {
                    CommonMethods.showLog(TAG,
                        R.string.someErrorOccurredText.toString())
                    commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))

                }
            }
            MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                CommonMethods.showLog(TAG,
                    R.string.someErrorOccurredText.toString())
                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))

            }
        }
    }
}