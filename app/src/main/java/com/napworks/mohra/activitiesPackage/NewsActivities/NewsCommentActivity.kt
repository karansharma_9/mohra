package com.napworks.mohra.activitiesPackage.NewsActivities

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.napworks.mohra.R
import com.napworks.mohra.adapterPackage.News.CommentsNewsRecyclerAdapter
import com.napworks.mohra.dialogPackage.CommonMessageDialog
import com.napworks.mohra.dialogPackage.LoadingDialog
import com.napworks.mohra.interfacePackage.News.AddCommentsInterface
import com.napworks.mohra.interfacePackage.News.CommentsNewsInterface
import com.napworks.mohra.modelPackage.News.*
import com.napworks.mohra.utilPackage.ApiCalls
import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.utilPackage.MyConstants

class NewsCommentActivity : AppCompatActivity(), CommentsNewsInterface, View.OnClickListener,
    AddCommentsInterface {

    var TAG = javaClass.simpleName;
    private var sharedPreferences: SharedPreferences? = null
    var newsID = 0
    var commonMessageDialog: CommonMessageDialog? = null
    var loadingDialog: LoadingDialog? = null
    var noData: LinearLayout? = null
    lateinit var commentList: ArrayList<getCommentsNewsInnerModel>
    lateinit var recyclerViewComments: RecyclerView
    lateinit var editTextComment: EditText
    lateinit var sendComment: ImageView
    lateinit var menuBackPress: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_comment)

        sharedPreferences = getSharedPreferences(MyConstants.SHARED_PREFERENCE, MODE_PRIVATE)
        newsID = intent.extras!!.getInt("newsID")
        loadingDialog = LoadingDialog(this)
        editTextComment = findViewById(R.id.editTextComment)
        sendComment = findViewById(R.id.sendComment)
        menuBackPress = findViewById(R.id.menuBackPress)
        noData = findViewById(R.id.noData)

        commonMessageDialog = CommonMessageDialog(this)
        commentList = ArrayList<getCommentsNewsInnerModel>()
        recyclerViewComments = findViewById(R.id.recyclerViewComments)

        getCommentsApi(sharedPreferences!!,newsID)
        sendComment.setOnClickListener(this)
        menuBackPress.setOnClickListener(this)
    }

    fun getCommentsApi(sharedPreferences: SharedPreferences, newsID: Int)
    {
        loadingDialog!!.showDialog()
        ApiCalls.getComments(
            sharedPreferences,
            newsID.toString(),
            "news",
            this
        )
    }

    override fun onCommentsNewsResponse(status: String?, getCommentsNewsModel: GetCommentsNewsModel? )
    {
        when (status) {
            MyConstants.GO_TO_RESPONSE -> {
                if (getCommentsNewsModel != null) {
                    if (getCommentsNewsModel.status != null) {
                        when {
                            getCommentsNewsModel.status.equals("1") -> {
                                commentList.clear()
                                loadingDialog!!.hideDialog()
                                var innerdata : ArrayList<getCommentsNewsInnerModel> = getCommentsNewsModel.commentList
                                commentList.addAll(innerdata)
                                if(commentList.size == 0)
                                {
                                    noData!!.visibility = View.VISIBLE
                                    recyclerViewComments!!.visibility = View.GONE
                                }
                                else
                                {
                                    noData!!.visibility = View.GONE
                                    recyclerViewComments!!.visibility = View.VISIBLE
                                }
                                recyclerViewComments.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, true)
                                val CommentsNewsRecyclerAdapter = CommentsNewsRecyclerAdapter(this!!, commentList!!)
                                recyclerViewComments.adapter = CommentsNewsRecyclerAdapter

                                val intent = Intent()
                                intent.putExtra(
                                    MyConstants.BROADCAST_TYPE,
                                    MyConstants.UPDATE_COMMENTS
                                )
                                intent.action = MyConstants.SINGLE_NEWS_ACTIVITY
                                sendBroadcast(intent)

                            }
                            getCommentsNewsModel.status.equals("0") -> {
                                loadingDialog!!.hideDialog()
                                CommonMethods.showLog(TAG, getCommentsNewsModel.message)
                            }
                            getCommentsNewsModel.status.equals("10") -> {
                                loadingDialog!!.hideDialog()
                                CommonMethods.showLog(TAG, getCommentsNewsModel.message)
                            }
                            else -> {
                                loadingDialog!!.hideDialog()
                                CommonMethods.showLog(TAG,
                                    R.string.someErrorOccurredText.toString())
                            }
                        }
                    } else {
                        loadingDialog!!.hideDialog()
                        CommonMethods.showLog(TAG,
                            R.string.someErrorOccurredText.toString())
                    }
                } else {
                    loadingDialog!!.hideDialog()
                    CommonMethods.showLog(TAG,
                        R.string.someErrorOccurredText.toString())
                }
            }
            MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                CommonMethods.showLog(TAG,
                    R.string.someErrorOccurredText.toString())
            }
        }
    }

    override fun onClick(v: View?) {
        if(v!!.id == R.id.sendComment)
        {
            val messageText: String = editTextComment.text.toString().trim()
            if (TextUtils.isEmpty(messageText)) {
                commonMessageDialog?.showDialog(getString(R.string.enterComment))
                editTextComment.requestFocus()
            }
            else
            {
                editTextComment.text.clear()
                apiSendComment(messageText,sharedPreferences!!)
            }

        }
        else if (v!!.id == R.id.menuBackPress)
        {
            onBackPressed()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    private fun apiSendComment(messageText: String, sharedPreferences: SharedPreferences) {

        ApiCalls.addComment(
            sharedPreferences,
            newsID.toString(),
            "news",
            messageText,
            this
        )
    }

    override fun onAddCommentsResponse(status: String?, addCommentModel: AddCommentModel?)
    {
        when (status) {
            MyConstants.GO_TO_RESPONSE -> {
                if (addCommentModel != null) {
                    if (addCommentModel.status != null) {
                        when {
                            addCommentModel.status.equals("1") ->
                            {
                                CommonMethods.showLog(TAG,"message  ===> " + addCommentModel.message)
                                getCommentsApi(sharedPreferences!!,newsID)
                            }
                            addCommentModel.status.equals("0") -> {
                                loadingDialog!!.hideDialog()
                                CommonMethods.showLog(TAG, addCommentModel.message)
                            }
                            addCommentModel.status.equals("10") -> {
                                loadingDialog!!.hideDialog()
                                CommonMethods.showLog(TAG, addCommentModel.message)
                            }
                            else -> {
                                loadingDialog!!.hideDialog()
                                CommonMethods.showLog(TAG,
                                    R.string.someErrorOccurredText.toString())
                            }
                        }
                    } else {
                        loadingDialog!!.hideDialog()
                        CommonMethods.showLog(TAG,
                            R.string.someErrorOccurredText.toString())
                    }
                } else {
                    loadingDialog!!.hideDialog()
                    CommonMethods.showLog(TAG,
                        R.string.someErrorOccurredText.toString())
                }
            }
            MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                CommonMethods.showLog(TAG,
                    R.string.someErrorOccurredText.toString())
            }
        }
    }
}