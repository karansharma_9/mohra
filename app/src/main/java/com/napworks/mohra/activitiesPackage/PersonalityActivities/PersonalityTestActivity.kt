package com.napworks.mohra.activitiesPackage.PersonalityActivities

import android.content.SharedPreferences
import android.os.Bundle
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.R
import com.napworks.mohra.designPackage.CustomPersonalityTestContainer
import com.napworks.mohra.dialogPackage.CommonMessageDialog
import com.napworks.mohra.dialogPackage.LoadingDialog
import com.napworks.mohra.modelPackage.*
import com.napworks.mohra.utilPackage.ApiCalls
import com.napworks.mohra.utilPackage.MyConstants


class PersonalityTestActivity : AppCompatActivity(),
    com.napworks.mohra.interfacePackage.GetPersonalityDataInterface {

    var productList: ArrayList<GetPersonalityDataInnerModel>? = null
    var TAG = javaClass.simpleName;
//    var answereList: ArrayList<AnswerModel>? = null
    var mainContainerLayout: LinearLayout? = null
    var loadingDialog: LoadingDialog? = null

    var commonMessageDialog: CommonMessageDialog? = null
    private var sharedPreferences: SharedPreferences? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        commonMessageDialog = CommonMessageDialog(this)
        setContentView(R.layout.activity_personality_test)
        sharedPreferences = getSharedPreferences(MyConstants.SHARED_PREFERENCE, MODE_PRIVATE)
        loadingDialog = LoadingDialog(this)

        mainContainerLayout = findViewById(R.id.mainContainerLayout)

        ApiCalls.getPersonalityData(
            this
        )

//        productList = ArrayList<QuestionAnswereModel>()
//        answereList = ArrayList<AnswerModel>()
//
//
//        answereList!!.add(AnswerModel("1","Male","",R.drawable.male,false))
//        answereList!!.add(AnswerModel("2","Female","",R.drawable.female,false))
//        productList!!.add(
//                QuestionAnswereModel(" Your Personality","1","what is your gender", MyConstants.PERSONALITY, 10,answereList!!,"description",false)
//        )
//        answereList=ArrayList<AnswerModel>()
//
//
//
//
//        answereList!!.add(AnswerModel("3","Go With Flow","",R.drawable.gowithflow,false))
//        answereList!!.add(AnswerModel("4","Plan Ahead","",R.drawable.planahead,false))
//        productList!!.add(
//                QuestionAnswereModel(" Your Personality","2","How oraganized you are ?",MyConstants.PERSONALITY,20, answereList!!,"description",false)
//        )
//        answereList=ArrayList<AnswerModel>()
//
//
//
//
//        answereList!!.add(AnswerModel("5","Optimistic","",R.drawable.optimistic,false))
//        answereList!!.add(AnswerModel("6","Pessimistic","",R.drawable.optimistic,false,))
//        productList!!.add(
//                QuestionAnswereModel(" Your Personality","3","How yours feeling ?",MyConstants.PERSONALITY,30, answereList!!,"description",false)
//        )
//        answereList=ArrayList<AnswerModel>()
//
//
//
//
//        answereList!!.add(AnswerModel("7","Extrovert","",R.drawable.extrovert,false))
//                answereList!!.add(AnswerModel("8","Introvert","",R.drawable.introvert,false,))
//        productList!!.add(
//        QuestionAnswereModel(" Your Personality","3","How yours feeling ?",MyConstants.PERSONALITY,30, answereList!!,"description",false)
//        )
//        answereList=ArrayList<AnswerModel>()
//
//
//
//        answereList!!.add(AnswerModel("9","Celebrities","",R.drawable.celebrities,false))
//        answereList!!.add(AnswerModel("10","Music","",R.drawable.music,false))
//        answereList!!.add(AnswerModel("11","Travel","",R.drawable.travel,false))
//        answereList!!.add(AnswerModel("12","Politics","",R.drawable.politics,false))
//        answereList!!.add(AnswerModel("13","Economy","",R.drawable.economy,false))
//        answereList!!.add(AnswerModel("14","Sports","",R.drawable.sports,false))
//
//        productList!!.add(
//                QuestionAnswereModel(" Your Preferences","2","",MyConstants.PREFERENCES,20, answereList!!,"description",false)
//        )
//        answereList=ArrayList<AnswerModel>()
//
//
//
//        answereList!!.add(AnswerModel("15","Go Gym","",R.drawable.gogym,false))
//        answereList!!.add(AnswerModel("16","Eat Healthy","",R.drawable.eathealthy,false))
//        answereList!!.add(AnswerModel("17","To Busy","",R.drawable.tobusy,false))
//        answereList!!.add(AnswerModel("18","Pizza","",R.drawable.pizza,false))
//
//        productList!!.add(
//                QuestionAnswereModel(" Your Preferences","2","",MyConstants.PREFERENCES,20, answereList!!,"description",false)
//        )
//        answereList=ArrayList<AnswerModel>()
//
//
//
//        answereList!!.add(AnswerModel("19","Sunrise","",R.drawable.sunrise,false))
//        answereList!!.add(AnswerModel("20","Sunset","",R.drawable.sunset,false))
//
//        productList!!.add(
//                QuestionAnswereModel(" Your Preferences","2","",MyConstants.PERSONALITY,20, answereList!!,"description",false)
//        )
//        answereList=ArrayList<AnswerModel>()
//
//
//
//        answereList!!.add(AnswerModel("21","Book","",R.drawable.book,false))
//        answereList!!.add(AnswerModel("22","Story","",R.drawable.story,false))
//        answereList!!.add(AnswerModel("23","Movies","",R.drawable.movies,false))
//        answereList!!.add(AnswerModel("24","Podcast","",R.drawable.podcast,false))
//
//        productList!!.add(
//                QuestionAnswereModel(" Your Preferences","2","",MyConstants.PREFERENCES,20, answereList!!,"description",false)
//        )





    }

    override fun onGetPersonalityDataInterfaceResponse(status: String?, commonStatusModel: GetPersonalityDataModel?, ) {
        when (status) {
            MyConstants.GO_TO_RESPONSE -> {
                CommonMethods.showLog(TAG,"Get Personality Data  status  " + commonStatusModel!!.message)
                if (commonStatusModel != null) {
                    if (commonStatusModel.status != null) {
                        when {
                            commonStatusModel.status.equals("1") -> {
                                val innerData: ArrayList<GetPersonalityDataInnerModel> = commonStatusModel.questionsList
                                productList = innerData
                                CustomPersonalityTestContainer(this, productList!!, mainContainerLayout!!,loadingDialog!!,commonMessageDialog!!,sharedPreferences!!)
                            }
                            commonStatusModel.status.equals("0") -> {
                                commonMessageDialog!!.showDialog(commonStatusModel.message)
                            }
                            else -> {
                                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                            }
                        }
                    } else {
                        commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                    }
                } else {
                    commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
                }
            }
            MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                commonMessageDialog!!.showDialog(getString(R.string.someErrorOccurredText))
            }
        }

    }
}