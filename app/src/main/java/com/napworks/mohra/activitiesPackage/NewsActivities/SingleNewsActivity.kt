package com.napworks.mohra.activitiesPackage.NewsActivities

import android.Manifest
import android.content.*
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.napworks.mohra.R
import com.napworks.mohra.adapterPackage.News.RelatedNewsRecyclerAdapter
import com.napworks.mohra.adapterPackage.News.TagsRecyclerAdapter
import com.napworks.mohra.designPackage.CustomTagContainer
import com.napworks.mohra.dialogPackage.CommonMessageDialog
import com.napworks.mohra.dialogPackage.LoadingDialog
import com.napworks.mohra.interfacePackage.CommonStatusInterface
import com.napworks.mohra.interfacePackage.News.GetNewsDetailsInterface
import com.napworks.mohra.interfacePackage.News.LikeAndUnlikeInterface
import com.napworks.mohra.modelPackage.CommonStatusModel
import com.napworks.mohra.modelPackage.News.*
import com.napworks.mohra.utilPackage.ApiCalls
import com.napworks.mohra.utilPackage.CommonMethods
import com.napworks.mohra.utilPackage.MyConstants
import kotlinx.android.synthetic.main.fragment_search_news.*
import java.util.*
import kotlin.collections.ArrayList
import android.graphics.Bitmap

import android.graphics.BitmapFactory
import android.net.Uri
import com.napworks.mohra.interfacePackage.DownloadCompleteInterface
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.lang.Exception


import androidx.core.content.FileProvider
import com.napworks.mohra.utilPackage.DownloadFiles
import java.io.IOException
import android.media.MediaScannerConnection


import android.content.pm.PackageManager
import android.icu.text.Collator.ReorderCodes.DEFAULT
import android.os.*
import androidx.core.app.ActivityCompat
import android.provider.MediaStore
import android.os.Environment
import android.os.Build
import android.provider.Settings
import com.napworks.mohra.fragmentPackage.NavigationFragment


class SingleNewsActivity : AppCompatActivity(), View.OnClickListener, GetNewsDetailsInterface,
    CommonStatusInterface, LikeAndUnlikeInterface,DownloadCompleteInterface {

    lateinit var recyclerViewNewsTag: RecyclerView
    lateinit var recyclerViewRelatedNews: RecyclerView
    lateinit var menuButton: ImageView
    lateinit var shareIcon: ImageView
    lateinit var newsDetailImage: ImageView
    lateinit var favButton: ImageView
    lateinit var likeForNews: ImageView
    lateinit var newsDetailsTitle: TextView
    lateinit var newsDetailTime: TextView
    lateinit var newsMain: TextView
    lateinit var channelName: TextView
    lateinit var likesCountInNewsDetails: TextView
    var commonMessageDialog: CommonMessageDialog? = null
    var loadingDialog: LoadingDialog? = null
    lateinit var commentsInNewsDetails: TextView
    lateinit var dataLayout: LinearLayout
    lateinit var CommentSection: LinearLayout
    lateinit var likeSection: LinearLayout
    lateinit var inflateLinearLayoutForTag: LinearLayout
    var Id = ""
    var TAG = javaClass.simpleName;

    private lateinit var tagsRecyclerAdapter: TagsRecyclerAdapter
    var list : ArrayList<TagListModel>? = null
    var listImages : ArrayList<Uri>? = null
    var imageUri: Uri? = null
    var realtedNewsList : ArrayList<NewsDataModel>? = null
    var newsID = 0
    var photo = ""
    var title = ""
    var isFav = false
    private var sharedPreferences: SharedPreferences? = null
    val mainHandler = Handler(Looper.getMainLooper())

    var mainBroadcastReceiver: MainBroadcastReceiver? = null
    var intentFilter: IntentFilter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_news)
        mainBroadcastReceiver = MainBroadcastReceiver()
        intentFilter = IntentFilter(MyConstants.SINGLE_NEWS_ACTIVITY)
        loadingDialog = LoadingDialog(this)
//      recyclerViewNewsTag = findViewById(R.id.recyclerViewNewsTag)
        recyclerViewRelatedNews = findViewById(R.id.recyclerViewRelatedNews)
        newsDetailImage = findViewById(R.id.newsDetailImage)
        newsDetailsTitle = findViewById(R.id.newsDetailsTitle)
        channelName = findViewById(R.id.channelName)
        newsDetailTime = findViewById(R.id.newsDetailTime)
        newsMain = findViewById(R.id.newsMain)
        inflateLinearLayoutForTag = findViewById(R.id.inflateLinearLayoutForTag)
        shareIcon = findViewById(R.id.shareIcon)
        likeForNews = findViewById(R.id.likeForNews)
        likeSection = findViewById(R.id.likeSection)
        CommentSection = findViewById(R.id.CommentSection)
        commonMessageDialog = CommonMessageDialog(this)

        likesCountInNewsDetails = findViewById(R.id.likesCountInNewsDetails)
        commentsInNewsDetails = findViewById(R.id.commentsInNewsDetails)
        favButton = findViewById(R.id.favButton)
        sharedPreferences = getSharedPreferences(MyConstants.SHARED_PREFERENCE, MODE_PRIVATE)
        menuButton = findViewById(R.id.menuButton)
        dataLayout = findViewById(R.id.dataLayout)
        list = ArrayList<TagListModel> ()
        listImages = ArrayList<Uri> ()
        realtedNewsList = ArrayList<NewsDataModel> ()
        list!!.add(TagListModel("Politics", false))
        list!!.add(TagListModel("Health", false))
        list!!.add(TagListModel("Journalism", false))
        newsID = intent.extras!!.getInt("newsID")
        CommonMethods.showLog(TAG,"newsID => $newsID")
        callNewsDetails(sharedPreferences!!,newsID)
//
//        recyclerViewNewsTag.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true)
//        tagsRecyclerAdapter = TagsRecyclerAdapter(this!!, list!!)
//        recyclerViewNewsTag.adapter = tagsRecyclerAdapter

        menuButton.setOnClickListener(this)
        favButton.setOnClickListener(this)
        likeForNews.setOnClickListener(this)
        likeSection.setOnClickListener(this)
        CommentSection.setOnClickListener(this)
        shareIcon.setOnClickListener(this)
    }

//    override fun onStart() {
//        run()
//        super.onStart()
//    }
//
//    override fun onStop() {
//        mainHandler.removeCallbacksAndMessages(null);
//        super.onStop()
//    }
//
//    override fun onPause() {
//        mainHandler.removeCallbacksAndMessages(null);
//        super.onPause()
//    }
//
//    override fun onResume() {
//        run()
//        super.onResume()
//    }

    fun run()
    {
        mainHandler.post(object : Runnable {
            override fun run() {
                callNews(sharedPreferences!!,newsID)
                mainHandler.postDelayed(this, 2000)
            }
        })
    }

    fun callNews(sharedPreferences: SharedPreferences, newsID: Int)
    {
        CommonMethods.showLog(TAG,"newsId ===> $newsID")
        ApiCalls.GetNewsDetails(
            sharedPreferences,
            newsID.toString(),
            this
        )
    }

    fun callNewsDetails(sharedPreferences: SharedPreferences, newsID: Int)
    {
        CommonMethods.showLog(TAG,"newsId ===> $newsID")
        loadingDialog!!.showDialog()
        ApiCalls.GetNewsDetails(
            sharedPreferences,
            newsID.toString(),
            this
        )
    }
    override fun onClick(v: View?) {
        if (v?.id == R.id.menuButton) {
            onBackPressed()
        }
        else if(v?.id == R.id.favButton)
        {
            CommonMethods.showLog(TAG," clicked fav")
            setFav(sharedPreferences!!,newsID)
        }
        else if(v?.id == R.id.likeSection)
        {
            likeUnlikeApi(sharedPreferences,Id)
        }
        else if(v?.id == R.id.CommentSection)
        {
            val intent = Intent(this,NewsCommentActivity::class.java)
            intent.putExtra("newsID",newsID)
            startActivity(intent)
//            likeUnlikeApi(sharedPreferences,Id)
        }
        else if(v?.id == R.id.shareIcon)
        {
            CommonMethods.showLog(TAG," clicked Share")
//            val message = "Text I want to share."
//            val i = Intent(Intent.ACTION_SEND)
//            i.type = "text/plain";
//            i.putExtra(Intent.EXTRA_SUBJECT, title);
//            i.putExtra(Intent.EXTRA_TEXT, title);
//            startActivity(Intent.createChooser(i, "Share URL"));
            isReadStoragePermissionGranted()
//            prepareList()

//            listImages!!.add(photo)
//            DownloadFiles(this, listImages!!, loadingDialog!!,this)
//            val text = "Look at my awesome picture"
//            val pictureUri = Uri.parse("file://my_picture")
//            val shareIntent = Intent()
//            shareIntent.action = Intent.ACTION_SEND
//            shareIntent.putExtra(Intent.EXTRA_TEXT, text)
//            shareIntent.putExtra(Intent.EXTRA_STREAM, pictureUri)
//            shareIntent.type = "image/*"
//            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
//            startActivity(Intent.createChooser(shareIntent, "Share images..."))

        }
    }
    fun isReadStoragePermissionGranted () : Boolean {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
                CommonMethods.showLog(TAG,"Permission is granted1");
                askForPermissions()
//                prepareList()
                return true;
            } else {

                CommonMethods.showLog(TAG,"Permission is revoked1");
                ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), 3);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            CommonMethods.showLog(TAG,"Permission is granted1");
            return true;
        }
        return true;
    }

    fun askForPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            if (!Environment.isExternalStorageManager()) {
                val intent = Intent(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION)
                startActivity(intent)
                return
            }
            else{
                prepareList()
            }
//            createDir()
        }
    }

//    fun createDir() {
//        if (!dir.exists()) {
//            dir.mkdirs()
//        }
//    }


    fun isWriteStoragePermissionGranted(): Boolean {
        return if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED
            ) {
                true
            } else {
                ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    2)
                false
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            true
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray,
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            2 -> {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //resume tasks needing this permission
                    CommonMethods.showLog(TAG,"granted 2")
                } else {
//                    progress.dismiss()
                    CommonMethods.showLog(TAG,"Not granted 2")
                }
            }
            3 -> {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    CommonMethods.showLog(TAG,"granted 3")
                    askForPermissions()
//                    prepareList()
                } else {
                    CommonMethods.showLog(TAG,"Not granted 3")
//                    progress.dismiss()
                }
            }
        }
    }



    private fun prepareList() {
        if (listImages!!.size > 0) {
            listImages!!.clear()
        }
//            listImages!!.add(photo)

        val direct = File(
            Environment.getExternalStorageDirectory().toString() + File.separator +
                    getString(R.string.app_name)
        )
        if (!direct.exists()) {
            val wallpaperDirectory = File(
                (Environment.getExternalStorageDirectory().toString() + File.separator +
                        getString(R.string.app_name) + File.separator)
            )
            wallpaperDirectory.mkdirs()
        }


        val urlList: ArrayList<String> = ArrayList()
        val filePath: String = photo
        val fileName = filePath.substring(filePath.lastIndexOf('/'), filePath.length)
        val file =
            File(Environment.getExternalStorageDirectory().toString() + File.separator +
                    getString(R.string.app_name) + File.separator + fileName)
        if (!file.exists()) {
            CommonMethods.showLog(TAG, "File Not Exists"+filePath)
            urlList.add(filePath)
        } else {
            CommonMethods.showLog(TAG, "File  Exists")
            val path = FileProvider.getUriForFile(this,
                getString(R.string.file_provider_authority),
                file)
            listImages!!.add(path)
        }

            if (urlList.size > 0) {
                DownloadFiles(this, urlList!!, loadingDialog!!,this).execute()
            } else {
                convertViewToJpg()
            }

//        if (memoryModel.getFilesData().size() > 0) {
//            listImages!!.add(photo)
//            DownloadFiles(this, listImages!!, loadingDialog!!,this)
//            val urlList: ArrayList<String> = ArrayList()
//            for (i in 0 until memoryModel.getFilesData().size()) {
//                val filePath: String = memoryModel.getFilesData().get(i).getUrl()
//                val fileName = filePath.substring(filePath.lastIndexOf('/'), filePath.length)
//                val file =
//                    File(Environment.getExternalStorageDirectory().toString() + File.separator +
//                            activity.getString(R.string.app_name) + File.separator + fileName)
//                if (!file.exists()) {
//                    CommonMethods.showLog(TAG, "File Not Exists")
//                    urlList.add(memoryModel.getFilesData().get(i).getUrl())
//                } else {
//                    CommonMethods.showLog(TAG, "File  Exists")
//                    val path = FileProvider.getUriForFile(activity,
//                        activity.getString(R.string.file_provider_authority),
//                        file)
//                    uriList.add(path)
//                }
//            }
//            if (urlList.size() > 0) {
//                DownloadFiles(activity,
//                    urlList,
//                    loadingDialog!!,
//                    downloadingLoadingDialog,
//                    this).execute()
//            } else {
//                convertViewToJpg()
//            }
//        } else {
//            convertViewToJpg()
//        }
    }

    private fun saveBitMap(context: Context): File? {
        val pictureFileDir =
            File(Environment.getExternalStorageDirectory().toString() + File.separator +
                    getString(R.string.app_name))
        if (!pictureFileDir.exists()) {
            val isDirectoryCreated = pictureFileDir.mkdirs()
            if (!isDirectoryCreated) CommonMethods.showLog(TAG,
                "Can't create directory to save the image")
            return null
        }
        val imagename = "sharedImage.png"
        val pictureFile =
            File(File(Environment.getExternalStorageDirectory().toString() + File.separator +
                    getString(R.string.app_name) + File.separator), imagename)
        if (pictureFile.exists()) {
            pictureFile.delete()
        }
       var currentImage = Environment.getExternalStorageDirectory().toString() + File.separator +
                getString(R.string.app_name) + File.separator + imagename
        imageUri = FileProvider.getUriForFile(this,
            getString(R.string.file_provider_authority),
            pictureFile)
        CommonMethods.showLog(TAG, "Uri $imageUri Path : $currentImage")
//        val bitmap: Bitmap = getBitmapFromView(drawView)
        try {
            CommonMethods.showLog(TAG, "Try Works")
            pictureFile.createNewFile()
            val oStream = FileOutputStream(pictureFile)
//            bitmap.compress(Bitmap.CompressFormat.PNG, 100, oStream)
            oStream.flush()
            oStream.close()
        } catch (e: IOException) {
            e.printStackTrace()
            CommonMethods.showLog(TAG, "T : "+e.message)
            CommonMethods.showLog(TAG, "There was an issue saving the image.")
        }
        scanGallery(context, pictureFile.absolutePath)
        return pictureFile
    }

//    private fun getBitmapFromView(view: View): Bitmap? {
//        val returnedBitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
//        val canvas = Canvas(returnedBitmap)
//        val bgDrawable = view.background
//        if (bgDrawable != null) {
//            bgDrawable.draw(canvas)
//        } else {
//            canvas.drawColor(ContextCompat.getColor(activity, R.color.textColor5))
//        }
//        view.draw(canvas)
//        return returnedBitmap
//    }

    private fun scanGallery(cntx: Context, path: String) {
        try {
            MediaScannerConnection.scanFile(cntx, arrayOf(path), null
            ) { path, uri -> }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
    fun convertViewToJpg() {
//        val file: File = saveBitMap(this)!!
//        CommonMethods.showLog(TAG, "Uri : $imageUri")
//        if (file != null) {
            CommonMethods.showLog(TAG, "Drawing saved to the gallery!")
//            listImages!!.add(0, imageUri!!)
            for (i in 0 until listImages!!.size) {
                CommonMethods.showLog(TAG, "URI : " + listImages!![i])
            }


            val shareIntent = Intent()
            shareIntent.setAction(Intent.ACTION_SEND)
            shareIntent.putExtra(Intent.EXTRA_TEXT, title)
            shareIntent.putExtra(Intent.EXTRA_STREAM, listImages!![0])
            shareIntent.setType("image/jpeg")
            shareIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            shareIntent.setFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
            shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(Intent.createChooser(shareIntent,title))
//        }
//    else {
//            CommonMethods.showLog(TAG, "Oops! Image could not be saved.")
//            commonMessageDialog!!.showDialog(this.getString(R.string.filesNotSavedText))
//        }
    }

    fun likeUnlikeApi(sharedPreferences: SharedPreferences?, Id: String)
    {
        ApiCalls.likeUnlikePost(
            sharedPreferences!!,
            Id,
            "news",
            this
        )
    }
    fun setFav(sharedPreferences: SharedPreferences, newsID: Int)
    {
        var value = 5
        if (isFav== false)
        {
            isFav = true
            value = 1
            CommonMethods.showLog(TAG," colorPrimary")
            favButton.setColorFilter(ContextCompat.getColor(this,
                R.color.colorPrimary));
        }
        else
        {
            isFav = false
            value = 0
            CommonMethods.showLog(TAG," white")
            favButton.setColorFilter(ContextCompat.getColor(this,
                R.color.hintColor));
        }
        ApiCalls.favouriteNews(
            sharedPreferences,
            value.toString(),
            newsID.toString(),
            this

        )
    }

    override fun onGetNewsDetailsInterfaceResponse(
        status: String?,
        getNewsDetailsModel: GetNewsDetailsModel?,
    ) {
        when (status) {
            MyConstants.GO_TO_RESPONSE -> {
                if (getNewsDetailsModel != null) {
                    if (getNewsDetailsModel.status != null) {
                        when {
                            getNewsDetailsModel.status.equals("1") -> {
                                var like = getNewsDetailsModel.data.isLiked
                                var Fav = getNewsDetailsModel.data.isFavourite
                                title = getNewsDetailsModel.data.title.toString()
                                channelName.text = getNewsDetailsModel.data.newsChannel.toString()
                                photo = getNewsDetailsModel.data.image!!

                                var likeCount = getNewsDetailsModel.data.likesCount
                                var commentCount = getNewsDetailsModel.data.commentCount
                                if(likeCount == 0)
                                {
                                    likesCountInNewsDetails.text = "Like"
                                }
                                else if (likeCount!! < 2)
                                {
                                    likesCountInNewsDetails.text = getNewsDetailsModel.data.likesCount.toString().plus(" Like")
                                }
                                else
                                {
                                    likesCountInNewsDetails.text = getNewsDetailsModel.data.likesCount.toString().plus(" Likes")
                                }
                                if(like == 0)
                                {
                                    likeForNews.setColorFilter(ContextCompat.getColor(this,
                                        R.color.black));
                                    likesCountInNewsDetails.setTextColor(ContextCompat.getColor(this,
                                        R.color.black));
                                }
                                else
                                {
                                    likeForNews.setColorFilter(ContextCompat.getColor(this,
                                        R.color.colorPrimary));
                                    likesCountInNewsDetails.setTextColor(ContextCompat.getColor(this,
                                        R.color.colorPrimary));
                                }

                                if(Fav == 0)
                                {
                                    isFav== false
                                    CommonMethods.showLog(TAG,"Fav == $isFav")
                                    favButton.setColorFilter(ContextCompat.getColor(this,
                                        R.color.hintColor));
                                }
                                else
                                {
                                    isFav== true
                                    CommonMethods.showLog(TAG,"Fav == $isFav")
                                    favButton.setColorFilter(ContextCompat.getColor(this,
                                        R.color.colorPrimary));
                                }
                                CommonMethods.showLog(TAG,"postId ====> "+getNewsDetailsModel.data.newsId )
                                loadingDialog!!.hideDialog()

                                val date = Date()
                                val timeMilli: Long = date.getTime()
                                var time = getNewsDetailsModel.data.time!!.toLong()
                                var show = (timeMilli / 1000) - time

                                newsDetailTime .text = CommonMethods.timeAgoDisplay(show)

                                Id = getNewsDetailsModel.data.newsId!!.toString()
                                dataLayout.visibility = View.VISIBLE
                                var data : NewsDetailsInnerDataModel = getNewsDetailsModel.data
                                var realtedNews : ArrayList<NewsDataModel> = getNewsDetailsModel.relatedNews
                                realtedNewsList!!.addAll(realtedNews)
                                newsDetailsTitle.text = data.title
                                newsMain.text = data.news
                                CommonMethods.showLog(TAG,"photo : "+data.image)
                                var list : List<String> = data.tags!!
                                var  width = inflateLinearLayoutForTag.getMeasuredWidth()
                                CustomTagContainer(
                                    this,
                                    list!!,
                                    inflateLinearLayoutForTag,
                                    false,
                                    width
                                )
                                Glide.with(this)
                                    .load(data.image)
                                    .into(newsDetailImage);
                                if(commentCount == 0)
                                {
                                    commentsInNewsDetails.text =  " Comment"
                                }
                                else if (commentCount == 1){
                                    commentsInNewsDetails.text = data.commentCount.toString() + " Comment"
                                }
                                else
                                {
                                    commentsInNewsDetails.text = data.commentCount.toString() + " Comments"
                                }


                                recyclerViewRelatedNews.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true)
                                val RelatedNewsRecyclerAdapter = RelatedNewsRecyclerAdapter(this!!, realtedNewsList!!)
                                recyclerViewRelatedNews.adapter = RelatedNewsRecyclerAdapter
                            }
                            getNewsDetailsModel.status.equals("0") -> {
                                loadingDialog!!.hideDialog()
                                CommonMethods.showLog(TAG, getNewsDetailsModel.message)
                            }
                            getNewsDetailsModel.status.equals("10") -> {
                                loadingDialog!!.hideDialog()
                                CommonMethods.showLog(TAG, getNewsDetailsModel.message)
                            }
                            else -> {
                                loadingDialog!!.hideDialog()
                                CommonMethods.showLog(TAG,
                                    R.string.someErrorOccurredText.toString())
                            }
                        }
                    } else {
                        loadingDialog!!.hideDialog()
                        CommonMethods.showLog(TAG,
                            R.string.someErrorOccurredText.toString())
                    }
                } else {
                    loadingDialog!!.hideDialog()
                    CommonMethods.showLog(TAG,
                        R.string.someErrorOccurredText.toString())
                }
            }
            MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                CommonMethods.showLog(TAG,
                    R.string.someErrorOccurredText.toString())
            }
        }
    }

    override fun onCommonStatusInterfaceResponse(
        status: String?,
        commonStatusModel: CommonStatusModel?,
    )
    {
        when (status) {
            MyConstants.GO_TO_RESPONSE -> {
                CommonMethods.showLog(TAG, "login  status  " + commonStatusModel!!.status)
                if (commonStatusModel != null) {
                    if (commonStatusModel.status != null) {
                        when {
                            commonStatusModel.status.equals("1") -> {
                                Toast.makeText(this, commonStatusModel.message, Toast.LENGTH_SHORT).show()
                            }
                            commonStatusModel.status.equals("0") -> {
                                loadingDialog!!.hideDialog()
                                CommonMethods.showLog(TAG, commonStatusModel.message)
                            }
                            commonStatusModel.status.equals("10") -> {
                                loadingDialog!!.hideDialog()
                                CommonMethods.showLog(TAG, commonStatusModel.message)
                            }
                            else -> {
                                loadingDialog!!.hideDialog()
                                CommonMethods.showLog(TAG,
                                    R.string.someErrorOccurredText.toString())
                            }
                        }
                    } else {
                        loadingDialog!!.hideDialog()
                        CommonMethods.showLog(TAG,
                            R.string.someErrorOccurredText.toString())
                    }
                } else {
                    loadingDialog!!.hideDialog()
                    CommonMethods.showLog(TAG,
                        R.string.someErrorOccurredText.toString())
                }
            }
            MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                CommonMethods.showLog(TAG,
                    R.string.someErrorOccurredText.toString())
            }
        }
    }

    override fun onLikeAndUnlikeResponse(status: String?, likeUnlikeModel: LikeUnlikeModel?) {
            when (status) {
                MyConstants.GO_TO_RESPONSE -> {
                    CommonMethods.showLog(TAG, "login  status  " + likeUnlikeModel!!.status)
                    if (likeUnlikeModel != null) {
                        if (likeUnlikeModel.status != null) {
                            when {
                                likeUnlikeModel.status.equals("1") -> {
                                    var like = likeUnlikeModel.isLiked
                                    var likeCount = likeUnlikeModel.count
//                                    likesCountInNewsDetails.text = likeUnlikeModel.count.toString()+" Likes"
                                    if(likeCount == 0)
                                {
                                    likesCountInNewsDetails.text = "Like"
                                }
                                else if (likeCount!! < 2)
                                {
                                    likesCountInNewsDetails.text = likeCount.toString().plus(" Like")
                                }
                                else
                                {
                                    likesCountInNewsDetails.text = likeCount.toString().plus(" Likes")
                                }


                                    if(like == 0)
                                    {
                                        likeForNews.setColorFilter(ContextCompat.getColor(this,
                                            R.color.black));
                                        likesCountInNewsDetails.setTextColor(ContextCompat.getColor(this,
                                            R.color.black));
                                    }
                                    else
                                    {
                                        likeForNews.setColorFilter(ContextCompat.getColor(this,
                                            R.color.colorPrimary));
                                        likesCountInNewsDetails.setTextColor(ContextCompat.getColor(this,
                                            R.color.colorPrimary));
                                    }
//                                    Toast.makeText(this, likeUnlikeModel.message, Toast.LENGTH_SHORT).show()
                                }
                                likeUnlikeModel.status.equals("0") -> {
                                    loadingDialog!!.hideDialog()
                                    CommonMethods.showLog(TAG, likeUnlikeModel.message)
                                }
                                likeUnlikeModel.status.equals("10") -> {
                                    loadingDialog!!.hideDialog()
                                    CommonMethods.showLog(TAG, likeUnlikeModel.message)
                                }
                                else -> {
                                    loadingDialog!!.hideDialog()
                                    CommonMethods.showLog(TAG,
                                        R.string.someErrorOccurredText.toString())
                                }
                            }
                        } else {
                            loadingDialog!!.hideDialog()
                            CommonMethods.showLog(TAG,
                                R.string.someErrorOccurredText.toString())
                        }
                    } else {
                        loadingDialog!!.hideDialog()
                        CommonMethods.showLog(TAG,
                            R.string.someErrorOccurredText.toString())
                    }
                }
                MyConstants.FAILURE_RESPONSE, MyConstants.NULL_RESPONSE -> {
                    CommonMethods.showLog(TAG,
                        R.string.someErrorOccurredText.toString())
                }
            }
        }

    override fun onDownlaodComplete(downloadedUriList: java.util.ArrayList<Uri>) {
        CommonMethods.showLog(TAG,"uri after $downloadedUriList")
        listImages!!.addAll(downloadedUriList);
        convertViewToJpg();
    }

    inner class MainBroadcastReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val type = intent.getStringExtra(MyConstants.BROADCAST_TYPE)
            CommonMethods.showLog(TAG, " type : $type")
            if (type != null && type.equals(MyConstants.UPDATE_COMMENTS)) {
                callNewsDetails(sharedPreferences!!,newsID)
            }
        }
    }

    override fun onDestroy() {
            super.onDestroy()
            unregisterReceiver(mainBroadcastReceiver)
    }

    override fun onResume() {
            super.onResume()
            registerReceiver(mainBroadcastReceiver, intentFilter)
    }
}