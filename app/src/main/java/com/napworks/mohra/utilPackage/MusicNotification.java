package com.napworks.mohra.utilPackage;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.v4.media.session.MediaSessionCompat;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.napworks.mohra.R;
import com.napworks.mohra.ServicesPackage.NotificationActionService;
import com.napworks.mohra.modelPackage.MusicModel.MusicFilesModel;

import java.util.ArrayList;

public class MusicNotification {

     public static final  String CHANNEL_ID = "channel1";
     public static final  String ACTION_PREVIOUS = "actionprevious";
     public static final  String ACTION_PLAY = "actionplay";
     public static final  String ACTION_NEXT = "actionnext";

     public static Notification notification;

     public static void createNotification(Context context, int pos, int size , ArrayList<MusicFilesModel> musicList , int isplaying)
     {
         if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
         {
              NotificationManagerCompat notificationManagerCompat =NotificationManagerCompat.from(context);
              MediaSessionCompat  mediaSessionCompat =  new MediaSessionCompat(context ,"tag");

             Bitmap icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ed);
             PendingIntent pendingIntentPrevious;
             String title = musicList.get(pos).getName();
             int drw_previous;
             if(pos == 0)
             {
                 pendingIntentPrevious = null;
             drw_previous =R.drawable.backwardplayer;
             }
             else
             {
                 Intent intentPrevious = new Intent(context, NotificationActionService.class).setAction(ACTION_PREVIOUS);
                 pendingIntentPrevious = PendingIntent.getBroadcast(context,0,intentPrevious,PendingIntent.FLAG_UPDATE_CURRENT);
                 drw_previous = R.drawable.backwardplayer;

             }
             int playbutton;
             Intent intentPlay = new Intent(context, NotificationActionService.class).setAction(ACTION_PLAY);
             PendingIntent pendingIntentPlay = PendingIntent.getBroadcast(context,0,intentPlay,PendingIntent.FLAG_UPDATE_CURRENT);
              if(isplaying == 0)
              {
                  playbutton = R.drawable.k;
              }
              else
              {
                  playbutton = R.drawable.k;
              }



             PendingIntent pendingIntentNext;
             int drw_next;
             if(pos == size)
             {
                 pendingIntentNext = null;
                 drw_next =0;
             }
             else
             {
                 Intent intentNext = new Intent(context, NotificationActionService.class).setAction(ACTION_NEXT);
                 pendingIntentNext = PendingIntent.getBroadcast(context,0,intentNext,PendingIntent.FLAG_UPDATE_CURRENT);
                 drw_next = R.drawable.forwardplayer;

             }


             notification = new NotificationCompat.Builder(context,CHANNEL_ID)
                     .setSmallIcon(R.drawable.logosplashscreen)
                     .setContentTitle(title)
                     .setContentText("kkkkkkk")
                     .setLargeIcon(icon)
                     .setOnlyAlertOnce(true)
                     .setShowWhen(false)
                     .addAction(drw_previous,"Previous",pendingIntentPrevious)
                     .addAction(playbutton,"play",pendingIntentPlay)
                     .addAction(drw_next,"Next",pendingIntentNext)
                     .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                     .setShowActionsInCompactView(0,1,2)
                     .setMediaSession(mediaSessionCompat.getSessionToken()))
                     .setPriority(NotificationCompat.PRIORITY_LOW)
                     .build();
             notificationManagerCompat.notify(1,notification);
         }
     }
}
