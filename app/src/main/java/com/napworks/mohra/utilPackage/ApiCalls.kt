package com.napworks.mohra.utilPackage

import android.content.SharedPreferences
import com.napworks.mohra.interfacePackage.*
import com.napworks.mohra.interfacePackage.News.*
import com.napworks.mohra.interfacePackage.music.CreatePlaylistInterface
import com.napworks.mohra.interfacePackage.music.GetMusicHomeDataInterface
import com.napworks.mohra.interfacePackage.music.GetMusicInterface
import com.napworks.mohra.modelPackage.*
import com.napworks.mohra.modelPackage.MusicModel.CreatePlaylistResponse
import com.napworks.mohra.modelPackage.MusicModel.GetMusicHomeDataModel
import com.napworks.mohra.modelPackage.MusicModel.GetMusicModel
import com.napworks.mohra.modelPackage.News.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.io.File
import java.io.IOException

public class ApiCalls {

    companion object {
        private val TAG = "CommonWebServices"

        private fun getRetrofitObject(): com.napworks.mohra.interfacePackage.WebApis? {
            val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(MyConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            return retrofit.create(com.napworks.mohra.interfacePackage.WebApis::class.java)
        }

        fun getScalarRetrofit(): WebApis? {
            val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(MyConstants.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build()
            return retrofit.create(com.napworks.mohra.interfacePackage.WebApis::class.java)
        }


        public fun sendOtp(
            mobileNumber: String?,
            sendOtpInterface: com.napworks.mohra.interfacePackage.SendOtpInterface
        ) {
            val call: Call<SendOtpModel>? = getRetrofitObject()?.sendOtp(
                mobileNumber,
                if (MyConstants.IS_LIVE) "1" else "0"
            )
            call?.enqueue(object : Callback<SendOtpModel?> {
                override fun onResponse(
                    call: Call<SendOtpModel?>,
                    response: Response<SendOtpModel?>
                ) {
                    val commonStringResponseModel: SendOtpModel? = response.body()
                    if (commonStringResponseModel != null) {
                        sendOtpInterface.onSendOtpInterfaceResponse(
                            MyConstants.GO_TO_RESPONSE,
                            commonStringResponseModel
                        )
                    } else {
                        CommonMethods.showLog(TAG, "sendOtp Response null")
                        sendOtpInterface.onSendOtpInterfaceResponse(MyConstants.NULL_RESPONSE, null)
                    }
                }

                override fun onFailure(call: Call<SendOtpModel?>, t: Throwable) {
                    CommonMethods.showLog(TAG, "sendOtp Exception " + t.message)
                    sendOtpInterface.onSendOtpInterfaceResponse(MyConstants.FAILURE_RESPONSE, null)
                }
            })
        }
        public fun verifyOtp(
            otpId: String?,
            otp: String?,
            verifyOtpInterface: com.napworks.mohra.interfacePackage.VerifyOtpInterface
        ) {
            val call: Call<CommonStatusModel>? = getRetrofitObject()?.verifyOtp(
                otpId,
                otp
            )
            call?.enqueue(object : Callback<CommonStatusModel?> {
                override fun onResponse(
                    call: Call<CommonStatusModel?>,
                    response: Response<CommonStatusModel?>
                ) {
                    val commonStringResponseModel: CommonStatusModel? = response.body()
                    if (commonStringResponseModel != null) {
                        verifyOtpInterface.onVerifyOtpInterfaceResponse(
                            MyConstants.GO_TO_RESPONSE,
                            commonStringResponseModel
                        )
                    } else {
                        CommonMethods.showLog(TAG, "verifyOtp Response null")
                        verifyOtpInterface.onVerifyOtpInterfaceResponse(MyConstants.NULL_RESPONSE, null)
                    }
                }

                override fun onFailure(call: Call<CommonStatusModel?>, t: Throwable) {
                    CommonMethods.showLog(TAG, "verifyOtp Exception " + t.message)
                    verifyOtpInterface.onVerifyOtpInterfaceResponse(MyConstants.FAILURE_RESPONSE, null)
                }
            })
        }

        public fun signUp(
            email: String?,
            password: String?,
            first_name: String?,
            family_name: String?,
            mobile: String?,
            birthDate: Long?,
            userType: String?,
            userName: String?,
            countryId: String?,
            signUpInterface: com.napworks.mohra.interfacePackage.SignUpInterface
        ) {
            val call: Call<SignUpModel>? = getRetrofitObject()?.signUp(
                email,
                password,
                MyConstants.ANDROID,
                first_name,
                family_name,
                mobile,
                birthDate,
                userType,
                userName,
                countryId
            )
            call?.enqueue(object : Callback<SignUpModel?> {
                override fun onResponse(
                    call: Call<SignUpModel?>,
                    response: Response<SignUpModel?>
                ) {
                    val commonStringResponseModel: SignUpModel? = response.body()
                    if (commonStringResponseModel != null) {
                        signUpInterface.onSignUpInterfaceResponse(
                            MyConstants.GO_TO_RESPONSE,
                            commonStringResponseModel
                        )
                    } else {
                        CommonMethods.showLog(TAG, "signUp Response null")
                        signUpInterface.onSignUpInterfaceResponse(MyConstants.NULL_RESPONSE, null)
                    }
                }

                override fun onFailure(call: Call<SignUpModel?>, t: Throwable) {
                    CommonMethods.showLog(TAG, "signUp Exception " + t.message)
                    signUpInterface.onSignUpInterfaceResponse(MyConstants.FAILURE_RESPONSE, null)
                }
            })
        }


        public fun login(
            email: String?,
            password: String?,
            type: String?,
            google_id: String?,
            facebook_id: String?,
            first_name: String?,
            family_name: String?,
            mobile: String?,
            birtDate: Long?,
            userType: String?,
            userName: String?,
            countryId: String?,
            loginInterface: com.napworks.mohra.interfacePackage.LoginInterface
        ) {
            val call: Call<LoginModel>? = getRetrofitObject()?.login(
                email,
                password,
                MyConstants.ANDROID,
                google_id,
                facebook_id,
                type,
                first_name,
                family_name,
                mobile,
                birtDate,
                userType,
                userName,
                countryId
            )
            call?.enqueue(object : Callback<LoginModel?> {
                override fun onResponse(
                    call: Call<LoginModel?>,
                    response: Response<LoginModel?>
                ) {
                    val commonStringResponseModel: LoginModel? = response.body()
                    if (commonStringResponseModel != null) {
                        loginInterface.onLoginInterfaceResponse(
                            MyConstants.GO_TO_RESPONSE,
                            commonStringResponseModel
                        )
                    } else {
                        CommonMethods.showLog(TAG, "login Response null")
                        loginInterface.onLoginInterfaceResponse(MyConstants.NULL_RESPONSE, null)
                    }
                }

                override fun onFailure(call: Call<LoginModel?>, t: Throwable) {
                    CommonMethods.showLog(TAG, "login Exception " + t.message)
                    loginInterface.onLoginInterfaceResponse(MyConstants.FAILURE_RESPONSE, null)
                }
            })
        }

        public fun getIntroData(
            getIntroDataInterface: com.napworks.mohra.interfacePackage.GetIntroDataInterface
        ) {
            val call: Call<GetIntroDataModel>? = getRetrofitObject()?.introData
            call?.enqueue(object : Callback<GetIntroDataModel?> {
                override fun onResponse(
                    call: Call<GetIntroDataModel?>,
                    response: Response<GetIntroDataModel?>
                ) {
                    val commonStringResponseModel: GetIntroDataModel? = response.body()
                    if (commonStringResponseModel != null) {
                        getIntroDataInterface.onGetIntroDataInterfaceResponse(
                            MyConstants.GO_TO_RESPONSE,
                            commonStringResponseModel
                        )
                    } else {
                        CommonMethods.showLog(TAG, "getIntroData Response null")
                        getIntroDataInterface.onGetIntroDataInterfaceResponse(MyConstants.NULL_RESPONSE, null)
                    }
                }

                override fun onFailure(call: Call<GetIntroDataModel?>, t: Throwable) {
                    CommonMethods.showLog(TAG, "getIntroData Exception " + t.message)
                    getIntroDataInterface.onGetIntroDataInterfaceResponse(MyConstants.FAILURE_RESPONSE, null)
                }
            })
        }

        public fun getPersonalityData(
            getPersonalityDataInterface: com.napworks.mohra.interfacePackage.GetPersonalityDataInterface
        ) {
            val call: Call<GetPersonalityDataModel>? = getRetrofitObject()?.getPersonalityData(

            )
            call?.enqueue(object : Callback<GetPersonalityDataModel?> {
                override fun onResponse(
                    call: Call<GetPersonalityDataModel?>,
                    response: Response<GetPersonalityDataModel?>
                ) {
                    val commonStringResponseModel: GetPersonalityDataModel? = response.body()
                    if (commonStringResponseModel != null) {
                        getPersonalityDataInterface.onGetPersonalityDataInterfaceResponse(
                            MyConstants.GO_TO_RESPONSE,
                            commonStringResponseModel
                        )
                    } else {
                        CommonMethods.showLog(TAG, "getPersonalityData Response null")
                        getPersonalityDataInterface.onGetPersonalityDataInterfaceResponse(
                            MyConstants.NULL_RESPONSE, null)
                    }
                }

                override fun onFailure(call: Call<GetPersonalityDataModel?>, t: Throwable) {
                    CommonMethods.showLog(TAG, "getPersonalityData Exception " + t.message)
                    getPersonalityDataInterface.onGetPersonalityDataInterfaceResponse(MyConstants.FAILURE_RESPONSE, null)
                }
            })
        }


        public fun uploadPersonalityData(
            sharedPreferences: SharedPreferences,
            personalityData: String?,
            uploadPersonalityInterface: com.napworks.mohra.interfacePackage.UploadPersonalityInterface
        ) {
            val call: Call<CommonStatusModel>? = getRetrofitObject()?.uploadPersonalityData(

                sharedPreferences.getString(MyConstants.AUTH, ""),
                sharedPreferences.getString(MyConstants.SESSION_TOKEN, ""),
                sharedPreferences.getString(MyConstants.USER_ID, ""),
                personalityData
            )
            call?.enqueue(object : Callback<CommonStatusModel?> {
                override fun onResponse(
                    call: Call<CommonStatusModel?>,
                    response: Response<CommonStatusModel?>
                ) {
                    val commonStringResponseModel: CommonStatusModel? = response.body()
                    if (commonStringResponseModel != null) {
                        uploadPersonalityInterface.onUploadPersonalityInterfaceResponse(
                            MyConstants.GO_TO_RESPONSE,
                            commonStringResponseModel
                        )
                    } else {
                        CommonMethods.showLog(TAG, "uploadPersonalityData Response null")
                        uploadPersonalityInterface.onUploadPersonalityInterfaceResponse(MyConstants.NULL_RESPONSE, null)
                    }
                }

                override fun onFailure(call: Call<CommonStatusModel?>, t: Throwable) {
                    CommonMethods.showLog(TAG, "uploadPersonalityData Exception " + t.message)
                    uploadPersonalityInterface.onUploadPersonalityInterfaceResponse(MyConstants.FAILURE_RESPONSE, null)
                }
            })
        }

        public fun updateStatusPersonality(
            sharedPreferences: SharedPreferences,
            personalityStatus: String?,
            updateStatusPersonalityInterface: com.napworks.mohra.interfacePackage.UpdateStatusPersonalityInterface
        ) {
            val call: Call<CommonStatusModel>? = getRetrofitObject()?.updateStatusPersonality(
                sharedPreferences.getString(MyConstants.AUTH, ""),
                sharedPreferences.getString(MyConstants.SESSION_TOKEN, ""),
                sharedPreferences.getString(MyConstants.USER_ID, ""),
                personalityStatus
            )
            call?.enqueue(object : Callback<CommonStatusModel?> {
                override fun onResponse(
                    call: Call<CommonStatusModel?>,
                    response: Response<CommonStatusModel?>
                ) {
                    val commonStringResponseModel: CommonStatusModel? = response.body()
                    if (commonStringResponseModel != null) {
                        updateStatusPersonalityInterface.onUpdateStatusPersonalityInterfaceResponse(
                            MyConstants.GO_TO_RESPONSE,
                            commonStringResponseModel
                        )
                    } else {
                        CommonMethods.showLog(TAG, "updateStatusPersonality Response null")
                        updateStatusPersonalityInterface.onUpdateStatusPersonalityInterfaceResponse(
                            MyConstants.NULL_RESPONSE, null)
                    }
                }

                override fun onFailure(call: Call<CommonStatusModel?>, t: Throwable) {
                    CommonMethods.showLog(TAG, "updateStatusPersonality Exception " + t.message)
                    updateStatusPersonalityInterface.onUpdateStatusPersonalityInterfaceResponse(
                        MyConstants.FAILURE_RESPONSE, null)
                }
            })
        }


        public fun getUserPersonality(
            sharedPreferences: SharedPreferences,
            getUserPersonalityInterface: GetUserPersonalityInterface
        ) {
            val call: Call<PersonalityFinalScreenDataModel>? = getRetrofitObject()?.getUserPersonality(
                sharedPreferences.getString(MyConstants.AUTH, "as"),
                sharedPreferences.getString(MyConstants.SESSION_TOKEN, "as"),
                sharedPreferences.getString(MyConstants.USER_ID, "25"),
            )
            call?.enqueue(object : Callback<PersonalityFinalScreenDataModel?> {
                override fun onResponse(
                    call: Call<PersonalityFinalScreenDataModel?>,
                    response: Response<PersonalityFinalScreenDataModel?>
                ) {
                    val commonStringResponseModel: PersonalityFinalScreenDataModel? = response.body()
                    if (commonStringResponseModel != null) {
                        getUserPersonalityInterface.onGetUserPersonalityInterfaceResponse(
                            MyConstants.GO_TO_RESPONSE,
                            commonStringResponseModel
                        )
                    } else {
                        CommonMethods.showLog(TAG, "getUserPersonality Response null")
                        getUserPersonalityInterface.onGetUserPersonalityInterfaceResponse(
                            MyConstants.NULL_RESPONSE, null)
                    }
                }

                override fun onFailure(call: Call<PersonalityFinalScreenDataModel?>, t: Throwable) {
                    CommonMethods.showLog(TAG, "getUserPersonality Exception " + t.message)
                    getUserPersonalityInterface.onGetUserPersonalityInterfaceResponse(MyConstants.FAILURE_RESPONSE, null)
                }
            })
        }

        public fun getCountries(
            countryCodeDataInterface: CountryCodeDataInterface
        ) {
            val call: Call<CountryCodeModel>? = getRetrofitObject()?.getCountries(
            )
            call?.enqueue(object : Callback<CountryCodeModel?> {
                override fun onResponse(
                    call: Call<CountryCodeModel?>,
                    response: Response<CountryCodeModel?>
                ) {
                    val commonStringResponseModel: CountryCodeModel? = response.body()
                    if (commonStringResponseModel != null) {
                        countryCodeDataInterface.onCountryCodeDataInterfaceResponse(
                            MyConstants.GO_TO_RESPONSE,
                            commonStringResponseModel
                        )
                    } else {
                        CommonMethods.showLog(TAG, "getCountries Response null")
                        countryCodeDataInterface.onCountryCodeDataInterfaceResponse(MyConstants.NULL_RESPONSE, null)
                    }
                }

                override fun onFailure(call: Call<CountryCodeModel?>, t: Throwable) {
                    CommonMethods.showLog(TAG, "getCountries Exception " + t.message)
                    countryCodeDataInterface.onCountryCodeDataInterfaceResponse(MyConstants.FAILURE_RESPONSE, null)
                }
            })
        }


        public fun checkMobileExist(
            mobile: String,
            countryId: String?,
            uploadPersonalityInterface: com.napworks.mohra.interfacePackage.UploadPersonalityInterface
        ) {
            val call: Call<CommonStatusModel>? = getRetrofitObject()?.checkMobileExist(
                mobile,
                countryId
            )
            call?.enqueue(object : Callback<CommonStatusModel?> {
                override fun onResponse(
                    call: Call<CommonStatusModel?>,
                    response: Response<CommonStatusModel?>
                ) {
                    val commonStringResponseModel: CommonStatusModel? = response.body()
                    if (commonStringResponseModel != null) {
                        uploadPersonalityInterface.onUploadPersonalityInterfaceResponse(
                            MyConstants.GO_TO_RESPONSE,
                            commonStringResponseModel
                        )
                    } else {
                        CommonMethods.showLog(TAG, "checkMobileExist Response null")
                        uploadPersonalityInterface.onUploadPersonalityInterfaceResponse(MyConstants.NULL_RESPONSE, null)
                    }
                }

                override fun onFailure(call: Call<CommonStatusModel?>, t: Throwable) {
                    CommonMethods.showLog(TAG, "checkMobileExist Exception " + t.message)
                    uploadPersonalityInterface.onUploadPersonalityInterfaceResponse(MyConstants.FAILURE_RESPONSE, null)
                }
            })
        }

        public fun forgetUpdatePassword(
            user_id: String,
            password: String?,
            CommonStatusInterface: CommonStatusInterface
        ) {
            val call: Call<CommonStatusModel>? = getRetrofitObject()?.forgetUpdatePassword(
                user_id,
                password
            )
            call?.enqueue(object : Callback<CommonStatusModel?> {
                override fun onResponse(
                    call: Call<CommonStatusModel?>,
                    response: Response<CommonStatusModel?>
                ) {
                    val commonStringResponseModel: CommonStatusModel? = response.body()
                    if (commonStringResponseModel != null) {
                        CommonStatusInterface.onCommonStatusInterfaceResponse(
                            MyConstants.GO_TO_RESPONSE,
                            commonStringResponseModel
                        )
                    } else {
                        CommonMethods.showLog(TAG, "uploadPersonalityData Response null")
                        CommonStatusInterface.onCommonStatusInterfaceResponse(MyConstants.NULL_RESPONSE, null)
                    }
                }

                override fun onFailure(call: Call<CommonStatusModel?>, t: Throwable) {
                    CommonMethods.showLog(TAG, "uploadPersonalityData Exception " + t.message)
                    CommonStatusInterface.onCommonStatusInterfaceResponse(MyConstants.FAILURE_RESPONSE, null)
                }
            })
        }

        public fun checkEmailMobileExists(
            mobile: String,
            countryId: String?,
            email: String?,
            CommonStatusInterface: CommonStatusInterface
        ) {
            val call: Call<CommonStatusModel>? = getRetrofitObject()?.checkEmailMobileExists(
                mobile,
                countryId,
                email
            )
            call?.enqueue(object : Callback<CommonStatusModel?> {
                override fun onResponse(
                    call: Call<CommonStatusModel?>,
                    response: Response<CommonStatusModel?>
                ) {
                    val commonStringResponseModel: CommonStatusModel? = response.body()
                    if (commonStringResponseModel != null) {
                        CommonStatusInterface.onCommonStatusInterfaceResponse(
                            MyConstants.GO_TO_RESPONSE,
                            commonStringResponseModel
                        )
                    } else {
                        CommonMethods.showLog(TAG, "checkEmailMobileExists Response null")
                        CommonStatusInterface.onCommonStatusInterfaceResponse(MyConstants.NULL_RESPONSE, null)
                    }
                }

                override fun onFailure(call: Call<CommonStatusModel?>, t: Throwable) {
                    CommonMethods.showLog(TAG, "checkEmailMobileExists Exception " + t.message)
                    CommonStatusInterface.onCommonStatusInterfaceResponse(MyConstants.FAILURE_RESPONSE, null)
                }
            })
        }



        public fun getNewsCategories(
            sharedPreferences: SharedPreferences,
            getNewsCategoriesInterface: GetNewsCategoriesInterface
        ) {
            val call: Call<GetNewsCategoriesModel>? = getRetrofitObject()?.getNewsCategories(
                sharedPreferences.getString(MyConstants.AUTH, ""),
                sharedPreferences.getString(MyConstants.SESSION_TOKEN, ""),
                sharedPreferences.getString(MyConstants.USER_ID, ""),
            )
            call?.enqueue(object : Callback<GetNewsCategoriesModel?> {
                override fun onResponse(
                    call: Call<GetNewsCategoriesModel?>,
                    response: Response<GetNewsCategoriesModel?>
                ) {
                    val commonStringResponseModel: GetNewsCategoriesModel? = response.body()
                    if (commonStringResponseModel != null) {
                        getNewsCategoriesInterface.onGetNewsCategoriesInterfaceResponse(
                            MyConstants.GO_TO_RESPONSE,
                            commonStringResponseModel
                        )
                    } else {
                        CommonMethods.showLog(TAG, "getNewsCategories Response null")
                        getNewsCategoriesInterface.onGetNewsCategoriesInterfaceResponse(MyConstants.NULL_RESPONSE, null)
                    }
                }

                override fun onFailure(call: Call<GetNewsCategoriesModel?>, t: Throwable) {
                    CommonMethods.showLog(TAG, "getNewsCategories Exception " + t.message)
                    getNewsCategoriesInterface.onGetNewsCategoriesInterfaceResponse(MyConstants.FAILURE_RESPONSE, null)
                }
            })
        }

        public fun getNews(
            sharedPreferences: SharedPreferences,
            countryId: String?,
            getNewsInterface: GetNewsInterface
        ) {
            val call: Call<GetNewsModel>? = getRetrofitObject()?.getNews(
                sharedPreferences.getString(MyConstants.AUTH, ""),
                sharedPreferences.getString(MyConstants.SESSION_TOKEN, ""),
                sharedPreferences.getString(MyConstants.USER_ID, ""),
                countryId
            )
            call?.enqueue(object : Callback<GetNewsModel?> {
                override fun onResponse(
                    call: Call<GetNewsModel?>,
                    response: Response<GetNewsModel?>
                ) {
                    val commonStringResponseModel: GetNewsModel? = response.body()
                    if (commonStringResponseModel != null) {
                        getNewsInterface.onGetNewsInterfaceResponse(
                            MyConstants.GO_TO_RESPONSE,
                            commonStringResponseModel
                        )
                    } else {
                        CommonMethods.showLog(TAG, "getNews Response null")
                        getNewsInterface.onGetNewsInterfaceResponse(MyConstants.NULL_RESPONSE, null)
                    }
                }

                override fun onFailure(call: Call<GetNewsModel?>, t: Throwable) {
                    CommonMethods.showLog(TAG, "getNews Exception " + t.message)
                    getNewsInterface.onGetNewsInterfaceResponse(MyConstants.FAILURE_RESPONSE, null)
                }
            })
        }

        public fun GetNewsDetails(
            sharedPreferences: SharedPreferences,
            newsId: String?,
            getNewsDetailsInterface: GetNewsDetailsInterface
        ) {
            val call: Call<GetNewsDetailsModel>? = getRetrofitObject()?.getNewsDetail(
                sharedPreferences.getString(MyConstants.AUTH, ""),
                sharedPreferences.getString(MyConstants.SESSION_TOKEN, ""),
                sharedPreferences.getString(MyConstants.USER_ID, ""),
                newsId
            )
            call?.enqueue(object : Callback<GetNewsDetailsModel?> {
                override fun onResponse(
                    call: Call<GetNewsDetailsModel?>,
                    response: Response<GetNewsDetailsModel?>
                ) {
                    val commonStringResponseModel: GetNewsDetailsModel? = response.body()
                    if (commonStringResponseModel != null) {
                        getNewsDetailsInterface.onGetNewsDetailsInterfaceResponse(
                            MyConstants.GO_TO_RESPONSE,
                            commonStringResponseModel
                        )
                    } else {
                        CommonMethods.showLog(TAG, "GetNewsDetails Response null")
                        getNewsDetailsInterface.onGetNewsDetailsInterfaceResponse(MyConstants.NULL_RESPONSE, null)
                    }
                }

                override fun onFailure(call: Call<GetNewsDetailsModel?>, t: Throwable) {
                    CommonMethods.showLog(TAG, "GetNewsDetails Exception " + t.message)
                    getNewsDetailsInterface.onGetNewsDetailsInterfaceResponse(MyConstants.FAILURE_RESPONSE, null)
                }
            })
        }


        public fun favouriteNews(
            sharedPreferences: SharedPreferences,
            value: String?,
            newsId: String?,
            commonStatusInterface: CommonStatusInterface
        ) {
            val call: Call<CommonStatusModel>? = getRetrofitObject()?.favouriteNews(
                sharedPreferences.getString(MyConstants.AUTH, ""),
                sharedPreferences.getString(MyConstants.SESSION_TOKEN, ""),
                sharedPreferences.getString(MyConstants.USER_ID, ""),
                value,
                newsId
            )
            call?.enqueue(object : Callback<CommonStatusModel?> {
                override fun onResponse(
                    call: Call<CommonStatusModel?>,
                    response: Response<CommonStatusModel?>
                ) {
                    val commonStringResponseModel: CommonStatusModel? = response.body()
                    if (commonStringResponseModel != null) {
                        commonStatusInterface.onCommonStatusInterfaceResponse(
                            MyConstants.GO_TO_RESPONSE,
                            commonStringResponseModel
                        )
                    } else {
                        CommonMethods.showLog(TAG, "favouriteNews Response null")
                        commonStatusInterface.onCommonStatusInterfaceResponse(MyConstants.NULL_RESPONSE, null)
                    }
                }

                override fun onFailure(call: Call<CommonStatusModel?>, t: Throwable) {
                    CommonMethods.showLog(TAG, "favouriteNews Exception " + t.message)
                    commonStatusInterface.onCommonStatusInterfaceResponse(MyConstants.FAILURE_RESPONSE, null)
                }
            })
        }


        public fun getFavouriteNews(
            sharedPreferences: SharedPreferences,
            favNewsInterface: FavNewsInterface
        ) {
            val call: Call<FavNewsModel>? = getRetrofitObject()?.getFavouriteNews(
                sharedPreferences.getString(MyConstants.AUTH, ""),
                sharedPreferences.getString(MyConstants.SESSION_TOKEN, ""),
                sharedPreferences.getString(MyConstants.USER_ID, ""),
            )
            call?.enqueue(object : Callback<FavNewsModel?> {
                override fun onResponse(
                    call: Call<FavNewsModel?>,
                    response: Response<FavNewsModel?>
                ) {
                    val commonStringResponseModel: FavNewsModel? = response.body()
                    if (commonStringResponseModel != null) {
                        favNewsInterface.onFavNewsInterfaceResponse(
                            MyConstants.GO_TO_RESPONSE,
                            commonStringResponseModel
                        )
                    } else {
                        CommonMethods.showLog(TAG, "getFavouriteNews Response null")
                        favNewsInterface.onFavNewsInterfaceResponse(MyConstants.NULL_RESPONSE, null)
                    }
                }

                override fun onFailure(call: Call<FavNewsModel?>, t: Throwable) {
                    CommonMethods.showLog(TAG, "getFavouriteNews Exception " + t.message)
                    favNewsInterface.onFavNewsInterfaceResponse(MyConstants.FAILURE_RESPONSE, null)
                }
            })
        }

        public fun likeUnlikePost(
            sharedPreferences: SharedPreferences,
            postId: String?,
            type: String?,
            likeAndUnlikeInterface: LikeAndUnlikeInterface
        ) {
            val call: Call<LikeUnlikeModel>? = getRetrofitObject()?.likeUnlikePost(
                sharedPreferences.getString(MyConstants.AUTH, ""),
                sharedPreferences.getString(MyConstants.SESSION_TOKEN, ""),
                sharedPreferences.getString(MyConstants.USER_ID, ""),
                postId,
                type
            )
            call?.enqueue(object : Callback<LikeUnlikeModel?> {
                override fun onResponse(
                    call: Call<LikeUnlikeModel?>,
                    response: Response<LikeUnlikeModel?>
                ) {
                    val commonStringResponseModel: LikeUnlikeModel? = response.body()
                    if (commonStringResponseModel != null) {
                        likeAndUnlikeInterface.onLikeAndUnlikeResponse(
                            MyConstants.GO_TO_RESPONSE,
                            commonStringResponseModel
                        )
                    } else {
                        CommonMethods.showLog(TAG, "likeUnlikePost Response null")
                        likeAndUnlikeInterface.onLikeAndUnlikeResponse(MyConstants.NULL_RESPONSE, null)
                    }
                }
                override fun onFailure(call: Call<LikeUnlikeModel?>, t: Throwable) {
                    CommonMethods.showLog(TAG, "likeUnlikePost Exception " + t.message)
                    likeAndUnlikeInterface.onLikeAndUnlikeResponse(MyConstants.FAILURE_RESPONSE, null)
                }
            })
        }


        public fun getComments(
            sharedPreferences: SharedPreferences,
            postId: String?,
            type: String?,
            commentsNewsInterface: CommentsNewsInterface
        ) {
            val call: Call<GetCommentsNewsModel>? = getRetrofitObject()?.getComments(
                sharedPreferences.getString(MyConstants.AUTH, ""),
                sharedPreferences.getString(MyConstants.SESSION_TOKEN, ""),
                sharedPreferences.getString(MyConstants.USER_ID, ""),
                postId,
                type
            )
            call?.enqueue(object : Callback<GetCommentsNewsModel?> {
                override fun onResponse(
                    call: Call<GetCommentsNewsModel?>,
                    response: Response<GetCommentsNewsModel?>
                ) {
                    val commonStringResponseModel: GetCommentsNewsModel? = response.body()
                    if (commonStringResponseModel != null) {
                        commentsNewsInterface.onCommentsNewsResponse(
                            MyConstants.GO_TO_RESPONSE,
                            commonStringResponseModel
                        )
                    } else {
                        CommonMethods.showLog(TAG, "getComments Response null")
                        commentsNewsInterface.onCommentsNewsResponse(MyConstants.NULL_RESPONSE, null)
                    }
                }
                override fun onFailure(call: Call<GetCommentsNewsModel?>, t: Throwable) {
                    CommonMethods.showLog(TAG, "getComments Exception " + t.message)
                    commentsNewsInterface.onCommentsNewsResponse(MyConstants.FAILURE_RESPONSE, null)
                }
            })
        }

        public fun addComment(
            sharedPreferences: SharedPreferences,
            postId: String?,
            type: String?,
            comment: String?,
            addCommentsInterface: AddCommentsInterface
        ) {
            val call: Call<AddCommentModel>? = getRetrofitObject()?.addComment(
                sharedPreferences.getString(MyConstants.AUTH, ""),
                sharedPreferences.getString(MyConstants.SESSION_TOKEN, ""),
                sharedPreferences.getString(MyConstants.USER_ID, ""),
                postId,
                type,
                comment
            )
            call?.enqueue(object : Callback<AddCommentModel?> {
                override fun onResponse(
                    call: Call<AddCommentModel?>,
                    response: Response<AddCommentModel?>
                ) {
                    val commonStringResponseModel: AddCommentModel? = response.body()
                    if (commonStringResponseModel != null) {
                        addCommentsInterface.onAddCommentsResponse(
                            MyConstants.GO_TO_RESPONSE,
                            commonStringResponseModel
                        )
                    } else {
                        CommonMethods.showLog(TAG, "addComment Response null")
                        addCommentsInterface.onAddCommentsResponse(MyConstants.NULL_RESPONSE, null)
                    }
                }
                override fun onFailure(call: Call<AddCommentModel?>, t: Throwable) {
                    CommonMethods.showLog(TAG, "addComment Exception " + t.message)
                    addCommentsInterface.onAddCommentsResponse(MyConstants.FAILURE_RESPONSE, null)
                }
            })
        }


        public fun searchNews(
            sharedPreferences: SharedPreferences,
            search: String?,
            categoryId: String?,
            searchNewsInterface: SearchNewsInterface
        ) {
            val call: Call<SearchNewsModel>? = getRetrofitObject()?.searchNews(
                sharedPreferences.getString(MyConstants.AUTH, ""),
                sharedPreferences.getString(MyConstants.SESSION_TOKEN, ""),
                sharedPreferences.getString(MyConstants.USER_ID, ""),
                search,
                categoryId
            )
            call?.enqueue(object : Callback<SearchNewsModel?> {
                override fun onResponse(
                    call: Call<SearchNewsModel?>,
                    response: Response<SearchNewsModel?>
                ) {
                    val commonStringResponseModel: SearchNewsModel? = response.body()
                    if (commonStringResponseModel != null) {
                        searchNewsInterface.onSearchNewsResponse(
                            MyConstants.GO_TO_RESPONSE,
                            commonStringResponseModel
                        )
                    } else {
                        CommonMethods.showLog(TAG, "searchNews Response null")
                        searchNewsInterface.onSearchNewsResponse(MyConstants.NULL_RESPONSE, null)
                    }
                }
                override fun onFailure(call: Call<SearchNewsModel?>, t: Throwable) {
                    CommonMethods.showLog(TAG, "searchNews Exception " + t.message)
                    searchNewsInterface.onSearchNewsResponse(MyConstants.FAILURE_RESPONSE, null)
                }
            })
        }


        public fun update(
            sharedPreferences: SharedPreferences,
            first_name: String?,
            family_name: String?,
            mobile: String?,
            birthDate: Long?,
            userType: String?,
            userName: String?,
            countryId: String?,
            commonStatusInterface: CommonStatusInterface
        ) {
            val call: Call<CommonStatusModel>? = getRetrofitObject()?.update(

                sharedPreferences.getString(MyConstants.AUTH, ""),
                sharedPreferences.getString(MyConstants.SESSION_TOKEN, ""),
                sharedPreferences.getString(MyConstants.USER_ID, ""),
                first_name,
                family_name,
                mobile,
                birthDate,
                userType,
                userName,
                countryId
            )
            call?.enqueue(object : Callback<CommonStatusModel?> {
                override fun onResponse(
                    call: Call<CommonStatusModel?>,
                    response: Response<CommonStatusModel?>
                ) {
                    val commonStringResponseModel: CommonStatusModel? = response.body()
                    if (commonStringResponseModel != null) {
                        commonStatusInterface.onCommonStatusInterfaceResponse(
                            MyConstants.GO_TO_RESPONSE,
                            commonStringResponseModel
                        )
                    } else {
                        CommonMethods.showLog(TAG, "update Response null")
                        commonStatusInterface.onCommonStatusInterfaceResponse(MyConstants.NULL_RESPONSE, null)
                    }
                }

                override fun onFailure(call: Call<CommonStatusModel?>, t: Throwable) {
                    CommonMethods.showLog(TAG, "update Exception " + t.message)
                    commonStatusInterface.onCommonStatusInterfaceResponse(MyConstants.FAILURE_RESPONSE, null)
                }
            })
        }

        //Music Apis

        public fun getMusicHomeData(
            sharedPreferences: SharedPreferences,
            getMusicHomeDataInterface: GetMusicHomeDataInterface
        ) {
            val call: Call<GetMusicHomeDataModel>? = getRetrofitObject()?.getMusicHomeData(
                sharedPreferences.getString(MyConstants.AUTH, ""),
                sharedPreferences.getString(MyConstants.SESSION_TOKEN, ""),
                sharedPreferences.getString(MyConstants.USER_ID, "")
            )


            call?.enqueue(object : Callback<GetMusicHomeDataModel?> {
                override fun onResponse(
                    call: Call<GetMusicHomeDataModel?>,
                    response: Response<GetMusicHomeDataModel?>
                ) {
                    val commonStringResponseModel: GetMusicHomeDataModel? = response.body()
                    if (commonStringResponseModel != null) {
                        getMusicHomeDataInterface.onGetMusicHomeDataResponse(
                            MyConstants.GO_TO_RESPONSE,
                            commonStringResponseModel
                        )
                    } else {
                        CommonMethods.showLog(TAG, "getMusicHomeData Response null")
                        getMusicHomeDataInterface.onGetMusicHomeDataResponse(MyConstants.NULL_RESPONSE, null)
                    }
                }

                override fun onFailure(call: Call<GetMusicHomeDataModel?>, t: Throwable) {
                    CommonMethods.showLog(TAG, "getMusicHomeData Exception " + t.message)
                    getMusicHomeDataInterface.onGetMusicHomeDataResponse(MyConstants.FAILURE_RESPONSE, null)
                }
            })
        }

        public fun getMusicLibraryData(
            sharedPreferences: SharedPreferences,
            getMusicHomeDataInterface: GetMusicHomeDataInterface
        ) {
            val call: Call<GetMusicHomeDataModel>? = getRetrofitObject()?.getLibraryData(
                sharedPreferences.getString(MyConstants.AUTH, ""),
                sharedPreferences.getString(MyConstants.SESSION_TOKEN, ""),
                sharedPreferences.getString(MyConstants.USER_ID, "")
            )


            call?.enqueue(object : Callback<GetMusicHomeDataModel?> {
                override fun onResponse(
                    call: Call<GetMusicHomeDataModel?>,
                    response: Response<GetMusicHomeDataModel?>
                ) {
                    val commonStringResponseModel: GetMusicHomeDataModel? = response.body()
                    if (commonStringResponseModel != null) {
                        getMusicHomeDataInterface.onGetMusicHomeDataResponse(
                            MyConstants.GO_TO_RESPONSE,
                            commonStringResponseModel
                        )
                    } else {
                        CommonMethods.showLog(TAG, "getMusicLibraryData Response null")
                        getMusicHomeDataInterface.onGetMusicHomeDataResponse(MyConstants.NULL_RESPONSE, null)
                    }
                }

                override fun onFailure(call: Call<GetMusicHomeDataModel?>, t: Throwable) {
                    CommonMethods.showLog(TAG, "getMusicLibraryData Exception " + t.message)
                    getMusicHomeDataInterface.onGetMusicHomeDataResponse(MyConstants.FAILURE_RESPONSE, null)
                }
            })
        }

        fun uploadImage(
            sharedPreferences: SharedPreferences,
            image: String?,
            type: String?,
            uploadImageInterface: UploadImageInterface
        ) {
            val file = File(image)
            val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
            val requestBody = MultipartBody.Part.createFormData("image", file.name, requestFile)
            val call: Call<ResponseBody> = getScalarRetrofit()
                ?.uploadAppImage(
                    sharedPreferences.getString(MyConstants.AUTH, ""),
                    sharedPreferences.getString(MyConstants.SESSION_TOKEN, ""),
                    sharedPreferences.getString(MyConstants.USER_ID, ""),
                    requestBody
                )!!
            call.enqueue(object : Callback<ResponseBody?> {
                override fun onResponse(
                    call: Call<ResponseBody?>,
                    response: Response<ResponseBody?>
                ) {
                    try {
                        if (response.body() != null) {
                            CommonMethods.showLog(
                                TAG,
                                "call.request().url()" + call.request().url()
                            )
                            val jsonString = response.body()!!.string()
                            uploadImageInterface.onUploadImageResponse("1", jsonString, type)
                        } else {
                            uploadImageInterface.onUploadImageResponse("0", null, "")
                            CommonMethods.showLog(TAG, "No Response")
                        }
                    } catch (e: IOException) {
                        e.printStackTrace()
                        uploadImageInterface.onUploadImageResponse("0", null, "")
                        CommonMethods.showLog(TAG, "IOException " + e.message)
                    }
                }

                override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                    CommonMethods.showLog(TAG, "uploadImage onFailure  " + t.message)
                    uploadImageInterface.onUploadImageResponse("0", null, "")
                }
            })
        }

        public fun createPlaylist(
            sharedPreferences: SharedPreferences,
            title:String,
            image:String,
            createPlaylistInterface: CreatePlaylistInterface
        ) {
            val call: Call<CreatePlaylistResponse>? = getRetrofitObject()?.createPlaylist(
                sharedPreferences.getString(MyConstants.AUTH, ""),
                sharedPreferences.getString(MyConstants.SESSION_TOKEN, ""),
                sharedPreferences.getString(MyConstants.USER_ID, ""),
                image,
                title
            )


            call?.enqueue(object : Callback<CreatePlaylistResponse?> {
                override fun onResponse(
                    call: Call<CreatePlaylistResponse?>,
                    response: Response<CreatePlaylistResponse?>
                ) {
                    val commonStringResponseModel: CreatePlaylistResponse? = response.body()
                    if (commonStringResponseModel != null) {
                        createPlaylistInterface.onCreatePlaylistResponse(
                            MyConstants.GO_TO_RESPONSE,
                            commonStringResponseModel
                        )
                    } else {
                        CommonMethods.showLog(TAG, "createPlaylist Response null")
                        createPlaylistInterface.onCreatePlaylistResponse(MyConstants.NULL_RESPONSE, null)
                    }
                }

                override fun onFailure(call: Call<CreatePlaylistResponse?>, t: Throwable) {
                    CommonMethods.showLog(TAG, "createPlaylist Exception " + t.message)
                    createPlaylistInterface.onCreatePlaylistResponse(MyConstants.FAILURE_RESPONSE, null)
                }
            })
        }

        public fun getPlaylistDetails(
            sharedPreferences: SharedPreferences,
            playlistId:String,
            createPlaylistInterface: CreatePlaylistInterface
        ) {
            val call: Call<CreatePlaylistResponse>? = getRetrofitObject()?.getPlaylistDetail(
                sharedPreferences.getString(MyConstants.AUTH, ""),
                sharedPreferences.getString(MyConstants.SESSION_TOKEN, ""),
                sharedPreferences.getString(MyConstants.USER_ID, ""),
                playlistId
            )


            call?.enqueue(object : Callback<CreatePlaylistResponse?> {
                override fun onResponse(
                    call: Call<CreatePlaylistResponse?>,
                    response: Response<CreatePlaylistResponse?>
                ) {
                    val commonStringResponseModel: CreatePlaylistResponse? = response.body()
                    if (commonStringResponseModel != null) {
                        createPlaylistInterface.onCreatePlaylistResponse(
                            MyConstants.GO_TO_RESPONSE,
                            commonStringResponseModel
                        )
                    } else {
                        CommonMethods.showLog(TAG, "getPlaylistDetails Response null")
                        createPlaylistInterface.onCreatePlaylistResponse(MyConstants.NULL_RESPONSE, null)
                    }
                }

                override fun onFailure(call: Call<CreatePlaylistResponse?>, t: Throwable) {
                    CommonMethods.showLog(TAG, "getPlaylistDetails Exception " + t.message)
                    createPlaylistInterface.onCreatePlaylistResponse(MyConstants.FAILURE_RESPONSE, null)
                }
            })
        }

        public fun getAlbumDetail(
            sharedPreferences: SharedPreferences,
            albumId:String,
            createPlaylistInterface: CreatePlaylistInterface
        ) {
            val call: Call<CreatePlaylistResponse>? = getRetrofitObject()?.getAlbumDetail(
                sharedPreferences.getString(MyConstants.AUTH, ""),
                sharedPreferences.getString(MyConstants.SESSION_TOKEN, ""),
                sharedPreferences.getString(MyConstants.USER_ID, ""),
                albumId
            )


            call?.enqueue(object : Callback<CreatePlaylistResponse?> {
                override fun onResponse(
                    call: Call<CreatePlaylistResponse?>,
                    response: Response<CreatePlaylistResponse?>
                ) {
                    val commonStringResponseModel: CreatePlaylistResponse? = response.body()
                    if (commonStringResponseModel != null) {
                        createPlaylistInterface.onCreatePlaylistResponse(
                            MyConstants.GO_TO_RESPONSE,
                            commonStringResponseModel
                        )
                    } else {
                        CommonMethods.showLog(TAG, "getAlbumDetail Response null")
                        createPlaylistInterface.onCreatePlaylistResponse(MyConstants.NULL_RESPONSE, null)
                    }
                }

                override fun onFailure(call: Call<CreatePlaylistResponse?>, t: Throwable) {
                    CommonMethods.showLog(TAG, "getAlbumDetail Exception " + t.message)
                    createPlaylistInterface.onCreatePlaylistResponse(MyConstants.FAILURE_RESPONSE, null)
                }
            })
        }

        public fun getMusicData(
            sharedPreferences: SharedPreferences,
            getMusicInterface: GetMusicInterface
        ) {
            val call: Call<GetMusicModel>? = getRetrofitObject()?.getMusic(
                sharedPreferences.getString(MyConstants.AUTH, ""),
                sharedPreferences.getString(MyConstants.SESSION_TOKEN, ""),
                sharedPreferences.getString(MyConstants.USER_ID, "")
            )


            call?.enqueue(object : Callback<GetMusicModel?> {
                override fun onResponse(
                    call: Call<GetMusicModel?>,
                    response: Response<GetMusicModel?>
                ) {
                    val commonStringResponseModel: GetMusicModel? = response.body()
                    if (commonStringResponseModel != null) {
                        getMusicInterface.onGetMusicResponse(
                            MyConstants.GO_TO_RESPONSE,
                            commonStringResponseModel
                        )
                    } else {
                        CommonMethods.showLog(TAG, "getMusicData Response null")
                        getMusicInterface.onGetMusicResponse(MyConstants.NULL_RESPONSE, null)
                    }
                }

                override fun onFailure(call: Call<GetMusicModel?>, t: Throwable) {
                    CommonMethods.showLog(TAG, "getMusicData Exception " + t.message)
                    getMusicInterface.onGetMusicResponse(MyConstants.FAILURE_RESPONSE, null)
                }
            })
        }

        public fun addSongsToPlaylist(
            sharedPreferences: SharedPreferences,
            songId: String?,
            playListId: String?,
            commonStatusInterface: CommonStatusInterface
        ) {
            val call: Call<CommonStatusModel>? = getRetrofitObject()?.uploadSongsPLaylist(
                sharedPreferences.getString(MyConstants.AUTH, ""),
                sharedPreferences.getString(MyConstants.SESSION_TOKEN, ""),
                sharedPreferences.getString(MyConstants.USER_ID, ""),
                songId,
                playListId
            )
            call?.enqueue(object : Callback<CommonStatusModel?> {
                override fun onResponse(
                    call: Call<CommonStatusModel?>,
                    response: Response<CommonStatusModel?>
                ) {
                    val commonStringResponseModel: CommonStatusModel? = response.body()
                    if (commonStringResponseModel != null) {
                        commonStatusInterface.onCommonStatusInterfaceResponse(
                            MyConstants.GO_TO_RESPONSE,
                            commonStringResponseModel
                        )
                    } else {
                        CommonMethods.showLog(TAG, "addSongsToPlaylist Response null")
                        commonStatusInterface.onCommonStatusInterfaceResponse(MyConstants.NULL_RESPONSE, null)
                    }
                }

                override fun onFailure(call: Call<CommonStatusModel?>, t: Throwable) {
                    CommonMethods.showLog(TAG, "addSongsToPlaylist Exception " + t.message)
                    commonStatusInterface.onCommonStatusInterfaceResponse(MyConstants.FAILURE_RESPONSE, null)
                }
            })
        }





    }
}