package com.napworks.mohra.utilPackage

class MyConstants {
    companion object {
        const val ALBUM_DETAIL="album_detail"
        const val PLAYLIST_DETAIL="playlist_detail"
        const val CREATE_PLAYLIST = "create_playlist"
        const val ID = "id"
        const val PLAYLIST_IMAGE = "playlist_image"
        const val SINGLE_NEWS_ACTIVITY="single_news_activity"
        const val UPDATE_COMMENTS = "update_comments"
        const val FAMILY_NAME = "family_name"
        const val COUNTRY_REQUEST_CODE: Int = 1
        const val BASE_URL = "http://mohraapi-env.eba-gwgfpnpu.us-east-2.elasticbeanstalk.com/public/api/"
        const val COUNTRY_ID = "country_id"
        const val USERTYPE = "usertype"
        const val USERNAME = "username"
        const val DATEOFBIRTH = "dateofbirth"
        const val COUNTRY_TABLE = "country_table"
        const val COUNTRY_NAME = "country_name"
        const val COUNTRY_SHORT_CODE = "country_short_code"
        const val COUNTRY_LONG_CODE = "country_long_code"
        const val COUNTRY_CALLING_CODE = "country_calling_code"
        const val ANDROID = "android"
        const val SHARED_PREFERENCE = "shared_preference"
        const val NOTIFICATION_TOKEN = "notification_token"
        const val CAKE = "cake"
        const val BASBOOSH = "basboosh"
        const val OTP_API = "otpapi"
        const val OTP_API_ID = "otpapiid"
        const val SETSCREEN = "setscreen"
        const val FIRST_NAME = "firstname"
        const val LAST_NAME = "lastname"
        const val TERMS_CONDITIONS_URL =
            "https://napworks.in/horoscope/api/termsAndConditions.html"

        const val NOTIFICATION_TYPE = "notificationtype"
        const val PRODUCT_PRICE = "productprice"
        const val ANDROID_ID = "androidid"
        const val AMOUNT_OF_NOTIFICATION = "amountofnotification"
        const val QUESTION_ID = "questionId"
        const val NOTIFICATION_MESSAGE = "notificationmessage"
        const val ATTACHEDMESSAGE = "attachedMessage"
        const val MESSAGE = "message"
        const val AUTH = "auth"
        const val APPSTATUS = "appstatus"
        const val SESSION_TOKEN = "session"
        const val USER_ID = "userId"
        const val EMAIL = "email"
        const val NAME = "name"
        const val GOOGLE_ID = "google_id"
        const val FACEBOOK_ID = "facebook_id"
        const val IS_PASSWORD_ENTERED = "is_password_entered"
        const val IS_LOGGED_IN = "is_logged_in"
        const val MUSIC_PLAYING = "music_playing"
        const val ZODIAC_SIGN_ID = "zodiac_sign_id"
        const val ZODIAC_SIGN_NAME = "zodiac_sign_name"
        const val ZODIAC_SIGN_SYMBOL = "zodiac_sign_symbol"
        const val CALLED_FROM = "called_from"
        const val GO_TO_RESPONSE = "go_to_response"


        const val NULL_RESPONSE = "null_response"
        const val FAILURE_RESPONSE = "failure_response"
        const val OPEN_MAIN = "open_main"
        const val GOOGLE = "google"
        const val FACEBOOK = "facebook"
        const val BROADCAST_TYPE = "broadcast_type"
        const val UPDATE_PROFILE = "update_profile"
        const val NAVIGATION_FRAGMENT = "navigation_fragment"
        const val ZODIAC_SIGNS_TABLE = "zodiac_signs_table"
        const val IS_LIVE = false
        const val VERIFY = "verify"
        const val RESEND = "resend"
        const val FORGOT_PASSWORD = "forgot_password"
        const val PROFILE = "profile"
        const val ZODIAC_MODAL = "zodiac_modal"
        const val PRIVACY_POLICY = "privacy_policy"
        const val TERMS_CONDITION = "terms_condition"
        const val URL = "url"
        const val TITLE = "title"
            const val PROFILE_ACTIVITY = "profile_activity"
            const  val PERSONALITY  = "personality"
            const  val PREFERENCES  = "preferences"
            const  val UDATEUSERNAME  = "udateusername"

        const val MOBILE = "mobile"
        const val COUNTRY_MODEL = "country_model"
        const val LOGIN ="login"
        const val TYPE = "type"

        const val CAMERA_IMAGE_REQUEST_CODE = 2
        const val GALLERY_RESPONSE_CODE = 3
        const val CAMERA_PERMISSION_REQUEST = 4
        const val STORAGE_PERMISSION_REQUEST = 5
        const val VIDEO_GALLERY_RESPONSE_CODE = 6
        const val CAMERA_VIDEO_REQUEST_CODE = 7
        const val ADD_SONGS_REQUEST_CODE: Int = 8

        const val PHOTOS = "photos"



    }
}