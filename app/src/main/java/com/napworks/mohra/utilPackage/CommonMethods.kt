package com.napworks.mohra.utilPackage

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Base64
import android.util.DisplayMetrics
import android.util.Log
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.napworks.mohra.activitiesPackage.SplashScreenActivity
import com.napworks.mohra.interfacePackage.FacebookInterfaceCallBack
import com.napworks.mohra.modelPackage.FacebookDataModel
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.SimpleDateFormat
import java.util.*
import android.R
import android.net.Uri

import android.os.Environment
import android.provider.OpenableColumns
import com.napworks.mohra.modelPackage.FileChoosedModel
import java.io.File


public class CommonMethods {
    companion object {

        val TAG: String = "CommonMethods"

        fun showLog(tag: String?, message: String?) {
            Log.e("$tag", "$message")
        }

        fun checkFileValidOrNot(activity: Activity, uri: Uri?): FileChoosedModel? {
            val fileChoosedModel = FileChoosedModel("", "")
            try {
                if (uri != null) {
                    val mimeType = activity.contentResolver.getType(uri)
                    var filename = ""
                    if (mimeType == null) {
                        val path = GetFilePath.getPath(activity, uri)
                        if (path != null) {
                            val file = File(path)
                            filename = file.name
                            fileChoosedModel.setPath(path)
                        }
                    } else {
                        val returnUri: Uri = uri
                        val returnCursor =
                            activity.contentResolver.query(returnUri, null, null, null, null)
                        if (returnCursor != null) {
                            val nameIndex =
                                returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
                            //                        int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
                            returnCursor.moveToFirst()
                            filename = returnCursor.getString(nameIndex)
                            //                        String size = Long.toString(returnCursor.getLong(sizeIndex));
                            returnCursor.close()
                        }
                    }
                    var sourcePath = ""
                    fileChoosedModel.setFileName(filename)
                    if (activity.getExternalFilesDir(null) != null) {
                        val innerFile = activity.getExternalFilesDir(null)
                        if (innerFile != null) {
                            sourcePath = innerFile.path
                        }
                    }
                    try {
                        GetFilePath.copyFileStream(File("$sourcePath/$filename"), uri, activity)
                        if (mimeType != null) {
                            if (mimeType.contains("pdf")) {
                                fileChoosedModel.setType("pdf")
                            } else if (mimeType.contains("image")) {
                                fileChoosedModel.setType("image")
                            }
                        }
                    } catch (e: java.lang.Exception) {
                        e.printStackTrace()
                    }
                    val completePath = "$sourcePath/$filename"
                    fileChoosedModel.setPath(completePath)
                    showLog(TAG, "checkFileValidOrNot URI $uri")
                    showLog(TAG, "checkFileValidOrNot mimeType $mimeType")
                    showLog(TAG, "checkFileValidOrNot filename $filename")
                    showLog(TAG, "checkFileValidOrNot sourcePath $sourcePath")
                    showLog(
                        TAG,
                        "checkFileValidOrNot fileChoosedModel.getPath " + fileChoosedModel.getPath()
                    )
                    showLog(
                        TAG,
                        "checkFileValidOrNot completePath $completePath"
                    )
                    showLog(TAG, "checkFileValidOrNot type " + fileChoosedModel.getType())
                    showLog(TAG, "checkFileValidOrNot final path  " + fileChoosedModel.getPath())
                    if (fileChoosedModel.getType().equals("")) {
                        if (completePath.endsWith(".png") ||
                            completePath.endsWith(".jpg") ||
                            completePath.endsWith(".jpeg") ||
                            completePath.endsWith(".gif")
                        ) {
                            fileChoosedModel.setType("image")
                            fileChoosedModel.setPath(completePath)
                        } else if (completePath.endsWith(".pdf")) {
                            fileChoosedModel.setType("pdf")
                            fileChoosedModel.setPath(completePath)
                        }
                    }
                    showLog(TAG, "checkFileValidOrNot after type " + fileChoosedModel.getType())
                    showLog(
                        TAG,
                        "checkFileValidOrNot after final path  " + fileChoosedModel.getPath()
                    )
                }
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
            return fileChoosedModel
        }

        fun dateMonthYearSignUp(timeInMillis: Long): String? {
            val myFormat = "MM/dd/yyyy"
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            return sdf.format(Date(timeInMillis))
        }

        fun getCurrentDate(timeInMillis: Long): String? {
            val myFormat = "dd-MM-yyyy"
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            return sdf.format(Date(timeInMillis))
        }

        fun getFullDate(timeInMillis: Long): String? {
            val myFormat = "dd MMMM, yyyy hh:mm a"
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            return sdf.format(Date(timeInMillis))
        }

        fun getDateForOroscope(timeInMillis: Long): String? {
            val myFormat = "EEEE MMM dd"
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            return sdf.format(Date(timeInMillis))
        }



        fun getFolderPath(activity: Activity): String? {
//            val path = Environment.getExternalStorageDirectory().toString()+File.separator + "Mohra"
            val path = Environment.getExternalStorageDirectory().absolutePath+File.separator + "Mohra"
            showLog(TAG, "path : $path")
            val direct = File(path)
            val result: Boolean = direct.mkdirs()
            showLog(TAG, "getImagesFolderPath result : $result")
            return path
        }

        fun getHashKey(activity: Activity) {
            try {
                val info = activity.packageManager.getPackageInfo(
                    activity.packageName,
                    PackageManager.GET_SIGNATURES
                )
                for (signature in info.signatures) {
                    val md = MessageDigest.getInstance("SHA")
                    md.update(signature.toByteArray())
                    Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT))
                }
            } catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
            } catch (e: NoSuchAlgorithmException) {
                e.printStackTrace()
            }
        }


        fun loginWithFacebook(
            callbackManager: CallbackManager?,
            facebookInterfaceCallBack: FacebookInterfaceCallBack,
        ) {
            LoginManager.getInstance().registerCallback(callbackManager,
                object : FacebookCallback<LoginResult> {
                    override fun onSuccess(loginResult: LoginResult) {
                        showLog(TAG, "onSuccess" + loginResult.accessToken.userId)
                        val graphRequest = GraphRequest.newMeRequest(
                            AccessToken.getCurrentAccessToken()
                        ) { jsonObject, graphResponse ->
                            showLog(TAG, "Success")
                            if (graphResponse != null) {
                                try {
                                    val facebookDataModel = FacebookDataModel()
                                    facebookDataModel.accountId = jsonObject.getString("id")
                                    if (jsonObject.has("first_name")) {
                                        showLog(
                                            TAG,
                                            "name " + jsonObject.getString("first_name")
                                        )
                                        facebookDataModel.firstName =
                                            jsonObject.getString("first_name")
                                    }
                                    if (jsonObject.has("last_name")) {
                                        facebookDataModel.lastName =
                                            jsonObject.getString("last_name")
                                        showLog(
                                            TAG,
                                            "last_name " + jsonObject.getString("last_name")
                                        )
                                    }
                                    if (jsonObject.has("name")) {
                                        facebookDataModel.userName = jsonObject.getString("name")
                                        showLog(
                                            TAG,
                                            "fullName " + jsonObject.getString("name")
                                        )
                                    }
                                    if (jsonObject.has("email")) {
                                        facebookDataModel.email = jsonObject.getString("email")
                                        showLog(
                                            TAG,
                                            "email " + jsonObject.getString("email")
                                        )
                                    } else {
                                        showLog(TAG, "Email heni aayiii")
                                    }
                                    val urlImage =
                                        "https://graph.facebook.com/" + jsonObject.getString("id") + "/picture?width=480&height=480"
                                    facebookDataModel.profileImage = urlImage
                                    facebookInterfaceCallBack.getFacebookCall(
                                        "1",
                                        facebookDataModel
                                    )
                                    showLog(TAG, "url image$urlImage")
                                } catch (e: Exception) {
                                    facebookInterfaceCallBack.getFacebookCall("0", null)
                                }
                            } else {
                                facebookInterfaceCallBack.getFacebookCall("0", null)
                            }
                        }
                        val bundle = Bundle()
                        bundle.putString(
                            "fields",
                            "id,email,gender,birthday,name,first_name,last_name,timezone,location"
                        )
                        graphRequest.parameters = bundle
                        graphRequest.executeAsync()
                    }

                    override fun onCancel() {
                        showLog(TAG, "Cancel")
                        facebookInterfaceCallBack.getFacebookCall("0", null)
                        showLog(CommonMethods.TAG, "cancel")
                    }

                    override fun onError(error: FacebookException) {
                        facebookInterfaceCallBack.getFacebookCall("0", null)
                        showLog(CommonMethods.TAG, "error " + error.message)
                    }
                })
        }

        fun getWidth(activity: Activity): Int {
            val displayMetrics = DisplayMetrics()
            activity.windowManager.defaultDisplay.getMetrics(displayMetrics)
            return displayMetrics.widthPixels
        }

        fun getHeight(context: Activity): Int {
            val displayMetrics = DisplayMetrics()
            context.windowManager.defaultDisplay.getMetrics(displayMetrics)
            return displayMetrics.heightPixels
        }

        fun callLogout(activity: Activity) {
//            Toast.makeText(
//                activity,
//                activity.getString(R.string.),
//                Toast.LENGTH_SHORT
//            ).show()

            val sharedPreferences =
                activity.getSharedPreferences(MyConstants.SHARED_PREFERENCE, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.clear()
            editor.apply()
            activity.finishAffinity()
            activity.startActivity(Intent(activity, SplashScreenActivity::class.java))
        }

        fun timeAgoDisplay(secondsAgo : Long) : String {

            if (secondsAgo < 60) {
                return "$secondsAgo seconds ago"
            }

            else if (secondsAgo < 60 * 60) {
                return ""+secondsAgo/60 +" minutes ago"
            }

            else if (secondsAgo < 60 * 60 * 24 )
            {
                return ""+secondsAgo / 60 / 60 + " hours ago"
            }
            else if (secondsAgo < 60 * 60 * 24 * 7){
                return ""+secondsAgo / 60 / 60 / 24 +" days ago"
            }
            return ""+secondsAgo / 60 / 60 / 24 / 7+" weeks ago"
        }

        fun getScreenDensity(activity: Activity): Float {
            val metrics = activity.resources.displayMetrics
            return metrics.density
        }
    }
}