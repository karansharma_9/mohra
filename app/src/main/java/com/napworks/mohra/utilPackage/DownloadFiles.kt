package com.napworks.mohra.utilPackage
import android.annotation.SuppressLint
import android.app.Activity
import android.net.Uri
import android.os.AsyncTask
import android.os.Environment
import androidx.core.content.FileProvider
import com.napworks.mohra.R

import com.napworks.mohra.dialogPackage.LoadingDialog
import com.napworks.mohra.interfacePackage.DownloadCompleteInterface
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.util.ArrayList


class DownloadFiles(
    @field:SuppressLint("StaticFieldLeak") private val activity: Activity,
    private val urlList: ArrayList<String>,
    private val loadingDialog: LoadingDialog,
//    downloadingLoadingDialog: DownloadingLoadingDialog,
    downloadCompleteInterface: DownloadCompleteInterface,
) : AsyncTask<String?, String?, String?>() {
//    private val downloadingLoadingDialog: DownloadingLoadingDialog
    private val downloadCompleteInterface: DownloadCompleteInterface
    private val TAG = javaClass.simpleName
    var uriList = ArrayList<Uri>()
    override fun onPreExecute() {
        super.onPreExecute()
        CommonMethods.showLog(TAG, "START Downloading")
        loadingDialog.hideDialog()
//        downloadingLoadingDialog.showDialog(activity.getString(R.string.downloading))
//        downloadingLoadingDialog.updateProgress(0, urlList.size)
    }

    override fun onPostExecute(s: String?) {
        super.onPostExecute(s)
        CommonMethods.showLog(TAG, "FINISH Downloading : " + uriList.size)
//        downloadingLoadingDialog.hideDialog()
        downloadCompleteInterface.onDownlaodComplete(uriList)
    }

//    protected override fun onProgressUpdate(vararg values: String) {
//        super.onProgressUpdate(*values)
//        CommonMethods.showLog(TAG, "onProgressUpdate values " + values.size)
//        for (s in values) {
//            CommonMethods.showLog(TAG, "onProgressUpdate $s")
////            downloadingLoadingDialog.updateProgress(s.toInt() + 1, urlList.size)
//        }
//    }

    override fun onCancelled(s: String?) {
        super.onCancelled(s)
    }

    override fun onCancelled() {
        super.onCancelled()
    }

//     override fun doInBackground(vararg params: String): String? {
//        downloadFIle()
//        return ""
//    }

    private fun downloadFIle() {
        for (i in urlList.indices) {
            val filePath = urlList[i]
            val fileName = filePath.substring(filePath.lastIndexOf('/'), filePath.length)
            CommonMethods.showLog(TAG, "URL $filePath")
            try {
                val url = URL(filePath)
                val urlConnection = url.openConnection() as HttpURLConnection
                urlConnection.requestMethod = "GET"
                urlConnection.doOutput = true
                urlConnection.connect()
                val imagePath: String = CommonMethods.getFolderPath(activity)!!
                val fileName1 = imagePath + fileName
                CommonMethods.showLog(TAG, "FILE NAME : $fileName")
                CommonMethods.showLog(TAG, "FILE NAME1 : $fileName1")
                val file = File(File(Environment.getExternalStorageDirectory()
                    .toString() + File.separator +
                        activity.getString(R.string.app_name)), fileName)

                //                if (file.exists()) {
//                    file.delete();
//                }
                val path = Environment.getExternalStorageDirectory().toString() + File.separator +
                        activity.getString(R.string.app_name) + File.separator + fileName
                val imageUri = FileProvider.getUriForFile(activity,
                    activity.getString(R.string.file_provider_authority),
                    file)
                uriList.add(imageUri)

               val outputFile = file //Create Output file in Main File
                if (!outputFile.exists()) {
                    outputFile.createNewFile()
                    CommonMethods.showLog(
                        TAG,
                        "File Created"
                    )
                } else {
                    CommonMethods.showLog(
                        TAG,
                        "File Exists"
                    )
                    outputFile.delete()
                }

                //                File galleryFile = createNewFile(activity, fileName);
//                FileOutputStream fileOutput = activity.openFileOutput(fileName1, Activity.MODE_PRIVATE);
                val fileOutput = FileOutputStream(outputFile)
                val inputStream = urlConnection.inputStream
                val buffer = ByteArray(1024)
                var bufferLength: Int
                while (inputStream.read(buffer).also { bufferLength = it } > 0) {
                    fileOutput.write(buffer, 0, bufferLength)
                }
                fileOutput.flush()
                fileOutput.close()
                inputStream.close()
//                publishProgress(i.toString())
//                //                return CommonMethods.getLocalFilePath(activity) + filName;
            } catch (e: MalformedURLException) {
                CommonMethods.showLog(TAG, "MalformedURLException " + e.message)
                //                return "";
            } catch (e: IOException) {
                CommonMethods.showLog(TAG, "IOException " + e.message)
                //                return "";
            }
        }
    }

    init {
//        this.downloadingLoadingDialog = downloadingLoadingDialog
        this.downloadCompleteInterface = downloadCompleteInterface
    }

    override fun doInBackground(vararg params: String?): String? {
        downloadFIle()
        return ""
    }


}
